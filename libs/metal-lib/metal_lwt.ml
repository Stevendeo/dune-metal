include Metal

let get_account () =
  let (promise, resolver) = Lwt.task () in
  get_account (Lwt.wakeup resolver);
  promise

let send ~destination
    ?fee ?parameter ?gas_limit ?storage_limit ~amount =
   let (promise, resolver) = Lwt.task () in
   send ~destination ~amount
     ?fee ?parameter ?gas_limit ?storage_limit (Lwt.wakeup resolver);
   promise

let originate ?code
    ?fee ?gas_limit ?storage_limit ~balance =
  let (promise, resolver) = Lwt.task () in
  originate ~balance ?code
    ?fee ?gas_limit ?storage_limit (Lwt.wakeup resolver);
  promise

let delegate ?fee ?gas_limit ?storage_limit delegate =
   let (promise, resolver) = Lwt.task () in
   Metal.delegate
     ?fee ?gas_limit ?storage_limit delegate (Lwt.wakeup resolver);
   promise
