open Js_of_ocaml

class type network = object
  method name : Js.js_string Js.t Js.prop
  method url : Js.js_string Js.t Js.prop
end

class type send_result = object
  method ok : bool Js.t Js.prop
  method msg : Js.js_string Js.t Js.Optdef.t Js.prop
end

class type originate_msg = object
  method op_hash : Js.js_string Js.t Js.prop
  method contract : Js.js_string Js.t Js.prop
end

class type originate_result = object
  method ok : bool Js.t Js.prop
  method msg : originate_msg Js.t Js.Optdef.t Js.prop
end

class type send_param = object
  method dst : Js.js_string Js.t Js.prop
  method amount : Js.js_string Js.t Js.prop (* in mudun *)
  method fee : Js.js_string Js.t Js.Optdef.t Js.prop (* in mudun *)
  method parameter : Js.js_string Js.t Js.Optdef.t Js.prop (* in json *)
  method entrypoint : Js.js_string Js.t Js.Optdef.t Js.prop (* in json *)
  method gas_limit_ : Js.js_string Js.t Js.Optdef.t Js.prop
  method storage_limit_ : Js.js_string Js.t Js.Optdef.t Js.prop
  method cb : send_result Js.t -> unit Js.meth
end

class type originate_param = object
  method balance : Js.js_string Js.t Js.prop (* in mudun *)
  method fee : Js.js_string Js.t Js.Optdef.t Js.prop (* in mudun *)
  method gas_limit_ : Js.js_string Js.t Js.Optdef.t Js.prop
  method storage_limit_ : Js.js_string Js.t Js.Optdef.t Js.prop
  method sc_code_hash_ : Js.js_string Js.t Js.Optdef.t Js.prop (* in json *)
  method sc_storage_ : Js.js_string Js.t Js.Optdef.t Js.prop (* in json *)
  method sc_code_ : Js.js_string Js.t Js.Optdef.t Js.prop (* in json *)
  method cb : originate_result Js.t -> unit Js.meth
end

class type delegate_param = object
  method delegate : Js.js_string Js.t Js.Optdef.t Js.prop
  method fee : Js.js_string Js.t Js.Optdef.t Js.prop (* in mudun *)
  method gas_limit_ : Js.js_string Js.t Js.Optdef.t Js.prop
  method storage_limit_ : Js.js_string Js.t Js.Optdef.t Js.prop
  method cb : send_result Js.t -> unit Js.meth
end

class type metal = object
  method isEnabled : (bool Js.t -> unit) -> unit Js.meth
  method isUnlocked : (bool Js.t -> unit) -> unit Js.meth
  method isApproved : (bool Js.t -> unit) -> unit Js.meth
  method getAccount : (Js.js_string Js.t -> unit) -> unit Js.meth
  method getNetwork : (network Js.t -> unit) -> unit Js.meth

  method onStateChanged : (unit -> unit) -> unit Js.meth

  method send : send_param Js.t -> unit Js.meth
  method originate : originate_param Js.t -> unit Js.meth
  method delegate : delegate_param Js.t -> unit Js.meth
end

let ready ?(not_installed=fun () -> ()) ?(timeout=500.) f =
  Js.Optdef.case Js.Unsafe.global##.metal
  (fun () ->
     let cb () =
       Js.Optdef.case Js.Unsafe.global##.metal
         not_installed f in
       ignore @@ Dom_html.window##setTimeout (Js.wrap_callback cb) timeout)
  f

let metal : metal Js.t = Js.Unsafe.global##.metal
