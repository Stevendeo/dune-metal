open Js_of_ocaml
open Metal_js

let metal : metal Js.t ref = ref metal
let is_installed = ref false

let ready ?not_installed ?timeout f =
  ready ?not_installed ?timeout (fun m -> metal := m; is_installed := true; f ())

let installed () = !is_installed

let is_enabled f =
  !metal##isEnabled(fun js_b -> f (Js.to_bool js_b))

let is_unlocked f =
  !metal##isUnlocked(fun js_b -> f (Js.to_bool js_b))

let is_approved f =
  !metal##isApproved(fun js_b -> f (Js.to_bool js_b))

let get_account f =
  !metal##getAccount(fun js_str -> f (Js.to_string js_str))

let get_network f =
  !metal##getNetwork(fun res ->
      f (Js.to_string res##.name, Js.to_string res##.url))

let on_state_changed f =
  !metal##onStateChanged f

type 'a result =
  | Ok of 'a
  | Canceled

type code =
  | Nocode
  | Code of string * string
  | CodeHash of string * string

let map_opt f = function
  | None -> Js.Optdef.empty
  | Some o -> Js.Optdef.return (f o)

let send ~destination ~amount
    ?fee ?parameter ?entrypoint ?gas_limit ?storage_limit callback =
  let destination = Js.string destination in
  let amount = Js.string amount in
  let fee = map_opt Js.string fee in
  let parameter = map_opt Js.string parameter in
  let entrypoint = map_opt Js.string entrypoint in
  let gas_limit = map_opt (fun x -> Js.string (string_of_int x)) gas_limit in
  let storage_limit =
    map_opt (fun x -> Js.string (string_of_int x)) storage_limit in
  let callback = fun res ->
    match Js.to_bool res##.ok with
    | false -> callback Canceled
    | true ->
        callback
          (Ok (Js.to_string (Js.Optdef.get (res##.msg) (fun () -> assert false))))
  in
  !metal##send(object%js
    val mutable dst = destination
    val mutable amount = amount
    val mutable fee = fee
    val mutable parameter = parameter
    val mutable entrypoint = entrypoint
    val mutable gas_limit_ = gas_limit
    val mutable storage_limit_ = storage_limit
    method cb res = callback res
  end)

let originate ~balance ?(code=Nocode)
    ?fee ?gas_limit ?storage_limit callback =
  let balance = Js.string balance in
  let fee = map_opt Js.string fee in
  let gas_limit = map_opt (fun x -> Js.string (string_of_int x)) gas_limit in
  let storage_limit =
    map_opt (fun x -> Js.string (string_of_int x)) storage_limit in
  let sc_storage, sc_code, sc_code_hash = match code with
    | Nocode -> Js.Optdef.empty, Js.Optdef.empty, Js.Optdef.empty
    | Code (code, storage) ->
        Js.Optdef.return (Js.string storage),
        Js.Optdef.return (Js.string code),
        Js.Optdef.empty
    | CodeHash (code_hash, storage) ->
        Js.Optdef.return (Js.string storage),
        Js.Optdef.empty,
        Js.Optdef.return (Js.string code_hash)
  in
  let callback = fun res ->
    match Js.to_bool res##.ok with
    | false -> callback Canceled
    | true ->
        let msg = Js.Optdef.get (res##.msg) (fun () -> assert false) in
        let op_hash = Js.to_string msg##.op_hash in
        let contract = Js.to_string msg##.contract in
        callback (Ok (op_hash, contract))
  in
  !metal##originate(object%js
    val mutable balance = balance
    val mutable fee = fee
    val mutable gas_limit_ = gas_limit
    val mutable storage_limit_ = storage_limit
    val mutable sc_storage_ = sc_storage
    val mutable sc_code_ = sc_code
    val mutable sc_code_hash_ = sc_code_hash
    method cb res = callback res
  end)

let delegate
    ?fee ?gas_limit ?storage_limit delegate callback =
  let delegate = match delegate with None -> Js.undefined | Some d -> Js.def (Js.string d) in
  let fee = map_opt Js.string fee in
  let gas_limit = map_opt (fun x -> Js.string (string_of_int x)) gas_limit in
  let storage_limit =
    map_opt (fun x -> Js.string (string_of_int x)) storage_limit in
  let callback = fun res ->
    match Js.to_bool res##.ok with
    | false -> callback Canceled
    | true ->
        callback
          (Ok (Js.to_string (Js.Optdef.get (res##.msg) (fun () -> assert false))))
  in
  !metal##delegate(object%js
    val mutable delegate = delegate
    val mutable fee = fee
    val mutable gas_limit_ = gas_limit
    val mutable storage_limit_ = storage_limit
    method cb res = callback res
  end)
