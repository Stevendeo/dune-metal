#!/bin/bash

# see https://developers.google.com/web/updates/2019/01/emscripten-npm

export PROG=hacl.js
export WASM=hacl.wasm
SRC=( kremlib.c FStar.c Hacl_Policies.c AEAD_Poly1305_64.c Hacl_Chacha20.c Hacl_Chacha20Poly1305.c Hacl_Curve25519.c Hacl_Ed25519.c Hacl_Poly1305_32.c Hacl_Poly1305_64.c Hacl_SHA2_256.c Hacl_SHA2_384.c Hacl_SHA2_512.c Hacl_HMAC_SHA2_256.c Hacl_Salsa20.c NaCl.c Hacl_Unverified_Random.c )

SRC=( "${SRC[@]/#/src/}" )
declare -p SRC

# -O0: Don't do any optimization. No dead code is eliminated, and Emscripten does not minify the JavaScript code it emits, either. Good for debugging.
# -O3: Optimize aggressively for performance.
# -Os: Optimize aggressively for performance and size as a secondary criterion.
# -Oz: Optimize aggressively for size, sacrificing performance if necessary.
export OPTIMIZE=" -O0"

echo "============================================="
echo "Compiling wasm bindings"
echo "============================================="
(
  # Compile C/C++ code
  emcc $OPTIMIZE -o $PROG "${SRC[@]}" \
   -DKRML_NOUINT128 \
   -s ALLOW_MEMORY_GROWTH=1 \
   -s MODULARIZE=1 \
   -s EXPORT_NAME=HACL \
   -s MALLOC=emmalloc \
   -s EXPORTED_FUNCTIONS='[ "_randombytes", "_Hacl_HMAC_SHA2_256_hmac", "_Hacl_SHA2_256_init", "_Hacl_SHA2_256_update", "_Hacl_SHA2_256_update_last", "_Hacl_SHA2_256_finish", "_Hacl_SHA2_512_init", "_Hacl_SHA2_512_update", "_Hacl_SHA2_512_update_last", "_Hacl_SHA2_512_finish", "_Hacl_Curve25519_crypto_scalarmult", "_NaCl_crypto_secretbox_easy", "_NaCl_crypto_secretbox_open_detached", "_NaCl_crypto_box_beforenm", "_NaCl_crypto_box_easy_afternm", "_NaCl_crypto_box_open_easy_afternm", "_Hacl_Ed25519_secret_to_public", "_Hacl_Ed25519_sign", "_Hacl_Ed25519_verify"]'

  # Create output folder
  rm -Rf dist
  mkdir -p dist
  # Move artifacts
  mv $PROG $WASM dist
)
echo "============================================="
echo "Compiling wasm bindings done"
echo "============================================="
