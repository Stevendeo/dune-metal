var HACL = HACL();

function wasm_allocate_u8(ml_obj) {
  var ml_obj_ptr = HACL._malloc(ml_obj.data.length * ml_obj.data.BYTES_PER_ELEMENT);
  HACL.HEAPU8.set(ml_obj.data, ml_obj_ptr);
  return ml_obj_ptr;
}

function wasm_extract_u8(ml_obj, ml_ptr) {
  var ml_data = new Uint8Array(HACL.HEAPU8.buffer, ml_ptr, ml_obj.data.length);
  for (var i = 0; i < ml_obj.data.length; i++) {
    ml_obj.data[i] = ml_data[i];
  }
  HACL._free(ml_ptr);
  return 0;
}

function wasm_allocate_u32(ml_obj) {
  var data = new Uint32Array(ml_obj.data.buffer);
  var ml_obj_ptr = HACL._malloc(data.length * data.BYTES_PER_ELEMENT);
  HACL.HEAPU32.set(data, ml_obj_ptr >> 2);
  return ml_obj_ptr;
}

function wasm_extract_u32(ml_obj, ml_ptr) {
  var ml_data = new Uint8Array(HACL.HEAPU32.buffer, ml_ptr, ml_obj.data.length);
  for (var i = 0; i < ml_obj.data.length; i++) {
    ml_obj.data[i] = ml_data[i];
  }
  HACL._free(ml_ptr);
  return 0;
}

//Provides: ml_randombytes
function ml_randombytes(x) {
  let len = x.data.length;
  var x_ptr = wasm_allocate_u8(x);
  HACL._randombytes(x_ptr, len);
  wasm_extract_u8(x, x_ptr);
  return 0;
}

//Provides: ml_Hacl_SHA2_256_init
function ml_Hacl_SHA2_256_init(state) {
  var state_ptr = wasm_allocate_u32(state);
  HACL._Hacl_SHA2_256_init(state_ptr);
  wasm_extract_u32(state, state_ptr);
  return 0;
}

//Provides: ml_Hacl_SHA2_256_update
function ml_Hacl_SHA2_256_update(state, bytes) {
  var state_ptr = wasm_allocate_u32(state);
  var bytes_ptr = wasm_allocate_u8(bytes);
  HACL._Hacl_SHA2_256_update(state_ptr, bytes_ptr);
  wasm_extract_u32(state, state_ptr);
  wasm_extract_u8(bytes, bytes_ptr);
  return 0;
}

//Provides: ml_Hacl_SHA2_256_update_last
function ml_Hacl_SHA2_256_update_last(state, bytes, len) {
  var state_ptr = wasm_allocate_u32(state);
  var bytes_ptr = wasm_allocate_u8(bytes);
  HACL._Hacl_SHA2_256_update_last(state_ptr, bytes_ptr, len);
  wasm_extract_u32(state, state_ptr);
  wasm_extract_u8(bytes, bytes_ptr);
  return 0;
}

//Provides: ml_Hacl_SHA2_256_finish
function ml_Hacl_SHA2_256_finish(state, hash) {
  var state_ptr = wasm_allocate_u32(state);
  var hash_ptr = wasm_allocate_u8(hash);
  HACL._Hacl_SHA2_256_finish(state_ptr, hash_ptr);
  wasm_extract_u32(state, state_ptr);
  wasm_extract_u8(hash, hash_ptr);
  return 0;
}

//Provides: ml_Hacl_SHA2_512_init
function ml_Hacl_SHA2_512_init(state) {
  var state_ptr = wasm_allocate_u32(state);
  HACL._Hacl_SHA2_512_init(state_ptr);
  wasm_extract_u32(state, state_ptr);
  return 0;
}

//Provides: ml_Hacl_SHA2_512_update
function ml_Hacl_SHA2_512_update(state, bytes) {
  var state_ptr = wasm_allocate_u32(state);
  var bytes_ptr = wasm_allocate_u8(bytes);
  HACL._Hacl_SHA2_512_update(state_ptr, bytes_ptr);
  wasm_extract_u32(state, state_ptr);
  wasm_extract_u8(bytes, bytes_ptr);
  return 0;
}

//Provides: ml_Hacl_SHA2_512_update_last
function ml_Hacl_SHA2_512_update_last(state, bytes, len) {
  var state_ptr = wasm_allocate_u32(state);
  var bytes_ptr = wasm_allocate_u8(bytes);
  HACL._Hacl_SHA2_512_update_last(state_ptr, bytes_ptr, len);
  wasm_extract_u32(state, state_ptr);
  wasm_extract_u8(bytes, bytes_ptr);
  return 0;
}

//Provides: ml_Hacl_SHA2_512_finish
function ml_Hacl_SHA2_512_finish(state, hash) {
  var state_ptr = wasm_allocate_u32(state);
  var hash_ptr = wasm_allocate_u8(hash);
  HACL._Hacl_SHA2_512_finish(state_ptr, hash_ptr);
  wasm_extract_u32(state, state_ptr);
  wasm_extract_u8(hash, hash_ptr);
  return 0;
}

//Provides: ml_Hacl_Ed25519_secret_to_public
function ml_Hacl_Ed25519_secret_to_public(out, secret) {
  HACL.Hacl_Ed25519_secret_to_public(out, secret);
  return 0;
}

//Provides: ml_Hacl_Ed25519_verify
function ml_Hacl_Ed25519_verify(public_key, msg, len1, signature) {
  HACL.Hacl_Ed25519_verify(public_key, msg, len1, signature);
  return 0;
}
