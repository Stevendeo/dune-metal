(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml.Typed_array
open Js_types
open Promise_lwt
open Async

class type cryptoKeyAlgorithm = object
  method name : js_string t prop
  method length : int prop
  method modulusLength : int prop
  method publicExponent : uint8Array prop
  method hash : js_string t prop
  method namedCurve : js_string t prop
  method saltLength : int prop
end

class type cryptoKey = object
  method _type : js_string t prop
  method extractable : bool t prop
  method algorithm : cryptoKeyAlgorithm t prop
  method usages : js_string t js_array t prop
end

class type cryptoKeyPair = object
  method privateKey : cryptoKey t prop
  method publicKey : cryptoKey t prop
end

class type algorithm = object
  method name : js_string t prop
  method label : arrayBuffer t opt prop
  method counter : arrayBuffer t prop
  method length : int opt prop
  method iv : arrayBuffer t prop
  method additionalData : arrayBuffer t opt prop
  method tagLength : int opt prop
  method public : cryptoKey t prop
  method salt : arrayBuffer t prop
  method info : arrayBuffer t prop
  method iterations : int prop
  method hash : js_string t prop
end

class type subtle = object
  method encrypt : algorithm t -> cryptoKey t -> arrayBuffer t -> arrayBuffer t promise t meth
  method decrypt : algorithm t -> cryptoKey t -> arrayBuffer t -> arrayBuffer t promise t meth
  method sign : algorithm t -> cryptoKey t -> arrayBuffer t -> arrayBuffer t promise t meth
  method verify : algorithm t -> cryptoKey t -> arrayBuffer t -> arrayBuffer t -> bool t promise t meth
  method digest : js_string t -> arrayBuffer t -> arrayBuffer t promise t meth
  method generateKey : algorithm t -> bool t -> js_string t js_array t -> cryptoKey t promise t meth
  method generateKey_pair : algorithm t -> bool t -> js_string t js_array t -> cryptoKeyPair t promise t meth
  method deriveKey : algorithm t -> cryptoKey t -> algorithm t -> bool t -> js_string t js_array t -> cryptoKey t promise t meth
  method deriveBits : algorithm t -> cryptoKey t -> int -> arrayBuffer t promise t meth
  method importKey : js_string t -> arrayBuffer t -> algorithm t -> bool t -> js_string t js_array t -> cryptoKey t promise t meth
  method exportKey : js_string t -> cryptoKey t -> arrayBuffer t promise t meth
  method wrapKey : js_string t -> cryptoKey t -> cryptoKey t -> algorithm t -> arrayBuffer t promise t meth
  method unwrapKey : js_string t -> arrayBuffer t -> cryptoKey t -> algorithm t -> algorithm t -> bool t -> js_string t js_array t -> cryptoKey t promise t meth
end

class type crypto = object
  method subtle : subtle t prop
  method getRandomValues : ('a, 'b) typedArray t -> unit meth
end

let crypto : crypto t = variable "window.crypto"
let subtle : subtle t = crypto##.subtle

let randomValues_uint8 n =
  let a : uint8Array t = new%js uint8Array(n) in
  crypto##getRandomValues(a);
  Bigstring.of_arrayBuffer a##.buffer

let make_algorithm ?label ?counter ?length ?iv ?additionalData ?tagLength
    ?public ?salt ?info ?iterations ?hash name =
  let algo : algorithm t = obj [||] in
  algo##.name := string name;
  (match label with None -> () | Some label -> algo##.label := some (Bigstring.to_arrayBuffer label));
  (match counter with None -> () | Some counter -> algo##.counter := Bigstring.to_arrayBuffer counter);
  (match length with None -> () | Some length -> algo##.length := some length);
  (match iv with None -> () | Some iv -> algo##.iv := Bigstring.to_arrayBuffer iv);
  (match additionalData with None -> () | Some additionalData -> algo##.additionalData := some (Bigstring.to_arrayBuffer additionalData));
  (match tagLength with None -> () | Some tagLength -> algo##.tagLength := tagLength);
  (match public with None -> () | Some public -> algo##.public := public);
  (match salt with None -> () | Some salt -> algo##.salt := Bigstring.to_arrayBuffer salt);
  (match info with None -> () | Some info -> algo##.info := Bigstring.to_arrayBuffer info);
  (match iterations with None -> () | Some iterations -> algo##.iterations := iterations);
  (match hash with None -> () | Some hash -> algo##.hash := string hash);
  algo

let wrap_err = function
  | Error e -> return (Error (Js_err e))
  | Ok x -> return (Ok x)

let to_bigstring = function
  | Ok b -> return @@ Ok (Bigstring.of_arrayBuffer b)
  | Error e -> return (Error (Js_err e))

let encrypt ~algo ~key b =
  to_lwt (subtle##encrypt algo key (Bigstring.to_arrayBuffer b)) >>= to_bigstring

let decrypt ~algo ~key b =
  to_lwt (subtle##decrypt algo key (Bigstring.to_arrayBuffer b)) >>= to_bigstring

let sign ~algo ~key b =
  to_lwt (subtle##sign algo key (Bigstring.to_arrayBuffer b)) >>= to_bigstring

let verify ~algo ~key b1 b2 =
  to_lwt (subtle##verify algo key (Bigstring.to_arrayBuffer b1)
            (Bigstring.to_arrayBuffer b2)) >>= function
  | Ok b -> return (Ok (to_bool b))
  | Error e -> return (Error (Js_err e))

let digest ~algo b =
  to_lwt (subtle##digest (string algo) (Bigstring.to_arrayBuffer b)) >>= to_bigstring

let generateKey algo extractable usages =
  to_lwt (subtle##generateKey algo (bool extractable)
         (array (Array.of_list (List.map string usages)))) >>= wrap_err

let generateKey_pair algo extractable usages =
  to_lwt (subtle##generateKey_pair algo (bool extractable)
            (array (Array.of_list (List.map string usages)))) >>= wrap_err

let deriveKey algo key algo2 extractable usages =
  to_lwt (subtle##deriveKey algo key algo2 (bool extractable)
            (array (Array.of_list (List.map string usages)))) >>= wrap_err

let deriveBits algo key length =
  to_lwt (subtle##deriveBits algo key length) >>= to_bigstring

let importKey format data algo extractable usages =
  to_lwt (subtle##importKey (string format) (Bigstring.to_arrayBuffer data) algo
            (bool extractable) (array (Array.of_list (List.map string usages))))
  >>= wrap_err

let exportKey format key =
  to_lwt (subtle##exportKey (string format) key) >>= to_bigstring

let wrapKey format key wkey walgo =
  to_lwt (subtle##wrapKey (string format)  key wkey walgo) >>= to_bigstring

let unwrapKey format wkey ukey ualgo ukeyalgo extractable usages =
  to_lwt (subtle##unwrapKey
            (string format) (Bigstring.to_arrayBuffer wkey) ukey ualgo
            ukeyalgo (bool extractable)
            (array (Array.of_list (List.map string usages)))) >>= wrap_err

let rsa_oaep = make_algorithm "RSA-OAEP"
