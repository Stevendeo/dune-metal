(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

let xorbuf a b =
  let alen = Bigstring.length a in
  if Bigstring.length b <> alen then
    invalid_arg "xor: both buffers must be of same size" ;
  for i = 0 to alen - 1 do
    Bigstring.(set a i Char.(chr (code (get a i) lxor code (get b i))))
  done ;
  a

let cdiv x y =
  if y < 1 then raise Division_by_zero else
  if x > 0 then 1 + ((x - 1) / y) else 0 [@@inline]

module SHA512 = Hacl.Hash.SHA512

let pbkdf2 ~password ~salt ~count ~dk_len =
  if count <= 0 then invalid_arg "count must be a positive integer" ;
  if dk_len <= 0l then invalid_arg "derived key length must be a positive integer" ;
  let h_len = SHA512.bytes in
  let dk_len = Int32.to_int dk_len in
  let l = cdiv dk_len h_len in
  let r = dk_len - (l - 1) * h_len in
  let block i =
    let rec f u xor = function
        0 -> xor
      | j -> let u = SHA512.HMAC.digest ~key:password ~msg:u in
        f u (xorbuf xor u) (j - 1)
    in
    let int_i = Bigstring.create 4 in
    EndianBigstring.BigEndian.set_int32 int_i 0 (Int32.of_int i);
    let u_1 = SHA512.HMAC.digest ~key:password ~msg:(Bigstring.concat "" [salt; int_i]) in
    f u_1 u_1 (count - 1)
  in
  let rec loop blocks = function
      0 -> blocks
    | i -> loop ((block i)::blocks) (i - 1)
  in
  Bigstring.concat "" (loop [Bigstring.sub (block l) 0 r] (l - 1))
