(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Collections
open Exceptions
open Utils

type traits =
  { tcomparable : bool; }

let equal_traits t1 t2 =
  Compare.Bool.equal t1.tcomparable t2.tcomparable

let default_trait = {tcomparable = false}
let comparable = {tcomparable = true}

type type_var = { tv_name : string; tv_traits : traits }

let equal_type_var t1 t2 =
  String.equal t1.tv_name t2.tv_name && equal_traits t1.tv_traits t2.tv_traits

type type_name = string Ident.t

let equal_tname n1 n2 = Ident.equal String.equal n1 n2

type structure_type_name = string Ident.t

type multiple_path =
    Local
  | AbsolutePath of string list
  | Multiple of multiple_path list

type dependency = string * string

type dependencies = dependency list

type struct_kind =
    Module
  | Contract of dependencies

let equal_multiple_path m1 m2 =
  let rec loop =
    function
      Local, Local -> true
    | AbsolutePath a1, AbsolutePath a2 ->
      Utils.list_strict_equal String.equal a1 a2
    | Multiple m1, Multiple m2 -> (
      try
        List.for_all2
          (fun l1 l2 -> loop (l1, l2))
          m1
          m2
      with _ -> false
    )
    | (Local | AbsolutePath _ | Multiple _),
      (Local | AbsolutePath _ | Multiple _) -> false
  in
  loop (m1, m2)

let rec pp_multiple_path fmt = function
  | Local ->
    Format.fprintf fmt "Local";
  | AbsolutePath p ->
    Format.fprintf fmt "Absolute : ";
    Utils.print_list_s "/" (fun fmt -> Format.fprintf fmt "%s") fmt p
  | Multiple m ->
    Utils.print_list_s "--" pp_multiple_path fmt m

type recursive = Rec | NonRec

type typedef =
  | Alias of {
      aparams : type_var list;
      atype   : t;
    }
  | SumType of {
      sparams : type_var list;
      scons   : (string * (t list)) list;
      srec    : recursive;
    }
  | RecordType of {
      rparams : type_var list;
      rfields : (string * t) list;
      rrec    : recursive;
    }

and sig_type =
  | SAbstract of type_var list (** Abstract type, exists outside but has no definition *)
  | SPrivate of typedef (** Private type, exists but cannot be built outside *)
  | SPublic of typedef (** Public type, no restriction *)

and sig_content =
  | SType of sig_type
  | SException of t list
  | SInit of t (* initializer parameter *)
  | SEntry of t (* entry points parameter *)
  | SView of t * t (* parameter and result *)
  | SValue of t
  | SStructure of structure_type
  | SSignature of structure_sig

and structure_sig = {
  sig_kind: struct_kind;
  sig_content : (string * sig_content) list;
  }

and structure_type =
  | Anonymous of structure_sig
  | Named of structure_type_name

and contract_type =
  | StructType of structure_type             (* either a usual structure type *)
  | ContractInstance of structure_type_name  (* or an instance of a contract structure *)

and t =
  (* Composite *)
  | TUser of type_name * (t list) (* ('a, 'b) t <=> TSum ("t", [TVar a, TVar b]) *)
  | TTuple of t list

  (* Functions, closures, primitive *)
  | TArrow of t * t

  (* Type variables *)
  | TVar of type_var

  (* Polymorphism *)
  | TForall of type_var * t (* comparable ? *)

  (* Contracts and entry points *)
  | TPackedStructure of structure_type (* type of packed structure *)
  | TContractInstance of structure_type (* type of contract instance *)

type type_exp = (* let lettypes in body *)
  {lettypes : (string Ident.t * typedef) list;
   body : t}

(* Arguments of type user_comp return true for user types that are comparable
   when their polymorphic arguments are. *)
type user_comp = type_name -> bool

(*| Abstract {absname; _} -> absname *)

let rec remove_foralls t = match t with TForall (_, t) -> remove_foralls t | _ -> t

let unit_contract_sig = {
    sig_content = [];
    sig_kind = Contract [] }

let unit_contract_anon_type =
  Anonymous unit_contract_sig

let unit_contract_name =
  Ident.create_id "UnitContract"

let unit_contract_named_type =
  Named (Ident.create_id ( "UnitContract"))

let find_content name sign =
  let rec loop = function
      [] -> None
    | (n, c) :: _ when String.equal name n -> Some c
    | _ :: tl -> loop tl
  in
  loop sign.sig_content

let find_storage s =
  match find_content "storage" s with
    None -> None
  | Some (SType t) -> Some t
  | Some _ -> None

module CTV =
struct
  type t = type_var
  let compare tv1 tv2 = String.compare tv1.tv_name tv2.tv_name
end

module TypeVarSet = Set.Make (CTV)
module TypeVarMap = Map.Make (CTV)

(*let opt t = TSum ({ tname = "option"; tid = 0 }, [t])
let is_opt t = Compare.Int.equal t.tid 0*)

let ( @=> ) t1 t2 = TArrow (t1, t2)

let tvar_to_ty tvar = TVar tvar

let unit = TUser (Ident.create_id "unit", [])
let bool = TUser (Ident.create_id "bool", [])
let int = TUser (Ident.create_id "int", [])
let dun = TUser (Ident.create_id "dun", [])
let nat = TUser (Ident.create_id "nat", [])
let operation = TUser (Ident.create_id "operation", [])
let list t = TUser (Ident.create_id "list", [t])
let set t = TUser (Ident.create_id "set", [t])
let map t t' = TUser (Ident.create_id "map", [t; t'])
let bigmap t t' = TUser (Ident.create_id "bigmap", [t; t'])

let entryPointType ?(type_storage=unit) type_param =
  type_storage @=>
  dun @=>
  type_param @=> TTuple [list operation; type_storage]

let viewType ?(type_storage=unit) type_param type_output =
  type_storage @=> type_param @=> type_output

let initType ?(type_storage=unit) type_arg =
  type_arg @=> type_storage

let is_module c =
  match c.sig_kind with
    Module -> true
  | Contract _ -> false

let typedef_parameters = function
    Alias {aparams = p; _}
  | SumType {sparams = p; _}
  | RecordType {rparams = p; _} -> p

let typedef_rec = function
    Alias _ -> NonRec
  | SumType {srec = r; _} | RecordType {rrec = r; _} -> r

let typevar_id = ref 0

let fresh_typevar ?(name="'a") ?(tv_traits=default_trait) () =
  let tv_name = Format.sprintf "%s_%i" name !typevar_id
  in typevar_id := !typevar_id + 1;
  {tv_name; tv_traits}

let typename_id = ref 1

let pretty_typename = Ident.print_strident

(* End helper functions *)

let rec pretty fmt = function
  (* Composite *)
  | TTuple l ->
    Format.fprintf fmt "(%a)"
      (Format.pp_print_list
         ~pp_sep:(fun fmt () -> Format.fprintf fmt " * ")
         pretty) l

  | TUser (name, []) -> pretty_typename fmt name
  | TUser (name, [t]) -> Format.fprintf fmt "%a %a" pretty t Ident.print_strident name
  | TUser (name, params) ->
    Format.fprintf fmt "(%a) %a"
      (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ")
         pretty) params
      pretty_typename name

  (* Contracts and entry points *)
  | TPackedStructure (Named sn) ->
    Format.fprintf fmt "packed_structure %a"
      (Ident.pretty (fun fmt -> Format.fprintf fmt "%s")) sn
  | TContractInstance (Named s) ->
    Format.fprintf fmt "instance %a"
      (Ident.pretty (fun fmt -> Format.fprintf fmt "%s")) s
  | TPackedStructure (Anonymous s) ->
    Format.fprintf fmt "packed_structure (@,@[<v 2>  %a@]@,)" pp_contract_sig s
  | TContractInstance (Anonymous a) ->
    Format.fprintf fmt "instance (@,@[<v 2>  %a@]@,)" pp_contract_sig a

  (* Functions, closures, primitive *)
  | TArrow (t1, t2) ->
    Format.fprintf fmt "(%a) -> %a"
      pretty t1 pretty t2

  (* Type variables *)
  | TVar v -> pp_typvar fmt v

  (* Polymorphism *)
  | TForall (v, t) ->
    Format.fprintf fmt "forall %a. %a" pp_typvar v pretty t

and pp_typvar fmt {tv_name; tv_traits} =
  let name =
    if Compare.Char.(tv_name.[0] = '@')
    then String.sub tv_name 1 (String.length tv_name - 1)
    else tv_name
  in
  Format.fprintf fmt (
    if tv_traits.tcomparable
    then "(%s[Comparable])"
    else "%s"
  )
    name

and pp_constr fmt (cname, tlist) =
  Format.fprintf fmt
    "%s of (%a)"
    cname
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ",") pretty) tlist

and pp_record fmt (recname, t) =
  Format.fprintf fmt
    "%s : (%a)"
    recname
    (pretty) t

and pp_trait fmt {tcomparable} =
  if tcomparable then Format.fprintf fmt "Comparable"
  else Format.fprintf fmt ""

and print_deps =
  Format.pp_print_list
    ~pp_sep:(fun fmt () -> Format.fprintf fmt "@,")
    (fun fmt (n, kt) -> Format.fprintf fmt "use %s = %s" n kt)

and pp_contract_sig fmt
    { sig_content;
      sig_kind } =
  let _prefix, deps =
    match sig_kind with
      Module -> "module", []
    | Contract l -> "contract", l in
  Format.fprintf fmt
    "@,@[<v 2>  sig@,\
     %a@,\
     %a@,\
     end@,@]"
    print_deps deps
    (Format.pp_print_list ~pp_sep:(fun fmt _ -> Format.fprintf fmt "@,") pp_sig_content)
    sig_content

and pp_sig_content fmt (s, content) =
  match content with
  | SType t -> pp_sig_type fmt ~name:s t
  | SException l ->
    Format.fprintf fmt
      "exception %s %a"
      s
      (fun fmt _ ->
         match l with
           [] -> ()
         | [elt] -> pretty fmt elt
         | _ ->
           (Format.pp_print_list
              ~pp_sep:(fun fmt _ -> Format.fprintf fmt "@,") pretty) fmt l
      ) l
  | SInit t -> Format.fprintf fmt "val%%init %s : %a -> storage" s pretty t
  | SEntry t ->
    Format.fprintf fmt "val%%entry %s : %a"
      s
      pretty t
  | SView  (t1, t2) ->
    Format.fprintf fmt "val%%view %s : %a -> %a"
      s
      pretty t1
      pretty t2
  | SValue t -> Format.fprintf fmt "val %s : %a" s pretty t
  | SStructure s_sig ->
    Format.fprintf fmt "contract %s : %a" s pp_ccontract_sig s_sig
  | SSignature s_sig  ->
    Format.fprintf fmt
      "contract type %s = %a@,"
      s pp_contract_sig s_sig

and pp_sig_type fmt ~name = function
  | SAbstract tl -> Format.fprintf fmt "type %a%s" pp_typ_params tl name
  | SPrivate td -> pp_typdef fmt ~name ~privacy:"private" td
  | SPublic td -> pp_typdef fmt ~name ~privacy:"" td

and pp_ccontract_sig fmt str =
  match str with
  | Named n -> Ident.print_strident fmt n
  | Anonymous s -> pp_contract_sig fmt s

and pp_contract_type fmt ct =
  match ct with
  | StructType st -> pp_ccontract_sig fmt st
  | ContractInstance cn -> Ident.print_strident fmt cn

and pp_scons fmt (name, (l : t list)) =
  match l with
    [] -> Format.fprintf fmt "%s" name
  | [t] ->
    Format.fprintf fmt
      "%s of %a"
      name
      pretty t
  | _ ->
  Format.fprintf fmt
    "%s of (%a)"
    name
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ") pretty) l

and pp_rfields fmt (name,t) =
  Format.fprintf fmt
    "%s : %a"
    name
    pretty t

and pp_typ_params fmt = function
  | [] -> ()
  | [tv] -> pp_typvar fmt tv; Format.fprintf fmt " "
  | (tps : type_var list) -> Format.fprintf fmt "(%a) " (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ", ") pp_typvar) tps

and pp_sumtyp fmt name privacy sparams scons srec =
  Format.fprintf fmt
    "type%a %a%s = %s@,   %a"
    pp_rec srec
    pp_typ_params sparams
    name
    privacy
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt "@, | ") pp_scons) scons

and pp_rectyp fmt name privacy rparams rfields rrec =
  Format.fprintf fmt
    "type%a %a%s = %s {@,@[<v 2>  %a@]@,}"
    pp_rec rrec
    pp_typ_params rparams
    name
    privacy
    (Format.pp_print_list ~pp_sep:(fun fmt () -> Format.fprintf fmt ";@,") pp_rfields)
    rfields

and pp_aliastyp fmt name visib aparams atype =
  Format.fprintf fmt
    "type %a%s = %s%a"
    pp_typ_params aparams
    name (if String.equal visib "" then "" else visib ^ " ")
    pretty atype

and pp_rec fmt = function
    Rec -> Format.fprintf fmt ""
  | NonRec -> Format.fprintf fmt " nonrec"

and pp_typdef fmt ~name ~privacy = function
  | SumType {sparams; scons; srec} ->
      pp_sumtyp fmt name privacy sparams scons srec
  | RecordType {rparams; rfields; rrec} ->
      pp_rectyp fmt name privacy rparams rfields rrec
  | Alias {aparams; atype} ->
      pp_aliastyp fmt name privacy aparams atype

let paramsFromEntryPoint t =
  match t with
    TUser (Ident.LName "entrypoint", [pt]) -> pt
  | _ ->
    Log.debug "[paramsFromEntryPoint] Type %a is not an entry point" pretty t;
    raise BadEntryPoint

let paramsFromView t =
  match t with
    TUser (Ident.LName "view", [pt; rt]) -> pt, rt
  | _ ->
    Log.debug "[paramsFromView] Type %a is not a view" pretty t;
    raise BadView

let type_of_typedef n = function
  | Alias {aparams = p; _}
  | SumType {sparams = p; _}
  | RecordType {rparams = p; _} ->
    TUser (Ident.create_id n, List.map (fun v -> TVar v) p)
(*| Abstract {aparams = p; a} *)

let invalid_arg s = raise (Exceptions.InvariantBroken s)

let typ_by_str_args tname tparam = TUser (tname, tparam)

let rec isComparable (user_comp : user_comp) t =
  let isComparable = isComparable user_comp in
  match t with
  | TPackedStructure _
  | TContractInstance _ -> false

  | TTuple l -> List.for_all isComparable l
  | TUser (n, l) -> List.for_all isComparable l && user_comp n

  | TArrow _ -> false
  | TVar {tv_traits; _} -> tv_traits.tcomparable
  | TForall _ -> false (* Could it be made comparable ? I.e empty list ?*)

module SMap = StringMap

let rec search_cs name cs =
  let rec search_content_list prev next =
    match next with
      [] -> None
    | ((s, ((SStructure (Anonymous a)) | SSignature a))) as hd :: tl ->
      if String.equal s name then Some (s, a)
      else search_content_list (hd :: prev) tl
    | (_, (SStructure (Named n))) :: _ ->
      advance_in_id n cs
    | _ :: tl -> search_content_list prev tl
  in
  search_content_list [] cs.sig_content

and advance_in_id id cs =
  match Ident.split id with
    name, None -> Some (name, cs)
  | name, Some (rest_of_path) -> (
      match search_cs name cs with
        None -> None
      | Some (_,c) -> advance_in_id rest_of_path c
    )

let get_subc id c =
  match advance_in_id id c with
    None -> None
  | Some (name, subc) -> (
      match search_cs name subc with
        None -> None
      | Some (_, c) -> Some c
    )

type equal_result = (* Either Aliases (represents a success) or an error. *)
    Aliases of type_var SMap.t
  | TypeIncompatibility of t * t
  | SigContentIncompatibility of sig_content * sig_content
  | ModuleVSContract

let (&&&) a =
  fun r1 r2 ->
  match r1 a with
    Aliases a -> r2 a
  | _ as res -> res

let rec equal t1 t2 aliases : equal_result =
  let rec eq aliases t1 t2 =
    let equal t1 t2 aliases = eq aliases t1 t2 in
    Log.debug "[Love_type.equal] Is %a = %a ?" pretty t1 pretty t2;
    let (&&&) = (&&&) aliases in
    match t1, t2 with

    | TArrow (tk1, tb1), TArrow (tk2, tb2) ->
      equal tk1 tk2 &&& equal tb1 tb2
    | TContractInstance (Named n1), TContractInstance (Named n2)
    | TPackedStructure (Named n1), TPackedStructure (Named n2) ->
      if Ident.equal String.equal n1 n2
      then Aliases aliases else TypeIncompatibility (t1, t2)
    | TContractInstance (Anonymous s1), TContractInstance (Anonymous s2)
    | TPackedStructure (Anonymous s1), TPackedStructure (Anonymous s2) ->
      equal_ctr_sig s1 s2 aliases

    | TTuple l1, TTuple l2 -> (
        try eq_list l1 l2 aliases
        with Invalid_argument _ -> TypeIncompatibility (t1, t2)
      )
    | TUser (n1, l1), TUser (n2, l2) -> (
        if equal_tname n1 n2 then
          try eq_list l1 l2 aliases
          with Invalid_argument _ -> TypeIncompatibility (t1, t2)
        else TypeIncompatibility (t1, t2)
      )
    | TVar tv1, TVar tv2 ->
      let test =
      match SMap.find_opt tv1.tv_name aliases with
        None -> equal_type_var tv1 tv2
        | Some tv1' -> equal_type_var tv1' tv2
      in
      if test
      then Aliases aliases
      else TypeIncompatibility (t1, t2)
    | TForall (s1, t1), TForall (s2, t2) ->
      eq (SMap.add s1.tv_name s2 aliases) t1 t2

    | ( TContractInstance _
      | TPackedStructure _
      | TTuple _
      | TUser _ | TArrow _   | TForall _ | TVar _),
      ( TContractInstance _   | TPackedStructure _
      | TTuple _
      | TUser _ | TArrow _   | TForall _ | TVar _) ->
      TypeIncompatibility (t1, t2)
  in
  eq aliases t1 t2

and equal_typ_def (s1, t1) (s2, t2) aliases : equal_result =
  let error (sd1,td1) (sd2,td2) =
    let t1 = type_of_typedef sd1 td1 in
    let t2 = type_of_typedef sd2 td2 in
    TypeIncompatibility (t1, t2)
  in
  match t1, t2 with
  | SPrivate (Alias a1), SPrivate (Alias a2)
  | SPublic (Alias a1), SPublic (Alias a2)
    (*| SInternal (Alias a1), SInternal (Alias a2) *) ->
    equal a1.atype a2.atype aliases

  | SPrivate (SumType st1), SPrivate (SumType st2)
  | SPublic (SumType st1), SPublic (SumType st2)
    (* |  SInternal (SumType s1), SInternal (SumType s2) *)-> (
      try
        List.fold_left2
          (fun acc (c1, l1) (c2, l2) ->
             if String.equal c1 c2
             then
               match acc with
                 Aliases _ ->
                 List.fold_left2
                   (fun acc t1 t2 ->
                      match acc with
                        Aliases a -> equal t1 t2 a
                      | _ as res -> res
                   )
                   acc
                   l1
                   l2
               | res -> res
             else
               error (s1,SumType st1) (s2,SumType st2)
          )
          (Aliases aliases)
          st1.scons
          st2.scons
      with Invalid_argument _ -> error (s1,SumType st1) (s2,SumType st2)
    )
  | SPrivate (RecordType r1), SPrivate (RecordType r2)
  | SPublic (RecordType r1), SPublic (RecordType r2)
  (* | SInternal (RecordType r1), SInternal (RecordType r2) *) -> (
      try
        List.fold_left2
          (fun acc (f1, t1) (f2, t2) ->
             if String.equal f1 f2
             then
               match acc with
                 Aliases a -> equal t1 t2 a
               | res -> res
             else
               error (s1,RecordType r1) (s2,RecordType r2)
          )
          (Aliases aliases) r1.rfields r2.rfields
      with Invalid_argument _ -> error (s1,RecordType r1) (s2,RecordType r2)
    )
  | SAbstract l1, SAbstract l2 -> (
      try
        let l1 =
          List.map
            (fun tv ->
               match SMap.find_opt tv.tv_name aliases with
                 None -> tv
               | Some t -> t
            )
            l1
        in
        if List.for_all2 equal_type_var l1 l2
        then Aliases aliases
        else
          TypeIncompatibility
            (TUser (Ident.create_id s1, List.map tvar_to_ty l1),
             TUser (Ident.create_id s2, List.map tvar_to_ty l2))
      with Invalid_argument _ ->
        TypeIncompatibility
            (TUser (Ident.create_id s1, List.map tvar_to_ty l1),
             TUser (Ident.create_id s2, List.map tvar_to_ty l2))
  )

  | (SPrivate td1 | SPublic td1 (*| SInternal td1*)),
    (SPrivate td2 | SPublic td2 (*| SInternal td2*)) ->
    error (s1,td1) (s2,td2)
  | SAbstract l, (SPrivate td | SPublic td) ->
    TypeIncompatibility
      (TUser (Ident.create_id s1, List.map tvar_to_ty l),
       type_of_typedef s2 td)
  | (SPrivate td | SPublic td), SAbstract l ->
    TypeIncompatibility
      ( type_of_typedef s1 td,
        TUser (Ident.create_id s2, List.map tvar_to_ty l))

and equal_exceptions l1 l2 aliases =
  eq_list l1 l2 aliases

and equal_sig_content (s1, c1) (s2, c2) aliases =
    let (&&&) = (&&&) aliases in
  if String.equal s1 s2
  then (
    match c1, c2 with
    | SType t1, SType t2 -> equal_typ_def (s1, t1) (s2, t2) aliases
    | SException tl1, SException tl2 -> equal_exceptions tl1 tl2 aliases
    | SInit t1, SInit t2 -> equal t1 t2 aliases
    | SEntry tp1, SEntry tp2 ->
      equal tp1 tp2 aliases
    | SView  (tp1, tr1), SView  (tp2, tr2) ->
      (equal tp1 tp2) &&& (equal tr1 tr2)
    | SValue t1, SValue t2 -> equal t1 t2 aliases
    | SStructure (Named n1), SStructure (Named n2) ->
      if Ident.equal String.equal n1 n2
      then Aliases aliases
      else
        TypeIncompatibility (TContractInstance (Named n1), TContractInstance (Named n2))
    | SStructure (Anonymous s1), SStructure (Anonymous s2)
    | SSignature s1, SSignature s2 -> equal_ctr_sig s1 s2 aliases

    (* False otherwise *)
    | (SType _ | SException _ | SInit _ | SEntry _ | SView _ | SValue _
      | SStructure _ | SSignature _),
      (SType _ | SException _ | SInit _ | SEntry _ | SView _ | SValue _
      | SStructure _ | SSignature _) ->
      SigContentIncompatibility (c1, c2)
  ) else
      SigContentIncompatibility (c1, c2)

and equal_str_kind k1 k2 =
  match k1, k2 with
    Module, Module -> true
  | Contract l1, Contract l2 -> (
      try
        List.for_all2 (fun (s1, s2) (s1', s2') -> String.equal s1 s1' && String.equal s2 s2') l1 l2
      with Invalid_argument _ -> false
    )
  | (Module | Contract _), (Module | Contract _) -> false

and equal_ctr_sig c1 c2 aliases =
  let (&&&) = (&&&) aliases in
  if not (equal_str_kind c1.sig_kind c2.sig_kind)
  then ModuleVSContract
  else (
    let rec test_sig_contents l1 l2 aliases =
      match l1, l2 with
        [], [] -> Aliases aliases
      | hd1 :: tl1, hd2 :: tl2 ->
        equal_sig_content hd1 hd2 &&& test_sig_contents tl1 tl2
      | _,_ ->
        TypeIncompatibility
          ((TContractInstance (Anonymous c1)), (TContractInstance (Anonymous c2))) in
    test_sig_contents c1.sig_content c2.sig_content aliases
  )

and eq_list l1 l2 aliases =
  match l1, l2 with
    [], [] -> Aliases aliases
  | (hd1 :: tl1), (hd2 :: tl2) -> (
      match equal hd1 hd2 aliases with
        Aliases a -> eq_list tl1 tl2 a
      | res -> res
      )
    | _,_ -> raise (Invalid_argument "Love_type.eq_list")

let equal t1 t2 = equal t1 t2 SMap.empty
let equal_ctr_sig t1 t2 = equal_ctr_sig t1 t2 SMap.empty

let bool_equal t t' =
  match equal t t'
  with
    Aliases a -> SMap.is_empty a
  | _ -> false

(* Returns None if t is not an entry point type,
   Returns Some s otherwise with s the type of the storage *)
let isEntryPointType t =
  let rec ret_type =
    function
      TArrow (_, t) | TForall (_, t) -> ret_type t
    | t -> t
  in
  match t with
    TArrow (t1, TArrow (t2, t3)) -> (
      if bool_equal t2 dun
      then (
        match ret_type t3 with
          TTuple (_ :: operations :: new_storage :: []) -> (
            if
              bool_equal operations (list operation) &&
              bool_equal new_storage t1
            then Some new_storage
            else None
          )
        | _ -> None
      )
      else None
    )
  | _ -> None

let fvars t =
  (* todo : tail rec *)
  let rec fvars t : TypeVarSet.t =
    match t with
    | TVar v -> TypeVarSet.singleton v

    | TContractInstance _
    | TPackedStructure _ -> TypeVarSet.empty

    | TTuple l ->
      List.fold_left
        (fun acc t -> TypeVarSet.union acc @@ fvars t)
        TypeVarSet.empty
        l

    | TUser (_,l) ->
      List.fold_left
        (fun acc t -> TypeVarSet.union acc (fvars t))
        TypeVarSet.empty
        l
    | TArrow (t,t') ->
      TypeVarSet.union (fvars t) (fvars t')
    | TForall (v, t) -> TypeVarSet.remove v (fvars t)
  in
  fvars t

let rec isListOf = function
  | TForall (_, t) -> isListOf t
  | TVar _v -> None
  | TContractInstance _ | TPackedStructure _
  | TTuple _ | TUser _ | TArrow _ -> None

let rec occur tvar t =
  match t with
  | TVar v when String.equal v.tv_name tvar -> true
  | TVar _ -> false

  | TContractInstance _
  | TPackedStructure _ -> false

  | TTuple l -> List.exists (occur tvar) l
  | TUser (_n, params) ->
    List.exists
      (occur tvar)
      params
  | TArrow (t,t') -> occur tvar t || occur tvar t'
  | TForall (v, t) ->
    if String.equal v.tv_name tvar
    (* then tvar will be occulted in the following definition *)
    then false
    else occur tvar t

let rec replace_struct_sig (user_comp : user_comp) tvarmap (s : structure_sig)
  : structure_sig =
  { sig_content = List.map (replace_content user_comp tvarmap) s.sig_content;
    sig_kind = s.sig_kind;
  }

and replace_content (user_comp : user_comp) tvarmap (s,c) =
  let new_c =
    match c with
    | SType t -> SType (replace_sig_type user_comp tvarmap t)
    | SException tl -> SException (List.map (replace_map user_comp tvarmap) tl)
    | SInit t -> SInit (replace_map user_comp tvarmap t)
    | SEntry tp -> SEntry (replace_map user_comp tvarmap tp)
    | SView (tp, tr) ->
      SView (replace_map user_comp tvarmap tp, replace_map user_comp tvarmap tr)
    | SValue t -> SValue (replace_map user_comp tvarmap t)
    | SStructure (Named _) -> c
    | SStructure (Anonymous s) ->
      SStructure (Anonymous (replace_struct_sig user_comp tvarmap s))
    | SSignature s -> SSignature (replace_struct_sig user_comp tvarmap s)
  in
  s, new_c

and replace_tdef (user_comp : user_comp) tvarmap td : typedef =
  match td with
  | SumType {sparams; scons; srec} ->
    let tvarmap =
      List.fold_left
        (fun map param -> TypeVarMap.remove param map)
        tvarmap
        sparams
    in
    let scons =
      List.map
        (fun (cstr,l) -> cstr, (List.map (replace_map user_comp tvarmap) l))
        scons
    in
    SumType {sparams; scons; srec}
  | RecordType {rparams; rfields; rrec} ->
    let tvarmap =
      List.fold_left
        (fun map param -> TypeVarMap.remove param map)
        tvarmap
        rparams
    in
    let rfields =
      List.map
        (fun (field,t) -> field, replace_map user_comp tvarmap t)
        rfields
    in
    RecordType {rparams; rfields; rrec}
  | Alias {aparams; atype} ->
    let tvarmap =
      List.fold_left
        (fun map param -> TypeVarMap.remove param map)
        tvarmap
        aparams
    in
    Alias {aparams; atype = replace_map user_comp tvarmap atype}
  (*| Abstract {absparams; absname; abstdef = None} ->
    td
  | Abstract {absparams; absname; abstdef = Some tdef} ->
    let tvarmap =
      List.fold_left
        (fun map param -> TypeVarMap.remove param map)
        tvarmap
        absparams
    in
    Abstract {absparams; absname; abstdef = Some (replace_tdef tvarmap tdef)} *)

and replace_sig_type (user_comp : user_comp) tvarmap s =
  match s with
  | SAbstract _ -> s
  (*| SInternal t -> SInternal (replace_tdef tvarmap t)*)
  | SPrivate t -> SPrivate (replace_tdef user_comp tvarmap t)
  | SPublic t -> SPublic (replace_tdef user_comp tvarmap t)

and replace_map (user_comp : user_comp) tvarmap t =
  let replace = replace_map user_comp tvarmap in
  match t with
  | TVar v -> (
      match TypeVarMap.find_opt v tvarmap with
        None -> t
      | Some new_t ->
        if v.tv_traits.tcomparable && not (isComparable user_comp new_t)
        then (
          Log.debug
            "[replace_map] Error : trying to replace %a with %a : incompatible traits@."
            pretty t pretty new_t;
          raise (Exceptions.BadArgument "Replacing types with incomaptible traits")
        )
        else
          new_t
    )
  | TPackedStructure (Named _)
  | TContractInstance (Named _) -> t
  | TPackedStructure (Anonymous s) ->
    TPackedStructure (Anonymous (replace_struct_sig user_comp tvarmap s))
  | TContractInstance (Anonymous s) ->
    TContractInstance (Anonymous (replace_struct_sig user_comp tvarmap s))
  | TUser (n, args) ->
    TUser (n, List.map replace args)

  | TTuple l -> TTuple (List.map replace l)
  | TArrow (t,t') -> TArrow ((replace t),(replace t'))

  | TForall (v, t) as typ -> (
      match TypeVarMap.find_opt v tvarmap with
        None -> TForall (v, replace t)
      | Some _ -> typ
    )

let replace (user_comp : user_comp) tvar new_t t =
  replace_map user_comp (TypeVarMap.singleton tvar new_t) t

let instanciate_tdef
    (user_comp : user_comp) (n : string) (typedef : typedef) (params : t list) =
  let rec fold_params res tparams pparams =
    match tparams, pparams with
      [], [] -> res
    | t :: tl1, tv :: tl2 ->
      fold_params (TypeVarMap.add tv t res) tl1 tl2
    | _,_ -> raise (BadArgument "Bad number of parameters")
  in
  let create_map = fold_params TypeVarMap.empty params in
  match typedef with
    Alias {atype; aparams; _} ->
    let map = create_map aparams in
    replace_map user_comp map atype
  | SumType {sparams = p; _}
  | RecordType {rparams = p; _} ->
    if Compare.Int.((List.length p) = (List.length params))
    then
      TUser (Ident.create_id n, params)
    else
      raise (BadArgument "Bad number of parameters")

let contract_aliases (user_comp : user_comp) cs name params =
  let aparams, atype =
    let rec find_alias = function
        [] ->
        raise (
          BadArgument (
            Format.asprintf
              "Love_type.contract_aliases: unknown %s"
              name
          )
        )
      | (s,
         SType (
           (SPublic (Alias {aparams; atype; _}) | SPrivate Alias {aparams; atype; _})
         )
        )
        :: _ when String.equal name s ->
        aparams, atype
      | _ :: tl -> find_alias tl
    in
    find_alias cs.sig_content
  in
  let alias_map =
    try
      List.fold_left2
        (fun acc tvar p -> TypeVarMap.add tvar p acc)
        TypeVarMap.empty
        aparams
        params
    with Invalid_argument _ ->
      raise (
        BadArgument (
          Format.asprintf
            "Love_type.contract_aliases:\
             bad number of parameters: %i provided ; %i expected."
            (List.length aparams)
            (List.length params)
        )
      )
  in
  replace_map user_comp alias_map atype

type subtyp_result =
    Ok
  | TypeError of t * t
  | TypeDefError of (string * typedef) * (string * typedef)
  | SigContentError of (string * sig_content) * (string * sig_content)
  | Other of string

let str_subtyp_result = function
  | Ok -> "Ok"
  | TypeError (t1, t2) ->
    Format.asprintf "Type %a is not a subtype of %a" pretty t1 pretty t2
  | TypeDefError ((s1,td1), (s2, td2)) ->
    Format.asprintf "Type definition %a is incompatible with %a"
      (pp_typdef ~name:s1 ~privacy:"") td1 (pp_typdef ~name:s2 ~privacy:"") td2
  | SigContentError ((s1, _), (s2, _)) ->
    Format.asprintf "%s is incompatible with %s" s1 s2
  | Other s -> "Error: " ^ s

let rec subtyp_typedef
    (tvars : type_var TypeVarMap.t)
    (signatures : string Ident.t -> structure_sig)
    s1
    t1
    s2
    t2 : subtyp_result =
  let error () = TypeDefError ((s1,t1), (s2,t2)) in
  match t1, t2 with
    Alias a1, Alias a2 ->
    subtyp tvars signatures a1.atype a2.atype
  | SumType s1, SumType s2 -> (
      try
        List.fold_left2
          (fun acc (c1, l1) (c2, l2) ->
             if String.equal c1 c2
             then
               match acc with
                 Ok ->
                 List.fold_left2
                   (fun acc t1 t2 ->
                      match acc with
                        Ok -> subtyp tvars signatures t1 t2
                      | _ -> acc
                   )
                   acc
                   l1
                   l2
               | _ -> acc
             else error ()
          )
          Ok s1.scons s2.scons
      with Invalid_argument _ -> error ()
    )
  | RecordType r1, RecordType r2 -> (
      try
        List.fold_left2
          (fun acc (f1, t1) (f2, t2) ->
             if String.equal f1 f2 then
             match acc with
                 Ok -> subtyp tvars signatures t1 t2
               | _ -> acc
             else error ()
          )
          Ok r1.rfields r2.rfields
      with Invalid_argument _ -> error ()
    )(*
  | Abstract a1, Abstract a2 ->
    if
      String.equal a1.absname a2.absname &&
        (try List.for_all2 equal_type_var a1.absparams a2.absparams with _ -> false)
    then
      match a1.abstdef, a2.abstdef with
        None, None -> None
      | Some td1, Some td2 ->
        subtyp_typedef tvars aliases signatures td1 td2
      | _,_ ->
      Some (TUser (Ident.create_id a1.absname, List.map (fun t -> TVar t) a1.absparams),
            TUser (Ident.create_id a2.absname, List.map (fun t -> TVar t) a2.absparams))
    else
      Some (TUser (Ident.create_id a1.absname, List.map (fun t -> TVar t) a1.absparams),
            TUser (Ident.create_id a2.absname, List.map (fun t -> TVar t) a2.absparams))
*)
  | (Alias _ | SumType _ | RecordType _ ),
    (Alias _ | SumType _ | RecordType _ ) ->
    error ()

and subtyp
    (tvars : type_var TypeVarMap.t)
    (signatures : string Ident.t -> structure_sig)
    t1
    t2  : subtyp_result =
  Log.debug "[subtyp] Are types %a <= %a ?@." pretty t1 pretty t2;
  let subtyp t = subtyp t signatures in
  let res =
    match t1, t2 with
    | TTuple l1, TTuple l2 ->
      Log.debug "[subtyp] Tuples@.";
      let rec cross_lists l1 l2 =
        match l1,l2 with
          [],[] -> Ok
        | hd1::tl1, hd2::tl2 -> (
            match subtyp tvars hd1 hd2 with
              Ok -> cross_lists tl1 tl2
            | res -> res
          )
        | _,_ -> TypeError (t1, t2)
      in cross_lists l1 l2

    | TUser (n1, p1), TUser (n2, p2) -> (
        let rec cross_lists l1 l2 =
          match l1,l2 with
            [],[] -> Ok
          | hd1::tl1, hd2::tl2 -> (
              match subtyp tvars hd1 hd2 with
                Ok -> cross_lists tl1 tl2
              | res -> res
            )
          | _,_ -> TypeError (t1, t2)
        in
        (*
        Log.debug "[subtyp] Double user types@.";
        if Ident.equal String.equal n1 n2
        then (
          Log.debug "[subtyp] Equal name types@.";
          cross_lists p1 p2
        )
        else
          let st1 = aliases n1 p1 in
          Log.debug "[subtyp] Type 1: %a@."
            pretty st1;
          let st2 = aliases n2 p2 in
          Log.debug "[subtyp] Type 2: %a@."
            pretty st2;
          match st1, st2 with
            (TUser (name1, params1)), (TUser (name2, params2)) ->
            if Ident.equal String.equal name1 name2
            then cross_lists params1 params2
            else TypeError (t1, t2)
          | (TUser _), _
          | _, (TUser _) -> TypeError (t1, t2)
          |_ -> subtyp tvars st1 st2 *)
        (* Assumes types are well formed *)
        if Ident.equal String.equal n1 n2
        then cross_lists p1 p2
        else TypeError (t1, t2)
      )

    | TUser _, _
    | _, TUser _ -> TypeError (t1, t2)

    | TArrow (t1, t2), TArrow(t1', t2') -> (
        Log.debug "[subtyp] (Big)Map/Arrow@.";
        match subtyp tvars t1 t1' with
          Ok -> subtyp tvars t2 t2'
        | res -> res
      )
    | TContractInstance (Named n1), TContractInstance (Named n2)
    | TPackedStructure (Named n1), TPackedStructure (Named n2) ->
      Log.debug "[subtyp] Named contract : %a = %a ?"
        (Ident.pretty (fun fmt -> Format.fprintf fmt "%s")) n1
        (Ident.pretty (fun fmt -> Format.fprintf fmt "%s")) n2;
      let a1 = signatures n1 in let a2 = signatures n2 in
      sub_contract tvars signatures a1 a2

    | TContractInstance (Anonymous s1), TContractInstance (Anonymous s2)
    | TPackedStructure (Anonymous s1), TPackedStructure (Anonymous s2) ->
      Log.debug "[subtyp] Anonymous contracts";
      sub_contract tvars signatures s1 s2

    | TContractInstance (Named n), TContractInstance (Anonymous c)
    | TPackedStructure (Named n), TPackedStructure (Anonymous c) ->
      let a = signatures n in sub_contract tvars signatures a c

    | TContractInstance (Anonymous c), TContractInstance (Named n)
    | TPackedStructure (Anonymous c), TPackedStructure (Named n) ->
      let a = signatures n in sub_contract tvars signatures c a

    | TVar tv1, TVar tv2 -> (
        let tv1 = match TypeVarMap.find_opt tv1 tvars with None -> tv1 | Some t -> t in
        if equal_type_var tv1 tv2
        then Ok
        else TypeError (t1, t2)
      )
    | TVar _, _ -> ( (*
        match TypeVarMap.find_opt tv aliases with
        | None -> Some (t1, t2)
        | Some ({contents = None} as r) -> r := Some t'; None
        | Some ({contents = Some t}) -> subtyp  t t' *)
        TypeError (t1, t2)
    )
    | _t, (*((TVar tv) as t')*) TVar _ -> TypeError (t1, t2)

    | TForall (arg, t), TForall (arg', t') ->
      Log.debug "[subtyp] Foralls@.";
      subtyp (TypeVarMap.add arg arg' tvars) t t'

    | _, TForall (_arg, _) ->
      Log.debug "[subtyp] Right is forall, left is not@.";
      TypeError (t1, t2)

    | TForall (_arg, _t), _t' ->
      Log.debug "[subtyp] Left is forall, right is not@."; (*
      subtyp :(TypeVarMap.add arg (ref None) aliases) t t' *)
      TypeError (t1, t2)

    | ( TContractInstance _ | TPackedStructure _
       | TTuple _ | TArrow _),
      ( TContractInstance _ | TPackedStructure _
      | TTuple _ | TArrow _) ->
      Log.debug "[subtyp] These types are different@.";
      TypeError (t1, t2)
  in
  let () =
    match res with
      Ok ->
      Log.debug "[subtyp] %a is a subtype of %a@."
        pretty t1
        pretty t2
    | TypeError _ ->
      Log.debug "[subtyp] %a is not a subtype of %a@."
        pretty t1
        pretty t2
    | TypeDefError ((s1,td1), (s2,td2)) ->
      Log.debug "[subtyp] Typedef %a is not equal to %a@."
        (pp_typdef ~name:s1 ~privacy:"") td1
        (pp_typdef ~name:s2 ~privacy:"") td2
    | SigContentError (c1, c2) ->
      Log.debug "[subtyp] Signature content %a is not compatible with %a@."
        pp_sig_content c1
        pp_sig_content c2
    | Other s ->
      Log.debug "[subtyp] Error : %s@." s
  in
  res

and sub_content tvars signatures is_module (s1,cont1) (s2,cont2) =
  match cont1, cont2 with
    | SType t1, SType t2 -> sub_sigtype tvars signatures s1 t1 s2 t2
    | SException tl1, SException tl2 ->
      subtyp_exceptions tvars signatures (s1,tl1) (s2,tl2)
    | SInit t1, SInit t2 -> subtyp tvars signatures t1 t2
    | SEntry tp1, SEntry tp2 ->
      subtyp tvars signatures tp1 tp2
    | SView  (tp1, tr1), SView  (tp2, tr2) -> (
        if is_module
        then Other "Modules with entry point or view"
        else (
          match subtyp tvars signatures tp1 tp2 with
            Ok -> subtyp tvars signatures tr1 tr2
          | res -> res
        )
      )
    | SValue t1, SValue t2 -> subtyp tvars signatures t1 t2
    | SStructure (Named n1), SStructure (Named n2) ->
      let s1 = signatures n1 in
      let s2 = signatures n2 in
      sub_contract tvars signatures s1 s2
    | SStructure (Anonymous s1), SStructure (Anonymous s2)
    | SSignature s1, SSignature s2 -> sub_contract tvars signatures s1 s2
    | SStructure (Named n), SStructure (Anonymous s2) ->
      let s1 = signatures n in
      sub_contract tvars signatures s1 s2
    | SStructure (Anonymous s1), SStructure (Named n) ->
      let s2 = signatures n in
      sub_contract tvars signatures s1 s2
    (* False otherwise *)
    | (SType _ | SException _ | SInit _ | SEntry _ | SView _ | SValue _
      | SStructure _ | SSignature _),
      (SType _ | SException _ | SInit _ | SEntry _ | SView _ | SValue _
      | SStructure _ | SSignature _) ->
      SigContentError ((s1,cont1),(s2,cont2))

and subtyp_exceptions tvars signatures (s1,l1) (s2, l2)  =
  try
    List.fold_left2
      (fun acc t1 t2 ->
         match acc with
           Ok -> subtyp tvars signatures t1 t2
         | _ -> acc
      )
      Ok
      l1
      l2
  with Invalid_argument _ ->
    SigContentError ((s1,SException l1),(s2,SException l2))

and sub_sigtype tvars signatures n1 s1 n2 s2 =
  match s1, s2 with
    SPublic td1, SPublic td2
  | SPrivate td1, SPublic td2
  | SPrivate td1, SPrivate td2
    (*| SInternal td1, SPublic td2
      | SInternal td1, SPrivate td2
      | SInternal td1, SInternal td2 *) ->
    subtyp_typedef tvars signatures n1 td1 n2 td2
  | SPublic td1, SPrivate td2 ->
    let t1 = type_of_typedef n1 td1 in
    let t2 = type_of_typedef n2 td2 in
    TypeError (t1, t2)
  | (SPublic _ | SPrivate _), SAbstract _ ->
    Other "Right type is abstract, left has a definition."
  (*| SAbstract _, SInternal _ -> Some (TUnit, TUnit) *)

  | SAbstract tl, SPublic td
  | SAbstract tl, SPrivate td -> (
      let params = typedef_parameters td in
      try
        if List.for_all2 equal_type_var params tl then Ok
        else Other "Incompatible paremeter names or traits"
      with
        Invalid_argument _ -> Other "Incompatible number of paremeters"
    )
  | SAbstract tl1, SAbstract tl2 ->
    try
      if List.for_all2 equal_type_var tl1 tl2 then Ok
      else Other "Incompatible paremeter names or traits between abstract types"
    with
      Invalid_argument _ ->
      Other "Incompatible number of paremeters between abstract types"

and sub_contract tvars signatures c1 c2 : subtyp_result =
  Log.debug
    "[sub_contract] Subtyping %a and %a"
    pp_contract_sig c1
    pp_contract_sig c2;
  if equal_str_kind c1.sig_kind c2.sig_kind
  then
    begin
      Log.debug "[sub_contract] Testing subcontract@.";
      let rec check_content l1 l2 =
        match l1, l2 with
          [], _ -> Ok
        | ((n1, _) as co1) :: tl1, ((n2, _) as co2) :: tl2 when String.equal n1 n2 -> (
            match sub_content tvars signatures (is_module c1) co1 co2
            with
              Ok -> check_content tl1 tl2
            | res -> res
          )
        | (n,_) :: _, [] ->
          Other (
            Format.asprintf
              "Expected %s in contract definition. \
               Did you forget to make it public or to add it in the signature ? " n)
        | _, _ :: tl2 (* *) ->
          check_content l1 tl2
      in
      check_content c1.sig_content c2.sig_content
    end
  else (
    Log.debug "[subcontract] Module test failed@.";
    Other "Module against Contract"
  )

let subtyp = subtyp TypeVarMap.empty
let sub_contract = sub_contract TypeVarMap.empty
let subtyp_typedef = subtyp_typedef TypeVarMap.empty

let typ_arguments typ =
  let rec __args acc = function
      TForall (v, t) -> __args (v :: acc) t
    | _ -> List.rev acc
  in
  __args [] typ

let rec return_type t =
  match t with
    TArrow (_, t) | TForall (_, t) -> return_type t
  | _ -> t

let type_empty_list, tvl =
  let tv = {tv_name =  "@'l"; tv_traits = default_trait} in
  TForall (tv, list (TVar tv)), tv

let type_empty_set,tvset =
  let tv = {tv_name =  "@'s"; tv_traits = default_trait} in
  TForall (tv, set (TVar tv)), tv

let type_empty_map, tvmkey, tvmbnd =
  let tva = {tv_name =  "@'k"; tv_traits = comparable} in
  let tvb = {tv_name =  "@'b"; tv_traits = default_trait} in
  TForall (tva, TForall (tvb, map (TVar tva) (TVar tvb))), tva, tvb

let type_empty_bigmap, tvbmkey, tvbmbnd =
  let tva = {tv_name =  "@'bk"; tv_traits = comparable} in
  let tvb = {tv_name =  "@'bb"; tv_traits = default_trait} in
  TForall (tva, TForall (tvb, bigmap (TVar tva) (TVar tvb))), tva, tvb

(** Returning the corresponding types in t2 that are polymorphic in t1.
    The argument aliases corresponds to the type aliases (such as type t = int). *)
let search_aliases
    (user_comp : user_comp)
    aliases
    t1
    t2 =
  let rec search_in_lists acc l1 l2 =
    match l1,l2 with
      [],[] -> acc
    | hd1::tl1, hd2::tl2 ->
      search_in_lists (search acc hd1 hd2) tl1 tl2
    | _,_ -> raise (BadArgument "Types are different")

  and search_content acc content1 content2 =
    match content1, content2 with
      SType st1, SType st2 -> (
        match st1, st2 with
          (SPublic td1 | SPrivate td1), (SPublic td2 | SPrivate td2) ->
          search_in_typedef acc td1 td2
        | SAbstract _, SAbstract _ -> acc
        | (SPublic _ | SPrivate _ | SAbstract _),
          (SPublic _ | SPrivate _ | SAbstract _) ->
          raise (BadArgument "Abstract and public/private types are incomparable")
      )
    | SException e1, SException e2 ->
      search_in_lists acc e1 e2
    | SInit t1, SInit t2 -> search acc t1 t2
    | SEntry t1, SEntry t2
    | SValue t1, SValue t2 -> search acc t1 t2
    | SView (t1, t2), SView (t1', t2') -> search (search acc t1 t1') t2 t2'
    | SStructure (Anonymous a1), SStructure (Anonymous a2)
    | SSignature a1, SSignature a2 -> search_structures acc a1 a2
    | SStructure (Named _), SStructure (Named _) -> acc
    | (SType _ | SException _ | SInit _ | SEntry _ | SView _ | SValue _ | SStructure _ | SSignature _),
      (SType _ | SException _ | SInit _ | SEntry _ | SView _ | SValue _ | SStructure _ | SSignature _)
      -> raise (BadArgument "Signature content are incompatible")

  and search_structures acc c1 c2 =
    let rec cross_content acc l1 l2 =
      match l1, l2 with
        [], _ -> acc
      | (name1,content1) :: l1, (name2,content2) :: l2 when String.equal name1 name2 ->
        let acc = search_content acc content1 content2 in
        cross_content acc l1 l2
      | _, _ :: l2 -> cross_content acc l1 l2
      | _, [] -> raise (BadArgument "Cannot find all aliases : missing fields in signature")
    in cross_content acc c1.sig_content c2.sig_content

  and search_in_typedef acc td1 td2 =
    let map_params =
      try
        List.fold_left2
          (fun (orig_bindings, fake_bindings) p1 p2 ->
             (TypeVarMap.add p1 (TypeVarMap.find_opt p1 acc) orig_bindings),
             (TypeVarMap.add p1 (TVar p2) fake_bindings)
          )
          (TypeVarMap.empty, acc)
      with
        Invalid_argument s -> raise (BadArgument s)
    in
    let replace_map =
      TypeVarMap.fold
        (fun param topt new_acc ->
           match topt with
             None -> TypeVarMap.remove param new_acc
           | Some t -> TypeVarMap.add param t new_acc
        )
    in
    match td1, td2 with
      Alias {aparams = p1; atype = t1}, Alias {aparams = p2; atype = t2} ->
      let orig, fake = map_params p1 p2 in
      let acc = search fake t1 t2 in
      replace_map orig acc
    | SumType {sparams = p1; scons = s1; _}, SumType {sparams = p2; scons = s2; _} ->
      let orig, fake = map_params p1 p2 in
      let acc =
        try
          List.fold_left2
            (fun acc (_, tl1) (_, tl2) -> List.fold_left2 search acc tl1 tl2)
            fake
            s1
            s2
        with
          Invalid_argument s -> raise (BadArgument s) in
      replace_map orig acc
    | RecordType {rparams = p1; rfields = s1; _}, RecordType {rparams = p2; rfields = s2; _} ->
      let orig, fake = map_params p1 p2 in
      let acc =
        try
          List.fold_left2
            (fun acc (_,t1) (_, t2) -> search acc t1 t2)
            fake
            s1
            s2
        with
          Invalid_argument s -> raise (BadArgument s) in
      replace_map orig acc
    | (Alias _ | SumType _ | RecordType _), (Alias _ | SumType _ | RecordType _) ->
      raise (BadArgument "Incompatible type def for alias search")

  and search acc t1 t2 =
    Log.debug "[search_aliases] Matching %a with %a@." pretty t1 pretty t2;
    match t1, t2 with
      TVar tv, _ -> (
        let add_if_ok () =
          if tv.tv_traits.tcomparable && not(isComparable user_comp t2)
          then raise (
              BadArgument
                (Format.asprintf "Cannot bind %a to %a"  pp_typvar tv pretty t2))
          else (
            Log.debug "[search_aliases] Binding %a to %a@."pretty t1 pretty t2;
            TypeVarMap.add tv t2 acc
          )
        in
      match TypeVarMap.find_opt tv acc with
        None -> add_if_ok ()
      | Some t ->(
          match equal t t2 with
            Aliases _ -> add_if_ok ()
          | _ -> (
              Log.debug
                "[search_aliases] Error : %a is already matched to %a,\
                 cannot be matched to %a."
                pp_typvar tv pretty t pretty t2;
              raise (BadArgument "Search alias error")
            )
        )
      )
    | _, TVar _ ->
      Log.debug "[search_aliases] Trying to merge %a into %a : error@.."
        pretty t1 pretty t2;
      raise (BadArgument "Search : genericity error.")

    | TTuple l1, TTuple l2 ->
      Log.debug "[search_aliases] Tuples@.";
      search_in_lists acc l1 l2

    | TUser (n1, l1),TUser (n2, l2)  ->
      Log.debug "[search_aliases] User defined type@.";
      if Ident.equal String.equal n1 n2
      then search_in_lists acc l1 l2
      else (
        Log.debug "[search_aliases] Different id types : %a <> %a@."
          pretty_typename n1 pretty_typename n2;
        raise (BadArgument "Different user type name")
      )
    | TUser (tn, p), t -> (
        try
          match aliases tn with
            Alias {atype; aparams} ->
            let p =
              List.fold_left2
                (fun acc tv t -> TypeVarMap.add tv t acc)
                TypeVarMap.empty
                aparams
                p
            in
            search acc (replace_map user_comp p atype) t
          | _ -> raise (BadArgument "Cannot search between non alias tuser and type.")
        with Invalid_argument _ -> raise (BadArgument "Bad number of type arguments")
      )
    | t, TUser (tn, p) -> (
        try
          match aliases tn with
            Alias {atype; aparams} ->
            let p =
              List.fold_left2
                (fun acc tv t -> TypeVarMap.add tv t acc)
                TypeVarMap.empty
                aparams
                p
            in
            search acc t (replace_map user_comp p atype)
          | _ -> raise (BadArgument "Cannot search between type and non alias tuser.")
        with Invalid_argument _ -> raise (BadArgument "Bad number of type arguments")
      )

    | TArrow (t1, t2), TArrow(t1', t2') -> (
        Log.debug "[search_aliases] (Big)Map/Arrow@.";
        let acc = search acc t1 t1' in
        search acc t2 t2'
      )
    | TArrow (t1, _), t -> search acc t1 t
    | t, TArrow (t1, _) -> search acc t t1
    | TContractInstance (Named  _n1), TContractInstance (Named _n2)
    | TPackedStructure (Named _n1), TPackedStructure (Named _n2) ->
      Log.debug "[search_aliases] Aliases between named contracts cannot be found";
      acc
    | TContractInstance (Anonymous c1), TContractInstance (Anonymous c2)
    | TPackedStructure (Anonymous c1), TPackedStructure (Anonymous c2) ->
      search_structures acc c1 c2
    | TContractInstance (Named n), TContractInstance (Anonymous _c)
    | TPackedStructure (Named n), TPackedStructure (Anonymous _c) -> (
        Log.debug "[search_aliases] Aliases search : Named VS anonymous";
        match Ident.split n with
          "UnitContract", None -> acc
        | _ ->
          raise (BadArgument "Searching aliases between named and anonymous contracts")
      )
    | TContractInstance (Anonymous _c), TContractInstance (Named n)
    | TPackedStructure (Anonymous _c), TPackedStructure (Named n) -> (
        Log.debug "[search_aliases] Aliases search : Anonymous VS Named";
        match Ident.split n with
          "UnitContract", None -> acc
        | _ ->
          raise (BadArgument "Searching aliases betweenanonymous and named contracts")
      )
    | TForall (arg, t), TForall (_arg', t') -> (
        Log.debug "[search_aliases] Foralls@.";
        let old_binding = TypeVarMap.find_opt arg acc in
        let acc = search acc t t' in
        match old_binding with
          None -> TypeVarMap.remove arg acc
        | Some t -> TypeVarMap.add arg t acc
      )

    | ( TContractInstance _ | TPackedStructure _
       | TTuple _ | TForall _ ),
      ( TContractInstance _ | TPackedStructure _
       | TTuple _ | TForall _ ) ->
      Log.debug "[search_aliases] Types %a and %a are different@."
        pretty t1 pretty t2;
      raise (
        BadArgument
          "Error : trying to find aliases between different types.")
  in
  search TypeVarMap.empty t1 t2

(*let entryPointTypeToTypes =
  function
  | TArrow (tstorage1, TArrow (tdun, TArrow (tparam, treturn))) ->
    if bool_equal tdun TDun
    then begin match treturn with
         | TTuple [toperations; tstorage2 ] ->
            if bool_equal toperations (TList TOperation) &&
                 bool_equal tstorage1 tstorage2
            then Some (tparam, treturn)
            else None
         | _ -> None
         end
    else None
 | _ -> None*)

let type_arity =
  let rec arity acc t =
    match t with
      TArrow (_, t) -> arity (acc + 1) t
    | _ -> acc
  in
  arity 0

let compare_traits t1 t2 = Compare.Bool.compare t1.tcomparable t2.tcomparable

let compare_tvar tv1 tv2 =
  let nc = String.compare tv1.tv_name tv2.tv_name in
  if Compare.Int.equal nc 0
  then compare_traits tv1.tv_traits tv2.tv_traits
  else nc

let tid = function
  | TTuple _ -> 0
  | TUser _ -> 1
  | TPackedStructure _ -> 2
  | TContractInstance _ -> 3
  | TArrow _ -> 4
  | TVar _ -> 5
  | TForall _ -> 6

let scid = function
  | SType _ -> 0
  | SException _ -> 1
  | SInit _ -> 2
  | SValue _ -> 2
  | SView _ -> 3
  | SEntry _ -> 4
  | SStructure _ -> 5
  | SSignature _ -> 6

let compare_rec r1 r2 =
  match r1, r2 with
  | Rec, Rec | NonRec, NonRec -> 0
  | Rec, NonRec -> 1
  | NonRec, Rec -> -1

let rec compare t1 t2 =
    match t1, t2 with
    | TVar v1, TVar v2 -> compare_tvar v1 v2

    | TArrow (tk1,tb1), TArrow (tk2, tb2) ->
      let kc = compare tk1 tk2 in
      if Compare.Int.equal kc 0 then compare tb1 tb2
      else kc

    | TContractInstance c1, TContractInstance c2
    | TPackedStructure c1, TPackedStructure c2 -> compare_struct_type c1 c2

    | TTuple l1, TTuple l2 -> compare_list compare l1 l2

    | TUser (n1,l1), TUser (n2, l2) ->
      Utils.compare_pair
        (Ident.compare String.compare)
        (compare_list compare)
        (n1, l1)
        (n2, l2)

    | TForall (v1, t1), TForall (v2, t2) ->
      Utils.compare_pair
        compare_tvar
        compare
        (v1, t1)
        (v2, t2)

    | _,_ -> Compare.Int.compare (tid t1) (tid t2)

and compare_struct_type (c1 : structure_type) (c2 : structure_type) : int =
  match c1, c2 with
    Named n1, Named n2 -> Ident.compare String.compare n1 n2
  | Anonymous a1, Anonymous a2 -> compare_sig_kind a1 a2
  | Named _, Anonymous _ -> 1
  | Anonymous _, Named _ -> -1

and compare_contract_type (c1 : contract_type) (c2 : contract_type) : int =
  match c1, c2 with
  | StructType st1, StructType st2 -> compare_struct_type st1 st2
  | ContractInstance cn1, ContractInstance cn2 ->
      Ident.compare String.compare cn1 cn2
  | StructType _, ContractInstance _ -> 1
  | ContractInstance _, StructType _ -> -1

and compare_sig_content c1 c2 =
  match c1, c2 with
    SType t1, SType t2 -> (
      match t1, t2 with
        SPublic td1, SPublic td2
      | SPrivate td1, SPrivate td2 ->
        compare_typedef td1 td2
      | SAbstract l1, SAbstract l2 ->
        compare_list compare_tvar l1 l2
      | SPublic _, _
      | SPrivate _, SAbstract _ -> 1
      | SAbstract _, _
      | SPrivate _, SPublic _ -> -1
    )
  | SException l1, SException l2 -> compare_list compare l1 l2
  | SEntry t1, SEntry t2
  | SValue t1, SValue t2 -> compare t1 t2

  | SView (tp1, tr1), SView (tp2, tr2) ->
    let pc = compare tp1 tp2 in
    if Compare.Int.equal pc 0 then compare tr1 tr2 else pc
  | SStructure s1, SStructure s2 -> compare_struct_type s1 s2
  | SSignature s1, SSignature s2 -> compare_sig_kind s1 s2
  | _ -> Compare.Int.compare (scid c1) (scid c2)

and compare_struct_kind k1 k2 =
  match k1, k2 with
    Module, Module -> 0
  | Contract d1, Contract d2 ->
    compare_list (Utils.compare_pair String.compare String.compare) d1 d2
  | Module, Contract _ -> 1
  | Contract _, Module -> -1

and compare_sig_kind s1 s2 =
  let kc = compare_struct_kind s1.sig_kind s2.sig_kind in
  if Compare.Int.equal (compare_sig_contents s1.sig_content s2.sig_content) 0
  then compare_sig_contents s1.sig_content s2.sig_content
  else kc

and compare_sig_contents l1 l2 =
  compare_list (Utils.compare_pair String.compare compare_sig_content) l1 l2

and compare_typedef t1 t2 =
  match t1, t2 with
    Alias {aparams = ap1; atype = at1},
    Alias {aparams = ap2; atype = at2} ->
    Utils.compare_pair (compare_list compare_tvar) compare (ap1, at1) (ap2, at2)
  | SumType {sparams = sp1; scons = sc1; srec = sr1},
    SumType {sparams = sp2; scons = sc2; srec = sr2} ->
    Utils.(
      compare_pair
        (compare_list compare_tvar)
        (compare_pair
           (compare_list @@ compare_pair String.compare (compare_list compare))
           (compare_rec
           )
        )
        (sp1, (sc1, sr1))
        (sp2, (sc2, sr2))
    )

  | RecordType {rparams = rp1; rfields = rc1; rrec = rr1},
    RecordType {rparams = rp2; rfields = rc2; rrec = rr2} ->
    Utils.(
      compare_pair
        (compare_list compare_tvar)
        (compare_pair
           (compare_list @@ compare_pair String.compare compare)
           (compare_rec
           )
        )
        (rp1, (rc1, rr1))
        (rp2, (rc2, rr2))
    )

  | Alias _, _
  | SumType _, RecordType _ -> 1

  | RecordType _, _
  | SumType _, Alias _ -> -1
