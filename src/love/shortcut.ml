

module Signature = struct

  module Public_key = struct
    type t = string
    let compare = String.compare
    let encoding = (Binary_writer.pk, Binary_reader.pk)
    let pp ppf t = Format.pp_print_string ppf t
    let of_b58check_opt s = Some s
    let to_b58check s = s
  end

  module Public_key_hash = struct
    type t = string
    let compare = String.compare
    let encoding = (Binary_writer.pkh, Binary_reader.pkh)
    let pp ppf t = Format.pp_print_string ppf t
    let of_b58check_opt s = Some s
    let to_b58check s = s
  end

  type t = string
  let compare = String.compare
  let encoding = (Binary_writer.signature, Binary_reader.signature)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = Some s
  let to_b58check s = s
end

module Contract_repr = struct
  type t = string
  let compare = String.compare
  let encoding = (Binary_writer.contract, Binary_reader.contract)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = Some s
  let of_b58check s = Ok s
  let to_b58check s = s
end

module Script_expr_hash = struct
  type t = string
  let compare = String.compare
  let encoding = (Binary_writer.script_expr_hash, Binary_reader.script_expr_hash)
  let pp ppf t = Format.pp_print_string ppf t
  let of_b58check_opt s = try Some s with _ -> None
  let to_b58check s = s
end

module Script_timestamp_repr = struct
  type t = Z.t
  let compare = Z.compare
  let to_zint x = x
  let of_zint x = x
  let to_string t = Z.to_string t
  let of_string t = try Some (Z.of_string t) with _ -> None
end

module Tez_repr = struct
  type t = int64
  let compare = Int64.compare
  let to_mutez t = t
  let of_mutez t = Some t
  let to_string t = Int64.to_string t
  let of_string t = Int64.of_string_opt t
end

module Data_encoding = struct
  module Binary = struct
    let to_bytes encoding x =
      try Some ((fst encoding) x) with _ -> None
    let of_bytes encoding b =
      let state = Binary_reader.{ b; offset = 0} in
      try Some (fst ((snd encoding) state)) with _ -> None
  end
end
