(**************************************************************************)
(*                             Dune Network                               *)
(*                                                                        *)
(*  Copyright 2019 Origin-Labs                                            *)
(*                                                                        *)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)

open Love_pervasives
open Exceptions
open Collections

type primitive_kind =
  | PrimFunction
  | PrimInfix
  | PrimPrefix

type extended_arg_def =
  | ADType
  | ADContractType

type extended_arg =
  | AType of Love_type.t
  | AContractType of Love_type.contract_type

type generic_primitive = {
  prim_name : string ;
  prim_kind : primitive_kind ;
  prim_ext_args : extended_arg_def list ;
  prim_type :
    sig_of_contract:(string Ident.t -> Love_type.structure_sig) ->
    extended_arg list -> Love_type.t ;
  prim_arity : int ;
  prim_id : int ;
}

type t = generic_primitive

let init_prim_id = -1
let max_possible_prim_id = 191 (* should consider another encoding above this *)
(* generic primitive by name. Used both for names and JSON encoding! *)
let generic_primitives_by_name = ref StringMap.empty
let prim_ids = Array.make (max_possible_prim_id + 1) None
let prim_arities = Array.make (max_possible_prim_id + 1) None
let max_prim_id = ref init_prim_id

let reset () =
  max_prim_id := init_prim_id;
  generic_primitives_by_name := StringMap.empty;
  for i = 0 to max_possible_prim_id do prim_ids.(i) <- None done;
  for i = 0 to max_possible_prim_id do prim_arities.(i) <- None done

let string_of_kind = function
  | PrimFunction -> "function"
  | PrimPrefix -> "prefix"
  | PrimInfix -> "infix"

let same_kind k1 k2 = match k1, k2 with
  | PrimFunction, PrimFunction
  | PrimPrefix, PrimPrefix
  | PrimInfix, PrimInfix -> true
  | _ -> false

let from_string prim =
  StringMap.find_opt prim !generic_primitives_by_name

let check_generic_primitive kind prim =
  match StringMap.find_opt prim !generic_primitives_by_name with
  | Some p ->
      if not (same_kind p.prim_kind kind) then
        raise (GenericError (
            Format.asprintf "Primitive %S: %s, but found as %s" prim
              (string_of_kind p.prim_kind) (string_of_kind kind)));
      p
  | None ->
      raise (GenericError (Format.asprintf "Unknown primitive %s" prim))

let of_string prim = check_generic_primitive PrimFunction prim

let infix_of_string prim = check_generic_primitive PrimInfix prim

let prefix_of_string prim = check_generic_primitive PrimPrefix prim

let is_infix p = same_kind p.prim_kind PrimInfix

let is_prefix p = same_kind p.prim_kind PrimPrefix

let type_of ?(sig_of_contract=(fun _ -> Love_type.unit_contract_sig)) (p, xl) =
  p.prim_type ~sig_of_contract xl

let name p = p.prim_name

let id_of_prim p = p.prim_id

let arity p = p.prim_arity

let compare p1 p2 = Compare.Int.compare (id_of_prim p1) (id_of_prim p2)

let prim_of_id id =
  if Compare.Int.(id > max_possible_prim_id) then
    raise (GenericError ("Invalid primitive id " ^ (string_of_int id))) ;
  match prim_ids.(id) with
  | Some a -> a
  | None -> raise (GenericError ("Unknown primitive id " ^ (string_of_int id)))

let add_primitive p =

  if Compare.Int.(p.prim_id < 0) then
    raise (GenericError (Format.asprintf "prim_id %d is below 0" p.prim_id));

  if Compare.Int.(p.prim_id > max_possible_prim_id) then
    raise (GenericError (
        Format.asprintf "prim_id %d is above max_possible_prim_id %d"
          p.prim_id max_possible_prim_id ));

  begin
    match prim_ids.(p.prim_id) with
    | None ->
        prim_ids.(p.prim_id) <- Some p;
        prim_arities.(p.prim_id) <- Some p.prim_arity ;
    | Some _ ->
        raise (GenericError (
          Format.asprintf "prim_id %d used twice" p.prim_id ));
  end;

  begin
    match from_string p.prim_name with
    | None ->
        generic_primitives_by_name :=
          StringMap.add p.prim_name p !generic_primitives_by_name
    | Some _ ->
        raise (GenericError (
          Format.asprintf "prim_name %S used twice" p.prim_name ));
  end ;
  let unit = Love_type_list.get_type "unit" [] in
  let xl = match p.prim_ext_args with
    | [] -> []
    | [ADType] -> [AType unit]
    | [ADType; ADType] -> [AType unit;AType unit]
    | [ADContractType] ->
        [AContractType (Love_type.StructType Love_type.unit_contract_anon_type)]
    | _ -> raise (GenericError ("Unexpected extended arg pattern"))
  in

  let dummy_sig_of_contract = (fun _ -> Love_type.unit_contract_sig) in
  let computed_arity =
    p.prim_type ~sig_of_contract:dummy_sig_of_contract xl |>
    Love_type.remove_foralls |> Love_type.type_arity in

  if Compare.Int.(p.prim_arity <> computed_arity) then
    raise (GenericError (
        Format.asprintf "Bad arity %d for primitive %S: computed %d"
          p.prim_arity p.prim_name computed_arity)) ;

  if Compare.Int.(p.prim_id > !max_prim_id) then
    max_prim_id := p.prim_id


let get_primitives_as_modules () =

  let root_module = ref [] in
  let modules = ref StringMap.empty in

  for id = 0 to !max_prim_id do
    let p = match prim_ids.(id) with
      | Some p -> p
      | None ->
          raise (GenericError ("Unknown primitive id " ^ (string_of_int id)))
    in
    let sl = String.split_on_char '.' p.prim_name in
    match sl with
    | [ prim ] ->
      root_module := (prim, p) :: !root_module
    | [ mod_name; prim ] ->
      let m = match StringMap.find_opt mod_name !modules with
        | Some m -> m
        | None -> []
      in
      modules := StringMap.add mod_name ((prim, p) :: m) !modules
    | [] ->
        raise (GenericError ("Empty primitive name"))
    | _ ->
        raise (GenericError ("Primitives have at most one module prefix"))
  done;

  !root_module, StringMap.bindings !modules


let compare_ext_arg x1 x2 =
  match x1, x2 with
  | AType t1, AType t2 -> Love_type.compare t1 t2
  | AContractType ct1, AContractType ct2 ->
     Love_type.compare_contract_type ct1 ct2
  | AType _, _ -> -1
  | _, AType _ -> 1
