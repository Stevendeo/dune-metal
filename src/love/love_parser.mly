(**************************************************************************)
(*  This program is free software: you can redistribute it and/or modify  *)
(*  it under the terms of the GNU General Public License as published by  *)
(*  the Free Software Foundation, either version 3 of the License, or     *)
(*  any later version.                                                    *)
(*                                                                        *)
(*  This program is distributed in the hope that it will be useful,       *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of        *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *)
(*  GNU General Public License for more details.                          *)
(*                                                                        *)
(*  You should have received a copy of the GNU General Public License     *)
(*  along with this program.  If not, see <https://www.gnu.org/licenses/>.*)
(**************************************************************************)
%{

open Love_pervasives
open Exceptions
open Love_type
open Love_ast
open Love_parser_utils
open Location
open Utils
open Love_ast_utils
open Love_value
open Value
open Shortcut

type sig_item =
  | SigType of string * Love_type.sig_type
  | SigException of string * Love_type.t list
  | SigInit of string * Love_type.t
  | SigEntry of string * Love_type.t
  | SigView of string * Love_type.t * Love_type.t
  | SigValue of string * Love_type.t
  | SigStructType of string * Love_type.structure_sig
  | SigStruct of string * Love_type.structure_type

let storage_type = TUser (Ident.create_id "storage", [])

let mkinfixi ?loc e1 (p, t) e2 =
  mk_apply ?loc (mk_tapply ?loc (mk_var ?loc (Ident.create_id p)) t) [e1; e2]

let type_of_exp e =
  match e.content with
  | Const {content = c; _} ->
     begin match c with
     | CUnit -> Some (Love_type_list.get_type "unit" [])
     | CBool _ -> Some (Love_type_list.get_type "bool" [])
     | CString _ -> Some (Love_type_list.get_type "string" [])
     | CBytes _ -> Some (Love_type_list.get_type "bytes" [])
     | CInt _ -> Some (Love_type_list.get_type "int" [])
     | CNat _ -> Some (Love_type_list.get_type "nat" [])
     | CDun _ -> Some (Love_type_list.get_type "dun" [])
     | CKey _ -> Some (Love_type_list.get_type "key" [])
     | CKeyHash _ -> Some (Love_type_list.get_type "keyhash" [])
     | CSignature _ -> Some (Love_type_list.get_type "signature" [])
     | CTimestamp _ -> Some (Love_type_list.get_type "timestamp" [])
     | CAddress _ -> Some (Love_type_list.get_type "address" [])
     end
  | _ -> None

let mkinfix ?loc e1 p e2 =
  match p with
  | "|>" ->
     begin match e2.content with
     | Apply { fct; args } ->
        { e2 with content = Apply { fct; args = args @ [e1] } }
     | _ -> { e2 with content = Apply { fct = e2; args = [e1] } }
     end
  | _ ->
     let var = mk_var ?loc (Ident.create_id p) in
     let e = match p with
       | "=" | "<>" | "<" | "<=" | ">" | ">=" ->
          begin match type_of_exp e1 with
          | Some t -> mk_tapply ?loc var t
          | None ->
             match type_of_exp e2 with
             | Some t -> mk_tapply ?loc var t
             | None -> var
          end
       | _ -> var
     in
     mk_apply ?loc e [e1; e2]

let mkprefix ?loc p e =
  mk_apply ?loc (mk_var ?loc (Ident.create_id p)) [e]

let var_or_prim ?loc v x =
  let open Love_primitive in
  let xl = match x with
    | `Type t -> [AType t]
    | `TypePair (t1, t2) -> [AType t1; AType t2]
    | `StructType st -> [AContractType (StructType st)]
    | `ContractInstance id -> [AContractType (ContractInstance id)]
    | `None -> []
    | _ -> raise (ParseError ("Bad extended argument"))
  in
  match xl with
  | [] -> mk_var ?loc v
  | _ -> mk_var_with_args ?loc v xl

let preprocess_dun s =
  let b = Buffer.create 10 in
  let len = String.length s in
  for i = 0 to len - 1 do
    match s.[i] with
    | '_' -> ()
    | c -> Buffer.add_char b c
  done;
  Buffer.contents b

let dun_of_string s =
  let s = preprocess_dun s in
  let s' = try
      let pos = String.index s '.' in
      let len = String.length s in
      let duns = String.sub s 0 pos in
      let mudun = String.sub s (pos+1) (len - pos - 1) in
      let mudun_len = String.length mudun in
      let mudun = match mudun_len with
        | 0 -> "000000"
        | l when l <= 6 ->
            String.init 6 (fun i -> if i < l then mudun.[i] else '0')
        | _ -> invalid_arg "bad mudun in dun_of_string"
      in
      duns ^ "." ^ mudun
    with Not_found ->
      s ^ ".000000"
  in
  Tez_repr.of_string s'

let bytes_of_string s =
  if String.length s < 2 then invalid_arg "bad hex string in bytes_of_string"
  else (MBytes.of_hex (`Hex (String.sub s 2 (String.length s - 2))))

let typ = Love_type.typ_by_str_args

let unique_tvar tv =
  let new_name = "@"^ tv.tv_name in
  tv, { tv with tv_name = new_name }

let unique_tvars_in_tdef td =
  let param_list_rev, tvarmap =
    match td with
     | Alias { aparams = p; _ }
     | RecordType { rparams = p; _ }
     | SumType { sparams = p; _ } ->
	List.fold_left (fun (acc_p, acc_map) p ->
          let k, b = unique_tvar p in
	  (b :: acc_p, Love_type.TypeVarMap.add k (TVar b) acc_map)
	) ([], Love_type.TypeVarMap.empty) p
  in
  let param_list = List.rev param_list_rev in
  match td with
  | SumType { scons; srec; _ } ->
      let scons = List.map (fun (cstr,l) ->
        cstr, (List.map (replace_map (fun _ -> true) tvarmap) l)) scons in
      SumType { sparams = param_list; scons; srec }
  | RecordType { rfields; rrec; _ } ->
      let rfields = List.map (fun (field,t) ->
        field, replace_map (fun _ -> true) tvarmap t) rfields in
      RecordType { rparams = param_list; rfields; rrec }
  | Alias { atype; _ } ->
      Alias { aparams = param_list;
              atype = replace_map (fun _ -> true) tvarmap atype }

let loc ((Lexing.{ pos_fname = _; pos_lnum; pos_bol = pb1; pos_cnum = cn1 },
    Lexing.{ pos_fname = _; pos_lnum = _; pos_bol = pb2; pos_cnum = cn2 }) :
    Lexing.position * Lexing.position ) : Love_ast.location =
  { pos_lnum; pos_bol = pb2 - pb1; pos_cnum = cn2 - cn1 }

let parse_loc loc = {loc_start = loc; loc_end = loc; loc_ghost = false}

let unclosed opening_name opening_loc closing_name closing_loc =
  raise (Syntaxerr.Error
          (Syntaxerr.Unclosed
             (parse_loc opening_loc, opening_name,
              parse_loc closing_loc, closing_name)))

let syntax_error () =
  raise Syntaxerr.Escape_error

let expecting pos nonterm =
  raise Syntaxerr.(Error (Expecting (rhs_loc pos, nonterm)))

let _not_expecting pos nonterm =
  raise Syntaxerr.(Error (Not_expecting (rhs_loc pos, nonterm)))

let parse_error (loc_st, loc_en) s =
  raise (Exceptions.ParseError (
	   Format.asprintf "Line %i-%i : %s"
	     loc_st.Lexing.pos_lnum loc_en.Lexing.pos_lnum s))

let pl_to_lambdas l e pl =
  List.fold_left (fun e -> function
    | `Var (_, None) -> parse_error l "Type of all arguments must be given"
    | `Var (p, Some t) -> mk_lambda ~loc:(loc l) p e t
    | `TypeVar tv -> mk_tlambda ~loc:(loc l) tv e
  ) e (List.rev pl)

let pl_to_lambdas_t l (e, t) pl =
  List.fold_left (fun (e, typ) -> function
    | `Var (_, None) -> parse_error l "Type of all arguments must be given"
    | `Var (p, Some t) -> mk_lambda ~loc:(loc l) p e t, TArrow (t, typ)
    | `TypeVar tv -> mk_tlambda ~loc:(loc l) tv e, TForall (tv, typ)
  ) (e, t) (List.rev pl)

let top_val_or_entry l ea a r n pl rt e fc =
  match ea with
  | Some "entry" ->
     if r then parse_error l "Entry points can not be recursive";
     let (sn, st), (dn, dt), (pn, pt) = match pl with
       | `Var (sn, st) :: `Var (dn, dt) :: `Var (pn, pt) :: [] ->
           (sn, st), (dn, dt), (pn, pt)
       | _ -> parse_error l "Incorrect entry point definition"
     in
     let st = match st with Some t -> t | None -> storage_type in
     let dt =
       match dt with
	 Some t -> t
       | None -> Love_type_list.get_type "dun" []
     in
     let pt = match pt with Some t -> t
       | None -> parse_error l "Type of third argument must be given" in
     let () =
       match rt with
	 None -> ()
       | Some (TTuple [
		   TUser (Ident.LName "list", [TUser (Ident.LName "operation", [])]);
		   TUser (tn, _)
		 ]
	      ) ->
          begin match (Ident.get_list tn) with
          | ["storage"] -> ()
          | _ -> parse_error l "Second element of return type \
                                must be of type storage"
          end
       | Some _ -> (* could allow any type and let the typechecker check *)
          parse_error l "Return type must be an \
                         (operation list, storage) pair"
     in
     let code = mk_lambda ~loc:(loc l) sn
                  (mk_lambda ~loc:(loc l) dn
                    (mk_lambda ~loc:(loc l) pn e pt) dt) st in
     n, mk_entry code fc pt
  | Some "view" ->
     if r then parse_error l "Views can not be recursive";
     let (sn, st), (pn, pt) = match pl with
       | `Var (sn, st) :: `Var (pn, pt) :: [] -> (sn, st), (pn, pt)
       | _ -> parse_error l "Incorrect view definition"
     in
     let st = match st with Some t -> t | None -> storage_type in
     let pt = match pt with Some t -> t
       | None -> parse_error l "Type of second argument must be given" in
     let rt = match rt with Some t -> t
       | None -> parse_error l "Return type must be given" in
     let () = match fc with None -> ()
       | Some _ -> parse_error l "Fee code not allowed on views" in
     let code = mk_lambda ~loc:(loc l) sn
                  (mk_lambda ~loc:(loc l) pn e pt) st in
     n, mk_view code pt rt
  | Some "init" ->
     if r then parse_error l "Initializer can not be recursive";
     let (pn, pt) = match pl with
       | `Var (pn, pt) :: [] -> (pn, pt)
       | _ -> parse_error l "Incorrect storage initializer definition"
     in
     let pt = match pt with Some t -> t
       | None -> parse_error l "Type of argument must be given" in
     let () =
       match rt with
	 None -> ()
       | Some _ ->
	  parse_error l
	    "Init function always returns the storage type: \
	     type annotation is not necessary."
     in
     let n = match n with
       | "storage" -> Constants.init_storage
       | _ -> parse_error l "Storage initializer must be named storage"
     in
     let init_code = mk_lambda ~loc:(loc l) pn e pt in
     let init_typ = pt in
     let init_persist = match a with
       | Some "persist" -> true
       | _ -> false
     in
     n, Init { init_code; init_typ; init_persist }
  | Some "private" ->
     let t = match rt with Some t -> t
       | None -> parse_error l "Return type of top-level \
                                values must be specified"
     in
     let () = match fc with None -> ()
       | Some _ -> parse_error l "Fee code not allowed on functions"
     in
     let value_code, value_typ = pl_to_lambdas_t l (e, t) pl in
     let value_recursive = if r then Rec else NonRec in
     n, Value { value_code; value_typ;
                value_visibility = Private; value_recursive }
  | Some s ->
     parse_error l ("Unknown extension : " ^ s)
  | None ->
     let t = match rt with Some t -> t
       | None -> parse_error l "Return type of top-level \
                                values must be specified"
     in
     let () = match fc with None -> ()
       | Some _ -> parse_error l "Fee code not allowed on functions"
     in
     let value_code, value_typ = pl_to_lambdas_t l (e, t) pl in
     let value_recursive = if r then Rec else NonRec in
     n, Value { value_code; value_typ;
                value_visibility = Public; value_recursive }

%}

/* Identifiers */
%token <string> LIDENT
%token <string> UIDENT

/* Value-carrying Constants */
%token <string> CINT
%token <string> CNAT
%token <string> CSTRING
%token <string> CBYTES
%token <string> CDUN
%token <string> CKEY
%token <string> CKEYHASH
%token <string> CSIGNATURE
%token <string> CADDRESS
%token <string> CTIMESTAMP

/* Plain constants */
%token TRUE FALSE

/* Types */
%token EXCEPTION
%token STRUCT INSTANCE
%token FORALL DOT MINUSGREATER BAR
%token CONTRACT MODULE SIG VAL

/* Operators */
%token EQUAL LESS GREATER
%token PLUS PLUSPLUS MINUS MINUSPLUS STAR
%token BARBAR AMPERAMPER
/*%token LSL LSR*/
%token <string> INFIXOP0 /* != = < > | & $ |> */
%token <string> INFIXOP1 /* @ ^ */
%token <string> INFIXOP2 /* + - */
%token <string> INFIXOP3 /* * / % mod	(n)lor	(n)lxor	(n)land */
%token <string> INFIXOP4 /* ** (n)lsl	(n)lsr	asr */
%token <string> PREFIXOP /* ! ~ ? not	lnot */

/* Grouping */
%token LPAREN RPAREN
%token LBRACE RBRACE     /* { } */
%token LBRACKET RBRACKET /* [ ] */
%token LBRACKETCOLON LBRACKETAT LBRACKETPERCENT
%token COLON SEMI COMMA
%token BEGIN END

/* Expressions */
%token LET REC IN
%token FUN FUNCTION
%token IF THEN ELSE
%token MATCH TRY WITH
%token RAISE
%token COLONCOLON
%token LESSMINUS

/* Generic keywords */
%token UNDERSCORE TYPE OF AS
%token PUBLIC PRIVATE ABSTRACT INTERNAL
%token LESSCOLON LESSBANG
%token FAILURE FAILWITH
%token QUOTE
%token USE
%token NONREC

/* Unused OCaml tokens (reserved) */
%token BACKQUOTE TILDE QUESTION HASH AMPERSAND
%token DOTDOT BANG PLUSDOT PLUSEQ MINUSDOT
%token COLONEQUAL COLONGREATER SEMISEMI PERCENT
%token LBRACKETBAR LBRACKETGREATER LBRACELESS
%token BARRBRACKET GREATERRBRACE
%token LBRACKETATAT LBRACKETATATAT
%token LBRACKETPERCENTPERCENT
%token FOR WHILE DO DONE TO DOWNTO WHEN
%token CLASS INHERIT VIRTUAL METHOD NEW OBJECT INITIALIZER
%token INCLUDE OPEN EXTERNAL FUNCTOR CONSTRAINT
%token LAZY MUTABLE ASSERT AND OR
%token <string> DOTOP
%token <string> HASHOP
%token <string> LABEL
%token <string> OPTLABEL
%token <string> COMMENT

/* End tokens */
%token EOL EOF

/* Precedence and associativity */
/*%nonassoc IN*/
/*%nonassoc TYPE*/
%nonassoc below_SEMI
%nonassoc SEMI
/*%nonassoc LET*/
%nonassoc below_WITH
%nonassoc WITH
%nonassoc THEN
%nonassoc ELSE
%left AS
%left BAR
%nonassoc below_COMMA
%left COMMA
%nonassoc above_COMMA
%right MINUSGREATER
%right /*OR*/ BARBAR
%right /*AMPERSAND*/ AMPERAMPER
/*%nonassoc below_EQUAL*/
%left INFIXOP0 EQUAL LESS GREATER
%right INFIXOP1
%right COLONCOLON
%left INFIXOP2 PLUS PLUSPLUS MINUS MINUSPLUS
%left INFIXOP3 STAR
%right INFIXOP4
%nonassoc unary_op
%nonassoc const_cstr
/*%nonassoc below_DOT*/
%nonassoc DOT
%nonassoc BEGIN TRUE FALSE LBRACE LBRACKET LBRACKETCOLON LPAREN
          /*PREFIXOP*/ LIDENT UIDENT
          CSTRING CBYTES CINT CNAT CDUN CKEY CKEYHASH CSIGNATURE
          CTIMESTAMP CADDRESS /*UNDERSCORE*/

/* Start symbols */
%start <Love_ast.top_contract> top_contract
%start <Love_value.val_or_type> top_value
%start <unit> unused

%%

/* Start rules */

top_contract:
  | s = structure EOF { { version = Options.version; code = s } }

top_value:
  | v = value EOF            { Value v }
  | t = typ EOF              { Type t }

/* Utility rules */

reversed_separated_nontrivial_llist(separator, X):
  | xs = reversed_separated_nontrivial_llist(separator, X) separator x = X
    { x :: xs }
  | x1 = X separator x2 = X
    { [ x2; x1 ] }

%inline separated_nontrivial_llist(separator, X):
  | xs = rev(reversed_separated_nontrivial_llist(separator, X))
    { xs }

separated_or_terminated_nonempty_list(delimiter, X):
  | x = X ioption(delimiter)
    { [x] }
  | x = X delimiter xs = separated_or_terminated_nonempty_list(delimiter, X)
    { x :: xs }

%inline separated_or_terminated_list(delimiter, X):
  | /*epsilon*/
    { [] }
  | xs = separated_or_terminated_nonempty_list(delimiter, X)
    { xs }

reversed_preceded_or_separated_nonempty_llist(delimiter, X):
  | ioption(delimiter) x = X
    { [x] }
  | xs = reversed_preceded_or_separated_nonempty_llist(delimiter, X)
      delimiter x = X
    { x :: xs }

%inline preceded_or_separated_nonempty_llist(delimiter, X):
  | xs = rev(reversed_preceded_or_separated_nonempty_llist(delimiter, X))
    { xs }

/* Module paths */

path:
  | i = UIDENT       /* %prec below_DOT */ { [i] }
  | l = path DOT i = UIDENT                { i :: l }

%inline lident_path:
  | v = LIDENT
    { Ident.create_id v }
  | p = path DOT v = LIDENT
    { Ident.put_in_namespaces p (Ident.create_id v) }

%inline mident_path:
  | p = path
    { match p with
      | [] -> assert false
      | c :: p -> Ident.put_in_namespaces p (Ident.create_id c)}


/* Types */

typ:
  | t1 = typ MINUSGREATER t2 = typ
    { TArrow (t1, t2) }
  | tl = separated_nonempty_list(STAR, atomic_typ)
    { match tl with [t] -> t | _ -> TTuple tl }
  | FORALL tv = type_variable DOT t = typ   %prec below_COMMA
    { TForall (tv, t) }

atomic_typ:
  | tv = type_variable                 { TVar tv }
  | tn = lident_path                   { typ tn [] }
  | tl = typ_params tn = lident_path   { typ tn tl }
  | a = CADDRESS DOT tn = lident_path  { typ (Ident.put_in_namespace a tn) [] }
  | tl = typ_params a = CADDRESS DOT tn = lident_path
                                       { typ (Ident.put_in_namespace a tn) tl }
  | CONTRACT st = struct_type          { TPackedStructure st }
  | INSTANCE st = struct_type          { TContractInstance st }
  | LPAREN t = typ RPAREN              { t }
  | LPAREN _t = typ error              { unclosed "(" $startpos ")" $endpos }

/* Type parameters : must be atomic ; two-args inlined to avoid conflict */
typ_params:
  | t = atomic_typ                                      { [t] }
  | LPAREN t1 = typ COMMA t2 = typ RPAREN               { [t1; t2] }
  | LPAREN t1 = typ COMMA t2 = typ COMMA
      tl = separated_nonempty_list(COMMA, typ) RPAREN   { t1 :: t2 :: tl }
  | LPAREN _t1 = typ COMMA _t2 = typ error
      { unclosed "(" $startpos ")" $endpos }
  | LPAREN _t1 = typ COMMA _t2 = typ COMMA
      _tl = separated_nonempty_list(COMMA, typ) error
      { unclosed "(" $startpos ")" $endpos }

struct_type:
  | LPAREN st = struct_type RPAREN   { st }
  | sn = mident_path                 { Love_type.Named (sn) }
  | SIG s = signature END            { Love_type.Anonymous s }
  | SIG _s = signature error         { unclosed "sig" $startpos "end" $endpos }


/* Modules and contract structures */

use:
  | USE UIDENT EQUAL CADDRESS {($2,$4)}

structure:
  | dependencies = list(use) sil = list(struct_item)
    { let s = { structure_content = sil; kind = Contract dependencies } in
      let sc = List.fold_left (fun content (n, c) ->
          match c with
	  | Entry ({ entry_code =
                       { content = Lambda lambda; _ } as c; _ } as e) ->
             (n, Entry {
                e with entry_code = {
                  c with content =
                    Lambda { lambda with arg_typ = storage_type } }
             }) :: content
	  | View ({ view_code =
                       { content = Lambda lambda; _ } as c; _ } as v) ->
	     (n, View {
                v with view_code = {
                  c with content =
                    Lambda { lambda with arg_typ = storage_type } }
             }) :: content
	  | _ -> (n, c) :: content
	) [] s.structure_content
      in
      { s with structure_content = List.rev sc } }

struct_item:
  | td = typedef
    { let n, v, t = td in
      n, DefType (v, unique_tvars_in_tdef t) }
  | EXCEPTION e = construct_decl
    { fst e, DefException (snd e) }
  | VAL ea = ext_attribute r = rec_opt a = attrib_opt
      n = LIDENT pl = params rt = type_opt
      EQUAL fc = fee_code_opt e = seq_exp
    { top_val_or_entry ($startpos, $endpos) ea a r n pl rt e fc }
  | VAL ea = ext_attribute r = rec_opt a = attrib_opt
      LPAREN n = LIDENT COLON t = typ RPAREN
      EQUAL fc = fee_code_opt e = seq_exp
    { top_val_or_entry ($startpos, $endpos) ea a r n [] (Some t) e fc }
  | CONTRACT TYPE ctn = UIDENT EQUAL SIG s = signature END
    { ctn, Signature s }
  | CONTRACT TYPE _ctn = UIDENT EQUAL SIG _s = signature error
    { unclosed "sig" $startpos "end" $endpos }
  | CONTRACT cn = UIDENT EQUAL STRUCT s = structure END
    { cn, Structure s }
  | CONTRACT _cn = UIDENT EQUAL STRUCT _s = structure error
    { unclosed "struct" $startpos "end" $endpos }
  | MODULE TYPE mtn = UIDENT EQUAL SIG s = signature END
    { mtn, Signature { s with sig_kind = Module } }
  | MODULE TYPE _mtn = UIDENT EQUAL SIG _s = signature error
    { unclosed "sig" $startpos "end" $endpos }
  | MODULE mn = UIDENT EQUAL STRUCT s = structure END
    {
      match s.kind with
      | Module | Contract [] -> mn, Structure { s with kind = Module }
      | Contract _ ->
         parse_error ($startpos,$endpos)
                     "Module definitions cannot have dependencies"
    }
  | MODULE _mn = UIDENT EQUAL STRUCT _s = structure error
    { unclosed "struct" $startpos "end" $endpos }

params:
  | pl = list(param)   { pl }

param:
  | LPAREN p = atomic_pattern COLON t = typ RPAREN   { `Var (p, Some t) }
  | LPAREN _p = atomic_pattern COLON _t = typ error  { unclosed "(" $startpos ")" $endpos }
  | p = atomic_pattern                               { `Var (p, None) }
  | tv = type_variable                               { `TypeVar tv }

type_opt:
  | /* epsilon */    { None }
  | COLON t = typ    { Some t }

typedef:
  | ti = typedef_intro t = typ
    { let tvl, tn, v, rflag = ti in
      match rflag with
        | None | Some NonRec -> tn, v, Alias { aparams = tvl; atype = t }
        | Some Rec -> parse_error ($startpos,$endpos)
                                  "Recursive aliases are forbidden" }
  | ti = typedef_intro cl = constructor_declarations
    { let tvl, tn, v, srec = ti in
      let srec =
	match srec with
        | None | Some Rec -> Rec
        | Some NonRec -> NonRec in
      tn, v,
      SumType {
        sparams = tvl;
        scons = List.fold_left (fun acc (c, l) -> (c, l) :: acc) [] cl;
        srec
      } }
  | ti = typedef_intro LBRACE fl = field_defs RBRACE
    { let tvl, tn, v, rrec = ti in
      let rrec =
	match rrec with
        | None | Some Rec -> Rec
        | Some NonRec -> NonRec in
      tn, v, RecordType { rparams = tvl; rfields = fl; rrec } }
  | _ti = typedef_intro LBRACE _fl = field_defs error
    { unclosed "{" $startpos "}" $endpos  }

%inline nonrec_opt:
  | NONREC           { Some NonRec }
  | REC              { Some Rec }
  | /* epsilon */    { None }

typedef_intro:
  | TYPE r = nonrec_opt tvl = tvar_params_opt tn = LIDENT
      EQUAL v = visibility_opt
    { tvl, tn, v, r }

visibility_opt:
  | /* epsilon */   { TPublic }
  | PRIVATE         { TPrivate }
  | ABSTRACT        { TAbstract }
  | INTERNAL        { TInternal }

tvar_params_opt:
  | /* epsilon */                                                      { [] }
  | tv = type_variable                                                 { [tv] }
  | LPAREN tvl = separated_nonempty_list(COMMA, type_variable) RPAREN  { tvl }
  | LPAREN _tvl = separated_nonempty_list(COMMA, type_variable) error
    { unclosed "(" $startpos ")" $endpos }

trait:
  | t = UIDENT
    { match t with
      | "Comparable" -> `TComparable
      | _ ->  parse_error ($startpos,$endpos) ("Unknown trait : " ^ t) }

type_variable:
  | QUOTE tv = LIDENT
    { let tv_name = ("'" ^ tv) in
      { tv_name; tv_traits = Love_type.default_trait } }
  | LPAREN QUOTE tv = LIDENT
      LBRACKET tl = separated_list(COMMA, trait) RBRACKET RPAREN
    { let tv_name = ("'" ^ tv) in
      { tv_name;
	tv_traits =
	  List.fold_left
	    (fun _ -> function
		`TComparable -> {tcomparable = true}
	    )
	    Love_type.default_trait
	    tl }
    }

constructor_declarations:
  | BAR                                                    { [] }
  | c = construct_decl                                     { [c] }
  | BAR c = construct_decl                                 { [c] }
  | cl = constructor_declarations BAR c = construct_decl   { c :: cl }

construct_decl:
  | cn = UIDENT
      pl = option(OF tl = separated_nonempty_list(STAR, atomic_typ) { tl })
    { cn, match pl with None -> [] | Some tl -> tl  }

field_defs:
  | fd = field_def                         { [fd]  }
  | fd = field_def SEMI                    { [fd]  }
  | fd = field_def SEMI fdl = field_defs   { fd :: fdl  }

field_def:
  | fn = LIDENT COLON ft = typ             { fn, ft  }

ext_attribute:
  | /* empty */              { None }
  | PERCENT a = attr_id      { Some a }

attrib_opt:
  | /* epsilon */                       { None }
  | LBRACKETAT a = attr_id RBRACKET     { Some a }

rec_opt:
  | /* epsilon */    { false }
  | REC              { true }

fee_code_opt:
  | /* epsilon */                                      { None }
  | LBRACKETPERCENT a = attr_id e = seq_exp RBRACKET
    { if a = "fee" then Some e
      else  parse_error ($startpos,$endpos) ("Unknown extension : " ^ a) }


/* Modules and contract signatures */

signature:
  | deps = list(use) sil = list(sig_item)
    { let open Love_type in
      let s =
	List.fold_left (
	    fun s si ->
            match si with
            | SigType (n, td) ->
               { s with sig_content = (n, SType td) :: s.sig_content }
            | SigException (n, tl) ->
               { s with sig_content = (n, SException tl) :: s.sig_content }
            | SigEntry (n, t) ->
               { s with sig_content = (n,SEntry t) :: s.sig_content }
            | SigInit (n, t) ->
               { s with sig_content = (n,SInit t) :: s.sig_content }
            | SigView (n, t1, t2) ->
               { s with sig_content = (n,SView (t1, t2)) :: s.sig_content }
            | SigValue (n, t) ->
               { s with sig_content = (n,SValue t) :: s.sig_content }
            | SigStructType (n, ss) ->
               { s with sig_content = (n,SSignature ss) :: s.sig_content }
            | SigStruct (n, st) ->
               { s with sig_content = (n,SStructure st) :: s.sig_content }
	  )
	  { sig_content = [];
	    sig_kind = Contract deps }
	  sil
      in
      {s with sig_content = List.rev s.sig_content}
    }

sig_item:
  | td = typedef_sig
    { let n, t = td in SigType (n, t) }
  | EXCEPTION ed = construct_decl
    { SigException (fst ed, snd ed) }
  | VAL a = ext_attribute n = LIDENT COLON t = typ
    { match a with
      | None -> SigValue (n, t)
      | Some "entry" -> SigEntry (n, t)
      | Some "view" ->
         begin match t with
         | TArrow (t1, t2) -> SigView (n, t1, t2)
         | _ -> parse_error ($startpos, $endpos) "Invalid view type"
         end
      | Some "init" ->
         let n = match n with
           | "storage" -> Constants.init_storage
           | _ -> parse_error ($startpos, $endpos)
                              "Storage initializer must be named storage"
         in
         SigInit (n, t)
      | Some s -> parse_error ($startpos, $endpos)
                              ("Unknown extension : " ^ s) }
  | CONTRACT TYPE ctn = UIDENT EQUAL SIG s = signature END
    { SigStructType (ctn, s) }
  | CONTRACT TYPE _ctn = UIDENT EQUAL SIG _s = signature error
    { unclosed "sig" $startpos "end" $endpos }
  | MODULE TYPE mtn = UIDENT EQUAL SIG s = signature END
    {
      match s.sig_kind with
      | Module
      | Contract [] -> SigStructType (mtn, { s with sig_kind = Module })
      | Contract _ -> parse_error ($startpos, $endpos)
                                  "Module cannot have dependencies"
    }
  | MODULE TYPE _mtn = UIDENT EQUAL SIG _s = signature error
    { unclosed "sig" $startpos "end" $endpos }
  | CONTRACT cn = UIDENT COLON s = struct_type
    { SigStruct (cn, s) }
  | MODULE mn = UIDENT COLON s = struct_type
    {
      let s = match s with
        | Love_type.Anonymous s ->
           Love_type.Anonymous { s with sig_kind = Module }
        | _ -> s
      in
      SigStruct (mn, s) }

typedef_sig:
  | TYPE tvl = tvar_params_opt tn = LIDENT
    { tn, SAbstract tvl }
  | td = typedef
    { let n, k, t = td in
      let stdef =
	match k with
	| TPublic -> SPublic t
	| TPrivate -> SPrivate t
	| TAbstract -> parse_error ($startpos, $endpos)
            "Forbiden explicit definition of abstract types in signature"
	| TInternal -> parse_error ($startpos, $endpos)
            "Forbidden declaration of internal types in signature"
      in n, stdef
    }


/* Constants */

const:
  | LPAREN RPAREN    { mk_cunit ~loc:(loc ($startpos, $endpos)) () }
  | TRUE             { mk_cbool ~loc:(loc ($startpos, $endpos)) true }
  | FALSE            { mk_cbool ~loc:(loc ($startpos, $endpos)) false }
  | s = CSTRING      { mk_cstring ~loc:(loc ($startpos, $endpos)) s }
  | b = CBYTES       { mk_cbytes ~loc:(loc ($startpos, $endpos))
                         (bytes_of_string b) }
  | i = CINT         { mk_cint ~loc:(loc ($startpos, $endpos)) (Z.of_string i) }
  | i = CNAT         { mk_cnat ~loc:(loc ($startpos, $endpos)) (Z.of_string i) }
  | d = CDUN         { match dun_of_string d with
                       | Some d -> mk_cdun ~loc:(loc ($startpos, $endpos)) d
                       | None -> parse_error ($startpos, $endpos) "Bad dun" }
  | k = CKEY         { match Signature.Public_key.of_b58check_opt k with
                       | Some k -> mk_ckey ~loc:(loc ($startpos, $endpos)) k
                       | None -> parse_error ($startpos, $endpos) "Bad key" }
  | kh = CKEYHASH    { match Signature.Public_key_hash.of_b58check_opt kh with
                       | Some kh ->
                          mk_ckeyhash ~loc:(loc ($startpos, $endpos)) kh
                       | None -> parse_error ($startpos, $endpos) "Bad keyhash"}
  | s = CSIGNATURE   { match Signature.of_b58check_opt s with
                       | Some s -> mk_csig ~loc:(loc ($startpos, $endpos)) s
                       | None -> parse_error ($startpos, $endpos)
                                             "Bad signature" }
  | t = CTIMESTAMP   { match Script_timestamp_repr.of_string t with
                       | Some t ->
                          mk_ctimestamp ~loc:(loc ($startpos, $endpos)) t
                       | None -> parse_error ($startpos, $endpos)
                                             "Bad timestamp" }
  | a = CADDRESS     { match Contract_repr.of_b58check a with
                       | Ok a -> mk_caddress a
                       | Error _ -> parse_error ($startpos, $endpos)
                                                 "Bad contract" }


/* Patterns */

pattern:
  | p = solid_pattern            { p }
  | p = pattern_common           { p }

pattern_ni:
  | p = solid_pattern_ni         { p }
  | p = pattern_common           { p }

pattern_common:
  | p = pattern AS v = LIDENT
    { mk_palias ~loc:(loc ($startpos, $endpos)) p v }
  | _p = pattern AS error
    { expecting 3 "identifier" }
  | pl = pattern_list %prec below_COMMA
    { mk_ptuple ~loc:(loc ($startpos, $endpos)) pl }
  | p1 = pattern COLONCOLON p2 = pattern
    { match p2.content with
      | PList [] ->
          if has_revision 3 then
            { p2 with content = PList (p1 :: [p2]) }
          else
            { p2 with content = PList (p1 :: []) }
      | PList pl -> { p2 with content = PList (p1 :: pl) }
      | _ -> { p2 with content = PList [p1; p2] }
    }
  | _p1 = pattern COLONCOLON error
    { expecting 3 "pattern" }
  | CONTRACT id = UIDENT COLON st = struct_type
    { mk_pcontract ~loc:(loc ($startpos, $endpos)) id (StructType st) }
  | CONTRACT id = UIDENT COLON INSTANCE sn = mident_path
    { mk_pcontract ~loc:(loc ($startpos, $endpos)) id (ContractInstance sn) }

solid_pattern:
  | p = atomic_pattern           { p }
  | p = solid_pattern_common     { p }

solid_pattern_ni:
  | p = atomic_pattern_ni        { p }
  | p = solid_pattern_common     { p }

solid_pattern_common:
  | cstr = mident_path p = solid_pattern
    { match p.content with
      | PTuple pl -> {p with content = PConstr (Ident.get_final cstr, pl)}
      | _ -> {p with content = PConstr (Ident.get_final cstr, [p]) }
    }

atomic_pattern:
  | p = atomic_pattern_ni        { p }
  | v = LIDENT                   { mk_pvar v }

atomic_pattern_ni:
  | LPAREN p = pattern RPAREN    { p }
  | LPAREN _p = pattern error    { unclosed "(" $startpos ")" $endpos }
  | UNDERSCORE                   { mk_pany ~loc:(loc ($startpos, $endpos)) () }
  | c = const                    { mk_pconst ~loc:(loc ($startpos, $endpos)) c }
  | MINUS i = CINT
    { let loc = loc ($startpos, $endpos) in
      mk_pconst ~loc (mk_cint ~loc (Z.neg (Z.of_string i))) }
  | _op = uminus i = CNAT
    { let loc = loc ($startpos, $endpos) in
      mk_pconst ~loc (mk_cint ~loc (Z.neg (Z.of_string i))) }
  | PLUS i = CINT
    { let loc = loc ($startpos, $endpos) in
      mk_pconst ~loc (mk_cint ~loc (Z.of_string i)) }
  | _op = uplus i = CNAT
    { let loc = loc ($startpos, $endpos) in
      mk_pconst ~loc (mk_cnat ~loc (Z.of_string i)) }
  | LBRACKET RBRACKET
    { mk_plist ~loc:(loc ($startpos, $endpos)) [] }
  | LBRACKET pl = separated_nonempty_list(SEMI, pattern) RBRACKET
    { mk_plist ~loc:(loc ($startpos, $endpos))
        (pl @ [ mk_plist ~loc:(loc ($startpos, $endpos)) [] ]) }
  | LBRACKET error
    { unclosed "[" $startpos "]" $endpos }
  | cstr = mident_path /* %prec const_cstr */
    { mk_pconstr ~loc:(loc ($startpos, $endpos)) (Ident.get_final cstr) [] }


%inline pattern_list:
 | pl = separated_nontrivial_llist(COMMA, pattern)   { pl }


/* Expressions */

seq_exp:
  | e = exp        %prec below_SEMI   { e : exp }
  | e = exp SEMI                      { e }
  | e1 = exp SEMI e2 = seq_exp
    { match e1.content, e2.content with
      | Seq e1, Seq e2 -> mk_seq ~loc:(loc ($startpos, $endpos)) (e1 @ e2)
      | Seq e1, _ -> mk_seq ~loc:(loc ($startpos, $endpos)) (e1 @ [e2])
      | _, Seq e2 -> mk_seq ~loc:(loc ($startpos, $endpos))  (e1 :: e2)
      | _, _ -> mk_seq ~loc:(loc ($startpos, $endpos))[e1; e2] }

exp:
  | e = atomic_exp     /* %prec below_DOT */   { e }
  | LET p = pattern_ni EQUAL e1 = seq_exp IN e2 = seq_exp
    { mk_let ~loc:(loc ($startpos, $endpos)) p e1 e2 }
  | LET v = LIDENT pl = params EQUAL e1 = seq_exp IN e2 = seq_exp
    { let e1 = pl_to_lambdas ($startpos, $endpos) e1 pl in
      mk_let ~loc:(loc ($startpos, $endpos)) (mk_pvar v) e1 e2 }
  | LET REC v = LIDENT pl = params COLON t = typ
      EQUAL e1 = seq_exp IN e2 = seq_exp
    { if pl = [] then
        parse_error ($startpos, $endpos) (
                 "Recursive functions must have at least one argument");
      let e1, t = pl_to_lambdas_t ($startpos, $endpos) (e1, t) pl in
      mk_let_rec v e1 e2 t }
  | FUN pl = params MINUSGREATER e = seq_exp
    { pl_to_lambdas ($startpos, $endpos) e pl }
  | e = atomic_exp al = nonempty_list(atomic_exp_or_type)
    { let exp = List.fold_left (fun exp a ->
        match a, exp.Utils.content with
        | `Exp e, Apply { fct; args } ->
           mk_apply ~loc:(loc ($startpos, $endpos)) fct (e :: args)
        | `Exp e, _ ->
           mk_apply ~loc:(loc ($startpos, $endpos)) exp [e]
        | `Type t, Apply { fct; args } ->
           mk_tapply ~loc:(loc ($startpos, $endpos))
	     (mk_apply ~loc:(loc ($startpos, $endpos)) fct (List.rev args)) t
        | `Type t, List (el, {content = Nil; _}) ->
	   mk_list ~loc:(loc ($startpos, $endpos)) ~typ:t el
        | `Type t, _ ->
           mk_tapply ~loc:(loc ($startpos, $endpos)) exp t
       ) e al in
      match exp.content with
        | Apply { fct; args } ->
           { exp with content = Apply { fct; args = List.rev args } }
        | _ -> exp }
  | IF e1 = seq_exp THEN e2 = exp ELSE e3 = exp
    { mk_if ~loc:(loc ($startpos, $endpos)) e1 e2 e3 }
  | IF e1 = seq_exp THEN e2 = exp
    { mk_if ~loc:(loc ($startpos, $endpos)) e1 e2
            (mk_const ~loc:(loc ($startpos, $endpos)) @@ mk_cunit ())}
  | MATCH e = seq_exp WITH cl = match_cases
    { mk_match ~loc:(loc ($startpos, $endpos)) e cl}
  | FUNCTION LBRACKETCOLON t = typ RBRACKET cl = match_cases %prec below_WITH
    { let loc = loc ($startpos, $endpos) in
      let v = mk_var ~loc (Ident.create_id "@m") in
      let e = mk_match ~loc v cl in
      let pv = mk_pvar ~loc "@m" in
      mk_lambda ~loc pv e t }
  | cstr = mident_path a = atomic_exp_or_type tl = list(type_arg)
                                                         /* %prec below_DOT */
    {  match a with
        | `Exp { content = Tuple el; _ } ->
            mk_constr ~loc:(loc ($startpos, $endpos)) cstr tl el
        | `Exp e -> mk_constr ~loc:(loc ($startpos, $endpos)) cstr tl [e]
        | `Type t -> mk_constr ~loc:(loc ($startpos, $endpos)) cstr (t :: tl) []
    }
  | e1 = exp COLONCOLON e2 = exp
    { match e2.content with
      | List (el, e) -> {e2 with content = List (e1 :: el, e)}
      | _ -> {e2 with content = List ([e1], e2) }
    }
  | el = exp_list  %prec below_COMMA
    { mk_tuple ~loc:(loc ($startpos, $endpos)) el }
  | t = atomic_exp LESSMINUS LPAREN ul = tuple_updates RPAREN
    { mk_update ~loc:(loc ($startpos, $endpos)) t ul }
  | RAISE e = exn
    { mk_raise ~loc:(loc ($startpos, $endpos)) (fst e) (snd e) }
  | RAISE e = exn LBRACKETCOLON t = typ RBRACKET
    { let e = mk_raise ~loc:(loc ($startpos, $endpos)) (fst e) (snd e) in
      mk_tapply ~loc:(loc ($startpos, $endpos)) e t }
  | FAILWITH LBRACKETCOLON t = typ RBRACKET e = atomic_exp
    { mk_raise ~loc:(loc ($startpos, $endpos)) (Fail t) [e] }
  | FAILWITH LBRACKETCOLON t1 = typ RBRACKET e = atomic_exp
      LBRACKETCOLON t2 = typ RBRACKET
    { let e = mk_raise ~loc:(loc ($startpos, $endpos)) (Fail t1) [e] in
      mk_tapply ~loc:(loc ($startpos, $endpos)) e t2 }
  | TRY e = seq_exp WITH cl = try_cases
    { mk_try_with ~loc:(loc ($startpos, $endpos)) e cl }
  | TRY _e = seq_exp WITH error
    { syntax_error() }
  | p = PREFIXOP e = atomic_exp
    { mkprefix p e }
  | LPAREN p = PREFIXOP RPAREN e = atomic_exp
    { mkprefix p e }
  | op = uminus e = exp %prec unary_op
    { match op, e.content with
      | "-", Const ({content = CInt i; annot}) ->
	{ e with content = Const (mk_cint ?loc:annot (Z.neg i)) }
      | ("-" | "-+"), Const ({content = CNat i; annot}) ->
	{ e with content = Const (mk_cint ?loc:annot (Z.neg i)) }
      | _ -> mkprefix ("~" ^ op) e }
  | op = uplus e = exp %prec unary_op
    { match op, e.content with
      | "+", Const ({content = CInt _; _}) -> e
      | ("+" | "++"), Const ({content = CNat _; _}) -> e
      | _ -> mkprefix ("~" ^ op) e }
  | i = infix
    { i }

exn:
  | exn = mident_path
    { Exception exn, [] }
  | LPAREN exn = mident_path RPAREN
    { Exception exn, [] }
  | LPAREN exn = mident_path e = atomic_exp RPAREN
    { Exception exn, [e] }

uminus:
  | MINUS         { "-" }
  | MINUSPLUS     { "-+" }

uplus:
  | PLUS          { "+" }
  | PLUSPLUS      { "++" }

infix:
  | e1 = exp p = infixop0 t = typ_opt e2 = exp   %prec INFIXOP0
    { match t with
      | Some t -> mkinfixi ~loc:(loc ($startpos, $endpos)) e1 (p, t) e2
      | None -> mkinfix ~loc:(loc ($startpos, $endpos)) e1 p e2 }
  | e1 = exp p = INFIXOP1 t = typ_opt e2 = exp   %prec INFIXOP1
    { match t with
      | Some t -> mkinfixi ~loc:(loc ($startpos, $endpos)) e1 (p, t) e2
      | None -> mkinfix ~loc:(loc ($startpos, $endpos)) e1 p e2 }
  | e1 = exp p = infixop2 e2 = exp               %prec INFIXOP2
    { mkinfix ~loc:(loc ($startpos, $endpos)) e1 p e2 }
  | e1 = exp p = infixop3 e2 = exp               %prec INFIXOP3
    { mkinfix ~loc:(loc ($startpos, $endpos)) e1 p e2 }
  | e1 = exp p = INFIXOP4 e2 = exp
    { mkinfix ~loc:(loc ($startpos, $endpos)) e1 p e2 }
  | e1 = exp BARBAR e2 = exp
    { mkinfix ~loc:(loc ($startpos, $endpos)) e1 "||" e2 }
  | e1 = exp AMPERAMPER e2 = exp
    { mkinfix ~loc:(loc ($startpos, $endpos)) e1 "&&" e2 }

infixop0:
  | p = INFIXOP0   { p }
  | LESS           { "<" }
  | GREATER        { ">" }
  | EQUAL          { "=" }

infixop2:
  | p = INFIXOP2   { p }
  | PLUS           { "+" }
  | PLUSPLUS       { "++" }
  | MINUS          { "-" }
  | MINUSPLUS      { "-+" }

infixop3:
  | p = INFIXOP3   { p }
  | STAR           { "*" }

%inline typ_opt:
  | /* epsilon */                              { None }
  | LBRACKETCOLON t = typ RBRACKET             { Some t }

%inline extended_arg:
  | /* epsilon */                              { `None }
  | LESSCOLON t = typ GREATER                  { `Type t }
  | LESSCOLON t1 = typ COMMA t2 = typ GREATER  { `TypePair (t1, t2) }
  | LESSBANG st = struct_type GREATER          { `StructType st }
  | LESSBANG INSTANCE sn = mident_path GREATER { `ContractInstance sn }

atomic_exp_or_type:
  | e = atomic_exp                             { `Exp e }
  | LBRACKETCOLON t = typ RBRACKET             { `Type t }

type_arg:
  | LBRACKETCOLON t = typ RBRACKET             { t }

atomic_exp:
  | LPAREN e = seq_exp RPAREN                  { e }
  | BEGIN e = seq_exp END                      { e }
  | c = const
    { mk_const ~loc:(loc ($startpos, $endpos)) c }
  | v = lident_path x = extended_arg
    { var_or_prim ~loc:(loc ($startpos, $endpos)) v x }
  | cstr = mident_path   %prec const_cstr
    { mk_constr ~loc:(loc ($startpos, $endpos)) cstr [] [] }
  | LBRACKET RBRACKET
    { mk_enil ~loc:(loc ($startpos, $endpos)) () }
  | LBRACKET el = separated_nonempty_list(SEMI, exp) RBRACKET
    { mk_list ~loc:(loc ($startpos, $endpos)) el }
  | t = atomic_exp DOT p = proj_indexes
    { mk_projection ~loc:(loc ($startpos, $endpos)) t p }
  | LBRACE r = record_exp RBRACE
    { match r with
      | None, fields ->
         mk_record ~loc:(loc ($startpos, $endpos)) None fields
      | Some record, updates ->
         mk_set_field ~loc:(loc ($startpos, $endpos)) record updates }
  | p = mident_path DOT LBRACE r = record_exp RBRACE
    { match r with
      | None, fields ->
         mk_record ~loc:(loc ($startpos, $endpos)) (Some p) fields
      | Some _, _ -> parse_error ($startpos, $endpos)
                                 "Record updates should not be path-prefixed" }
  | r = atomic_exp DOT fn = LIDENT
    { mk_get_field ~loc:(loc ($startpos, $endpos)) r fn }
/*  | LPAREN MODULE mn = mident_path RPAREN
    { mk_packstruct (Named mn) }
  | LPAREN MODULE STRUCT s = structure END RPAREN
    { mk_packstruct (Anonymous { s with is_module = true }) } */
  | LPAREN CONTRACT cn = mident_path RPAREN
    { mk_packstruct ~loc:(loc ($startpos, $endpos)) (Named (cn)) }
  | LPAREN CONTRACT STRUCT s = structure END RPAREN
    { mk_packstruct ~loc:(loc ($startpos, $endpos)) (Anonymous s) }

proj_indexes:
 | i = CINT                                        { [int_of_string i] }
 | LPAREN il = separated_list(COMMA, CINT) RPAREN  { List.map int_of_string il }

%inline exp_list:
 | el = separated_nontrivial_llist(COMMA, exp)      { el }

tuple_updates:
  | updates = separated_nontrivial_llist(COMMA, exp_or_nothing)
    { List.fold_left (fun (updates, idx) update ->
          match update with
          | None -> (updates, idx + 1)
          | Some exp -> ((idx, exp) :: updates, idx + 1)
        ) ([], 0) updates |> fst }

exp_or_nothing:
  | e = exp            %prec above_COMMA   { Some e }
  | UNDERSCORE                             { None }

record_exp:
  | eo = ioption(terminated(atomic_exp, WITH))
    fields = separated_or_terminated_nonempty_list(SEMI, record_field)
    { eo, fields }

%inline record_field:
  | fn = LIDENT eo = preceded(EQUAL, exp)?
    { let e = match eo with
        | None -> mk_var ~loc:(loc ($startpos, $endpos)) @@ Ident.create_id fn
        | Some e -> e
      in
      fn, e }

%inline match_cases:
  | xs = preceded_or_separated_nonempty_llist(BAR, match_case)
    { xs }

match_case:
  | p = pattern MINUSGREATER e = seq_exp                       { (p, e) }

%inline try_cases:
  | xs = preceded_or_separated_nonempty_llist(BAR, try_case)
    { xs }

try_case:
  | ep = exn_pattern MINUSGREATER e = seq_exp                  { (ep, e) }

%inline exn_pattern:
  | FAILURE LBRACKETCOLON t = typ RBRACKET                     { (Fail t), [] }
  | FAILURE LBRACKETCOLON t = typ RBRACKET p = pattern         { (Fail t), [p] }
  | exn = mident_path                       { (Love_ast.Exception exn, []) }
  | exn = mident_path p = pattern           { (Love_ast.Exception exn, [p]) }


/* Values */

value:
  | vl = separated_nonempty_list(COMMA, solid_value)
    { match vl with [v] -> v | _ ->  VTuple vl }

value_list_semi:
  | LPAREN v = value SEMI vl = separated_nonempty_list(SEMI, value) RPAREN
    { v :: vl }
  | LPAREN _v = value SEMI _vl = separated_nonempty_list(SEMI, value) error
    { unclosed "(" $startpos ")" $endpos }
  | v = atomic_value
    { [v] }

solid_value:
  | v = atomic_value                        { v }
  | cstr = UIDENT vl = value_list_semi      { VConstr (cstr, vl) }

atomic_value:
  | LPAREN v = value RPAREN      { v }
  | LPAREN _v = value error      { unclosed "(" $startpos ")" $endpos }
  | LPAREN RPAREN    { VUnit }
  | TRUE             { VBool true }
  | FALSE            { VBool false }
  | s = CSTRING      { VString s }
  | b = CBYTES       { VBytes (bytes_of_string b) }
  | i = CINT         { VInt (Z.of_string i) }
  | MINUS i = CINT   { VInt (Z.neg (Z.of_string i)) }
  | i = CNAT         { VNat (Z.of_string i) }
  | d = CDUN         { match dun_of_string d with
                       | Some d -> VDun d
                       | None -> parse_error ($startpos, $endpos) "Bad dun" }
  | k = CKEY         { match Signature.Public_key.of_b58check_opt k with
                       | Some k -> VKey k
                       | None -> parse_error ($startpos, $endpos) "Bad key" }
  | kh = CKEYHASH    { match Signature.Public_key_hash.of_b58check_opt kh with
                       | Some kh -> VKeyHash kh
                       | None -> parse_error ($startpos, $endpos) "Bad keyhash"}
  | s = CSIGNATURE   { match Signature.of_b58check_opt s with
                       | Some s -> VSignature s
                       | None -> parse_error ($startpos, $endpos)
                                             "Bad signature" }
  | t = CTIMESTAMP   { match Script_timestamp_repr.of_string t with
                       | Some t -> VTimestamp t
                       | None -> parse_error ($startpos, $endpos)
                                             "Bad timestamp" }
  | a = CADDRESS     { match Contract_repr.of_b58check a with
                       | Ok a -> VAddress a
                       | Error _ -> parse_error ($startpos, $endpos)
                                                "Bad contract" }
  | cstr = UIDENT    { VConstr (cstr, []) }
  | LBRACE fvl = separated_or_terminated_nonempty_list(SEMI, valuefield) RBRACE
    { VRecord fvl }
  | LBRACE _fvl = separated_or_terminated_nonempty_list(SEMI, valuefield) error
    { unclosed "{" $startpos "}" $endpos }
  | LBRACKET vl = separated_or_terminated_list(SEMI, value) RBRACKET
    { VList vl }
  | LBRACKET _vl = separated_or_terminated_list(SEMI, value) error
    { unclosed "[" $startpos "]" $endpos }
  | LBRACE vl = separated_or_terminated_list(SEMI, value) RBRACE
    { VSet (ValueSet.of_list vl) }
  | LBRACE _vl = separated_or_terminated_list(SEMI, value) error
    { unclosed "{" $startpos "}" $endpos }
  | LBRACE MINUS RBRACE
    { VMap (ValueMap.empty) }
  | LBRACE MINUS error
    { unclosed "{" $startpos "}" $endpos }
  | LBRACE bl = separated_or_terminated_nonempty_list(SEMI, valuebnd) RBRACE
    { VMap (Utils.bindings_to_map ValueMap.add ValueMap.empty bl) }
  | LBRACE _bl = separated_or_terminated_nonempty_list(SEMI, valuebnd) error
    { unclosed "{" $startpos "}" $endpos }
  | LBRACE t1 = typ COMMA t2 = typ COLON
      bl = separated_or_terminated_nonempty_list(SEMI, valuebnd) RBRACE
    { VBigMap { id = None;
                diff = Utils.bindings_to_map
                         (fun k v m -> ValueMap.add k (Some v) m)
                         ValueMap.empty bl;
                key_type = t1; value_type = t2 } }
  | LBRACE _t1 = typ COMMA _t2 = typ COLON
      _bl = separated_or_terminated_nonempty_list(SEMI, valuebnd) error
    { unclosed "{" $startpos "}" $endpos }
  | LBRACE t1 = typ COMMA t2 = typ COLON MINUS RBRACE
    { VBigMap { id = None; diff = ValueMap.empty;
                key_type = t1; value_type = t2 } }
  | LBRACE _t1 = typ COMMA _t2 = typ COLON MINUS error
    { unclosed "{" $startpos "}" $endpos }
/*
    No VOperation, VPackedStructure, VContractInstance,
       VEntryPoint, VView, VClosure, VPrimitive
    | p = path          { VPrimitive (p, []) }
*/

valuefield:
  | fn = LIDENT EQUAL v = value          { fn, v  }

valuebnd:
  | v1 = value MINUSGREATER v2 = value   { v1, v2  }



/* Attributes : keywords can be used */

attr_id:
  | id = LIDENT              { id }
  | id = UIDENT              { id }
  | AND                      { "and" }
  | AS                       { "as" }
  | ASSERT                   { "assert" }
  | BEGIN                    { "begin" }
  | CLASS                    { "class" }
  | CONSTRAINT               { "constraint" }
  | CONTRACT                 { "contract" }
  | DO                       { "do" }
  | DONE                     { "done" }
  | DOWNTO                   { "downto" }
  | ELSE                     { "else" }
  | END                      { "end" }
  | EXCEPTION                { "exception" }
  | EXTERNAL                 { "external" }
  | FALSE                    { "false" }
  | FOR                      { "for" }
  | FORALL                   { "forall" }
  | FUNCTION                 { "function" }
  | FUNCTOR                  { "functor" }
  | IF                       { "if" }
  | IN                       { "in" }
  | INCLUDE                  { "include" }
  | INHERIT                  { "inherit" }
  | INITIALIZER              { "initializer" }
  | LAZY                     { "lazy" }
  | LET                      { "let" }
  | MATCH                    { "match" }
  | METHOD                   { "method" }
  | MODULE                   { "module" }
  | MUTABLE                  { "mutable" }
  | NEW                      { "new" }
  | NONREC                   { "nonrec" }
  | OBJECT                   { "object" }
  | OF                       { "of" }
  | OPEN                     { "open" }
  | OR                       { "or" }
  | PRIVATE                  { "private" }
  | PUBLIC                   { "public" }
  | REC                      { "rec" }
  | SIG                      { "sig" }
  | STRUCT                   { "struct" }
  | THEN                     { "then" }
  | TO                       { "to" }
  | TRUE                     { "true" }
  | TRY                      { "try" }
  | TYPE                     { "type" }
  | VAL                      { "val" }
  | VIRTUAL                  { "virtual" }
  | WHEN                     { "when" }
  | WHILE                    { "while" }
  | WITH                     { "with" }


unused:
  | BACKQUOTE                { () }
  | TILDE                    { () }
  | QUESTION                 { () }
  | HASH                     { () }
  | AMPERSAND                { () }
  | DOTDOT                   { () }
  | BANG                     { () }
  | PLUSDOT                  { () }
  | PLUSEQ                   { () }
  | MINUSDOT                 { () }
  | COLONEQUAL               { () }
  | COLONGREATER             { () }
  | SEMISEMI                 { () }
  | PERCENT                  { () }
  | LBRACKETBAR              { () }
  | LBRACKETGREATER          { () }
  | LBRACELESS               { () }
  | BARRBRACKET              { () }
  | GREATERRBRACE            { () }
  | LBRACKETAT               { () }
  | LBRACKETATAT             { () }
  | LBRACKETATATAT           { () }
  | LBRACKETPERCENTPERCENT   { () }
  | DOTOP                    { () }
  | HASHOP                   { () }
  | LABEL                    { () }
  | OPTLABEL                 { () }
  | COMMENT                  { () }
  | EOL                      { () }

%%
