module Data_encoding = struct
  type json =
    [ `O of (string * json) list
    | `Bool of bool
    | `Float of float
    | `A of json list
    | `Null
    | `String of string ]

  module Json = Json_encoding
  include Json
  let json = any_ezjson_value
  let int31 = int
  let bytes = conv MBytes.to_string MBytes.of_string string
  let z = conv Z.to_string Z.of_string string

  module Tez_repr = struct
    let encoding = conv Int64.to_string Int64.of_string string
  end

  module Signature = struct
    module Public_key = struct
      let encoding = string
    end
    module Public_key_hash = struct
      let encoding = string
    end
    let encoding = string
  end

  module Script_timestamp_repr = struct
    let to_zint t = t
    let of_zint t = t
  end

  module Contract_repr = struct
    let encoding = string
  end
  module Script_expr_hash = struct
    let encoding = string
  end
end
