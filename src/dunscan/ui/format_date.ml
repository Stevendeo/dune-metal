(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Lang

module TEXT = struct
  let s_day = ss_ "day"
  let s_days = ss_ "days"
end
open TEXT

let days d = if d > 1 then s_days else s_day


let get_now () = Jsdate.now_value ()

let diff_server = ref 0.
let set_server_date date =
  let date_ms = date *. 1000. in
  let now_ms = get_now () in
  diff_server := now_ms -. date_ms

let get_now () =
  get_now () -. !diff_server

let parse_seconds t =
  if t < 60 then
    Printf.sprintf "%02d s" t
  else
    if t < 3600 then
      let secs = t mod 60 in
      let mins = t / 60 in
      if t < 600 then
        Printf.sprintf "%dm %02ds" mins secs
      else
        Printf.sprintf "%d min" mins
    else
      let t = t / 60 in
      if t < 60 * 24 then
        let mins = t mod 60 in
        let hours = t / 60 in
        Printf.sprintf "%dh %dm" hours mins
      else
        let t = t / 60 in
        if t < 72 then
          let hours = t mod 24 in
          let ds = t / 24 in
          Printf.sprintf "%d %s %dh" ds (t_ (days ds)) hours
        else
          let ds = t / 24 in
          Printf.sprintf "%d %s" ds (t_ (days ds))

let ago timestamp_f =
  (get_now () -. timestamp_f) /. 1000.

let ago_str ?(future=false) timestamp_f =
  let diff = int_of_float @@ ago timestamp_f in
  parse_seconds @@ if future then -1 * diff else diff

let time_before_level ~time_between_blocks diff_level =
  let seconds_left = diff_level * time_between_blocks in
  let divmod a b = a / b, a mod b in
  let days_left, hours_left = divmod seconds_left (24 * 60 * 60) in
  let hours_left, minutes_left = divmod hours_left (60 * 60) in
  let minutes_left, seconds_left = divmod minutes_left 60 in
  if days_left = 0 then
    if hours_left = 0 then
      if minutes_left = 0 then
        Printf.sprintf "%ds" seconds_left
      else
      Printf.sprintf "%dmin" minutes_left
    else
      Printf.sprintf "%dh %dm" hours_left minutes_left
  else
    Printf.sprintf "%dd %dh %dm" days_left hours_left minutes_left

let float_of_iso timestamp = Jsdate.parse timestamp
