(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,           *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

module Date = struct
  type t = DATE of string
  let format = "%Y-%m-%dT%H:%M:%SZ"
  let to_string (DATE s) = s
  let from_string s = DATE s
  let pretty_date = function
      DATE d ->
       match String.split_on_char 'T' d with
         [] -> "",""
       | hd :: [] -> hd,""
       | date :: time :: _ -> date,(String.sub time 0 (String.length time - 1))
end

type block_hash = string
type operation_hash = string
type account_hash = string
type proposal_hash = string
type protocol_hash = string
type network_hash = string
type nonce_hash = string
type timestamp = string

type ballot_type = Yay | Nay | Pass

type script_expr_t =
  | Int of Z.t
  | String of string
  | Bytes of Hex.t
  | Prim of string * script_expr_t list * string list
  | Seq of script_expr_t list

type script = {
  sc_code : string option;
  sc_storage : string;
  sc_code_hash : string option;
}

(* Independant part *)

type account_name = { dn : account_hash; alias : string option }

type protocol = {
  proto_name : string ;
  proto_hash : protocol_hash ;
}

type header = {
  hd_level : int ;
  hd_proto : int ;
  hd_predecessor : block_hash ;
  hd_timestamp : Date.t ;
  hd_validation_pass : int ;
  hd_operations_hash : string ;
  hd_fitness : string ;
  hd_context : string ;
  hd_priority : int ;
  hd_seed_nonce_hash : nonce_hash ;
  hd_proof_of_work_nonce : string ;
  hd_signature : string ;
  hd_hash : block_hash option ;
  hd_network : network_hash option ;
}

type block = {
  hash : block_hash ;
  predecessor_hash : block_hash ;
  fitness : string ;
  baker : account_name ;
  timestamp : Date.t ;
  validation_pass: int ;
  operations : (operation_hash * string * string) list ;
  nb_operations : int ;
  protocol : protocol ;
  test_protocol : protocol ;
  network : network_hash ;
  test_network : network_hash ;
  test_network_expiration : timestamp ;
  priority : int ;
  level : int ;
  cycle : int;
  commited_nonce_hash : nonce_hash ;
  pow_nonce : string ;
  proto : int ;
  data : string ;
  signature : string ;
  volume : int64 ;
  fees : int64 ;
  distance_level : int ;
}

type op_error = {
  err_kind: string;
  err_id: string;
  err_info: string;
}

type dictator_operation =
  | Activate
  | Activate_testnet

type ballot = {
  ballot_voting_period : int32 ;
  ballot_proposal : proposal_hash ;
  ballot_vote : ballot_type ;
}

type proposals = {
  prop_voting_period : int32 ;
  prop_proposals : proposal_hash list ;
}

type amendment_operation =
  | Proposal of proposals
  | Ballot of ballot

type endorsement = {
  endorse_src : account_name ;
  endorse_block_hash : block_hash ;
  endorse_block_level : int ;
  endorse_slot : int list ;
  endorse_op_level : int ;
  endorse_priority : int ;
  endorse_timestamp : string
}

type parameter = {
  p_entrypoint : string option ;
  p_value : Json_repr.any option ;
}

type transaction = {
  tr_src : account_name ;
  tr_dst : account_name ;
  tr_amount : int64 ;
  tr_counter : int32 ;
  tr_fee : int64 ;
  tr_gas_limit : Z.t ;
  tr_storage_limit : Z.t ;
  tr_parameters : parameter option ;
  tr_failed : bool ;
  tr_internal : bool ;
  tr_burn : int64 ;
  tr_op_level : int ;
  tr_timestamp : string ;
  tr_errors : op_error list option;
  tr_collect_fee_gas : int64 option;
  tr_collect_pk : string option;
}

type origination = {
  or_src : account_name ;
  or_manager : account_name ;
  or_delegate : account_name ;
  or_script : script option ;
  or_spendable : bool ;
  or_delegatable : bool ;
  or_balance : int64 ;
  or_counter : int32 ;
  or_fee : int64 ;
  or_gas_limit : Z.t ;
  or_storage_limit : Z.t ;
  or_kt1 : account_name ;
  or_failed : bool ;
  or_internal : bool ;
  or_burn : int64 ;
  or_op_level : int ;
  or_timestamp : string ;
  or_errors : op_error list option;
}

type delegation = {
  del_src : account_name ;
  del_delegate : account_name ;
  del_counter : int32 ;
  del_fee : int64 ;
  del_gas_limit : Z.t ;
  del_storage_limit : Z.t ;
  del_failed : bool ;
  del_internal : bool ;
  del_op_level : int ;
  del_timestamp : string ;
  del_errors : op_error list option;
}

type reveal = {
  rvl_src : account_name ;
  rvl_pubkey : string ;
  rvl_counter : int32 ;
  rvl_fee : int64 ;
  rvl_gas_limit : Z.t ;
  rvl_storage_limit : Z.t ;
  rvl_failed : bool ;
  rvl_internal : bool ;
  rvl_op_level : int ;
  rvl_timestamp : string ;
  rvl_errors : op_error list option;
}

type manager_operation =
  | Transaction of transaction
  | Origination of origination
  | Reveal of reveal
  | Delegation of delegation

type manage_account = {
  mac_src : account_name;
  mac_maxrolls : int option option;
  mac_admin : account_name option option;
  mac_white_list : account_name list option;
  mac_delegation : bool option;
  mac_target : (account_name * string option) option;
  mac_counter : int32;
  mac_fee : int64;
  mac_gas_limit : Z.t;
  mac_storage_limit : Z.t;
  mac_failed : bool;
  mac_internal : bool;
  mac_op_level : int;
  mac_timestamp : string;
  mac_errors : op_error list option;
}

type manage_accounts = {
  macs_src : account_name;
  macs_bytes : Hex.t;
  macs_counter : int32;
  macs_fee : int64;
  macs_gas_limit : Z.t;
  macs_storage_limit : Z.t;
  macs_failed : bool;
  macs_internal : bool;
  macs_op_level : int;
  macs_timestamp : string;
  macs_errors : op_error list option;
}

type activate_protocol = {
  acp_src : account_name;
  acp_parameters : string option;
  acp_protocol : string option;
  acp_counter : int32;
  acp_fee : int64;
  acp_gas_limit : Z.t;
  acp_storage_limit : Z.t;
  acp_failed : bool;
  acp_internal : bool;
  acp_op_level : int;
  acp_timestamp : string;
  acp_errors : op_error list option;
}

type dune_manager_operation =
  | Manage_account of manage_account
  | Manage_accounts of manage_accounts
  | Activate_protocol of activate_protocol

type sourced_operations =
  | Endorsement of endorsement
  | Amendment of (account_name * amendment_operation)
  | Manager of (string * account_name * manager_operation list)
  | Dictator of dictator_operation
  | Dune_manager of (account_name * dune_manager_operation list)

type seed_nonce_revelation = {
  seed_level : int ;
  seed_nonce : string ;
}

type activation = {
  act_pkh : account_name ;
  act_secret : string ;
  act_balance : int64 option ;
  act_op_level : int ;
  act_timestamp : string
}

type double_baking_evidence = {
  double_baking_header1 : header ;
  double_baking_header2 : header ;
  double_baking_main : int ;
  double_baking_accused : account_name ;
  double_baking_denouncer : account_name ;
  double_baking_lost_deposit : int64 ;
  double_baking_lost_rewards : int64 ;
  double_baking_lost_fees : int64 ;
  double_baking_gain_rewards : int64 ;
}

type double_endorsement_evidence = {
  double_endorsement1 : endorsement ;
  double_endorsement2 : endorsement ;
  double_endorsement_accused : account_name;
  double_endorsement_denouncer : account_name;
  double_endorsement_lost_deposit : int64;
  double_endorsement_lost_rewards : int64;
  double_endorsement_lost_fees : int64;
  double_endorsement_gain_rewards : int64
}

type anonymous_operation =
  | Seed_nonce_revelation of seed_nonce_revelation
  | Activation of activation
  | Double_endorsement_evidence of double_endorsement_evidence
  | Double_baking_evidence of double_baking_evidence

type ts_transfer = {
  ts_src : account_name;
  ts_dst : account_name;
  ts_amount : int64;
  ts_fee : int64;
  ts_flag : string;
}

type token_standard_operation =
  | TS_Transfer of ts_transfer
  | TS_TransferFrom of ts_transfer
  | TS_Approve of ts_transfer
  | TS_Kyc of string
  | TS_Unknown

type operation_type =
  | Anonymous of anonymous_operation list
  | Sourced of sourced_operations
  | Tokened of (int * string * account_name * token_standard_operation)

type operation = {
  op_hash : operation_hash ;
  op_block_hash : block_hash ;
  op_network_hash : network_hash ;
  op_type : operation_type ;
}

type account_details = {
  acc_name : account_name ;
  acc_balance : int64 ;
  acc_delegate : account_name option;
  acc_script : (script_expr_t option * script_expr_t * string option) option;
  acc_storage : script_expr_t option ;
  acc_counter : Z.t option;
  acc_node_timestamp : string option;
  acc_manager : account_name option ;
  acc_spendable : bool option;
  acc_delegatable : bool option
}

type account_info = {
  ai_name : account_name;
  ai_balance : int64;
  ai_delegate : account_name option;
  ai_script : script option;
  ai_origination : operation_hash option;
  ai_maxrolls : int option;
  ai_admin : account_name option;
  ai_white_list : account_name list option;
  ai_delegation : bool;
  ai_reveal : operation_hash option;
  ai_activation : operation_hash option;
}
