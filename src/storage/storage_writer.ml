(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_min
open Js
open Metal_types
open Storage_utils

let notified_set ?callback v = match callback with
  | None ->
    Chrome.Storage.set
      ~callback:(fun () -> Notif_background.send ())
      Chrome.sync
      v
  | Some cb ->
    Chrome.Storage.set
      ~callback:(fun () -> cb (); Notif_background.send ())
      Chrome.sync
      v

let set ?callback v =
  Chrome.Storage.set ?callback Chrome.sync v

let get_accounts f =
  Chrome.Storage.get Chrome.sync (fun (o:accounts_entry t) -> f (to_accounts o))

let get_notifs f =
  Chrome.Storage.get Chrome.Storage.local (fun (n : notif_entry t) -> f (to_notifs n))

let update_selected ?callback pkh =
  notified_set ?callback (of_selected pkh)

let add_account ?(select=false) ?callback ?accounts account =
  let aux accounts =
    let b, accounts =
      List.fold_left (fun (b, acc) a ->
          if a.pkh = account.pkh then
            true, { a with vault = account.vault } :: acc
          else false || b, a :: acc)
        (false, []) accounts in
    let accounts =
      if b then List.rev accounts
      else List.rev accounts @ [ account ] in
    let callback = match callback, select with
      | None, false -> None
      | Some callback, select -> Some (fun () ->
          if select then
            update_selected ~callback:(fun () -> callback accounts) (Some account.pkh)
          else
            callback accounts)
      | None, true ->
        Some (fun () -> update_selected (Some account.pkh)) in
    Chrome.Storage.set ?callback Chrome.sync (of_accounts accounts) in
  match accounts with
  | None -> get_accounts aux
  | Some accounts -> aux accounts

let remove_account ?callback ?accounts account_pkh =
  let aux accounts =
    let accounts = List.filter (fun {pkh; _} -> pkh <> account_pkh) accounts in
    let callback = match callback with
      | None -> None
      | Some callback -> Some (fun () -> callback accounts) in
    Chrome.Storage.set ?callback Chrome.sync (of_accounts accounts) in
  match accounts with
  | None -> get_accounts aux
  | Some accounts -> aux accounts

let update_account ?callback ?accounts account =
  let aux accounts =
    let accounts = List.map (fun a -> if a.pkh = account.pkh then account else a) accounts in
    let callback = match callback with
      | None -> None
      | Some callback -> Some (fun () -> callback account) in
    Chrome.Storage.set ?callback Chrome.sync (of_accounts accounts) in
  match accounts with
  | None -> get_accounts aux
  | Some accounts -> aux accounts

let update_network ?callback network =
  notified_set ?callback (of_network network)

let update_accounts ?callback accounts =
  notified_set ?callback (of_accounts accounts)

let get_approved f =
  Chrome.Storage.get Chrome.sync (fun (o:approved_entry t) ->
      let app = unoptdef [||] to_array o##.approved in
      f (Array.to_list app))

let add_approved name callback =
  get_approved (fun old_approved ->
      let approved = name :: old_approved in
      let entry : approved_entry t = Unsafe.obj [||] in
      entry##.approved := def @@ array @@ Array.of_list approved ;
      set ~callback entry)

let get_custom_networks f =
  Chrome.Storage.get Chrome.sync (fun (o:custom_networks_entry t) ->
      f (to_custom_networks o))

let add_custom_network ?callback ?networks name node api =
  let aux networks =
    match Js_of_ocaml.Url.url_of_string node, Js_of_ocaml.Url.url_of_string api with
    | Some node, Some api ->
      let network = name, node, api in
      let networks =
        if List.mem network networks then networks
        else networks @ [ network ] in
      let callback = match callback with
        | None -> None
        | Some callback -> Some (fun () -> callback networks) in
      Chrome.Storage.set ?callback Chrome.sync (of_custom_networks networks)
    | _ ->
      Js_log.log_str "Cannot get url from string";
      match callback with None -> () | Some callback -> callback networks in
  match networks with
  | Some networks -> aux networks
  | None -> get_custom_networks (fun networks -> aux networks)

let remove_custom_network ?callback ?networks name =
  let aux networks =
    let networks = List.filter (fun (n, _, _) -> n <> name) networks in
    let callback = match callback with
      | None -> None
      | Some callback -> Some (fun () -> callback networks) in
    Chrome.Storage.set ?callback Chrome.sync (of_custom_networks networks) in
  match networks with
  | Some networks -> aux networks
  | None -> get_custom_networks (fun networks -> aux networks)

let remove_notif ?callback n =
  let id = Metal_helpers.notif_id_of_not n in
  get_notifs (fun ns ->
      let ns = List.map (fun {notif_acc ; notifs} ->
          { notif_acc ;
            notifs =
              List.filter (function
                  | TraNot n | DlgNot n | OriNot n | ManNot n -> n.not_id <> id
                  | ApprovNot n -> n.not_approv_id <> id) notifs
          }
        ) ns in
      Chrome.Storage.set ?callback Chrome.Storage.local @@ of_notifs ns)

let add_notif ?callback acc notif =
  get_notifs (fun ns ->
      let ok, notifs =
        List.fold_left (fun (ok, n) { notif_acc ; notifs } ->
            if notif_acc = acc then
              true, { notif_acc ; notifs = notif :: notifs } :: n
            else ok, { notif_acc ; notifs } :: n
          ) (false, []) ns in
      let notifs =
        if ok then of_notifs notifs
        else of_notifs @@ { notif_acc = acc ; notifs = [ notif ] } :: notifs in
      Chrome.Storage.set ?callback Chrome.Storage.local notifs)

let reset_storage ?callback () =
  Chrome.Storage.clear Chrome.sync;
  Chrome.Storage.clear Chrome.Storage.local;
  match callback with
  | None -> ()
  | Some cb -> cb ()
