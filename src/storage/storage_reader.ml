(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_min
open Js
open Metal_types
open Storage_utils

let get_network f =
  Chrome.Storage.get Chrome.sync (fun (o:network_entry t) -> f (to_network o))
let get_selected f =
  Chrome.Storage.get Chrome.sync (fun (o:selected_entry t) -> f (to_selected o))
let get_accounts f =
  Chrome.Storage.get Chrome.sync (fun (o:accounts_entry t) -> f (to_accounts o))

let get_notifs f =
  Chrome.Storage.get Chrome.Storage.local (fun (n : notif_entry t) -> f (to_notifs n))

let get_account f = get_accounts (fun l ->
    get_selected (function
        | None -> f (List.nth_opt l 0, l)
        | Some selected -> f (List.find_opt (fun {pkh; _} -> pkh = selected) l, l)))

let get_custom_networks f =
  Chrome.Storage.get Chrome.sync (fun (o:custom_networks_entry t) ->
      f (to_custom_networks o))

let get_approved f =
  Chrome.Storage.get Chrome.sync (fun (o:approved_entry t) ->
      let app = unoptdef [||] to_array o##.approved in
      f (Array.to_list app))

let is_approved name callback =
  get_approved (fun approved ->
      callback @@ List.exists (fun n -> n = name) approved)

let get_state f =
  Chrome.Storage.get Chrome.sync (fun (o:state_entry t) -> f o##.state)

let is_unlocked callback =
  Vault.Cb.is_locked (fun b -> callback @@ not b)

let is_enabled callback =
  get_accounts (fun accs -> callback (accs <> []))

let get_keys () =
  Chrome.Storage.get Chrome.sync (fun o -> log o)
