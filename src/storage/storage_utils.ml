(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml
open Js
open Storage_types
open Metal_types
open Metal_helpers

let optdef f = function
  | None -> undefined
  | Some x -> def (f x)
let to_optdef f x = match Optdef.to_option x with
  | None -> None
  | Some x -> Some (f x)

let unoptdef def f x = match Optdef.to_option x with
  | None -> def
  | Some x -> f x

include Current_version

(* From js object to ocaml object *)
let to_transaction (o: transaction_js t) =
  { trs_hash = to_string o##.hash;
    trs_src = to_string o##.src;
    trs_dst = to_string o##.dst;
    trs_amount = Int64.of_string (to_string o##.amount);
    trs_fee = Int64.of_string (to_string o##.fee);
    trs_status = "pending";
    trs_tsp = "pending" ;
    trs_counter = Int64.of_string (to_string o##.counter) ;
    trs_gas_limit = Z.of_string (to_string o##.gasLimit);
    trs_storage_limit = Z.of_string (to_string o##.storageLimit);
    trs_entrypoint =
      Optdef.case
        o##.entrypoint
        (fun () -> None)
        (fun s -> Some (to_string s)) ;
    trs_parameters =
      Optdef.case
        o##.parameters
        (fun () -> None)
        (fun s -> Some (to_string s)) ;
    trs_internal = None ;
    trs_burn = None ;
    trs_op_level = None ;
    trs_errors = None
 }

let to_network_assoc a =
  let a = Array.to_list @@ to_array a in
  List.map
    (fun x -> network_of_string (to_string x##.network), to_string x##.hash) a

let to_vault (o: vault_js t) =
  match to_string o##.kind with
  | "ledger" -> begin match Optdef.to_option o##.path, Optdef.to_option o##.pwd with
      | path, Some pwd ->
        let pwd = Hex.to_bigstring (`Hex (to_string pwd)) in
        begin match path with
          | None -> Ledger ("44'/1729'/0'/0'", pwd)
          | Some path -> Ledger (to_string path, pwd) end
      | _ -> Js_utils.log "error: no password hash found"; assert false end
  | "local" -> begin match Optdef.to_option o##.iv, Optdef.to_option o##.esk with
      | Some iv, Some esk ->
        Local (Hex.to_bigstring (`Hex (to_string iv)),
               Hex.to_bigstring (`Hex (to_string esk)))
      | _ -> Js_utils.log "error: no data for decryption"; assert false end
  | _ -> Js_utils.log "error: type of vault unknown"; assert false

let to_notif_op (o: notif_js t) =
  let not_origin = to_string o##.origin in
  let not_msg = to_optdef to_string o##.msg in
  let not_dst = to_optdef to_string o##.dst in
  let not_tsp = to_string o##.tsp in
  let not_id = to_string o##.id in
  let not_wid = o##.wid in
  let not_amount = to_optdef (fun s -> Int64.of_string (to_string s)) o##.amount in
  let not_fee = to_optdef (fun s -> Int64.of_string @@ to_string s) o##.fee in
  let not_gas_limit = to_optdef (fun s -> Z.of_string @@ to_string s) o##.gasLimit in
  let not_storage_limit = to_optdef (fun s -> Z.of_string @@ to_string s) o##.storageLimit in
  let not_entry = to_optdef to_string o##.entry in
  let not_params = to_optdef to_string o##.params in
  let not_sc_code = to_optdef to_string o##.scCode in
  let not_sc_code_hash = to_optdef to_string o##.scCodeHash in
  let not_sc_storage = to_optdef to_string o##.scStorage in
  let not_delegate = to_optdef to_string o##.delegate  in
  let not_maxrolls = to_optdef Optdef.to_option o##.maxrolls in
  let not_admin = to_optdef (to_optdef to_string) o##.admin in
  let not_white_list =
    to_optdef (fun a -> List.map to_string @@ Array.to_list @@ to_array a) o##.whiteList in
  let not_delegation = to_optdef to_bool o##.delegation in
  notif_op_of_kind (to_string o##.kind)
    {not_origin ; not_msg ; not_dst ; not_tsp ; not_id ; not_wid ; not_amount ;
     not_fee ; not_gas_limit ; not_storage_limit ; not_entry; not_params ; not_sc_code ;
     not_sc_code_hash ; not_sc_storage ; not_delegate ;
     not_maxrolls; not_admin; not_white_list; not_delegation}

let to_notif_app (o: notif_js t) =
  let not_approv_id = to_string o##.id in
  let not_approv_wid = o##.wid in
  let not_approv_icon = convopt to_string @@ Optdef.to_option o##.icon in
  let not_approv_name = to_string o##.name in
  let not_approv_url = to_string o##.url in
  let not_approv_tsp = to_string o##.tsp in
  ApprovNot {
    not_approv_id ;
    not_approv_wid ;
    not_approv_icon ;
    not_approv_name ;
    not_approv_url ;
    not_approv_tsp ;
  }

let to_notif (o: notif_js t) =
  match (to_string o##.kind) with
  | "approval" -> to_notif_app o
  | _ -> to_notif_op o

let to_acc_notif (o: notif_acc t) =
  let notif_acc = to_string o##.acc in
  let notifs =
    match Optdef.to_option o##.ns with
    | None -> []
    | Some a -> List.map to_notif (Array.to_list @@ to_array a) in
  { notif_acc ; notifs }

let to_account (o: account_js t) =
  let pkh = to_string o##.pkh in
  let manager = to_string o##.manager in
  let name = to_string o##.name in
  let vault = to_vault o##.vault in
  let revealed = to_network_assoc o##.revealed in
  let pending = List.map to_transaction (Array.to_list @@ to_array o##.pending) in
  let manager_kt = to_optdef to_bool o##.managerKT in
  let admin = to_network_assoc o##.admin in
  {pkh ; manager ; name ; vault ; revealed ; pending; manager_kt; admin }

let to_accounts (o: accounts_entry t) =
  List.map to_account (Array.to_list (unoptdef [||] to_array o##.accounts))

let to_network (o: network_entry t) = network_of_string (unoptdef "Testnet" to_string o##.network)

let to_custom_network (o: custom_network t) =
  let name, node, api = to_string o##.name, to_string o##.node, to_string o##.api in
  let f s = match  Js_of_ocaml.Url.url_of_string s with
    | None -> assert false
    | Some u -> u in
  (name, f node, f api)

let to_custom_networks (o: custom_networks_entry t) =
  match Optdef.to_option o##.custom_networks with
  | None -> []
  | Some a -> List.map to_custom_network (Array.to_list (to_array a))

let to_selected (o: selected_entry t) =
  match Optdef.to_option o##.selected with
  | Some s when s != (string "") -> Some (to_string s)
  | _ -> None

(* From ocaml object to js object *)

let of_transaction trs =
  let tr : transaction_js t = Unsafe.obj [||] in
  tr##.hash := string trs.trs_hash;
  tr##.src := string trs.trs_src;
  tr##.dst := string trs.trs_dst;
  tr##.amount := string (Int64.to_string trs.trs_amount);
  tr##.fee := string (Int64.to_string trs.trs_fee);
  tr##.counter := string (Int64.to_string trs.trs_counter );
  tr##.gasLimit := string (Z.to_string trs.trs_gas_limit);
  tr##.storageLimit := string (Z.to_string trs.trs_storage_limit);
  tr##.entrypoint := optdef string trs.trs_entrypoint ;
  tr##.parameters := optdef string trs.trs_parameters ;
  tr##.tsp := string trs.trs_tsp;
  tr##.status := string trs.trs_status;
  tr##.failed := string @@ if trs.trs_status = "pending" then "true" else trs.trs_status ;
  tr##.internal :=
    string
      (match trs.trs_internal with None -> "pending" | Some b -> string_of_bool b) ;
  tr##.burn :=
    string @@
    Int64.to_string
      (match trs.trs_burn with None -> 0L | Some i64 -> i64) ;
  tr##.opLevel :=
    string (match trs.trs_op_level with None -> "pending" | Some i -> string_of_int i) ;
  tr

let of_vault v =
  let vault : vault_js t = Unsafe.obj [||] in
  begin match v with
    | Ledger (path, pwd) -> vault##.kind := string "ledger";
      vault##.path := def (string path);
      vault##.pwd := def (string Hex.(show (of_bigstring pwd)));
    | Local (iv, esk) -> vault##.kind := string "local";
      vault##.iv := def (string Hex.(show (of_bigstring iv)));
      vault##.esk := def (string Hex.(show (of_bigstring esk))) end;
  vault

let of_notif no =
  let notif : notif_js t = Unsafe.obj [||] in
  begin match no with
    | TraNot n | DlgNot n | OriNot n | ManNot n ->
      notif##.kind := string (notif_kind_to_string no);
      notif##.origin := string n.not_origin;
      notif##.msg := optdef string n.not_msg;
      notif##.dst := optdef string n.not_dst;
      notif##.tsp := string n.not_tsp;
      notif##.id := string n.not_id;
      notif##.wid := n.not_wid;
      notif##.amount := optdef (fun a -> string (Int64.to_string a)) n.not_amount;
      notif##.fee := optdef (fun f -> string (Int64.to_string f)) n.not_fee;
      notif##.gasLimit := optdef (fun f -> string (Z.to_string f)) n.not_gas_limit;
      notif##.storageLimit := optdef (fun f -> string (Z.to_string f)) n.not_storage_limit;
      notif##.entry := optdef string n.not_entry;
      notif##.params := optdef string n.not_params;
      notif##.scCode := optdef string n.not_sc_code;
      notif##.scCodeHash := optdef string n.not_sc_code_hash;
      notif##.scStorage := optdef string n.not_sc_storage;
      notif##.delegate := optdef string n.not_delegate;
      notif##.maxrolls := optdef Optdef.option n.not_maxrolls;
      notif##.admin := optdef (optdef string) n.not_admin;
      notif##.whiteList := optdef (fun l -> array @@ Array.of_list (List.map string l)) n.not_white_list;
      notif##.delegation := optdef bool n.not_delegation
    | ApprovNot n ->
      notif##.kind := string (notif_kind_to_string no);
      notif##.id := string n.not_approv_id;
      notif##.wid := n.not_approv_wid;
      notif##.tsp := string n.not_approv_tsp ;
      notif##.icon := optdef string n.not_approv_icon ;
      notif##.name := string n.not_approv_name ;
      notif##.url := string n.not_approv_url ;
  end ;
  notif

let of_acc_notif {notif_acc ; notifs} =
  let data : notif_acc t = Unsafe.obj [||] in
  data##.acc := string notif_acc ;
  data##.ns := Js.def @@ array (Array.of_list ( List.map of_notif notifs)) ;
  data

let of_network_assoc a =
  array @@ Array.of_list @@ List.map (fun (network, hash) ->
      let na : network_assoc_js t = Unsafe.obj [||] in
      na##.hash := string hash;
      na##.network := string (network_to_string network);
      na) a

let of_account {pkh; manager; vault; name; revealed; pending; manager_kt; admin } =
  let account : account_js t = Unsafe.obj [||] in
  account##.pkh := string pkh;
  account##.manager := string manager;
  account##.name := string name;
  account##.vault := of_vault vault;
  account##.revealed := of_network_assoc revealed;
  account##.pending := array (Array.of_list (List.map of_transaction pending));
  account##.managerKT := optdef bool manager_kt;
  account##.admin := of_network_assoc admin;
  account

let of_accounts accounts =
  let data : accounts_entry t = Unsafe.obj [||] in
  data##.accounts := def (array (Array.of_list (List.map of_account accounts)));
  data

let of_network network =
  let data : network_entry t = Unsafe.obj [||] in
  data##.network := def (string (network_to_string network));
  data

let of_custom_network ~name ~node ~api =
  let data : custom_network t = Unsafe.obj [||] in
  data##.name := string name;
  data##.node := string (Js_of_ocaml.Url.string_of_url node);
  data##.api := string (Js_of_ocaml.Url.string_of_url api);
  data

let of_custom_networks networks =
  let a = array @@ Array.of_list @@
    List.map (fun (name, node, api) -> of_custom_network ~name ~node ~api) networks in
  let data : custom_networks_entry t = Unsafe.obj [||] in
  data##.custom_networks := Js.def a;
  data

let of_selected pkh =
  let data : selected_entry t = Unsafe.obj [||] in
  data##.selected := optdef string pkh;
  data

let to_notifs (n : notif_entry t) =
  match Optdef.to_option n##.notifs with
  | None -> []
  | Some a -> List.map to_acc_notif (Array.to_list (to_array a))

let of_notifs notifs =
  let a = array @@ Array.of_list @@
    List.map (fun n -> of_acc_notif n) notifs in
  let data : notif_entry t = Unsafe.obj [||] in
  data##.notifs := Js.def a ;
  data
