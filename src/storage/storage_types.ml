(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml
open Js

class type version_entry = object
  method version : int optdef prop
end

class type notif_js = object
  method kind : js_string t prop
  method id : js_string t prop
  method wid : int prop
  method tsp : js_string t prop
  (* Op *)
  method origin : js_string t prop
  method msg : js_string t optdef prop
  method dst : js_string t optdef prop
  method amount : js_string t optdef prop
  method fee : js_string t optdef prop
  method gasLimit : js_string t optdef prop
  method storageLimit : js_string t optdef prop
  method entry : js_string t optdef prop
  method params : js_string t optdef prop
  method scCode : js_string t optdef prop
  method scCodeHash : js_string t optdef prop
  method scStorage : js_string t optdef prop
  method spendable : bool t optdef prop
  method delegatable : bool t optdef prop
  method manager : js_string t optdef prop
  method delegate : js_string t optdef prop
  method maxrolls : int optdef optdef prop
  method admin : js_string t optdef optdef prop
  method whiteList : js_string t js_array t optdef prop
  method delegation : bool t optdef prop
  (* Approv *)
  method icon : js_string t optdef prop
  method name : js_string t prop
  method url : js_string t prop
end



module V0 = struct

  class type transaction_js = object
    method hash : js_string t prop
    method src : js_string t prop
    method fee : js_string t prop
    method dst : js_string t prop
    method amount : js_string t prop
    method counter : js_string t prop
    method gasLimit : js_string t prop
    method storageLimit : js_string t prop
    method entrypoint : js_string t optdef prop
    method parameters : js_string t optdef prop
    method failed : js_string t prop
    method internal : js_string t prop
    method burn : js_string t prop
    method opLevel : js_string t prop
    method tsp : js_string t prop
    method status : js_string t prop
    method errors : 'a js_array t optdef prop
  end

  class type vault_js = object
    method kind : js_string t prop
    method iv : js_string t optdef prop
    method esk : js_string t optdef prop
    method path : js_string t optdef prop
    method pwd : js_string t optdef prop
  end

  class type account_js0 = object
    method pkh : js_string t prop
    method manager : js_string t prop
    method vault : vault_js t prop
    method name : js_string t prop
    method revealed : js_string t optdef prop
    method pending : transaction_js t js_array t prop
  end

  class type accounts_entry0 = object
    method accounts : account_js0 t js_array t optdef prop
  end

  class type approved_entry = object
    method approved : js_string t js_array t optdef prop
  end

  class type state_entry = object
    method state : js_string t prop
  end

  class type network_entry = object
    method network : js_string t optdef prop
  end

  class type custom_network = object
    method name : js_string t prop
    method node : js_string t prop
    method api : js_string t prop
  end
  class type custom_networks_entry = object
    method custom_networks : custom_network t js_array t optdef prop
  end

  class type selected_entry = object
    method selected : js_string t optdef prop
  end

  class type notif_acc = object
    method acc : js_string t prop
    method ns : notif_js t js_array t optdef prop
  end

  class type notif_entry = object
    method notifs : notif_acc t js_array t optdef prop
  end
end

module V1 = struct

  include V0

  class type network_assoc_js = object
    method hash : js_string t prop
    method network : js_string t prop
  end

  class type account_js1 = object
    method pkh : js_string t prop
    method manager : js_string t prop
    method vault : vault_js t prop
    method name : js_string t prop
    method revealed : network_assoc_js t js_array t prop
    method pending : transaction_js t js_array t prop
  end

  class type accounts_entry1 = object
    method accounts : account_js1 t js_array t optdef prop
  end
end

module V2 = struct

  include V1

  class type account_js = object
    method pkh : js_string t prop
    method manager : js_string t prop
    method vault : vault_js t prop
    method name : js_string t prop
    method revealed : network_assoc_js t js_array t prop
    method pending : transaction_js t js_array t prop
    method managerKT : bool t optdef prop
    method admin : network_assoc_js t js_array t prop
  end

  class type accounts_entry = object
    method accounts : account_js t js_array t optdef prop
  end
end

let current_version = 2
module Current_version = V2

let upgrade_0_to_1 () =
  Chrome.Storage.get Chrome.sync (fun (o:V0.accounts_entry0 t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V1.account_js1 t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := a_old##.pending;
              a_new##.revealed := array [||];
              a_new) (to_array accounts) in
      let data : V1.accounts_entry1 t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let upgrade_1_to_2 () =
  Chrome.Storage.get Chrome.sync (fun (o:V2.accounts_entry t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V2.account_js t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := a_old##.pending;
              a_new##.revealed := array [||];
              a_new##.managerKT := undefined ;
              a_new##.admin := array [||] ;
              a_new) (to_array accounts) in
      let data : V2.accounts_entry t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let downgrade_1_to_0 () =
  Chrome.Storage.get Chrome.sync (fun (o:V1.accounts_entry1 t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V0.account_js0 t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := a_old##.pending;
              a_new##.revealed := undefined;
              a_new) (to_array accounts) in
      let data : V0.accounts_entry0 t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let downgrade_2_to_1 () =
  Chrome.Storage.get Chrome.sync (fun (o:V2.accounts_entry t) ->
      let accounts =
        match Optdef.to_option o##.accounts with
        | None -> undefined
        | Some accounts ->
          def @@ array @@ Array.map (fun a_old ->
              let a_new : V1.account_js1 t = Unsafe.obj [||] in
              a_new##.pkh := a_old##.pkh;
              a_new##.manager := a_old##.manager;
              a_new##.vault := a_old##.vault;
              a_new##.name := a_old##.name;
              a_new##.pending := a_old##.pending;
              a_new##.revealed := a_old##.revealed;
              a_new) (to_array accounts) in
      let data : V1.accounts_entry1 t = Unsafe.obj [||] in
      data##.accounts := accounts;
      Chrome.Storage.set Chrome.sync data)

let upgrades : (int * (unit -> unit)) list = [
  0, upgrade_0_to_1 ;
  1, upgrade_1_to_2
]

let downgrades : (int * (unit -> unit)) list = [
  1, downgrade_1_to_0 ;
  2, downgrade_2_to_1
]

let update_storage () =
  Chrome.Storage.get Chrome.sync (fun (o:version_entry t) ->
      let storage_version = match Optdef.to_option o##.version with
        | None -> 0
        | Some i -> i in
      let direction = compare current_version storage_version in
      let rec aux = function
        | i when i = current_version -> ()
        | i ->
          begin match
              List.assoc_opt i
                (if direction = 1 then upgrades else downgrades) with
          | None -> ()
          | Some update -> update () end;
          let data : version_entry t = Unsafe.obj [||] in
          data##.version := def (i+direction);
          Chrome.Storage.set Chrome.sync data;
          aux (i+direction) in
      aux storage_version)
