open Async
open Dune_types_min

(* printers *)

let print_love_expr = function
  | LoveValue lv -> Format.asprintf "%a" Love_printer.Value.print lv
  | LoveType _lt -> ""

let print_love_code = function
  | LoveTopContract ltc ->
    Format.asprintf "%a" Love_printer.Ast.print_structure ltc.Love_ast.code
  | LoveLiveContract llc ->
    Format.asprintf "%a"
      Love_printer.Value.print_livestructure llc.Love_value.LiveContract.root_struct
  | LoveFeeCode _lfc -> ""

let print_dune_expr = function LoveExpr le -> print_love_expr le
let print_dune_code = function LoveCode le -> print_love_code le

let print_script ?contract = function
  | Micheline sc -> Micheline_printer.to_string ?contract sc
  | DuneExpr de -> print_dune_expr de
  | DuneCode dc -> print_dune_code dc


let get_header s =
  if String.length s >= 1 && s.[0] = '#' then
    let rec iter s pos len =
      if pos = len then pos else
        match s.[pos] with
        | '\n' | '\r' | ':' | ' ' | '\t' -> pos
        | _ -> iter s (pos+1) len
    in
    let i = iter s 1 (String.length s) in
    if i - 1 <= 0 then None, 0
    else Some (String.sub s 1 (i - 1)), i + 1
  else None, 0

(* parsers *)

let parse_micheline s =
  let tokens, errors = Micheline_parser.tokenize s in
  match errors with
  | _ :: _ -> Error (Async.Exn_err errors)
  | [] ->
    let node, errors = Micheline_parser.parse_expression ~check:false tokens in
    match errors with
    | _ :: _ -> Error (Async.Exn_err errors)
    | [] -> Ok (Dune_types_min.Micheline (Micheline.strip_locations node))

let parse_json s =
  try Ok (Dune_encoding_min.Script.decode s)
  with _ -> Error (Async.Str_err ("Cannot parse json " ^ s))

let parse_love ?(contract=false) s =
  let lb = Lexing.from_string s in
  try (
    if contract then
      let code = Love_parser.top_contract Love_lexer.token lb in
      Ok (DuneCode (LoveCode (LoveTopContract code)))
    else
      match Love_parser.top_value Love_lexer.token lb with
      | Love_value.Value v -> Ok Dune_types_min.(DuneExpr (LoveExpr (LoveValue v)))
      | Love_value.Type t -> Ok Dune_types_min.(DuneExpr (LoveExpr (LoveType t))))
  with _ -> Error (Async.Str_err ("Cannot parse love " ^ s))

let parse_script ?contract s = match fst (get_header s) with
  | Some "mic" -> parse_micheline s
  | Some "love" -> parse_love ?contract s
  | Some "json" -> parse_json s
  | Some s -> Error (Async.Str_err ("Cannot parse header " ^ s))
  | None -> parse_json s

(* string to json *)
let json_of_string s = match get_header s with
  | Some "mic", i ->  let s = (String.sub s i ((String.length s) - i)) in
    parse_micheline s >|? Dune_encoding_min.Script.encode
  | Some "love", _ -> parse_love s >|? Dune_encoding_min.Script.encode
  | Some "json", i -> Ok (String.sub s i ((String.length s) - i))
  | Some s, _ ->   Error (Async.Str_err ("Cannot parse header " ^ s))
  | None, _ -> Ok s
