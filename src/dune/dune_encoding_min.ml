(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

open Json_encoding
open Dune_types_min

let int64 = EzEncoding.int64

let z_encoding =
  def "zarith"
    ~title: "Big number"
    ~description: "Decimal representation of a big number" @@
  conv Z.to_string Z.of_string string

let int_of_string =
  union [
    case int (fun i -> Some i) (fun i -> i) ;
    case string (fun i -> Some (string_of_int i)) int_of_string
  ]

let hex_encoding = conv (fun h -> Hex.show h) (fun s -> `Hex s) string

module Date = struct
  let encoding = conv Date.to_string Date.from_string string
end

module Forge = struct
  let forge_uint8 i = Bytes.make 1 @@ char_of_int i

  let forge_int32 i = (* 4 bytes *)
  let rec f i acc x =
    let c = forge_uint8 (x land 0xff) in
    if i = 3 then c :: acc
    else f (i+1) (c :: acc) (x lsr 8) in
  Bytes.concat Bytes.empty (f 0 [] i)
end

module Love = struct
  let love_expr = union [
      case Love_json_encoding.Value.encoding
        (function LoveValue s -> Some s | _ -> None)
        (fun s -> LoveValue s);
      case Love_json_encoding.Type.encoding
        (function LoveType s -> Some s | _ -> None)
        (fun s -> LoveType s) ]

  let love_code = union [
     case Love_json_encoding.Ast.top_contract_encoding
        (function LoveTopContract s -> Some s | _ -> None)
        (fun s -> LoveTopContract s);
      case Love_json_encoding.Value.live_contract_encoding
        (function LoveLiveContract s -> Some s | _ -> None)
        (fun s -> LoveLiveContract s);
     case Love_json_encoding.Value.fee_code_encoding
       (function LoveFeeCode s -> Some s | _ -> None)
       (fun s -> LoveFeeCode s) ]
end

module DuneExpr = struct
  let encoding = conv
      (function LoveExpr x -> x)
      (fun x -> LoveExpr x)
      (obj1 (req "dune_expr" Love.love_expr))
end

module DuneCode = struct
  let encoding = conv
      (function LoveCode x -> x)
      (fun x -> LoveCode x)
      (obj1 (req "dune_code" Love.love_code))
end

module Script = struct
  let expr_encoding =
    union [
      case (Micheline_encoding.canonical_encoding ~variant:"" string)
        (function Micheline s -> Some s | _ -> None)
        (fun s -> Micheline s);
      case DuneExpr.encoding
        (function DuneExpr s -> Some s | _ -> None)
        (fun s -> DuneExpr s);
      case DuneCode.encoding
        (function DuneCode s -> Some s | _ -> None)
        (fun s -> DuneCode s)
    ]

  let encode script = EzEncoding.construct expr_encoding script

  let decode str = EzEncoding.destruct expr_encoding str

  let expr_str_encoding =
    conv decode encode expr_encoding

  let script_encoding =
    obj3
      (opt "code" expr_encoding)
      (req "storage" expr_encoding)
      (opt "code_hash" string)

  let script_str_encoding =
    conv
      (fun {sc_code; sc_storage; sc_code_hash} -> (sc_code, sc_storage, sc_code_hash))
      (fun (sc_code, sc_storage, sc_code_hash) -> {sc_code; sc_storage; sc_code_hash})
    (obj3
       (opt "code" expr_str_encoding)
       (req "storage" expr_str_encoding)
       (opt "code_hash" string))

  let string_of_ep = function
    | EPDefault ->  "default"
    | EPRoot ->  "root"
    | EPDo ->  "do"
    | EPSet ->  "set_delegate"
    | EPRemove ->  "remove_delegate"
    | EPNamed s ->  s

  let ep_of_string = function
    | "default" -> EPDefault
    | "root" -> EPRoot
    | "do" -> EPDo
    | "set_delegate" -> EPSet
    | "remove_delegate" -> EPRemove
    | s -> EPNamed s

  let entrypoint_encoding = conv string_of_ep ep_of_string string

end

module Operation_hash = struct let encoding = string end
module Context_hash = struct let encoding = string  end
module Operation_list_list_hash = struct let encoding = string end
module Fitness = struct
  let encoding =
    conv (String.split_on_char ' ') (String.concat " ") (list string)
end
module Signature = struct let encoding = string end
module Block_hash = struct let encoding = string end
module Protocol_hash = struct let encoding = string end
module Chain_id = struct let encoding = string end
module Time = struct let encoding = string end
module Contracts = struct let encoding = list string end

module Constants = struct
  let encoding =
    conv
      (fun { hard_gas_limit_per_operation; hard_storage_limit_per_operation }
        -> ( hard_gas_limit_per_operation, hard_storage_limit_per_operation ), ())
      (fun (( hard_gas_limit_per_operation, hard_storage_limit_per_operation ), _)
        -> { hard_gas_limit_per_operation; hard_storage_limit_per_operation })
      (merge_objs
         (obj2
            (req "hard_gas_limit_per_operation" z_encoding)
            (req "hard_storage_limit_per_operation" z_encoding))
         unit)
end

module Level = struct
  let encoding =
    conv
      (fun {node_lvl_level; node_lvl_level_position;
            node_lvl_cycle; node_lvl_cycle_position;
            node_lvl_voting_period; node_lvl_voting_period_position ;
            node_lvl_expected_commitment }
        -> (node_lvl_level, node_lvl_level_position, node_lvl_cycle,
            node_lvl_cycle_position, node_lvl_voting_period,
            node_lvl_voting_period_position, node_lvl_expected_commitment))
      (fun (node_lvl_level, node_lvl_level_position, node_lvl_cycle,
            node_lvl_cycle_position, node_lvl_voting_period,
            node_lvl_voting_period_position, node_lvl_expected_commitment)
        -> {node_lvl_level; node_lvl_level_position;
            node_lvl_cycle; node_lvl_cycle_position;
            node_lvl_voting_period; node_lvl_voting_period_position ;
            node_lvl_expected_commitment })
      (obj7
         (req "level" int)
         (req "level_position" int)
         (req "cycle" int)
         (req "cycle_position" int)
         (req "voting_period" int)
         (req "voting_period_position" int)
         (req "expected_commitment" bool))
end

module Voting_period_repr = struct
  let kind_encoding =
    union  [
      case
        (constant "proposal")
        (function NProposal -> Some () | _ -> None)
        (fun () -> NProposal) ;
      case
        (constant "testing_vote")
        (function NTesting_vote -> Some () | _ -> None)
        (fun () -> NTesting_vote) ;
      case
        (constant "testing")
        (function NTesting -> Some () | _ -> None)
        (fun () -> NTesting) ;
      case
        (constant "promotion_vote")
        (function NPromotion_vote -> Some () | _ -> None)
        (fun () -> NPromotion_vote) ;
    ]
end

module Balance_updates = struct
  let balance_encoding =
    union
      [ case
          (obj3
             (req "kind" (constant "contract"))
             (req "contract" string)
             (req "change" int64))
          (function Contract (c, change) ->
             Some ((), c, change) | _ -> None )
          (fun ((), c, change) -> (Contract (c, change))) ;
        case
          (obj6
             (req "kind" (constant "freezer"))
             (req "category" (constant "rewards"))
             (req "delegate" string)
             (opt "level" int)
             (opt "cycle" int)
             (req "change" int64))
          (function Rewards (d, l, change) ->
             Some ((), (), d, Some l, None, change) | _ -> None)
          (fun ((), (), d, l, c, change) ->
             match l, c with
             | Some l, _ | _, Some l -> Rewards (d, l, change)
             | None, None -> assert false) ;
        case
          (obj6
             (req "kind" (constant "freezer"))
             (req "category" (constant "fees"))
             (req "delegate" string)
             (opt "level" int)
             (opt "cycle" int)
             (req "change" int64))
          (function Fees (d, l, change) ->
             Some ((), (), d, Some l, None, change) | _ -> None)
          (fun ((), (), d, l, c, change) ->
             match l, c with
             | Some l, _ | _, Some l -> Fees (d, l, change)
             | None, None -> assert false) ;
        case
          (obj6
             (req "kind" (constant "freezer"))
             (req "category" (constant "deposits"))
             (req "delegate" string)
             (opt "level" int)
             (opt "cycle" int)
             (req "change" int64))
          (function Deposits (d, l, change) ->
             Some ((), (), d, Some l, None, change) | _ -> None)
          (fun ((), (), d, l, c, change) ->
             match l, c with
             | Some l, _ | _, Some l -> Deposits (d, l, change)
             | None, None -> assert false) ]

  let encoding =
    list balance_encoding
end

module Protocol_param = struct
    let encoding = conv
    (fun { proof_of_work_nonce_size_opt; nonce_length_opt; max_revelations_per_block_opt;
           max_operation_data_length_opt; max_proposals_per_delegate_opt;
           preserved_cycles_opt; blocks_per_cycle_opt; blocks_per_commitment_opt;
           blocks_per_roll_snapshot_opt; blocks_per_voting_period_opt;
           time_between_blocks_opt; endorsers_per_block_opt;
           hard_gas_limit_per_operation_opt; hard_gas_limit_per_block_opt;
           proof_of_work_threshold_opt; tokens_per_roll_opt;
           michelson_maximum_type_size_opt; seed_nonce_revelation_tip_opt;
           origination_size_opt; block_security_deposit_opt;
           endorsement_security_deposit_opt; block_reward_opt;
           endorsement_reward_opt; cost_per_byte_opt;
           hard_storage_limit_per_operation_opt; hard_gas_limit_to_pay_fees_opt;
           max_operation_ttl_opt; protocol_revision_opt;
           frozen_account_cycles_opt; allow_collect_call_opt;
           quorum_min_opt; quorum_max_opt; min_proposal_quorum_opt;
           initial_endorsers_opt; delay_per_missing_endorsement_opt }
      -> ((proof_of_work_nonce_size_opt, nonce_length_opt, max_revelations_per_block_opt,
           max_operation_data_length_opt, max_proposals_per_delegate_opt,
           preserved_cycles_opt, blocks_per_cycle_opt, blocks_per_commitment_opt,
           blocks_per_roll_snapshot_opt, blocks_per_voting_period_opt,
           time_between_blocks_opt, endorsers_per_block_opt,
           hard_gas_limit_per_operation_opt, hard_gas_limit_per_block_opt,
           proof_of_work_threshold_opt, tokens_per_roll_opt,
           michelson_maximum_type_size_opt, seed_nonce_revelation_tip_opt,
           origination_size_opt, block_security_deposit_opt,
           endorsement_security_deposit_opt, block_reward_opt, endorsement_reward_opt,
           cost_per_byte_opt),
          (hard_storage_limit_per_operation_opt,
           hard_gas_limit_to_pay_fees_opt, max_operation_ttl_opt, protocol_revision_opt,
           frozen_account_cycles_opt, allow_collect_call_opt,
           quorum_min_opt, quorum_max_opt, min_proposal_quorum_opt,
           initial_endorsers_opt, delay_per_missing_endorsement_opt)))
    (fun ((proof_of_work_nonce_size_opt, nonce_length_opt, max_revelations_per_block_opt,
           max_operation_data_length_opt, max_proposals_per_delegate_opt,
           preserved_cycles_opt, blocks_per_cycle_opt, blocks_per_commitment_opt,
           blocks_per_roll_snapshot_opt, blocks_per_voting_period_opt,
           time_between_blocks_opt, endorsers_per_block_opt,
           hard_gas_limit_per_operation_opt, hard_gas_limit_per_block_opt,
           proof_of_work_threshold_opt, tokens_per_roll_opt,
           michelson_maximum_type_size_opt, seed_nonce_revelation_tip_opt,
           origination_size_opt, block_security_deposit_opt,
           endorsement_security_deposit_opt, block_reward_opt, endorsement_reward_opt,
           cost_per_byte_opt),
          (hard_storage_limit_per_operation_opt,
           hard_gas_limit_to_pay_fees_opt, max_operation_ttl_opt, protocol_revision_opt,
           frozen_account_cycles_opt, allow_collect_call_opt,
           quorum_min_opt, quorum_max_opt, min_proposal_quorum_opt,
           initial_endorsers_opt, delay_per_missing_endorsement_opt))
      -> { proof_of_work_nonce_size_opt; nonce_length_opt; max_revelations_per_block_opt;
           max_operation_data_length_opt; max_proposals_per_delegate_opt;
           preserved_cycles_opt; blocks_per_cycle_opt; blocks_per_commitment_opt;
           blocks_per_roll_snapshot_opt; blocks_per_voting_period_opt;
           time_between_blocks_opt; endorsers_per_block_opt;
           hard_gas_limit_per_operation_opt; hard_gas_limit_per_block_opt;
           proof_of_work_threshold_opt; tokens_per_roll_opt;
           michelson_maximum_type_size_opt; seed_nonce_revelation_tip_opt;
           origination_size_opt; block_security_deposit_opt;
           endorsement_security_deposit_opt; block_reward_opt; endorsement_reward_opt;
           cost_per_byte_opt; hard_storage_limit_per_operation_opt;
           hard_gas_limit_to_pay_fees_opt; max_operation_ttl_opt; protocol_revision_opt;
           frozen_account_cycles_opt; allow_collect_call_opt;
           quorum_min_opt; quorum_max_opt; min_proposal_quorum_opt;
           initial_endorsers_opt; delay_per_missing_endorsement_opt })
    (merge_objs
       (EzEncoding.obj24
          (opt "proof_of_work_nonce_size" int)
          (opt "nonce_length" int)
          (opt "max_revelations_per_block" int)
          (opt "max_operation_data_length" int)
          (opt "max_proposals_per_delegate" int)
          (opt "preserved_cycles" int)
          (opt "blocks_per_cycle" int)
          (opt "blocks_per_commitment" int)
          (opt "blocks_per_roll_snapshot" int)
          (opt "blocks_per_voting_period" int)
          (opt "time_between_blocks" (list int_of_string))
          (opt "endorsers_per_block" int)
          (opt "hard_gas_limit_per_operation" int64)
          (opt "hard_gas_limit_per_block" int64)
          (opt "proof_of_work_threshold" int64)
          (opt "tokens_per_roll" int64)
          (opt "michelson_maximum_type_size" int)
          (opt "seed_nonce_revelation_tip" int64)
          (opt "origination_size" int)
          (opt "block_security_deposit" int64)
          (opt "endorsement_security_deposit" int64)
          (opt "block_reward" int64)
          (opt "endorsement_reward" int64)
          (opt "cost_per_byte" int64))
       (EzEncoding.obj11
          (opt "hard_storage_limit_per_operation" int64)
          (opt "hard_gas_limit_to_pay_fees" int64)
          (opt "max_operation_ttl" int)
          (opt "protocol_revision" int)
          (opt "frozen_account_cycles" int)
          (opt "allow_collect_call" bool)
          (opt "quorum_min" int)
          (opt "quorum_max" int)
          (opt "min_proposal_quorum" int)
          (opt "initial_endorsers" int)
          (opt "delay_per_missing_endorsement" int_of_string)))
end

module Header = struct
  let content_encoding =
    obj4
      (req "command" string)
      (req "hash" string)
      (req "fitness" Fitness.encoding)
      (req "protocol_parameters" string)

  let block_header_data_encoding =
    obj8
      (dft "priority" int (-1))
      (dft "proof_of_work_nonce" string "No pow nonce for genesis")
      (opt "seed_nonce_hash" string)
      (dft "signature" Signature.encoding "No signature for genesis")
      (opt "content" content_encoding)
      (opt "hash" string)
      (opt "chain_id" string)
      (opt "protocol" string)

  let block_header_metadata_encoding =
    conv
      (fun { header_meta_baker ; header_meta_level ;
             header_meta_voting_period_kind  ;
             header_meta_nonce_hash ;
             header_meta_consumed_gas ;
             header_meta_deactivated ;
             header_meta_balance_updates } ->
        (header_meta_baker, header_meta_level,
         header_meta_voting_period_kind,
         header_meta_nonce_hash,
         header_meta_consumed_gas,
         header_meta_deactivated,
         header_meta_balance_updates))
      (fun (header_meta_baker, header_meta_level,
            header_meta_voting_period_kind,
            header_meta_nonce_hash,
            header_meta_consumed_gas,
            header_meta_deactivated,
            header_meta_balance_updates) ->
        { header_meta_baker ; header_meta_level ;
          header_meta_voting_period_kind ;
          header_meta_nonce_hash ;
          header_meta_consumed_gas ;
          header_meta_deactivated ;
          header_meta_balance_updates })
      (obj7
         (req "baker" string)
         (req "level" Level.encoding)
         (req "voting_period_kind" Voting_period_repr.kind_encoding)
         (req "nonce_hash" (option string))
         (req "consumed_gas" z_encoding)
         (req "deactivated" (list string))
         (opt "balance_updates" Balance_updates.encoding))

  let shell_encoding =
    conv
      (fun {shell_level; shell_proto; shell_predecessor; shell_timestamp;
            shell_validation_pass; shell_operations_hash; shell_fitness;
            shell_context} ->
        (shell_level, shell_proto, shell_predecessor, shell_timestamp,
         shell_validation_pass, shell_operations_hash, shell_fitness,
         shell_context))
      (fun (shell_level, shell_proto, shell_predecessor, shell_timestamp,
            shell_validation_pass, shell_operations_hash, shell_fitness,
            shell_context) ->
        {shell_level; shell_proto; shell_predecessor; shell_timestamp;
         shell_validation_pass; shell_operations_hash; shell_fitness;
         shell_context})
      (obj8
         (req "level" int)
         (req "proto" int)
         (req "predecessor" Block_hash.encoding)
         (req "timestamp" Date.encoding)
         (req "validation_pass" int)
         (req "operations_hash" Operation_list_list_hash.encoding)
         (req "fitness" Fitness.encoding)
         (req "context" Context_hash.encoding))

  let encoding =
    conv
      (fun {header_shell; header_priority; header_seed_nonce_hash;
            header_proof_of_work_nonce; header_signature; header_hash;
            header_network; header_protocol} ->
        let header_seed_nonce_hash =
          if header_seed_nonce_hash = "" then None
          else Some header_seed_nonce_hash in
        (header_shell, (
            header_priority, header_proof_of_work_nonce,
            header_seed_nonce_hash, header_signature,
            None, header_hash, header_network, header_protocol)))
      (fun (header_shell, (
           header_priority, header_proof_of_work_nonce, seed,
           header_signature, _, header_hash, header_network, header_protocol)) ->
        let header_seed_nonce_hash = match seed with None -> "" | Some s -> s in
        {header_shell; header_priority; header_seed_nonce_hash;
         header_proof_of_work_nonce; header_signature; header_hash;
         header_network; header_protocol})
      (merge_objs
         shell_encoding
         block_header_data_encoding)
end

module Error = struct
  let optional = merge_objs
      (EzEncoding.obj19
         (opt "contract_handle" string)
         (opt "contract_code" Script.expr_encoding)
         (opt "contractHandle" string)
         (opt "contractCode" Script.expr_encoding)
         (opt "contract" string)
         (opt "balance" int64)
         (opt "amount" int64)
         (opt "incompatible_protocol_version" string)
         (opt "missing_key" (list string))
         (opt "function" string)
         (opt "existing_key" (list string))
         (opt "corrupted_data" (list string))
         (opt "hash" string)
         (opt "expectedType" Script.expr_encoding)
         (opt "wrongExpression" Script.expr_encoding)
         (opt "expectedKinds" (list string))
         (opt "wrongKind" string)
         (opt "expected" string)
         (opt "provided" string))
      unit

  let encode_error e = EzEncoding.construct optional e
  let decode_error s = EzEncoding.destruct optional s

  let encoding =
    conv
      (fun {node_err_kind; node_err_id; node_err_info} -> (node_err_kind, node_err_id), node_err_info)
      (fun ((node_err_kind, node_err_id), node_err_info) -> {node_err_kind; node_err_id; node_err_info}) @@
    merge_objs
      (obj2
         (req "kind" string)
         (req "id" string))
      (conv decode_error encode_error optional)

end

module Operation = struct

  let manager_encoding =
    (obj5
       (req "source" string)
       (req "fee" int64)
       (req "counter" z_encoding)
       (req "gas_limit" z_encoding)
       (req "storage_limit" z_encoding))

  let big_map_diff_item_encoding =
    conv
      (fun {bmd_action; bmd_big_map; bmd_key_hash; bmd_key; bmd_value;
            bmd_source_big_map; bmd_destination_big_map; bmd_key_type;
            bmd_value_type}
        -> (bmd_action, bmd_big_map, bmd_key_hash, bmd_key, bmd_value,
            bmd_source_big_map, bmd_destination_big_map, bmd_key_type,
            bmd_value_type))
      (fun (bmd_action, bmd_big_map, bmd_key_hash, bmd_key, bmd_value,
            bmd_source_big_map, bmd_destination_big_map, bmd_key_type,
            bmd_value_type)
        -> {bmd_action; bmd_big_map; bmd_key_hash; bmd_key; bmd_value;
            bmd_source_big_map; bmd_destination_big_map; bmd_key_type;
            bmd_value_type})
      (obj9
         (opt "action" string)
         (opt "big_map" z_encoding)
         (opt "key_hash" string)
         (opt "key" Script.expr_encoding)
         (opt "value" Script.expr_encoding)
         (opt "source_big_map" z_encoding)
         (opt "destination_big_map" z_encoding)
         (opt "key_type" Script.expr_encoding)
         (opt "value_type" Script.expr_encoding))

  let op_metadata_encoding =
    conv
      (fun { meta_op_status ; meta_op_balance_updates ;
             meta_op_originated_contracts ; meta_op_consumed_gas ;
             meta_op_storage_size_diff ; meta_op_storage ;
             meta_op_delegate ; meta_op_slots ; meta_op_paid_storage_size_diff ;
             meta_op_big_map_diff ; meta_op_allocated_destination_contract;
             meta_op_errors; meta_op_result} ->
        (meta_op_status, meta_op_balance_updates,
         meta_op_originated_contracts, meta_op_consumed_gas,
         meta_op_storage_size_diff,
         meta_op_storage, meta_op_delegate, meta_op_slots,
         meta_op_paid_storage_size_diff, meta_op_errors,
         meta_op_big_map_diff, meta_op_allocated_destination_contract,
         meta_op_result))
      (fun ((meta_op_status, meta_op_balance_updates,
             meta_op_originated_contracts, meta_op_consumed_gas,
             meta_op_storage_size_diff,
             meta_op_storage, meta_op_delegate, meta_op_slots,
             meta_op_paid_storage_size_diff,
             meta_op_errors, meta_op_big_map_diff,
             meta_op_allocated_destination_contract, meta_op_result)) ->
        { meta_op_status ;
          meta_op_balance_updates ;
          meta_op_originated_contracts ;
          meta_op_consumed_gas ;
          meta_op_storage_size_diff ;
          meta_op_storage ;
          meta_op_delegate ;
          meta_op_paid_storage_size_diff ;
          meta_op_slots ;
          meta_op_big_map_diff ;
          meta_op_allocated_destination_contract;
          meta_op_errors;
          meta_op_result})
      (EzEncoding.obj13
         (opt "status" string)
         (opt "balance_updates" Balance_updates.encoding)
         (dft "originated_contracts" (list string) [])
         (dft "consumed_gas" z_encoding Z.zero)
         (opt "storage_size" int64)
         (opt "storage" Script.expr_encoding)
         (opt "delegate" string)
         (opt "slots" (list int))
         (dft "paid_storage_size_diff" z_encoding Z.zero)
         (dft "errors" (list Error.encoding) [])
         (opt "big_map_diff" (list big_map_diff_item_encoding))
         (dft "allocated_destination_contract" bool false)
         (opt "result" (obj1 (req "kind" string))))

  let parameter_encoding =
    obj2
      (opt "entrypoint" string)
      (opt "value" Script.expr_str_encoding)

  let internal_transaction_result_encoding =
    conv
      (fun { node_tr_src ; node_tr_fee = _ ; node_tr_counter = _ ;
             node_tr_gas_limit = _ ; node_tr_storage_limit = _;
             node_tr_amount ; node_tr_dst ; node_tr_collect_fee_gas ;
             node_tr_collect_pk; node_tr_entrypoint; node_tr_parameters ; node_tr_metadata } ->
        let result = match node_tr_metadata with
          | None -> None
          | Some meta -> meta.manager_meta_operation_result in
        let node_tr_collect = match node_tr_collect_fee_gas, node_tr_collect_pk with
          | None, None -> None
          | _ -> Some (node_tr_collect_fee_gas, node_tr_collect_pk) in
        ((), node_tr_src, 0, node_tr_amount, node_tr_dst, None,
         node_tr_collect, (node_tr_entrypoint, node_tr_parameters), result))
      (fun ((), node_tr_src, _nonce, node_tr_amount, node_tr_dst,
            collect_fee_gas0, node_tr_collect, (node_tr_entrypoint, node_tr_parameters), result) ->
        let node_tr_collect_fee_gas, node_tr_collect_pk =  match node_tr_collect with
          | Some (collect_fee_gas, collect_pk) -> collect_fee_gas, collect_pk
          | _ -> collect_fee_gas0, None in
        let node_tr_metadata =
          Some
            { manager_meta_balance_updates = None ;
              manager_meta_operation_result = result ;
              manager_meta_internal_operation_results = [] } in
        { node_tr_src ; node_tr_fee = 0L ; node_tr_counter = Z.minus_one ;
          node_tr_gas_limit = Z.minus_one ; node_tr_storage_limit = Z.minus_one;
          node_tr_amount ; node_tr_dst; node_tr_collect_fee_gas; node_tr_collect_pk;
          node_tr_entrypoint; node_tr_parameters ; node_tr_metadata })
      (obj9
         (req "kind" (constant "transaction"))
         (req "source" string)
         (req "nonce" int)
         (req "amount" int64)
         (req "destination" string)
         (opt "collect_fee_gas" z_encoding)
         (opt "collect_call" (obj2 (opt "fee_gas" z_encoding) (opt "reveal_public_key" string)))
         (dft "parameters" parameter_encoding (None, None))
         (opt "result" op_metadata_encoding))

  let internal_origination_result_encoding =
    conv
      (fun { node_or_src ; node_or_fee = _; node_or_counter = _ ;
             node_or_gas_limit = _ ; node_or_storage_limit = _ ;
             node_or_script ;
             node_or_balance ; node_or_metadata ; _ } ->
        let result = match node_or_metadata with
          | None -> None
          | Some meta -> meta.manager_meta_operation_result in
        ((), node_or_src, ~-1, None, node_or_balance,
         None, None, None, node_or_script, result))
      (fun ((), node_or_src, _nonce, _node_or_manager,
            node_or_balance, _node_or_spendable, _node_or_delegatable,
            _node_or_delegate, node_or_script, result) ->
        let node_or_metadata =
          Some
            { manager_meta_balance_updates = None ;
              manager_meta_operation_result = result ;
              manager_meta_internal_operation_results = [] } in
        { node_or_src ; node_or_fee = 0L ;
          node_or_counter = Z.minus_one ;
          node_or_gas_limit = Z.minus_one ;
          node_or_storage_limit = Z.minus_one ;
          node_or_script ; node_or_balance ;
          node_or_metadata })
      (obj10
         (req "kind" (constant "origination"))
         (req "source" string)
         (req "nonce" int)
         (opt "manager_pubkey" string)
         (req "balance" int64)
         (opt "spendable" bool)
         (opt "delegatable" bool)
         (opt "delegate" string)
         (opt "script" Script.script_str_encoding)
         (opt "result" op_metadata_encoding))

  let internal_delegation_result_encoding =
    conv
      (fun { node_del_src ;
             node_del_fee = _ ;
             node_del_counter = _ ;
             node_del_gas_limit = _ ;
             node_del_storage_limit = _ ;
             node_del_delegate ;
             node_del_metadata} ->
        let node_del_delegate =
          if node_del_delegate = "" then None else Some node_del_delegate in
        let result = match node_del_metadata with
            None -> None
          | Some m -> m.manager_meta_operation_result
        in
        ((), node_del_src, ~-1, node_del_delegate, result))
      (fun ((), node_del_src, _nonce, node_del_delegate, result) ->
         let node_del_delegate = Dune_utils.unopt node_del_delegate ~default:"" in
         { node_del_src ;
           node_del_fee = Int64.minus_one ;
           node_del_counter = Z.minus_one ;
           node_del_gas_limit = Z.minus_one ;
           node_del_storage_limit = Z.minus_one ;
           node_del_delegate ;
           node_del_metadata = Some
               {manager_meta_balance_updates = None;
                manager_meta_operation_result = result;
                manager_meta_internal_operation_results = []}  })
      (obj5
         (req "kind" (constant "delegation"))
         (req "source" string)
         (req "nonce" int)
         (opt "delegate" string)
         (opt "result" op_metadata_encoding))

  let internal_reveal_result_encoding =
    conv
      (fun { node_rvl_src ;
             node_rvl_fee = _ ; node_rvl_counter = _ ;
             node_rvl_gas_limit = _ ; node_rvl_storage_limit = _ ;
             node_rvl_pubkey ;
             node_rvl_metadata} ->
        let result = match node_rvl_metadata with
            None -> None
          | Some m -> m.manager_meta_operation_result
        in
        ((), node_rvl_src, ~-1, node_rvl_pubkey, result))
      (fun ((), node_rvl_src, _none, node_rvl_pubkey, result) ->
         { node_rvl_src ;
           node_rvl_fee = Int64.minus_one ;
           node_rvl_counter = Z.minus_one ;
           node_rvl_gas_limit = Z.minus_one ;
           node_rvl_storage_limit = Z.minus_one ;
           node_rvl_pubkey;
           node_rvl_metadata = Some
               {manager_meta_balance_updates = None;
                manager_meta_operation_result = result;
                manager_meta_internal_operation_results = []}})
      (obj5
         (req "kind" (constant "reveal"))
         (req "source" string)
         (req "nonce" int)
         (req "public_key" string)
         (opt "result" op_metadata_encoding))

  let internal_operation_result_encoding =
    union [
      case internal_transaction_result_encoding
        (function NTransaction tr -> Some tr | _ -> None)
        (fun tr -> NTransaction tr) ;
      case internal_origination_result_encoding
        (function NOrigination orig -> Some orig | _ -> None)
        (fun orig -> NOrigination orig) ;
      case internal_delegation_result_encoding
        (function NDelegation del -> Some del | _ -> None)
        (fun del -> NDelegation del) ;
      case internal_reveal_result_encoding
        (function NReveal rvl -> Some rvl | _ -> None)
        (fun rvl -> NReveal rvl) ;
    ]

  let manager_metadata_encoding =
    conv
      (fun { manager_meta_balance_updates ;
             manager_meta_operation_result ;
             manager_meta_internal_operation_results } ->
        manager_meta_balance_updates,
        manager_meta_operation_result,
	manager_meta_internal_operation_results)
      (fun (manager_meta_balance_updates,
            manager_meta_operation_result,
            manager_meta_internal_operation_results) -> (* not used ? *)
        { manager_meta_balance_updates ;
          manager_meta_operation_result ;
          manager_meta_internal_operation_results })
      (obj3
         (opt "balance_updates" Balance_updates.encoding)
         (opt "operation_result" op_metadata_encoding)
         (dft "internal_operation_results"
            (list internal_operation_result_encoding) []))

  let transaction_encoding =
    conv
      (fun { node_tr_src ; node_tr_fee ; node_tr_counter ;
             node_tr_gas_limit ; node_tr_storage_limit;
             node_tr_amount; node_tr_dst ; node_tr_collect_fee_gas ;
             node_tr_collect_pk; node_tr_entrypoint; node_tr_parameters ; node_tr_metadata } ->
        let node_tr_collect = match node_tr_collect_fee_gas, node_tr_collect_pk with
          | None, None -> None
          | _ -> Some (node_tr_collect_fee_gas, node_tr_collect_pk) in
        ((node_tr_src, node_tr_fee, node_tr_counter, node_tr_gas_limit,
          node_tr_storage_limit),
         ((), node_tr_amount, node_tr_dst, None, node_tr_collect,
          (node_tr_entrypoint, node_tr_parameters), node_tr_metadata )))
      (fun (
         ((node_tr_src, node_tr_fee, node_tr_counter, node_tr_gas_limit,
           node_tr_storage_limit),
          ((), node_tr_amount, node_tr_dst, collect_fee_gas0,
           node_tr_collect, (node_tr_entrypoint, node_tr_parameters), node_tr_metadata))) ->
         let node_tr_collect_fee_gas, node_tr_collect_pk = match node_tr_collect with
           | Some (collect_fee_gas, collect_pk) -> collect_fee_gas, collect_pk
           | _ -> collect_fee_gas0, None in
         { node_tr_src ; node_tr_fee ; node_tr_counter ;
           node_tr_gas_limit ; node_tr_storage_limit ;
           node_tr_amount ; node_tr_dst; node_tr_collect_fee_gas; node_tr_collect_pk;
           node_tr_entrypoint; node_tr_parameters ; node_tr_metadata })
      (merge_objs
         manager_encoding
         (obj7
            (req "kind" (constant "transaction"))
            (req "amount" int64)
            (req "destination" string)
            (opt "collect_fee_gas" z_encoding)
            (opt "collect_call" (obj2 (opt "fee_gas" z_encoding) (opt "reveal_public_key" string)))
            (dft "parameters" parameter_encoding (None, None))
            (opt "metadata" manager_metadata_encoding)))

  let origination_encoding =
    (conv
       (fun { node_or_src ; node_or_fee ; node_or_counter ; node_or_gas_limit ;
              node_or_storage_limit ; node_or_script ;
              node_or_balance ; node_or_metadata ; _ } ->
         ((node_or_src, node_or_fee, node_or_counter, node_or_gas_limit,
           node_or_storage_limit),
          ((), None, node_or_balance,
           None, None,
           None, node_or_script, node_or_metadata )))
       (fun ((node_or_src, node_or_fee, node_or_counter, node_or_gas_limit, node_or_storage_limit),
             ((), _node_or_manager, node_or_balance,
              _node_or_spendable, _node_or_delegatable, _node_or_delegate,
              node_or_script, node_or_metadata)) ->
         { node_or_src ; node_or_fee ; node_or_counter ;
           node_or_gas_limit ; node_or_storage_limit ;
           node_or_script ; node_or_balance ; node_or_metadata }))
      (merge_objs
         manager_encoding
         (obj8
            (req "kind" (constant "origination"))
            (* change during proto update, we need to do this to
               be able to recrawl the chain from scratch *)
            (opt "manager_pubkey" string)
            (req "balance" int64)
            (opt "spendable" bool)
            (opt "delegatable" bool)
            (opt "delegate" string)
            (opt "script" Script.script_str_encoding)
            (* ocp field *)
            (opt "metadata" manager_metadata_encoding)))

  let delegation_encoding =
    (conv
       (fun { node_del_src ; node_del_fee ; node_del_counter ; node_del_gas_limit ;
              node_del_storage_limit ; node_del_delegate ;
              node_del_metadata } ->
         let node_del_delegate =
           if node_del_delegate = "" then None else Some node_del_delegate in
         ((node_del_src, node_del_fee, node_del_counter, node_del_gas_limit,
           node_del_storage_limit), ((), node_del_delegate, node_del_metadata)))
       (fun ((node_del_src, node_del_fee, node_del_counter, node_del_gas_limit,
              node_del_storage_limit), ((), node_del_delegate, node_del_metadata)) ->
         let node_del_delegate = Dune_utils.unopt node_del_delegate ~default:"" in
         { node_del_src ; node_del_fee ; node_del_counter ; node_del_gas_limit ;
           node_del_storage_limit ; node_del_delegate ; node_del_metadata }))
      (merge_objs
         manager_encoding
         (obj3
            (req "kind" (constant "delegation"))
            (opt "delegate" string)
            (opt "metadata" manager_metadata_encoding)))

  let reveal_encoding =
    (conv
       (fun { node_rvl_src ; node_rvl_fee ; node_rvl_counter ; node_rvl_gas_limit ;
              node_rvl_storage_limit ; node_rvl_pubkey ;
              node_rvl_metadata } ->
         ((node_rvl_src, node_rvl_fee, node_rvl_counter, node_rvl_gas_limit,
           node_rvl_storage_limit), ((), node_rvl_pubkey, node_rvl_metadata)))
       (fun ((node_rvl_src, node_rvl_fee, node_rvl_counter,
              node_rvl_gas_limit,
              node_rvl_storage_limit),
             ((), node_rvl_pubkey, node_rvl_metadata)) ->
         { node_rvl_src ; node_rvl_fee ; node_rvl_counter ; node_rvl_gas_limit ;
           node_rvl_storage_limit ; node_rvl_pubkey; node_rvl_metadata }))
      (merge_objs
         manager_encoding
         (obj3
            (req "kind" (constant "reveal"))
            (req "public_key" string)
            (opt "metadata" manager_metadata_encoding)))



  let endorsement_encoding =
    (conv
       (fun { node_endorse_block_level ; node_endorse_metadata } ->
          ((), node_endorse_block_level, node_endorse_metadata, None))
       (fun ((), node_endorse_block_level, node_endorse_metadata,
             _node_endorse_slot) ->
         { node_endorse_block_level ; node_endorse_metadata }))
      (obj4
         (req "kind" (constant "endorsement"))
         (req "level" int)
         (opt "metadata" op_metadata_encoding)
         (opt "slot" int))

  let double_endorsement_evidence_op_encoding =
    conv
      (fun { ndee_branch ; ndee_level ; ndee_signature } ->
         (ndee_branch, ((), ndee_level), ndee_signature))
      (fun (ndee_branch, ((), ndee_level), ndee_signature) ->
         { ndee_branch ; ndee_level ; ndee_signature })
      (obj3
         (req "branch" string)
         (req "operations"
            (obj2
               (req "kind" (constant "endorsement"))
               (req "level" int)))
         (req "signature" string))

  let activation_encoding =
    (conv
       (fun { node_act_pkh; node_act_secret ; node_act_metadata } ->
          ((), node_act_pkh, node_act_secret, node_act_metadata))
       (fun ((), node_act_pkh, node_act_secret, node_act_metadata) ->
          { node_act_pkh ; node_act_secret ; node_act_metadata }))
      (obj4
         (req "kind" (constant "activate_account"))
         (req "pkh" string)
         (req "secret" string)
         (opt "metadata" op_metadata_encoding))

  let proposal_encoding =
    (conv
       (fun { node_prop_src ; node_prop_voting_period ;
              node_prop_proposals ; node_prop_metadata } ->
         ((), node_prop_src, node_prop_voting_period, node_prop_proposals,
          node_prop_metadata ))
       (fun ((), node_prop_src, node_prop_voting_period, node_prop_proposals,
             node_prop_metadata) ->
         { node_prop_src ; node_prop_voting_period ; node_prop_proposals ;
           node_prop_metadata }))
      (obj5
         (req "kind" (constant "proposals"))
         (req "source" string)
         (req "period" int32)
         (req "proposals" (list string))
         (opt "metadata" op_metadata_encoding))

  let ballot_encoding =
    (conv
       (fun { node_ballot_src ; node_ballot_voting_period ;
              node_ballot_proposal ; node_ballot_vote ; node_ballot_metadata } ->
         let node_ballot_vote = Dune_utils.string_of_ballot_vote node_ballot_vote in
         ((), node_ballot_src, node_ballot_voting_period,
          node_ballot_proposal, node_ballot_vote, node_ballot_metadata))
       (fun ((), node_ballot_src, node_ballot_voting_period,
             node_ballot_proposal, node_ballot_vote, node_ballot_metadata) ->
         let node_ballot_vote = Dune_utils.ballot_of_string node_ballot_vote in
         { node_ballot_src ; node_ballot_voting_period ;
           node_ballot_proposal ; node_ballot_vote ; node_ballot_metadata }))
      (obj6
         (req "kind" (constant "ballot"))
         (req "source" string)
         (req "period" int32)
         (req "proposal" string)
         (req "ballot" string)
         (opt "metadata" op_metadata_encoding))

  let activate_encoding =
    (obj3
       (req "kind" (constant "activate_protocol"))
       (req "hash" string)
       (opt "metadata" op_metadata_encoding))

  let activate_testnet_encoding =
    (obj3
       (req "kind" (constant "activate_test_protocol"))
       (req "hash" string)
       (opt "metadata" op_metadata_encoding))

  let seed_nonce_revelation_encoding =
    (conv
       (fun { node_seed_level; node_seed_nonce ; node_seed_metadata } ->
          ((), node_seed_level, node_seed_nonce,
           node_seed_metadata ))
       (fun ((), node_seed_level, node_seed_nonce, node_seed_metadata) ->
          { node_seed_level; node_seed_nonce ; node_seed_metadata} ))
      (obj4
         (req "kind" (constant "seed_nonce_revelation"))
         (req "level" int)
         (req "nonce" string)
         (opt "metadata" op_metadata_encoding))

  let double_endorsement_evidence_encoding =
    (conv
       (fun { node_double_endorsement1; node_double_endorsement2 ;
              node_double_endorsement_metadata } ->
         ((), node_double_endorsement1, node_double_endorsement2,
          node_double_endorsement_metadata))
       (fun ((), node_double_endorsement1, node_double_endorsement2,
             node_double_endorsement_metadata) ->
         { node_double_endorsement1; node_double_endorsement2 ;
           node_double_endorsement_metadata}))
      (obj4
         (req "kind" (constant "double_endorsement_evidence"))
         (req "op1" double_endorsement_evidence_op_encoding)
         (req "op2" double_endorsement_evidence_op_encoding)
         (opt "metadata" op_metadata_encoding))

  let double_baking_evidence_encoding =
    (conv
       (fun { node_double_bh1; node_double_bh2 ; node_double_bh_metadata }  ->
          ((), node_double_bh1, node_double_bh2, node_double_bh_metadata))
       (fun ((), node_double_bh1, node_double_bh2, node_double_bh_metadata) ->
          { node_double_bh1; node_double_bh2; node_double_bh_metadata } ))
      (obj4
         (req "kind" (constant "double_baking_evidence"))
         (req "bh1" (Header.encoding))
         (req "bh2" (Header.encoding))
         (opt "metadata" op_metadata_encoding))

  let activate_protocol_encoding = conv
    (fun {node_acp_src; node_acp_fee; node_acp_counter; node_acp_gas_limit;
          node_acp_storage_limit; node_acp_level; node_acp_protocol;
          node_acp_parameters; node_acp_metadata }
      -> ((node_acp_src, node_acp_fee, node_acp_counter, node_acp_gas_limit,
           node_acp_storage_limit),
          ((), (), node_acp_level, node_acp_protocol, node_acp_parameters,
           node_acp_metadata)))
    (fun ((node_acp_src, node_acp_fee, node_acp_counter, node_acp_gas_limit,
           node_acp_storage_limit),
          ((), (), node_acp_level, node_acp_protocol, node_acp_parameters,
           node_acp_metadata))
    -> {node_acp_src; node_acp_fee; node_acp_counter; node_acp_gas_limit;
          node_acp_storage_limit; node_acp_level; node_acp_protocol;
          node_acp_parameters; node_acp_metadata })
    (merge_objs
       manager_encoding
       (obj6
          (req "kind" (constant "dune_manager_operation"))
          (req "dune_kind" (constant "dune_activate_protocol"))
          (req "level" int)
          (opt "protocol" string)
          (opt "protocol_parameters" Protocol_param.encoding)
          (opt "metadata" manager_metadata_encoding)))

  let manage_accounts_encoding = conv
      (fun {node_macs_src; node_macs_fee; node_macs_counter; node_macs_gas_limit;
            node_macs_storage_limit; node_macs_bytes; node_macs_metadata}
        -> ((node_macs_src, node_macs_fee, node_macs_counter, node_macs_gas_limit,
             node_macs_storage_limit),
            ((), (), node_macs_bytes, node_macs_metadata)))
      (fun ((node_macs_src, node_macs_fee, node_macs_counter, node_macs_gas_limit,
             node_macs_storage_limit),
            ((), (), node_macs_bytes, node_macs_metadata))
        -> {node_macs_src; node_macs_fee; node_macs_counter; node_macs_gas_limit;
            node_macs_storage_limit; node_macs_bytes; node_macs_metadata})
      (merge_objs
         manager_encoding
         (obj4
            (req "kind" (constant "dune_manager_operation"))
            (req "dune_kind" (constant "dune_manage_accounts"))
            (req "bytes" hex_encoding)
            (opt "metadata" manager_metadata_encoding)))

    let mao_encoding = conv
      (fun {node_mao_maxrolls; node_mao_admin; node_mao_white_list;
            node_mao_delegation}
        -> (node_mao_maxrolls, node_mao_admin, node_mao_white_list,
            node_mao_delegation))
      (fun (node_mao_maxrolls, node_mao_admin, node_mao_white_list,
            node_mao_delegation)
        -> {node_mao_maxrolls; node_mao_admin; node_mao_white_list;
            node_mao_delegation})
      (obj4
         (opt "maxrolls" (option int))
         (opt "admin" (option string))
         (opt "white_list" (list string))
         (opt "delegation" bool))

  let manage_account_options = conv
      (fun mao ->
         let bs = Json_repr_bson.Json_encoding.construct mao_encoding mao in
         let b = Json_repr_bson.bson_to_bytes bs in
         let n = Forge.forge_int32 (Bytes.length b) in
         Hex.show @@ Hex.of_bytes @@ Bytes.concat Bytes.empty [n; b])
      (fun s ->
         let bs = Json_repr_bson.bytes_to_bson ~copy:false @@
           Hex.to_bytes (`Hex (String.sub s 8 ((String.length s) - 8))) in
         Json_repr_bson.Json_encoding.destruct mao_encoding bs)
      string

  let manage_account_encoding = conv
      (fun {node_mac_src; node_mac_fee; node_mac_counter; node_mac_gas_limit;
            node_mac_storage_limit; node_mac_target; node_mac_options; node_mac_metadata}
        -> ((node_mac_src, node_mac_fee, node_mac_counter, node_mac_gas_limit,
             node_mac_storage_limit),
            ((), (), node_mac_target, node_mac_options, node_mac_metadata)))
      (fun ((node_mac_src, node_mac_fee, node_mac_counter, node_mac_gas_limit,
             node_mac_storage_limit),
            ((), (), node_mac_target, node_mac_options, node_mac_metadata))
        -> {node_mac_src; node_mac_fee; node_mac_counter; node_mac_gas_limit;
            node_mac_storage_limit; node_mac_target; node_mac_options; node_mac_metadata})
      (merge_objs
         manager_encoding
         (obj5
            (req "kind" (constant "dune_manager_operation"))
            (req "dune_kind" (constant "dune_manage_account"))
            (opt "target" (obj2 (req "target" string) (opt "signature" string)))
            (req "options" manage_account_options)
            (opt "metadata" manager_metadata_encoding)))

  let clear_delegations_encoding = conv
      (fun {node_cld_src; node_cld_fee; node_cld_counter; node_cld_gas_limit;
            node_cld_storage_limit; node_cld_metadata}
        -> ((node_cld_src, node_cld_fee, node_cld_counter, node_cld_gas_limit,
             node_cld_storage_limit),
            ((), (), node_cld_metadata)))
      (fun ((node_cld_src, node_cld_fee, node_cld_counter, node_cld_gas_limit,
             node_cld_storage_limit),
            ((), (), node_cld_metadata))
        -> {node_cld_src; node_cld_fee; node_cld_counter; node_cld_gas_limit;
            node_cld_storage_limit; node_cld_metadata})
      (merge_objs
         manager_encoding
         (obj3
            (req "kind" (constant "dune_manager_operation"))
            (req "dune_kind" (constant "dune_clear_delegations"))
            (opt "metadata" manager_metadata_encoding)))

  let contents_and_result_encoding =
    union [
      (* Anonymous *)
      case seed_nonce_revelation_encoding
        (function
          | NSeed_nonce_revelation s -> Some s
          | _ -> None)
        (fun s -> NSeed_nonce_revelation s) ;
      case (activation_encoding)
        (function
          | NActivation a -> Some a
          | _ -> None)
        (fun act -> NActivation act) ;
      case double_baking_evidence_encoding
        (function
          | NDouble_baking_evidence dbe -> Some dbe
          | _ -> None)
        (fun evidence -> NDouble_baking_evidence evidence) ;
      case double_endorsement_evidence_encoding
        (function
          | NDouble_endorsement_evidence dee -> Some dee
          | _ -> None)
        (fun dbe -> NDouble_endorsement_evidence dbe) ;
      (*  Signed > Amendment *)
      case proposal_encoding
        (function
          | NProposals prop -> Some prop
          | _ -> None)
        (fun prop -> NProposals prop);
      case ballot_encoding
        (function
          | NBallot ballot -> Some ballot
          | _ -> None)
        (fun ballot -> NBallot ballot);
      (* Signed > Manager *)
      case reveal_encoding
        (function
          | NReveal rvl -> Some rvl
          | _ -> None)
        (fun rvl -> NReveal rvl) ;
      case transaction_encoding
        (function
          | NTransaction tr -> Some tr
          | _ -> None)
        (fun tr -> NTransaction tr) ;
      case origination_encoding
        (function
          | NOrigination ori -> Some ori
          | _ -> None)
        (fun ori -> NOrigination ori) ;
      case delegation_encoding
        (function
          | NDelegation del -> Some del
          | _ -> None)
        (fun del -> NDelegation del) ;
      case endorsement_encoding
        (function
          | NEndorsement e -> Some e
          | _ -> None)
        (fun c -> NEndorsement c) ;
      (* Signed > Dictator *)
      case activate_encoding
        (function _ -> None)
        (fun _hash -> NActivate) ;
      case activate_testnet_encoding
        (function _ -> None)
        (fun _hash -> NActivate) ;
      (* Dune manager operation *)
      case activate_protocol_encoding
        (function
          | NActivate_protocol a -> Some a
          | _ -> None)
        (fun a -> NActivate_protocol a) ;
      case manage_accounts_encoding
        (function
          | NManage_accounts m -> Some m
          | _ -> None)
        (fun m -> NManage_accounts m);
      case manage_account_encoding
        (function
          | NManage_account m -> Some m
          | _ -> None)
        (fun m -> NManage_account m);
      case clear_delegations_encoding
        (function
          | NClearDelegations m -> Some m
          | _ -> None)
        (fun m -> NClearDelegations m);
    ]

  let contents_and_result_list_encoding =
    (list contents_and_result_encoding)

  let operation_data_and_receipt_encoding =
    (obj2
       (req "contents" contents_and_result_list_encoding)
       (opt "signature" Signature.encoding))

  let shell_header_encoding = obj1 (req "branch" Block_hash.encoding)

  let operation_encoding =
    conv
      (fun { node_op_protocol ; node_op_chain_id ; node_op_hash ; node_op_branch ;
             node_op_contents ; node_op_signature } ->
        ((node_op_protocol, node_op_chain_id, node_op_hash),
         (node_op_branch, (node_op_contents, node_op_signature))))

      (fun ((node_op_protocol, node_op_chain_id, node_op_hash),
            (node_op_branch,
             (node_op_contents, node_op_signature))) ->
        { node_op_protocol ; node_op_chain_id ; node_op_hash ; node_op_branch ;
          node_op_contents ; node_op_signature  })
      (merge_objs
         (obj3
            (req "protocol" string)
            (req "chain_id" Chain_id.encoding)
            (req "hash" Operation_hash.encoding))
         (merge_objs
            shell_header_encoding
            operation_data_and_receipt_encoding))

  let encoding = list @@ list operation_encoding
end

module Test_chain_status = struct
  let encoding =
    union [
      case
        (obj1 (req "status" (constant "not_running")))
        (function CSNot_running -> Some () | _ -> None)
        (fun () -> CSNot_running) ;
      case
        (obj3
           (req "status" (constant "forking"))
           (req "protocol" Protocol_hash.encoding)
           (req "expiration" Time.encoding))
        (function
          | CSForking { protocol ; expiration } ->
            Some ((), protocol, expiration)
          | _ -> None)
        (fun ((), protocol, expiration) ->
           CSForking { protocol ; expiration }) ;
      case
        (obj5
           (req "status" (constant "running"))
           (req "chain_id" Chain_id.encoding)
           (req "genesis" Block_hash.encoding)
           (req "protocol" Protocol_hash.encoding)
           (req "expiration" Time.encoding))
        (function
          | CSRunning { chain_id ; genesis ; protocol ; expiration } ->
            Some ((), chain_id, genesis, protocol, expiration)
          | _ -> None)
        (fun ((), chain_id, genesis, protocol, expiration) ->
           CSRunning { chain_id ; genesis ; protocol ; expiration }) ;
    ]
end

module Block = struct

  let operation_list_quota_encoding =
    (obj2
       (req "max_size" int)
       (opt "max_op" int))

  let block_metadata_encoding =
    conv
      (fun { meta_protocol ; meta_next_protocol ; meta_test_chain_status ;
             meta_max_operations_ttl ; meta_max_operation_data_length ;
             meta_max_block_header_length ; meta_max_operation_list_length ;
             meta_header}
        ->
          ((meta_protocol, meta_next_protocol, meta_test_chain_status,
            meta_max_operations_ttl, meta_max_operation_data_length,
            meta_max_block_header_length,
            meta_max_operation_list_length), meta_header))
      (fun ((meta_protocol, meta_next_protocol, meta_test_chain_status,
             meta_max_operations_ttl, meta_max_operation_data_length,
             meta_max_block_header_length,
             meta_max_operation_list_length), meta_header) ->
        { meta_protocol ; meta_next_protocol ; meta_test_chain_status ;
          meta_max_operations_ttl ; meta_max_operation_data_length ;
          meta_max_block_header_length ; meta_max_operation_list_length ;
          meta_header })
      (merge_objs
         (obj7
            (req "protocol" string)
            (req "next_protocol" string)
            (req "test_chain_status" Test_chain_status.encoding)
            (req "max_operations_ttl" int)
            (req "max_operation_data_length" int)
            (req "max_block_header_length" int)
            (req "max_operation_list_length"
               (list operation_list_quota_encoding)))
         Header.block_header_metadata_encoding)

  let genesis_block_metadata_encoding =
    conv
      (fun { meta_protocol ; meta_next_protocol ; meta_test_chain_status ;
             meta_max_operations_ttl ; meta_max_operation_data_length ;
             meta_max_block_header_length ; meta_max_operation_list_length ;
             _ }
        ->
          (meta_protocol, meta_next_protocol, meta_test_chain_status,
           meta_max_operations_ttl, meta_max_operation_data_length,
           meta_max_block_header_length,
           meta_max_operation_list_length))
      (fun (meta_protocol, meta_next_protocol, meta_test_chain_status,
            meta_max_operations_ttl, meta_max_operation_data_length,
            meta_max_block_header_length,
            meta_max_operation_list_length) ->
        { meta_protocol ; meta_next_protocol ; meta_test_chain_status ;
          meta_max_operations_ttl ; meta_max_operation_data_length ;
          meta_max_block_header_length ; meta_max_operation_list_length ;
          meta_header = {
            header_meta_baker = "God" ;
            header_meta_level = {
              node_lvl_level = 0 ;
              node_lvl_level_position = 0 ;
              node_lvl_cycle = 0 ;
              node_lvl_cycle_position = 0 ;
              node_lvl_voting_period = 0 ;
              node_lvl_voting_period_position = 0 ;
              node_lvl_expected_commitment = false ;
            } ;
            header_meta_nonce_hash = None;
            header_meta_consumed_gas = Z.zero;
            header_meta_deactivated = [];
            header_meta_voting_period_kind = NProposal ;
            header_meta_balance_updates = None
          }
        })
      (obj7
         (req "protocol" string)
         (req "next_protocol" string)
         (req "test_chain_status" Test_chain_status.encoding)
         (req "max_operations_ttl" int)
         (req "max_operation_data_length" int)
         (req "max_block_header_length" int)
         (req "max_operation_list_length"
            (list operation_list_quota_encoding)))

  let encoding =
    conv
      (fun { node_protocol ; node_chain_id ; node_hash ; node_header ; node_metadata ; node_operations } ->
         (node_protocol, node_chain_id, node_hash, node_header, node_metadata, node_operations))
      (fun (node_protocol, node_chain_id, node_hash, node_header, node_metadata, node_operations) ->
         { node_protocol ; node_chain_id ; node_hash ; node_header ; node_metadata ; node_operations })
      (obj6
         (req "protocol" string)
         (req "chain_id" Chain_id.encoding)
         (req "hash" Block_hash.encoding)
         (req "header" Header.encoding)
         (req "metadata" block_metadata_encoding)
         (req "operations"
            (list (list Operation.operation_encoding))))

  let genesis_encoding =
    conv
      (fun { node_protocol ; node_chain_id ; node_hash ; node_header ; node_metadata ; node_operations } ->
         (node_protocol, node_chain_id, node_hash, node_header, node_metadata, node_operations))
      (fun (node_protocol, node_chain_id, node_hash, node_header, node_metadata, node_operations) ->
         { node_protocol ; node_chain_id ; node_hash ; node_header ; node_metadata ; node_operations })
      (obj6
         (req "protocol" string)
         (req "chain_id" Chain_id.encoding)
         (req "hash" Block_hash.encoding)
         (req "header" Header.encoding)
         (req "metadata" genesis_block_metadata_encoding)
         (req "operations"
            (list (list Operation.operation_encoding))))
end

module Account = struct
  let athen_encoding =
    conv
      (fun {node_acc_manager; node_acc_balance; node_acc_spendable;
            node_acc_delegatable; node_acc_delegate;
            node_acc_script; node_acc_storage; node_acc_counter}
        -> let manager = match node_acc_manager with
            | None -> "" | Some manager -> manager in
          (manager, node_acc_balance, node_acc_spendable,
            (node_acc_delegatable, node_acc_delegate),
            node_acc_script, node_acc_storage, node_acc_counter))
      (fun (manager, node_acc_balance, node_acc_spendable,
            (node_acc_delegatable, node_acc_delegate),
            node_acc_script, node_acc_storage, node_acc_counter)
        -> let node_acc_manager = Some manager in
          {node_acc_manager; node_acc_balance; node_acc_spendable;
           node_acc_delegatable; node_acc_delegate;
           node_acc_script; node_acc_storage; node_acc_counter})
      (obj7
         (req "manager" string)
         (req "balance" int64)
         (opt "spendable" bool)
         (req "delegate"
            (obj2
               (opt "setable" bool)
               (opt "value" string)))
         (opt "script" Script.script_encoding)
         (opt "storage" Script.expr_encoding)
         (opt "counter" z_encoding))

  let babylon_encoding =
    conv
      (fun {node_acc_balance; node_acc_delegate; node_acc_script; node_acc_storage;
            node_acc_counter; _}
        -> (node_acc_balance, node_acc_delegate, node_acc_script, node_acc_storage,
            node_acc_counter))
      (fun (node_acc_balance, node_acc_delegate, node_acc_script, node_acc_storage,
            node_acc_counter)
        -> {node_acc_balance; node_acc_delegate; node_acc_script; node_acc_storage;
            node_acc_counter; node_acc_spendable = None; node_acc_delegatable = None;
            node_acc_manager = None})
      (obj5
         (req "balance" int64)
         (opt "delegate" string)
         (opt "script" Script.script_encoding)
         (opt "storage" Script.expr_encoding)
         (opt "counter" z_encoding))
  let encoding =
    union [
      case athen_encoding
        (fun a -> if a.node_acc_manager = None then None else Some a)
        (fun a -> a);
      case babylon_encoding
        (fun a -> if a.node_acc_manager = None then Some a else None)
        (fun a -> a)
    ]

  let account_info = conv
    (fun {node_ai_balance; node_ai_frozen_balance; node_ai_delegate;
          node_ai_counter; node_ai_script; node_ai_nrolls; node_ai_maxrolls;
          node_ai_nrolls_avail; node_ai_delegation; node_ai_admin;
          node_ai_white_list; node_ai_is_inactive; node_ai_desactivation_cycle}
      -> (node_ai_balance, node_ai_frozen_balance, node_ai_delegate,
          node_ai_counter, node_ai_script, node_ai_nrolls, node_ai_maxrolls,
          node_ai_nrolls_avail, node_ai_delegation, node_ai_admin,
          node_ai_white_list, node_ai_is_inactive, node_ai_desactivation_cycle))
    (fun (node_ai_balance, node_ai_frozen_balance, node_ai_delegate,
          node_ai_counter, node_ai_script, node_ai_nrolls, node_ai_maxrolls,
          node_ai_nrolls_avail, node_ai_delegation, node_ai_admin,
          node_ai_white_list, node_ai_is_inactive, node_ai_desactivation_cycle)
      -> {node_ai_balance; node_ai_frozen_balance; node_ai_delegate;
          node_ai_counter; node_ai_script; node_ai_nrolls; node_ai_maxrolls;
          node_ai_nrolls_avail; node_ai_delegation; node_ai_admin;
          node_ai_white_list; node_ai_is_inactive; node_ai_desactivation_cycle}) @@
    EzEncoding.obj13
      (req "balance" int64)
      (req "frozen_balance" int64)
      (opt "delegate" string)
      (opt "counter" z_encoding)
      (opt "script" (
          obj3
            (opt "code" Script.expr_encoding)
            (req "storage" Script.expr_encoding)
            (opt "code_hash" string)))
      (opt "nrolls" int)
      (opt "maxrolls" int)
      (opt "nrolls_avail" int)
      (opt "delegation" bool)
      (opt "admin" string)
      (opt "white_list" (list string))
      (opt "is_inactive" bool)
      (opt "desactivation_cycle" int)

end
