(************************************************************************)
(*                                DunScan                               *)
(*                                                                      *)
(*  Copyright 2017-2018 OCamlPro                                        *)
(*                                                                      *)
(*  This file is distributed under the terms of the GNU General Public  *)
(*  License as published by the Free Software Foundation; either        *)
(*  version 3 of the License, or (at your option) any later version.    *)
(*                                                                      *)
(*  DunScan is distributed in the hope that it will be useful,          *)
(*  but WITHOUT ANY WARRANTY; without even the implied warranty of      *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the       *)
(*  GNU General Public License for more details.                        *)
(*                                                                      *)
(************************************************************************)

module Date = struct
  type t = DATE of string

  let format = "%Y-%m-%dT%H:%M:%SZ"
  let to_string (DATE s) = s
  let from_string s = DATE s
  let pretty_date = function
      DATE d ->
       match String.split_on_char 'T' d with
         [] -> "",""
       | hd :: [] -> hd,""
       | date :: time :: _ -> date,(String.sub time 0 (String.length time - 1))
end

type block_hash = string
type operation_hash = string
type account_hash = string
type proposal_hash = string
type protocol_hash = string
type network_hash = string
type nonce_hash = string
type timestamp = string

type voting_period_kind =
  | NProposal
  | NTesting_vote
  | NTesting
  | NPromotion_vote

type balance_updates =
  | Contract of account_hash * int64
  | Rewards of account_hash * int * int64
  | Fees of account_hash * int * int64
  | Deposits of account_hash * int * int64

type chain_status =
  | CSNot_running
  | CSForking of {
      protocol: protocol_hash ;
      expiration: timestamp ;
    }
  | CSRunning of {
      chain_id: string ;
      genesis: block_hash ;
      protocol: protocol_hash ;
      expiration: timestamp ;
    }

type constants = {
  hard_gas_limit_per_operation : Z.t ;
  hard_storage_limit_per_operation : Z.t ;
}

type block_shell = {
  shell_level : int ;
  shell_proto : int ;
  shell_predecessor : block_hash ;
  shell_timestamp : Date.t ;
  shell_validation_pass : int ;
  shell_operations_hash : string ;
  shell_fitness : string ;
  shell_context : string
}

type block_header = {
  header_shell : block_shell ;
  header_priority : int ;
  header_seed_nonce_hash : nonce_hash ;
  header_proof_of_work_nonce : string ;
  header_signature : string ;
  header_hash : block_hash option ;
  header_network : network_hash option ;
  header_protocol : protocol_hash option ;
}

type node_level = {
  node_lvl_level : int ;
  node_lvl_level_position : int ;
  node_lvl_cycle : int ;
  node_lvl_cycle_position : int ;
  node_lvl_voting_period : int ;
  node_lvl_voting_period_position : int ;
  node_lvl_expected_commitment : bool ;
}

type block_header_metadata = {
  header_meta_baker : account_hash ;
  header_meta_level : node_level ;
  header_meta_voting_period_kind : voting_period_kind ;
  header_meta_nonce_hash : string option;
  header_meta_consumed_gas : Z.t ;
  header_meta_deactivated : string list ;
  header_meta_balance_updates : balance_updates list option;
}

type block_metadata = {
  meta_protocol : protocol_hash ;
  meta_next_protocol : protocol_hash ;
  meta_test_chain_status : chain_status;
  meta_max_operations_ttl : int ;
  meta_max_operation_data_length : int ;
  meta_max_block_header_length : int ;
  meta_max_operation_list_length : (int * int option) list ;
  meta_header : block_header_metadata ;
}

type love_expr_t =
  | LoveValue of Love_value.Value.t
  | LoveType of Love_type.t

type dune_expr_t = LoveExpr of love_expr_t

type love_code_t =
  | LoveTopContract of Love_ast.top_contract
  | LoveLiveContract of Love_value.LiveContract.t
  | LoveFeeCode of Love_value.FeeCode.t

type dune_code_t = LoveCode of love_code_t

type script_expr_t =
  | Micheline of string Micheline.canonical
  | DuneExpr of dune_expr_t
  | DuneCode of dune_code_t

type entrypoint =
  | EPDefault | EPRoot | EPDo | EPSet | EPRemove | EPNamed of string

type node_op_error = {
  node_err_kind : string;
  node_err_id : string;
  node_err_info : string
}

type big_map_diff_item = {
  bmd_action : string option;
  bmd_big_map : Z.t option;
  bmd_key_hash : string option;
  bmd_key : script_expr_t option;
  bmd_value : script_expr_t option;
  bmd_source_big_map : Z.t option;
  bmd_destination_big_map : Z.t option;
  bmd_key_type : script_expr_t option;
  bmd_value_type : script_expr_t option;
}

type op_metadata = {
  meta_op_status : string option ;
  meta_op_balance_updates : balance_updates list option ;
  meta_op_storage : script_expr_t option ;
  meta_op_consumed_gas : Z.t ;
  meta_op_originated_contracts : account_hash list;
  meta_op_storage_size_diff : int64 option;
  meta_op_delegate : account_hash option ;
  meta_op_slots : int list option ;
  meta_op_paid_storage_size_diff : Z.t;
  meta_op_big_map_diff : big_map_diff_item list option ;
  meta_op_allocated_destination_contract : bool;
  meta_op_errors : node_op_error list ;
  meta_op_result : string option
}

type node_activation = {
  node_act_pkh : account_hash ;
  node_act_secret : string ;
  node_act_metadata : op_metadata option ;
}

type node_endorsement = {
  node_endorse_block_level : int ;
  node_endorse_metadata : op_metadata option ;
}

type node_proposals = {
  node_prop_src : account_hash ;
  node_prop_voting_period : int32 ;
  node_prop_proposals : proposal_hash list ;
  node_prop_metadata : op_metadata option ;
}

type ballot_type = Yay | Nay | Pass

type node_ballot = {
  node_ballot_src : account_hash ;
  node_ballot_voting_period : int32 ;
  node_ballot_proposal : proposal_hash ;
  node_ballot_vote : ballot_type ;
  node_ballot_metadata : op_metadata option ;
}

type node_seed_nonce_revelation = {
  node_seed_level : int ;
  node_seed_nonce : string ;
  node_seed_metadata : op_metadata option ;
}

type node_double_baking_evidence = {
  node_double_bh1 : block_header ;
  node_double_bh2 : block_header ;
  node_double_bh_metadata : op_metadata option ;
}

type node_double_endorsement_evidence_op = {
  ndee_branch : string ;
  ndee_level : int ;
  ndee_signature : string ;
}

type node_double_endorsement_evidence = {
  node_double_endorsement1 : node_double_endorsement_evidence_op ;
  node_double_endorsement2 : node_double_endorsement_evidence_op ;
  node_double_endorsement_metadata : op_metadata option ;
}

type script = {
  sc_code : string option;
  sc_storage : string ;
  sc_code_hash : string option
}

type activate_protocol_param = {
  proof_of_work_nonce_size_opt: int option;
  nonce_length_opt: int option;
  max_revelations_per_block_opt: int option;
  max_operation_data_length_opt: int option;
  max_proposals_per_delegate_opt: int option;
  preserved_cycles_opt: int option;
  blocks_per_cycle_opt: int option;
  blocks_per_commitment_opt: int option;
  blocks_per_roll_snapshot_opt: int option;
  blocks_per_voting_period_opt: int option;
  time_between_blocks_opt: int list option;
  endorsers_per_block_opt: int option;
  hard_gas_limit_per_operation_opt: int64 option;
  hard_gas_limit_per_block_opt: int64 option;
  proof_of_work_threshold_opt: int64 option;
  tokens_per_roll_opt: int64 option;
  michelson_maximum_type_size_opt: int option;
  seed_nonce_revelation_tip_opt: int64 option;
  origination_size_opt: int option;
  block_security_deposit_opt: int64 option;
  endorsement_security_deposit_opt: int64 option;
  block_reward_opt: int64 option;
  endorsement_reward_opt: int64 option;
  cost_per_byte_opt: int64 option;
  hard_storage_limit_per_operation_opt: int64 option;
  hard_gas_limit_to_pay_fees_opt: int64 option;
  max_operation_ttl_opt: int option;
  protocol_revision_opt: int option;
  frozen_account_cycles_opt: int option;
  allow_collect_call_opt: bool option;
  quorum_min_opt: int option;
  quorum_max_opt: int option;
  min_proposal_quorum_opt: int option;
  initial_endorsers_opt: int option;
  delay_per_missing_endorsement_opt: int option;
}

type manage_account_options = {
  node_mao_maxrolls : int option option;
  node_mao_admin : account_hash option option;
  node_mao_white_list : account_hash list option;
  node_mao_delegation : bool option
}

type manager_metadata = {
  manager_meta_balance_updates : balance_updates list option ;
  manager_meta_operation_result : op_metadata option ;
  manager_meta_internal_operation_results : node_operation_type list ;
}

and node_transaction = {
  node_tr_src : account_hash ;
  node_tr_fee : int64 ;
  node_tr_counter : Z.t ;
  node_tr_gas_limit : Z.t ;
  node_tr_storage_limit : Z.t ;
  node_tr_amount : int64 ;
  node_tr_dst : account_hash ;
  node_tr_collect_fee_gas : Z.t option ;
  node_tr_collect_pk : string option;
  node_tr_entrypoint : string option ;
  node_tr_parameters : string option ;
  node_tr_metadata : manager_metadata option ;
}

and node_origination = {
  node_or_src : account_hash ;
  node_or_fee : int64 ;
  node_or_counter : Z.t ;
  node_or_gas_limit : Z.t ;
  node_or_storage_limit : Z.t ;
  node_or_balance : int64 ;
  node_or_script : script option ;
  node_or_metadata : manager_metadata option ;
}

and node_delegation = {
  node_del_src : account_hash ;
  node_del_fee : int64 ;
  node_del_counter : Z.t ;
  node_del_gas_limit : Z.t ;
  node_del_storage_limit : Z.t ;
  node_del_delegate : account_hash ;
  node_del_metadata : manager_metadata option ;
}

and node_reveal = {
  node_rvl_src : account_hash ;
  node_rvl_fee : int64 ;
  node_rvl_counter : Z.t ;
  node_rvl_gas_limit : Z.t ;
  node_rvl_storage_limit : Z.t ;
  node_rvl_pubkey : string ;           (* public key *)
  node_rvl_metadata : manager_metadata option ;
}

and node_activate_protocol = {
  node_acp_src : account_hash ;
  node_acp_fee : int64 ;
  node_acp_counter : Z.t ;
  node_acp_gas_limit : Z.t ;
  node_acp_storage_limit : Z.t ;
  node_acp_level : int ;
  node_acp_protocol : protocol_hash option ;
  node_acp_parameters : activate_protocol_param option ;
  node_acp_metadata : manager_metadata option
}

and node_manage_accounts = {
  node_macs_src : account_hash ;
  node_macs_fee : int64 ;
  node_macs_counter : Z.t ;
  node_macs_gas_limit : Z.t ;
  node_macs_storage_limit : Z.t ;
  node_macs_bytes : Hex.t ;
  node_macs_metadata : manager_metadata option
}

and node_manage_account = {
  node_mac_src : account_hash ;
  node_mac_fee : int64 ;
  node_mac_counter : Z.t ;
  node_mac_gas_limit : Z.t ;
  node_mac_storage_limit : Z.t ;
  node_mac_target : (account_hash * string option) option;
  node_mac_options : manage_account_options;
  node_mac_metadata : manager_metadata option
}

and node_clear_delegations = {
  node_cld_src : account_hash ;
  node_cld_fee : int64 ;
  node_cld_counter : Z.t ;
  node_cld_gas_limit : Z.t ;
  node_cld_storage_limit : Z.t ;
  node_cld_metadata : manager_metadata option
}

and node_operation_type =
  | NTransaction of node_transaction
  | NOrigination of node_origination
  | NReveal of node_reveal
  | NDelegation of node_delegation
  | NSeed_nonce_revelation of node_seed_nonce_revelation
  | NActivation of node_activation
  | NDouble_endorsement_evidence of node_double_endorsement_evidence
  | NDouble_baking_evidence of node_double_baking_evidence
  | NEndorsement of node_endorsement
  | NProposals of node_proposals
  | NBallot of node_ballot
  | NActivate
  | NActivate_testnet
  | NActivate_protocol of node_activate_protocol
  | NManage_accounts of node_manage_accounts
  | NManage_account of node_manage_account
  | NClearDelegations of node_clear_delegations

type node_operation = {
  node_op_protocol : protocol_hash ;
  node_op_chain_id : string ;
  node_op_hash : operation_hash ;
  node_op_branch : string ;
  node_op_contents : node_operation_type list ;
  node_op_signature : string option;
}

type node_block = {
  node_protocol : protocol_hash ;
  node_chain_id : string ;
  node_hash : block_hash ;
  node_header : block_header ;
  node_metadata : block_metadata ;
  node_operations : node_operation list list ;
}

type node_account = {
  node_acc_manager : account_hash option ;
  node_acc_balance : int64 ;
  node_acc_spendable : bool option;
  node_acc_delegatable : bool option;
  node_acc_delegate : account_hash option;
  node_acc_script : (script_expr_t option * script_expr_t * string option) option;
  node_acc_storage : script_expr_t option ;
  node_acc_counter : Z.t option ;
}

type node_account_info = {
  node_ai_balance: int64 ;
  node_ai_frozen_balance: int64 ;
  node_ai_delegate: account_hash option ;
  node_ai_counter: Z.t option ;
  node_ai_script: (script_expr_t option * script_expr_t * string option) option;
  node_ai_nrolls: int option ;
  node_ai_maxrolls : int option ;
  node_ai_nrolls_avail : int option ;
  node_ai_delegation : bool option ;
  node_ai_admin : account_hash option ;
  node_ai_white_list : account_hash list option ;
  node_ai_is_inactive : bool option ;
  node_ai_desactivation_cycle : int option ;
}
