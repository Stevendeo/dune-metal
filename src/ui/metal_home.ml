(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Html

open Bs4.Items
open Bs4.Utils

open Metal_types
open Metal_helpers_ui
open Metal_helpers
open Metal_ids.IdHome

(* Network selector *)

let handler_network refresh state event =
  match src_value event with
  | None -> true
  | Some v ->
    let ho_net = network_of_string v in
    let callback () = refresh {state with ho_net}in
    Storage_writer.update_network ~callback ho_net;
    true

let update_network refresh state =
  Storage_reader.get_custom_networks (fun custom_networks ->
      let custom_networks = List.map (fun (name, _, _) -> Custom name) custom_networks in
      let network_select =
        select
          ~a:[ a_onchange (handler_network refresh state) ;
               a_class [ BForm.custom_select ] ] @@
        List.map (fun n ->
            let selected = if n = state.ho_net then [ a_selected () ] else [] in
            option ~a:(a_value (network_to_string n) :: selected) ( txt (network_to_string n)))
          (network_options @ custom_networks) in
      replace1 network_home_id network_select)

(* Selected account info *)

let update_account_info state =
  let cb () =
    Metal_request.Node.get_account_info ~network:state.ho_net
      ~error:(fun _code _content ->
          replace1 balance_home_id @@
          span ~a:[ a_title "cannot get account info"; a_class [Color.danger_tx] ] [
            txt "--" ])
      state.ho_acc.pkh
      (fun ai ->
         replace1 balance_home_id (Dun.pp_amount ai.Dune_types_min.node_ai_balance);
         update_account_admin ~accounts:state.ho_accs state.ho_net state.ho_acc ai;
         let maxrolls = match ai.Dune_types_min.node_ai_maxrolls with
           | None -> None
           | Some m -> Some (txt "Max Rolls", (txt @@ string_of_int m)) in
         let admin = match ai.Dune_types_min.node_ai_admin with
           | None -> None
           | Some dn ->
             Some (txt "Admin", account_w_blockies ~crop_len:10 state.ho_net {dn; alias = None}) in
         let white_list = match ai.Dune_types_min.node_ai_white_list with
           | None -> None
           | Some l ->
             Some (txt "White List",
                   div ~a:[ a_class [Grid.row] ] (List.map (fun dn ->
                       account_w_blockies ~classes:[Grid.col12]
                         ~crop_len:10 state.ho_net {dn; alias = None}) l)) in
         let delegation = match ai.Dune_types_min.node_ai_delegation with
           | None -> None
           | Some d ->
             Some (txt "Delegation",
                   if d then txt "open" else txt "closed") in
         let info_content = List.rev @@ List.fold_left (fun acc x -> match x with
             | None -> acc
             | Some (lab, value) ->
               div ~a:[ a_class [Grid.row; Spacing.py2] ] [
                 div ~a:[ a_class [Grid.col6] ] [lab];
                 div ~a:[ a_class [Grid.col6] ] [value] ] :: acc) []
             [maxrolls; admin; white_list; delegation] in
         if info_content = [] then
           hide account_info_home_id
         else (
           replace account_info_content_home_id info_content;
           show account_info_home_id)
         ) in
  Timer.clear_create ~name:"balance" 60 cb

let deny_handler refresh state n =
  Metal_core.deny_callback n ;
  Storage_writer.remove_notif ~callback:(fun () -> refresh state) n ;
  true

let approve_handler refresh state ?msg n =
  Metal_core.approve_callback ~msg n ;
  Storage_writer.remove_notif ~callback:(fun () -> refresh state) n

let update_approve_page refresh state notif =
  let nid = match notif with
    | ApprovNot n -> n.not_approv_id
    | DlgNot n | TraNot n | OriNot n | ManNot n -> n.not_id in
  let id_modal = "modal-" ^ nid in
  let id_body = id_modal ^ "-body" in
  let app_dom =
    Metal_core.mk_approve_page
      ~confirm:(approve_handler refresh state)
      ~attr:[ Attribute.a_data_custom "dismiss" "modal" ]
      ~deny:(deny_handler refresh state)
      notif in
  replace id_body app_dom

let update_reveal refresh state =
  let cb () =
    if not (is_revealed state.ho_net state.ho_acc) then
      Metal_request.Scan.get_revealed ~network:state.ho_net state.ho_acc.pkh
        (function
          | None -> ()
          | Some hash ->
            let ho_acc =
              {state.ho_acc with
               revealed = (state.ho_net, hash) :: state.ho_acc.revealed} in
            let callback ho_acc = refresh (update_ho_state state ho_acc) in
            Storage_writer.update_account ~callback ho_acc) in
  Timer.clear_create ~name:"reveal" 60 cb

let update_admin refresh state =
  let cb () =
    if not (is_revealed state.ho_net state.ho_acc) then
      Metal_request.Scan.get_revealed ~network:state.ho_net state.ho_acc.pkh
        (function
          | None -> ()
          | Some hash ->
            let ho_acc =
              {state.ho_acc with
               revealed = (state.ho_net, hash) :: state.ho_acc.revealed} in
            let callback ho_acc = refresh (update_ho_state state ho_acc) in
            Storage_writer.update_account ~callback ho_acc) in
  Timer.clear_create ~name:"reveal" 60 cb

let update_op_page refresh state notif =
  let open Async in
  let nid = match notif with
    | ApprovNot n -> n.not_approv_id
    | DlgNot n | TraNot n | OriNot n | ManNot n -> n.not_id in
  let id_modal = "modal-" ^ nid in
  let id_body = id_modal ^ "-body" in
  async (fun () ->
      Metal_core.mk_op_page
        ~confirm:(approve_handler refresh state)
        ~attr:[ Attribute.a_data_custom "dismiss" "modal" ]
        ~deny:(deny_handler refresh state)
        state.ho_acc
        state.ho_net
        notif >>|? fun app_dom ->
      replace id_body app_dom)

let update_full_notif refresh state n = match n with
  | ApprovNot _ -> update_approve_page refresh state n
  | TraNot _ | DlgNot _ | OriNot _ | ManNot _ -> update_op_page refresh state n

let add_notif_modal notif =
  let nid = match notif with
    | ApprovNot n -> n.not_approv_id
    | DlgNot n | TraNot n | OriNot n | ManNot n -> n.not_id in
  let id_modal = "modal-" ^ nid in
  let id_title = id_modal ^ "-title" in
  let id_body = id_modal ^ "-body" in
  let title_txt = match notif with
    | ApprovNot _n -> "Approval Request"
    | DlgNot _n -> "Delegation Request"
    | TraNot _n -> "Transaction Request"
    | OriNot _n -> "Origination Request"
    | ManNot _n -> "Manage Account Request"
  in
  let nmod =
    div ~a:[ a_id id_modal ;
             a_class [ Modal.modal ; BMisc.fade ] ;
             (* a_tabindex ~-1 ; *)
             a_role [ "dialog" ] ;
             (* a_aria "labelledby" [ id_title ] ; *)
             (* a_aria "hidden" [ "true" ] *) ] [
      div ~a:[ a_class [ Modal.modal_dialog ; Modal.modal_dialog_centered ] ;
               a_role [ "document" ] ] [
        div ~a:[ a_class [ Modal.modal_content ] ] [
          div ~a:[ a_class [ Modal.modal_header ] ] [
            h5 ~a:[ a_id id_title ;
                    a_class [ Modal.modal_title ] ] [ txt @@ title_txt ] ;
            button ~a:[ a_class [ BMisc.close ] ;
                        Attribute.a_data_custom "dismiss" "modal" ] [
              span ~a:[ a_aria "hidden" [ "true" ] ] [ entity "times" ]
            ]
          ] ;
          div ~a:[ a_id id_body ;
                   a_class [ Modal.modal_body ; (* Sizing.h100 ; *)
                             Flex.flex_column ; Display.d_flex ]
                 ] [
            Metal_core.make_forging_dom ()
          ]
        ]
      ]
    ] in
  replace1 modal_div_id nmod

let hide_modal () =
  ignore @@ Jquery.modal "hide" (Jquery.jQ "#modal-metal") ;
  Js_utils.(Manip.removeSelf @@ find_component "modal-metal")

let add_form_modal kind =
  let nid = "metal" in
  let id_modal = "modal-" ^ nid in
  let id_title = id_modal ^ "-title" in
  let id_body = id_modal ^ "-body" in
  let title_txt = kind ^ " Request" in
  let nmod =
    div ~a:[ a_id id_modal ;
             a_class [ Modal.modal ; BMisc.fade ] ;
             Attribute.a_data_custom "backdrop" "static" ;
             (* a_tabindex ~-1 ; *)
             a_role [ "dialog" ] ;
             (* a_aria "labelledby" [ id_title ] ; *)
             (* a_aria "hidden" [ "true" ] *) ] [
      div ~a:[ a_class [ Modal.modal_dialog ; Modal.modal_dialog_centered ] ;
               a_role [ "document" ] ] [
        div ~a:[ a_class [ Modal.modal_content ] ] [
          div ~a:[ a_class [ Modal.modal_header ] ] [
            h5 ~a:[ a_id id_title ;
                    a_class [ Modal.modal_title ] ] [ txt @@ title_txt ] ;
            button ~a:[ a_class [ BMisc.close ] ;
                        Attribute.a_data_custom "dismiss" "modal" ;
                        a_onclick (fun _ -> hide_modal () ; true)] [
              span ~a:[ a_aria "hidden" [ "true" ] ] [ entity "times" ]
            ]
          ] ;
          div ~a:[ a_id id_body ;
                   a_class [ Modal.modal_body ; (* Sizing.h100 ; *)
                             Flex.flex_column ; Display.d_flex ]
                 ] [
            Metal_core.make_forging_dom ()
          ]
        ]
      ]
    ] in
  replace1 modal_div_id nmod

let make_notif refresh state n =
  let nid = match n with
    | ApprovNot n -> n.not_approv_id
    | DlgNot n | TraNot n | OriNot n | ManNot n -> n.not_id in
  let id = "modal-" ^ nid in
  let time = notif_tsp_of_not n in
  div ~a:[ a_class [ Display.d_flex; Flex.flex_column; Spacing.p3;
                     Border.border_bottom ] ] [
    span ~a:[ a_class [ Text.center ] ] [ txt time ] ;
    notif_kind_to_row state.ho_net n ;
    div ~a:[ a_class [ Display.d_flex; Flex.justify_around ;
                       Spacing.mx2 ; Spacing.mt2 ] ] [
      button ~a:[ a_class [Button.btn; Button.btn_primary;
                           Display.d_flex; Flex.align_items_center ];
                  Attribute.a_data_toggle "modal" ;
                  Attribute.a_data_custom "target" ("#" ^ id) ;
                  a_onclick (fun _ -> update_full_notif refresh state n; true) ] [
        fas "eye";
        span ~a:[ a_class [ Spacing.mx2 ] ] [ txt "See" ]
      ] ;
      button ~a:[ a_class [Button.btn; Button.btn_danger;
                           Display.d_flex; Flex.align_items_center ];
                  a_onclick (fun _ -> deny_handler refresh state n) ] [
        fas "ban";
        span ~a:[ a_class [ Spacing.mx2 ] ]  [ txt "Deny" ]
      ]
    ]
  ]

(* <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">
 *   Launch demo modal
 * </button> *)

let update_account refresh state =
  update_reveal refresh state;
  Storage_reader.get_notifs (fun ns ->
      let notif =
        try
          (List.find (fun {notif_acc ; _} ->
               notif_acc = state.ho_acc.pkh) ns).notifs
        with Not_found -> [] in
      let revealed = match List.assoc_opt state.ho_net state.ho_acc.revealed with
        | Some op_hash ->
          make_link ~path:(dunscan_path ~suffix:op_hash state.ho_net) "Revealed"
        | _ -> txt "Unrevealed" in
      let notif_div = match notif with
        | [] -> div []
        | notif ->
          BCard.make_card ~card_title_content:(span [ txt "Notifications" ])
            ~card_title_class:[Text.lead; Color.light_tx]
            ~card_header_class:[Color.dark_bg]
            ~card_body_class:[Spacing.p0]
            ~card_body_content:[
              div ~a:[ a_id notif_list_id ;
                       a_class [List_group.list_group; Color.light_bg;
                                Spacing.pt3; "notif-list" ] ] @@
              List.map (fun n ->
                  add_notif_modal n ;
                  make_notif refresh state n) notif
            ] () in
      let info_div = div ~a:[ a_id account_info_home_id; a_style "display: none"] [
          BCard.make_card ~card_title_content:(span [ txt "Options" ])
            ~card_title_class:[Text.lead; Color.light_tx]
            ~card_header_class:[Color.dark_bg]
            ~card_body_class:Spacing.[px2; py0]
            ~card_body_content:[
              div ~a:[ a_id account_info_content_home_id ] []
            ] ()
        ] in
      let account_div =
        div ~a:[ a_class [Display.d_flex; Flex.flex_column; Spacing.mb5] ] [
          div ~a:[ a_class [Display.d_flex; Flex.flex_column; Text.center; Spacing.py5] ] [
            div ~a:[ a_class [Spacing.m4; Display.d_inline_flex; Flex.justify_around] ] [
              dunscan_link ~classes:["account-hash-home"]
                state.ho_net state.ho_acc.pkh;
              button ~a:[ a_class [Button.btn; Spacing.py0; Spacing.px1; Spacing.mx2];
                          a_onclick (fun _e -> copy_str state.ho_acc.pkh; false) ] [
                fas "copy" ] ];
            div ~a:[ a_class [Sizing.w50; Spacing.my4; Spacing.mxa] ] [
              blockies ~classes:["main-blockies"] state.ho_acc.pkh; br ();
              span ~a:[ a_class [Badge.badge; Badge.badge_pill; Badge.badge_dark] ] [ revealed ] ];
            span ~a:[ a_id balance_home_id; a_class [Spacing.my4] ] [ txt "--" ]
          ];
          info_div;
          notif_div;
        ] in
      replace1 account_home_id account_div;
      update_account_info state;
      update_badge state.ho_acc)

(* Accounts list *)

let update_accounts refresh state =
  let other = List.filter (fun a -> a <> state.ho_acc) state.ho_accs in
  let refresh_full {fo_acc; fo_net} =
    clear import_accounts_id;
    Storage_reader.get_accounts (fun ho_accs ->
        refresh {ho_acc = fo_acc; ho_net = fo_net; ho_accs}) in
  let accounts_div =
    BCard.make_card ~card_title_content:(span [ txt "Other Accounts" ])
      ~card_title_class:[Text.lead; Color.light_tx]
      ~card_header_class:[Color.dark_bg]
      ~card_body_class:[Spacing.p0]
      ~card_body_content:[
        div ~a:[ a_class [List_group.list_group; Color.light_bg; Spacing.pt3 ] ] @@
        (List.map (fun ac ->
             div ~a:[ a_class [Display.d_flex; Flex.justify_between;
                               Spacing.px4; Spacing.py1; Color.light_bg] ] [
               a ~a:[ a_class [Button.btn; Button.btn_light; Button.btn_block; Text.left;
                               "account-w-blockies"];
                      a_onclick (fun _e ->
                          Storage_writer.update_selected
                            ~callback:(fun () -> refresh {state with ho_acc = ac})
                            (Some ac.pkh); true) ] [
                 blockies (ac.pkh);
                 span ~a:[ a_class ["account-list"] ] [ txt @@ crop_hash ~crop_len:10 ac.pkh ] ];
               button ~a:[ a_class [Button.btn; Button.btn_light;
                                    Spacing.py0; Spacing.px1; Spacing.mx2];
                           a_onclick (fun _e -> copy_str ac.pkh; true) ] [ fas "copy" ]
             ]
           ) other) @ [
          div ~a:[ a_class [Display.d_flex; Flex.justify_between;
                            Spacing.p4; Color.light_bg] ] [
            a ~a:[
              a_class [Button.btn; Button.btn_light; Button.btn_block; Text.left];
              a_onclick (fun _e ->
                  Js_utils.(show (find_component hide_import_accounts_id));
                  Metal_forms.handler_import_form
                    ~refresh:refresh_full
                    ~update:(replace import_accounts_id) (to_fo_state state);
                  true) ] [ txt "Import KT1" ];
            button ~a:[
              a_id hide_import_accounts_id;
              a_style "display: none";
              a_class [Button.btn; Button.btn_light; Spacing.py0; Spacing.px1];
              a_onclick (fun _e -> clear import_accounts_id;
                          Js_utils.(hide (find_component hide_import_accounts_id));
                          true) ] [
              fas "arrow-up" ]
        ]
      ] ] () in
  replace1 accounts_home_id accounts_div

(* Operation Tabs *)

module OpTabs = Bs4_navs.Make(struct type t = unit end)

(* Transaction *)
module TrTable = Bs4_card.MakeCardTable(struct
    type t = transaction_sh
    type state = network
    let name = "transaction"
    let title_span n = span [
        txt "History";
        span ~a:[ a_class [ Badge.badge; Badge.badge_pill;
                            Badge.badge_dark; Spacing.mx2 ] ] [
          txt @@ string_of_int n] ]
    let page_size = 10
    let table_class = [Color.light_bg]
    let card_classes = [Color.light_tx; Color.dark_bg]
    let heads = [
      txt "Hash"; txt "Source"; txt "Destination"; txt "Amount";
      txt "Fee"; txt "Date"]
    let to_row network tx =
      let network = unopt Mainnet network in
      let timestamp =
        if tx.trs_status <> "pending" then
          ago_str @@ float_of_iso tx.trs_tsp
        else "waiting" in
      let tr_class = match tx.trs_status with
        | "failed" -> [ Table.btable_danger ]
        | "pending" -> [ Table.btable_warning ]
        | _ -> [] in
      tr ~a:[ a_class tr_class ] [
        td [ make_link ~crop_len:10
               ~path:(dunscan_path ~suffix:tx.trs_hash network) tx.trs_hash ];
        td [ account_w_blockies ~crop_len:8 network {dn=tx.trs_src; alias=None} ];
        td [ account_w_blockies ~crop_len:8 network {dn=tx.trs_dst; alias=None} ];
        td [ Dun.pp_amount tx.trs_amount ];
        td [ Dun.pp_amount tx.trs_fee ];
        td [ span ~a:[ a_class [Text.nowrap]] [ txt timestamp ] ]
      ]
    let empty = Some "No transaction yet"
    let head_class = []
  end)

let tr_tab ?(tab_state=Bs4_navs.Inactive) ?(collapsed=true) ?dst_value ?amount_value state =
  let get_trans state ?network ?error ?page ?page_size dn1 f =
    Metal_request.Scan.get_transactions ?network ?error ?page ?page_size dn1
      (fun l ->
         let pending = List.filter (fun trp ->
             not (List.exists (fun tra -> tra.trs_hash = trp.trs_hash) l)) state.fo_acc.pending in
         f @@ (pending @ l)) in
  let rec onshow state =
    let confirm = fun ?msg _n ->
      ignore msg ;
      hide_modal () ;
      Storage_reader.get_account (fun (acc, _) -> match acc with
          | None -> ()
          | Some acc -> onshow { state with fo_acc = acc }) in
    let deny = fun _n ->
      hide_modal () ;
      onshow state in
    Metal_forms.handler_transaction_form
      ~refresh:onshow
      ~update:(replace transaction_form_home_id)
      ~update_anim:(replace "modal-metal-body")
      ~redirect:false
      ~collapsed
      ?dst_value
      ?amount_value
      state
      ?mk_loading_ui:None
      ~mk_ok_ui:(fun n ->
          add_form_modal "Transaction" ;
          ignore @@ Jquery.modal "show" (Jquery.jQ "#modal-metal") ;
          Metal_core.mk_op_page ~confirm ~deny n)
      ~mk_error_ui:(fun err ->
          ignore @@ Jquery.modal "show" (Jquery.jQ "#modal-metal") ;
          Metal_core.make_error_dom
            (fun () ->
               hide_modal () ;
               onshow state)
            err) ;
    let cb () =
      Metal_request.Scan.(
        page_wrap ~network:state.fo_net ~state:state.fo_net
          nb_transactions (get_trans state)
          TrTable.update state.fo_acc.pkh) in
    Timer.clear_create ~name:"transaction" 60 cb
  in
  let tab_state =
    match Crypto.prefix_dn state.fo_acc.pkh, state.fo_acc.manager_kt with
    | pref, Some false when pref = Some Crypto.Prefix.contract_public_key_hash ->
      Bs4_navs.Disabled
    | _ -> tab_state in
  Bs4_navs.make
    ~state:tab_state
    ~onshow:(fun () -> onshow state)
    transaction_tab_content (fun _ -> span [ txt "Transfer" ])

let transaction_div =
  div ~a:[ a_id transaction_tab_content ] [
    div ~a:[ a_class [Spacing.pb3; Spacing.mb5; Border.border_dark;
                      Border.border_bottom ] ] [
      div ~a:[ a_id transaction_form_home_id;
               a_class ["home-form-container"; Spacing.ma] ] [] ];
    TrTable.make ~card_body_class:Spacing.[p0] ~card_class:Spacing.[mb5] ()
  ]

(* Delegation *)
module DlgTable = Bs4_card.MakeCardTable(struct
    type t = string * delegation
    type state = network
    let name = "delegation"
    let title_span n = span [
        txt "History";
        span ~a:[ a_class [ Badge.badge; Badge.badge_pill;
                            Badge.badge_dark; Spacing.mx2 ] ] [
          txt @@ string_of_int n] ]
    let page_size = 10
    let table_class = [Color.light_bg]
    let card_classes = [Color.light_tx; Color.dark_bg]
    let heads = [ txt "Hash"; txt "Delegate"; txt "Fee"; txt "Date"]
    let to_row network (hash, dlg) =
      let network = unopt Mainnet network in
      let time, date = pp_tsp dlg.del_timestamp in
      let tr_class = if dlg.del_failed then [ Table.btable_danger ] else [] in
      tr ~a:[ a_class tr_class ] [
        td [ make_link ~crop_len:10
               ~path:(dunscan_path ~suffix:hash network) hash ];
        td [ account_w_blockies ~crop_len:8 network dlg.del_delegate ];
        td [ Dun.pp_amount dlg.del_fee ];
        td [ span ~a:[ a_class [Text.nowrap]] [txt (date ^ " " ^ time)] ]
      ]
    let empty = Some "No delegation so far"
    let head_class = []
  end)

let dlg_tab ?(tab_state=Bs4_navs.Inactive) ?(collapsed=true) ?dlg_value state =
  let rec onshow state old services =
    let confirm = fun ?msg _n ->
      ignore msg ;
      hide_modal () ;
      Storage_reader.get_account (fun (acc, _) -> match acc with
          | None -> ()
          | Some acc -> onshow { state with fo_acc = acc } old services) in
    let deny = fun _n ->
      hide_modal () ;
      onshow state in
    Metal_forms.handler_delegation_form
      ~refresh:onshow
      ~update:(replace delegation_form_home_id)
      ~update_anim:(replace "modal-metal-body")
      ~redirect:false
      ~collapsed
      ?dlg_value
      state
      services
      ?old_delegate:old
      ?mk_loading_ui:None
      ~mk_ok_ui:(fun n ->
          add_form_modal "Delegation" ;
          ignore @@ Jquery.modal "show" (Jquery.jQ "#modal-metal") ;
          Metal_core.mk_op_page ~confirm ~deny n)
      ~mk_error_ui:(fun err ->
          ignore @@ Jquery.modal "show" (Jquery.jQ "#modal-metal") ;
          Metal_core.make_error_dom
            (fun () ->
               hide_modal () ;
               onshow state)
            err) ;
    let cb () =
      Metal_request.Scan.(
        page_wrap ~network:state.fo_net ~state:state.fo_net
          nb_delegations get_delegations
          DlgTable.update state.fo_acc.pkh) in
    Timer.clear_create ~name:"delegation" 60 cb
  in
  let tab_state =
    match Crypto.prefix_dn state.fo_acc.pkh, state.fo_acc.manager_kt with
    | pref, Some false when pref = Some Crypto.Prefix.contract_public_key_hash ->
      Bs4_navs.Disabled
    | _ -> tab_state in
  Bs4_navs.make
    ~state:tab_state
    ~onshow:(fun () ->
        Metal_request.Scan.get_node_account
          ~network:state.fo_net
          state.fo_acc.pkh
          (fun node_acc ->
             Metal_request.Scan.get_services ~network:state.fo_net
               ~error:(fun code error ->
                   Js_utils.log "Error %d %S" code error;
                   onshow state node_acc.acc_delegate [])
               (fun services -> onshow state node_acc.acc_delegate services)))
    delegation_tab_content (fun _ -> span [ txt "Delegate" ])

let delegation_div =
  div ~a:[ a_id delegation_tab_content ] [
    div ~a:[ a_class [Spacing.pb3; Spacing.mb5; Border.border_dark;
                      Border.border_bottom ] ] [
      div ~a:[ a_id delegation_form_home_id;
               a_class ["home-form-container"; Spacing.ma] ] [] ];
    DlgTable.make ~card_body_class:Spacing.[p0] ~card_class:Spacing.[mb5] ()
  ]

(* Origination *)
module OriTable = Bs4_card.MakeCardTable(struct
    type t = (string * origination)
    type state = network
    let name = "origination"
    let title_span n = span [
        txt "History";
        span ~a:[ a_class [ Badge.badge; Badge.badge_pill;
                            Badge.badge_dark; Spacing.mx2 ] ] [
          txt @@ string_of_int n] ]
    let page_size = 10
    let table_class = [Color.light_bg]
    let card_classes = [Color.light_tx; Color.dark_bg]
    let heads = [ txt "Hash"; txt "Contract"; txt "Balance";
                  txt "Fee"; txt "Date"]
    let to_row network (hash, ori) =
      let network = unopt Mainnet network in
      let time, date = pp_tsp ori.or_timestamp in
      let tr_class = if ori.or_failed then [ Table.btable_danger ] else [] in
      tr ~a:[ a_class tr_class ] [
        td [ make_link ~crop_len:10
               ~path:(dunscan_path ~suffix:hash network) hash ];
        td [ account_w_blockies ~crop_len:8 network ori.or_kt1 ];
        td [ Dun.pp_amount ori.or_balance ];
        td [ Dun.pp_amount ori.or_fee ];
        td [ span ~a:[ a_class [Text.nowrap]] [txt (date ^ " " ^ time)] ]
      ]
    let empty = Some "No origination so far"
    let head_class = []
  end)

let ori_tab ?(tab_state=Bs4_navs.Inactive) ?(collapsed=true) ?balance_value state =
  let rec onshow state =
    let confirm = fun ?msg _n ->
      ignore msg ;
      hide_modal () ;
      Storage_reader.get_account (fun (acc, _) -> match acc with
          | None -> ()
          | Some acc -> onshow { state with fo_acc = acc }) in
    let deny = fun _n ->
      hide_modal () ;
      onshow state in
    Metal_forms.handler_origination_form
      ~refresh:onshow
      ~update:(replace origination_form_home_id)
      ~update_anim:(replace  "modal-metal-body")
      ~redirect:false
      ~collapsed
      ?balance_value
      state
      ?mk_loading_ui:None
      ~mk_ok_ui:(fun n ->
          add_form_modal "Origination" ;
          ignore @@ Jquery.modal "show" (Jquery.jQ "#modal-metal") ;
          Metal_core.mk_op_page ~confirm ~deny n)
      ~mk_error_ui:(fun err ->
          ignore @@ Jquery.modal "show" (Jquery.jQ "#modal-metal") ;
          Metal_core.make_error_dom
            (fun () ->
               hide_modal () ;
               onshow state)
            err) ;
    let cb () =
      Metal_request.Scan.(
        page_wrap ~network:state.fo_net ~state:state.fo_net
          nb_originations get_originations
          OriTable.update state.fo_acc.pkh) in
    Timer.clear_create ~name:"origination" 60 cb in
  let tab_state = match Crypto.prefix_dn state.fo_acc.pkh with
    | Some pref when pref = Crypto.Prefix.contract_public_key_hash ->
      Bs4_navs.Disabled
    | _ -> tab_state in
  Bs4_navs.make
    ~state:tab_state
    ~onshow:(fun () -> onshow state)
    origination_tab_content (fun _ -> span [ txt "Deploy" ])

let origination_div =
  div ~a:[ a_id origination_tab_content ] [
    div ~a:[ a_class [Spacing.pb3; Spacing.mb5; Border.border_dark;
                      Border.border_bottom ] ] [
      div ~a:[ a_id origination_form_home_id;
               a_class ["home-form-container"; Spacing.ma] ] [] ];
    OriTable.make ~card_body_class:Spacing.[p0] ~card_class:Spacing.[mb5] ()
  ]

(* Notarization *)
let not_tab ?(tab_state=Bs4_navs.Disabled) _state =
  Bs4_navs.make
    ~state:tab_state
    ~onshow:(fun () -> ())
    notarization_tab_content (fun _ -> span [ txt "Notarize" ])

let notarization_div =
  div ~a:[ a_id notarization_tab_content ] [
    div ~a:[ a_id notarization_form_home_id ] []
  ]

(* Manage Account *)

module ManTable = Bs4_card.MakeCardTable(struct
    type t = (string * manage_account)
    type state = network
    let name = "manage_account"
    let title_span n = span [
        txt "History";
        span ~a:[ a_class [ Badge.badge; Badge.badge_pill;
                            Badge.badge_dark; Spacing.mx2 ] ] [
          txt @@ string_of_int n] ]
    let page_size = 10
    let table_class = [Color.light_bg]
    let card_classes = [Color.light_tx; Color.dark_bg]
    let heads = [ txt "Hash"; txt "Max Rolls"; txt "Admin"; txt "White list";
                  txt "Delegation"; txt "Fee"; txt "Date"]
    let to_row network (hash, mac) =
      let network = unopt Mainnet network in
      let time, date = pp_tsp mac.mac_timestamp in
      let maxrolls = unoptf "--" (unoptf "unset" string_of_int) mac.mac_maxrolls in
      let admin = unoptf (span [ txt "--" ])
          (unoptf (span [ txt "unset" ])
             (account_w_blockies ~crop_len:8 network)) mac.mac_admin in
      let white_list =
        unoptf (span [ txt "--" ]) (function
          | [] -> span [ txt "unset" ]
          | l -> crop_wtitle ~crop_len:10 @@ String.concat ", " @@ List.map (fun {dn; _} -> dn) l)
          mac.mac_white_list in
      let delegation = unoptf "--" string_of_bool mac.mac_delegation in
      let tr_class = if mac.mac_failed then [ Table.btable_danger ] else [] in
      tr ~a:[ a_class tr_class ] [
        td [ make_link ~crop_len:10
               ~path:(dunscan_path ~suffix:hash network) hash ];
        td [ txt maxrolls ];
        td [ admin ];
        td [ white_list ];
        td [ txt delegation ];
        td [ Dun.pp_amount mac.mac_fee ];
        td [ span ~a:[ a_class [Text.nowrap]] [txt (date ^ " " ^ time)] ]
      ]
    let empty = Some "No manage_account so far"
    let head_class = []
  end)

let man_tab ?(tab_state=Bs4_navs.Inactive) ?(collapsed=true)
    ?maxrolls_value ?admin_value ?white_list_value state =
  let rec onshow state =
    let confirm = fun ?msg _n ->
      ignore msg ;
      hide_modal () ;
      Storage_reader.get_account (fun (acc, _) -> match acc with
          | None -> ()
          | Some acc -> onshow { state with fo_acc = acc }) in
    let deny = fun _n ->
      hide_modal () ;
      onshow state in
    Metal_forms.handler_manage_account_form
      ~refresh:onshow
      ~update:(replace manage_account_form_home_id)
      ~update_anim:(replace "modal-metal-body")
      ~redirect:false
      ~collapsed
      ?maxrolls_value
      ?admin_value
      ?white_list_value
      state
      ?mk_loading_ui:None
      ~mk_ok_ui:(fun n ->
          add_form_modal "Manage Account" ;
          ignore @@ Jquery.modal "show" (Jquery.jQ "#modal-metal") ;
          Metal_core.mk_op_page ~confirm ~deny n)
      ~mk_error_ui:(fun err ->
          ignore @@ Jquery.modal "show" (Jquery.jQ "#modal-metal") ;
          Metal_core.make_error_dom
            (fun () ->
               hide_modal () ;
               onshow state)
            err);
    let cb () =
      Metal_request.Scan.(
        page_wrap ~network:state.fo_net ~state:state.fo_net
          nb_manage_account get_manage_account
          ManTable.update state.fo_acc.pkh) in
    Timer.clear_create ~name:"manage_account" 60 cb
  in
  let tab_state =
    match Crypto.prefix_dn state.fo_acc.pkh, state.fo_acc.manager_kt with
    | pref, Some false when pref = Some Crypto.Prefix.contract_public_key_hash ->
      Bs4_navs.Disabled
    | _ -> tab_state in
  Bs4_navs.make
    ~state:tab_state
    ~onshow:(fun () -> onshow state)
    manage_account_tab_content (fun _ -> span [ txt "Manage Account" ])

let manage_account_div =
  div ~a:[ a_id manage_account_tab_content ] [
    div ~a:[ a_class [Spacing.pb3; Spacing.mb5; Border.border_dark;
                      Border.border_bottom ] ] [
      div ~a:[ a_id manage_account_form_home_id;
               a_class ["home-form-container"; Spacing.ma] ] [] ];
    ManTable.make ~card_body_class:Spacing.[p0] ~card_class:Spacing.[mb5] ()
  ]

let update_tabs state =
  let tab_list =
    try
      let args = Metal_helpers_ui.get_extension_url () in
      let _, typ = List.find (fun (n, _v) -> n = "type") args in
      match typ with
      | "transaction" ->
        let dst_value =
          try
            let _, tr_dst = List.find (fun (n, _v) -> n = "tr_dst") args in
            Some tr_dst
          with _ -> None in
        let amount_value =
          try
            let _, tr_am = List.find (fun (n, _v) -> n = "tr_am") args in
            Some tr_am
          with _ -> None in
        [ tr_tab ~tab_state:Bs4_navs.Active ~collapsed:false ?dst_value ?amount_value ;
          dlg_tab;
          ori_tab;
          not_tab;
          man_tab ]
      | "origination" ->
        let balance_value =
          try
            let _, bal = List.find (fun (n, _v) -> n = "or_balance") args in
            Some bal
          with _ -> None in
        [ tr_tab;
          dlg_tab;
          ori_tab ~tab_state:Bs4_navs.Active ~collapsed:true ?balance_value ;
          not_tab;
          man_tab ]
      | "delegation" ->
        let dlg_value =
          try
            let _, dlg = List.find (fun (n, _v) -> n = "dlg_delegate") args in
            Some dlg
          with _ -> None in
        [ tr_tab;
          dlg_tab ~tab_state:Bs4_navs.Active ~collapsed:false ?dlg_value ;
          ori_tab;
          not_tab;
          man_tab ]
      | "manage_account" ->
        let maxrolls_value, admin_value, white_list_value = try
            let m = List.find_opt (fun (n, _v) -> n = "mac_maxrolls") args in
            let a = List.find_opt (fun (n, _v) -> n = "mac_admin") args in
            let w = List.find_opt (fun (n, _v) -> n = "mac_white_list") args in
            convopt snd m, convopt snd a, convopt snd w
          with _ -> None, None, None in
        [ tr_tab;
          dlg_tab;
          ori_tab;
          not_tab;
          man_tab ~tab_state:Bs4_navs.Active ~collapsed:false ?maxrolls_value
            ?admin_value ?white_list_value ]
      | _ -> [ tr_tab ~tab_state:Bs4_navs.Active ; dlg_tab; ori_tab; not_tab; man_tab ]
    with _ -> [ tr_tab ~tab_state:Bs4_navs.Active; dlg_tab; ori_tab; not_tab; man_tab ] in
  let tabs = List.map (fun f -> f state) tab_list in
  let new_div =
    div ~a:[ a_class [Grid.container; Spacing.py5; Sizing.h50] ] [
      OpTabs.make_navs ~set_args:(fun _ -> ()) ~once:false tabs;
      div ~a:[ a_class [Nav.tab_content] ] @@
      List.map2 Bs4_navs.make_tab_content tabs
        [ transaction_div; delegation_div; origination_div; notarization_div;
          manage_account_div ] ] in
  replace1 tabs_home_id new_div;
  OpTabs.init ~find_arg:(fun _ -> None) ~set_args:(fun _ -> ()) ()
(* Main recursive function *)

let rec update_home state =
  update_network update_home state;
  update_account update_home state;
  update_accounts update_home state;
  update_tabs (to_fo_state state)

let () =
  Storage_reader.get_network (fun ho_net ->
      Storage_reader.get_account (fun (selected, ho_accs) ->
          match selected with
          | None -> update_tab "/presentation.html"
          | Some ho_acc ->
            Vault.Cb.password (function
                | None -> update_tab "/unlock.html?page=home"
                | Some _ -> update_home {ho_net; ho_acc; ho_accs})))
