(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Html
open Metal_types
open Metal_helpers_ui
open Metal_ids.IdForms
open Bs4.Utils
open Bs4.Items
open Async

let btn_classes = [Button.btn; Button.btn_light; Button.btn_outline_success; Spacing.m2]
let btn_back_classes = [Button.btn; Button.btn_light; Button.btn_outline_danger; Spacing.m2]

let short_input ?(input_type=`Text) ?value ?error ?append ?placeholder ?(oninput=fun _ -> true) id tx =
  let append = match append with None -> [] | Some append -> [
      div ~a:[ a_class [BForm.input_group_append ] ] [
        span ~a:[ a_class [BForm.input_group_text; "input-append"] ] [ txt append ] ] ] in
  let value = match value with None -> [] | Some value -> [ a_value value ] in
  let placeholder = match placeholder with None -> [] | Some ph -> [ a_placeholder ph ] in
  div ~a:[ a_class [BForm.input_group; Spacing.p2] ] ([
      div ~a:[ a_class [BForm.input_group_prepend ] ] [
        span ~a:[ a_class [BForm.input_group_text; "input-prepend"] ] [ txt tx ] ];
      input ~a:([ a_id id;
                  a_oninput oninput ;
                  a_input_type input_type;
                  a_class [BForm.form_control] ]
                @ value @ placeholder) () ] @ append @
      [ div ~a:[a_class [BForm.invalid_feedback; Text.left]] [
            match error with | None -> txt ("Invalid " ^ tx) | Some error -> txt error ] ])

let short_input_wlabel ?(input_type=`Text) ?(classes=[Text.left; Spacing.p2])
    ?value ?error ?placeholder id tx =
  let value = match value with None -> [] | Some value -> [ a_value value ] in
  let placeholder = match placeholder with None -> [] | Some ph -> [ a_placeholder ph ] in
  div ~a:[ a_class classes ] [
    label [ txt tx ];
    input ~a:([ a_id id; a_input_type input_type; a_class [BForm.form_control] ]
              @ value @ placeholder) ();
    div ~a:[a_class [BForm.invalid_feedback; Text.left]] [
      match error with | None -> txt ("Invalid " ^ tx) | Some error -> txt error ] ]

let short_textarea ?value ?error ?placeholder id tx =
  let value = match value with None -> txt "" | Some value -> txt value in
  let placeholder = match placeholder with None -> [] | Some ph -> [ a_placeholder ph ] in
  div ~a:[ a_class [Text.left; Spacing.p2] ] [
    label [ txt tx ];
    textarea ~a:([ a_id id; a_class [BForm.form_control] ]
                 @ placeholder) value;
    div ~a:[a_class [BForm.invalid_feedback; Text.left]] [
      match error with | None -> txt ("Invalid " ^ tx) | Some error -> txt error ] ]

let short_select id tx options =
  div ~a:[ a_class [BForm.input_group; Spacing.p2] ] [
    div ~a:[ a_class [BForm.input_group_prepend ] ] [
      span ~a:[ a_class [BForm.input_group_text; "input-prepend"] ] [
        txt tx ] ];
    select ~a:[ a_id id; a_class [BForm.custom_select] ] @@
    List.mapi (fun i (v, tx) ->
        let selected = if i = 0 then [ a_selected () ] else [] in
        option ~a:(a_value v :: selected) (txt tx)) options ]

let short_checkbox id tx =
  div ~a:[ a_class [ BForm.form_group; BForm.form_check ] ] [
    input ~a:[ a_input_type `Checkbox; a_class [ BForm.form_check_input ];
               a_id id ] ();
    label ~a:[ a_class [ BForm.form_check_label; "overflow-hidden"]; Attribute.a_attrib "for" id ] [
      txt tx ] ]

let ask_confirmation ?(classes=[]) ?(msg="Confirm")
    ~refresh ~confirm ~update elts =
  let classes = ["confirm-operation-form"; Spacing.ma; Spacing.py3] @ classes in
  let confirmation_div =
    div ~a:[ a_class classes ] @@
    elts @ [
      div ~a:[ a_class [Spacing.mt3; Text.center] ] [
        button ~a:[ a_class btn_classes;
                    a_onclick (fun _e -> confirm (); true) ] [
          txt msg ];
        button ~a:[ a_class btn_back_classes;
                    a_onclick (fun _e -> refresh (); true) ] [
          txt "Cancel" ] ] ] in
  update confirmation_div

let post_confirmation ?(classes=[]) ?(kind="operation") ~network
    ~refresh ~update hash =
  let hash = Crypto.Operation_hash.b58enc hash in
  let classes = ["confirm-operation-form"; Spacing.ma; Spacing.py3] @ classes in
  let confirmation_div =
    div ~a:[ a_class classes ] [
      div [
        txt @@ String.capitalize_ascii kind; br ();
        make_link ~crop_len:20
          ~path:(dunscan_path ~suffix:hash network)
          hash; br();
        txt "sent!" ];
      button ~a:[ a_class btn_classes;
                  a_onclick (fun _e -> refresh (); true) ] [ txt "Continue" ]  ] in
  update confirmation_div

let failed_operation ?(classes=[]) ?(kind="operation") ~refresh ~update code msg =
  let classes = ["confirm-operation-form"; Spacing.ma; Spacing.py3] @ classes in
  let failed_div =
    div ~a:[ a_class classes ] [
      div [
        txt @@ Printf.sprintf "%s failed:" (String.capitalize_ascii kind); br ();
        txt @@ Printf.sprintf "code: %d" code; br ();
        txt @@ Printf.sprintf "error: %s" msg ];
      button ~a:[ a_class btn_classes;
                  a_onclick (fun _e -> refresh (); true) ] [ txt "Continue" ]  ] in
  update failed_div

let dun_of_str ?(factor=1_000_000.) s =
  Int64.of_float @@ (float_of_string s) *. factor

let opt_dun_of_str ?(factor=1_000_000.) s =
  try Some (Int64.of_float @@ (float_of_string s) *. factor)
  with _ -> None

let fee_with_reveal fee reveal =
  if reveal then
    span [ Dun.pp_amount (Int64.add fee 1300L);
           txt "("; Dun.pp_amount fee; txt " fee + ";
           Dun.pp_amount 1300L; txt " for reveal)" ]
  else
    Dun.pp_amount fee

let check_value f id =
  let aux = String.trim @@ id_value id in
  if aux = "" then (raise_invalid id; Error ())
  else (
    try Ok (f aux)
    with _ -> raise_invalid id; Error ())

let check_value_opt f id =
  let aux = String.trim @@ id_value id in
  if aux = "" then Ok None
  else (
    try Ok (Some (f aux))
    with _ -> raise_invalid id; Error ())

let get_options_value kind =
  match check_value_opt dun_of_str (fee_id kind),
        check_value_opt Z.of_string (gas_limit_id kind),
        check_value_opt Z.of_string (storage_limit_id kind) with
  | Ok fee, Ok gas_limit, Ok storage_limit -> Ok (fee, gas_limit, storage_limit)
  | _ -> Error ()

let check_pkh s = if Crypto.check_pkh s then s else failwith s

let get_options_value_transaction () =
  match
    check_value_opt (fun s -> s) entrypoint_id,
    check_value_opt (fun s -> s) parameters_id,
    get_options_value "transaction" with
  | Ok entry, Ok params, Ok (fee, gas_limit, storage_limit) ->
    Ok (entry, params, fee, gas_limit, storage_limit)
  | _ -> Error ()

let get_options_value_origination () =
  let idf s = s in
  match check_value_opt idf code_hash_id, check_value_opt idf code_id,
        check_value_opt idf storage_id, get_options_value "origination" with
  | Ok code_hash, Ok code, Ok storage,
    Ok (fee, gas_limit, storage_limit) ->
    Ok (code_hash, code, storage, fee, gas_limit,
        storage_limit)
  | _ -> Error ()

let get_transaction_info ?(more=false) () =
  if more then
    match check_value check_pkh dest_tr_id,
          check_value dun_of_str amount_tr_id,
          get_options_value_transaction () with
    | Ok dest, Ok amount, Ok (entry, params, fee, gas_limit, storage_limit) ->
      Ok (dest, amount, entry, params, fee, gas_limit, storage_limit)
    | Ok dest, Ok amount, Error _ ->
      Ok (dest, amount, None, None, None, None, None)
    | _ -> Error ()
  else
    match check_value check_pkh dest_tr_id,
          check_value dun_of_str amount_tr_id with
    | Ok dest, Ok amount ->
      Ok (dest, amount, None, None, None, None, None)
    | _ -> Error ()

let get_origination_info ?(more=false) () =
  if more then
    match check_value dun_of_str balance_or_id,
          get_options_value_origination () with
    | Ok balance,
      Ok (code_hash, code, storage, fee, gas_limit, storage_limit) ->
      Ok (balance, code_hash, code, storage, fee, gas_limit, storage_limit)
    | _ -> Error ()
  else
    match check_value dun_of_str balance_or_id with
    | Ok balance ->
      Ok (balance, None, None, None, None, None, None)
    | _ -> Error ()

let get_delegation_info ?(more=false) () =
  if more then
    match check_value (fun s -> if s = "unset" then s else check_pkh s) delegate_dlg_id,
          get_options_value "delegation" with
    | Ok delegate, Ok (fee, gas_limit, storage_limit) ->
      Ok (delegate, fee, gas_limit, storage_limit)
    | _ -> Error ()
  else
    match
      check_value
        (fun s -> if s = "unset" then s else check_pkh s)
        delegate_dlg_id with
    | Ok delegate ->
      Ok (delegate, None, None, None)
    | _ -> Error ()

let get_manage_account_info () =
  match check_value_opt (fun s ->
      if s = "none" || s = "unset" then None
      else Some (int_of_string s)) maxrolls_id,
        check_value_opt (fun s ->
            if s = "none" || s = "unset" then None
            else Some (check_pkh s)) admin_id,
        check_value_opt (fun s ->
            if s = "none" || s = "unset" then []
            else let l = String.split_on_char ' ' s in List.map check_pkh l)
          white_list_id,
        check_value (fun s ->
            if s = "none" || s = "unset" then None
            else Some (bool_of_string s)) delegation_id,
        get_options_value "manage_account" with
  | Ok maxrolls, Ok admin, Ok white_list, Ok delegation,
    Ok (fee, gas_limit, storage_limit) ->
    Ok (maxrolls, admin, white_list, delegation, fee, gas_limit, storage_limit)
  | _ -> Error ()

let mk_expand_url_kind kind =
  let url = Printf.sprintf "home.html?type=%s" kind in
  match kind with
  | "transaction" ->
    let dst = id_value dest_tr_id in
    let am = id_value amount_tr_id in
    let url =
      if dst = "" then url else Printf.sprintf "%s&tr_dst=%s" url dst in
    let url =
      if am = "" then url else Printf.sprintf "%s&tr_am=%s" url am in
    url
  | "delegation" ->
    let dlg = id_value delegate_dlg_id in
    let url =
      if dlg = "" then url else Printf.sprintf "%s&dlg_delegate=%s" url dlg in
    url
  | "origination" ->
    let bl = id_value balance_or_id in
    let url =
      if bl = "" then url else Printf.sprintf "%s&or_balance=%s" url bl in
    url
  | _ -> "home.html"

let contract_call_loading () =
  [
    div ~a:[ a_id contract_loading_id ;
             a_class [ Spacing.mr3; Text.right; BMisc.invisible ] ] [
      span [ txt "Reading contract..." ] ;
      div ~a:[ a_class [ "contract-loading" ; Spacing.ml2;
                        Spinner.spinner_border; Color.dark_tx] ] []
    ]
  ]

let options_form_redirect ?(kind="operation") () =
  [
    div ~a:[ a_class [ Spacing.mr3; Text.right ] ] [
      a ~a:[ a_href ("#") ;
             a_onclick (fun _ev ->
                 let url = mk_expand_url_kind kind in
                 let tab = Chrome.Tabs.make_create ~url () in
                 Chrome.Tabs.create tab;
                 true)
           ] [
        txt "More options" ] ]
  ]

let options_form ?(kind="operation") ?(collapsed=true) () =
  let kind_options = match kind with
    | "transaction" -> [ short_input entrypoint_id "Entrypoint";
                         short_textarea parameters_id "Parameters"; ]
    | "origination" -> []
    | _ -> [] in [
    div ~a:[ a_class [ Spacing.mr3; Text.right ] ] [
      a ~a:[ a_href ("#" ^ options_collapse_id kind); Collapse.toggle ] [
        txt "More options" ] ];
    div ~a:[ a_id (options_collapse_id kind);
             a_class ([Collapse.collapse] @ if not collapsed then [ "show" ] else [])
           ] ([
        short_input ~input_type:`Number ~append:dun_str ~value:"Auto" (fee_id kind) "Fee";
        short_input_wlabel ~input_type:`Number (gas_limit_id kind) "Gas Limit";
        short_input_wlabel ~input_type:`Number (storage_limit_id kind) "Storage Limit";
      ] @ kind_options) ]

let clear_invalid_transaction ?(more=false) () =
  let kind = "transaction" in
  clear_invalid dest_tr_id;
  clear_invalid amount_tr_id;
  if more then begin
    clear_invalid @@ fee_id kind;
    clear_invalid entrypoint_id;
    clear_invalid parameters_id;
    clear_invalid @@ gas_limit_id kind;
    clear_invalid @@ storage_limit_id kind
  end

let clear_invalid_origination ?(more=false) () =
  let kind = "origination" in
  clear_invalid balance_or_id;
  if more then begin
    clear_invalid @@ fee_id kind;
    clear_invalid delegate_id;
    clear_invalid spendable_id;
    clear_invalid delegatable_id;
    clear_invalid code_hash_id;
    clear_invalid code_id;
    clear_invalid storage_id;
    clear_invalid @@ gas_limit_id kind;
    clear_invalid @@ storage_limit_id kind
  end

let clear_invalid_delegation ?(more=false) () =
  let kind = "delegation" in
  clear_invalid delegate_dlg_id;
  if more then begin
    clear_invalid @@ fee_id kind;
    clear_invalid @@ gas_limit_id kind;
    clear_invalid @@ storage_limit_id kind
  end

let clear_invalid_manage_account () =
  let kind = "manage_account" in
  clear_invalid maxrolls_id;
  clear_invalid admin_id;
  clear_invalid white_list_id;
  clear_invalid delegation_id;
  clear_invalid @@ fee_id kind;
  clear_invalid @@ gas_limit_id kind;
  clear_invalid @@ storage_limit_id kind

let rec string_of_entry_ty = function
  | EAUnit -> "unit"
  | EABytes -> "bytes"
  | EAString -> "string"
  | EANat -> "nat"
  | EAInt -> "int"
  | EABool -> "bool"
  | EATimestamp -> "timestamp"
  | EAMutez -> "mutez"
  | EAAddress -> "address"
  | EAOperation -> "operation"
  | EAKey -> "key"
  | EAKey_hash -> "keyhash"
  | EASignature -> "signature"
  | EAChain_id -> "chainid"
  | EAContract -> Printf.sprintf "contract"
  | EAList t -> Printf.sprintf "%s list" (string_of_entry_arg t)
  | EAPair (t1, t2) -> Printf.sprintf "%s * %s" (string_of_entry_arg t1) (string_of_entry_arg t2)
  | EAOption t -> Printf.sprintf "%s option" (string_of_entry_arg t)
  | EAOr (t1, t2) -> Printf.sprintf "%s | %s" (string_of_entry_arg t1) (string_of_entry_arg t2)
  | EASet t -> Printf.sprintf "%s set" (string_of_entry_arg t)
  | EAMap (t1, t2) -> Printf.sprintf "%s * %s map" (string_of_entry_arg t1) (string_of_entry_arg t2)
  | EABigmap t -> Printf.sprintf "%s bigmap" (string_of_entry_arg t)

and string_of_entry_arg ea = match ea.ea_name, ea.ea_type with
  | None, None -> string_of_entry_ty ea.ea_arg
  | Some n, None -> Printf.sprintf "%s(%s)" n @@ string_of_entry_ty ea.ea_arg
  | None, Some t -> Printf.sprintf "%s(%s)" t @@ string_of_entry_ty ea.ea_arg
  | Some n, Some t -> Printf.sprintf "%s%s(%s)" n t @@ string_of_entry_ty ea.ea_arg

let rec flatten_pair ea = match ea.ea_arg with
  | EAPair (ea1, ea2) ->
    ea1 :: flatten_pair ea2
  | _ -> [ ea ]

let rec flatten_or ea = match ea.ea_arg with
  | EAOr (ea1, ea2) ->
    ea1 :: flatten_or ea2
  | _ -> [ ea ]

let def_name ea = match ea.ea_name with
  | None -> "anon arg"
  | Some t -> t

let def_type ea = match ea.ea_type with
  | None -> string_of_entry_ty ea.ea_arg
  | Some t -> t

let rec make_arg_dom arg = match arg.ea_arg with
  | EAUnit ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EABytes ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAString ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EANat ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAInt ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EABool ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EATimestamp ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAMutez ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAAddress ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAOperation ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAKey ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAKey_hash ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EASignature ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAChain_id ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAContract ->
    let ty = def_type arg in
    let name = def_name arg in
    short_input ~placeholder:ty "" name
  | EAList ea ->
    let ty = match arg.ea_type with
      | None ->
        let subty = string_of_entry_ty ea.ea_arg in
        Printf.sprintf "%s, %s..." subty subty
      | Some ty -> ty in
    let name = match arg.ea_name with
      | None -> "list"
      | Some n -> n in
    short_input ~placeholder:ty "" name
  | EAPair (_ea1, _ea2) ->
    let l = flatten_pair arg in
    div @@ List.map make_arg_dom l
    (* let name, ty = match arg.ea_name, arg.ea_type with
     *   | None, _ -> "", ""
     *   | Some n, None -> n, ""
     *   | Some n, Some ty -> n, ty in *)
    (* div [ span [ txt (name ^ ty) ] ; make_arg_dom ea1 ; make_arg_dom ea2 ] *)
    (* div [ span [ txt @@ string_of_entry_arg arg ] ] *)
  | EAOption ea ->
    let ty = match arg.ea_type with
      | None ->
        let subty = string_of_entry_ty ea.ea_arg in
        Printf.sprintf "optional %s" subty
      | Some ty -> "optional " ^ ty in
    let name = match arg.ea_name with
      | None -> "option"
      | Some n -> n in
    short_input ~placeholder:ty "" name
  | EAOr (_ea1, _ea2) ->
    let l = flatten_or arg in
    let ldom = List.map (fun ea ->
        let cnstr = match ea.ea_name, ea.ea_type with
          | None, _ -> "anon constructor"
          | Some n, None -> n
          | Some n, Some t -> n ^ t in
        div [ span [ txt cnstr ] ; make_arg_dom { ea with ea_name = None ; ea_type = None } ]
      ) l in
    div @@ [ span [ txt "Constructors" ] ] @ ldom
    (* let ldom = List.map make_arg_dom l in *)
    (* div [ span [ txt @@ String.concat "|" @@ List.map string_of_entry_arg l ] ] *)
  | EASet _ea -> span [ txt "EASet of entry_arg" ]
  | EAMap (_ea1, _ea2) -> span [ txt "EAMap of entry_arg * entry_arg" ]
  | EABigmap _ea -> span [ txt "EABigmap of entry_arg" ]

let make_entries_dom entries =
  div ~a:[ a_id "contract-ep" ;
           a_class [ Collapse.accordion ; Text.left ; Spacing.mxa ] ]
    (List.map (fun (ep_name, ep_args) ->
         let ep_id = Printf.sprintf "ep-%s" ep_name in
         let ep_id_card = Printf.sprintf "%s-card" ep_id in
         let ep_id_body = Printf.sprintf "%s-body" ep_id in
         let ep_id_header = Printf.sprintf "%s-header" ep_id in
         div ~a:[ a_id ep_id_card ; a_class [ BCard.card ; Spacing.mb2 ] ] [
           div ~a:[ a_id ep_id_header ;
                    a_class [ BCard.card_header ; Spacing.p0 ;
                              Border.border0 ] ] [
             h2 ~a:[ a_class [ Spacing.mb1 ; Spacing.py1 ; Spacing.px2 ] ] [
               button ~a:[
                 a_class [ Text.left ; Sizing.w100 ; Button.btn ; Spacing.p0 ;
                           Button.btn_link ] ;
                 Attribute.a_data_toggle Collapse.collapse;
                 Attribute.a_data_custom "target" ("#" ^ ep_id_body ) ;
                 Attribute.a_aria "expanded" "false" ; ] [
                 txt ep_name ]
             ]
           ] ;
           div ~a:[ a_id ep_id_body ;
                    a_class [ Collapse.collapse ] ;
                    Attribute.a_data_toggle Collapse.collapse ;
                    Attribute.a_data_custom "parent" "#contract-ep" ;
                    Attribute.a_aria "labelledby" ep_id_header ;
                  ] [
             div ~a:[ a_class [ BCard.card_body ] ] [
               (* make_arg_dom ep_args *)
               span [ txt @@ Dune_encoding_min.Script.encode ep_args ]
             ]
           ]
         ])
        entries)

let handler_transaction_form ?(title="") ~refresh ~update ?(update_anim=update)
    ?(redirect=true) ?(collapsed=true) ?dst_value ?amount_value state
    ?mk_loading_ui ~mk_ok_ui ~mk_error_ui =
  let kind = "transaction" in
  let options_f = if redirect then options_form_redirect else options_form ~collapsed in
  (* let detect_kt1_call ev =
   *   begin match src_value ev with
   *     | Some s ->
   *       if Crypto.check_pkh s then
   *         begin match Crypto.prefix_dn s with
   *           | Some p when p = Crypto.Prefix.contract_public_key_hash ->
   *             let load = Js_utils.find_component contract_loading_id in
   *             Js_utils.Manip.removeClass load BMisc.invisible ;
   *             Metal_request.Node.get_entrypoints ~network:state.fo_net s
   *               (fun eps -> match eps with
   *                  | [] ->
   *                    Js_utils.log "%s doesn't have entry points" s ;
   *                    Js_utils.Manip.addClass load BMisc.invisible
   *                  | _ ->
   *                    let dom = make_entries_dom eps in
   *                    replace1 contract_loading_id dom)
   *               ~error:(fun _code _msg -> Js_utils.Manip.addClass load BMisc.invisible)
   *           | _ -> ()
   *         end
   *       else ()
   *     | None -> ()
   *   end ;
   *   true
   * in *)
  update [
    div ~a:[ a_class ["operation-form"; Spacing.ma; Spacing.py3] ] ([
        div ~a:[ a_class [Text.lead] ] [ txt title ];
        short_input ?value:dst_value (* ~oninput:detect_kt1_call *) dest_tr_id "Destination";
        short_input ?value:amount_value ~input_type:`Number ~append:dun_str amount_tr_id "Amount" ]
        @ (options_f ~kind ())
        @ (contract_call_loading ())
        @ [
          div ~a:[ a_class [Text.center] ] [
            button ~a:[
              a_class btn_classes;
              a_onclick (fun _e ->
                  clear_invalid_transaction ();
                  match get_transaction_info ~more:(not redirect) () with
                  | Error () -> true
                  | Ok (dest, amount, entry, params, fee, gas_limit, storage_limit) ->
                    begin match mk_loading_ui with
                      | Some f ->
                        let dom = f () in
                        update [ dom ]
                      | None -> ()
                    end ;
                    let tsp = Jsdate.now_tsp () in
                    let notif =
                      Metal_helpers.make_notif_tr
                        ~amount
                        ?fee
                        ?gas_limit
                        ?storage_limit
                        ?entry
                        ?params
                        ~id:"metal"
                        ~wid:~-1
                        ~tsp
                        ~origin:"metal"
                        dest in
                    Async.async (fun () ->
                        mk_ok_ui state.fo_acc state.fo_net notif >>= function
                        | Ok res ->
                          update_anim res ;
                          return_unit
                        | Error err ->
                          let dom = mk_error_ui err in
                          update_anim dom ;
                          return_unit
                      );
                    true)
            ] [
              txt "Send" ];
            button ~a:[ a_onclick (fun _e -> refresh state; true);
                        a_class btn_back_classes ] [
              txt "Cancel" ] ]
        ])
  ]

let handler_origination_form ?(title="") ~refresh ~update ?(update_anim=update)
    ?(redirect=true) ?(collapsed=true) ?balance_value state
    ?mk_loading_ui ~mk_ok_ui ~mk_error_ui =
  let kind = "origination" in
  let options_f = if redirect then options_form_redirect else options_form ~collapsed in
  update [
    div ~a:[ a_class ["operation-form"; Spacing.ma; Spacing.py3] ] ([
        div ~a:[ a_class [Text.lead] ] [ txt title ];
        short_input
          ?value:balance_value
          ~input_type:`Number
          ~append:dun_str balance_or_id
          "Balance" ;
        short_input_wlabel code_hash_id "Code Hash";
        short_textarea storage_id "Storage" ]
        @ (options_f ~kind ()) @ [
          div ~a:[ a_class [Text.center] ] [
            button ~a:[
              a_class btn_classes;
              a_onclick (fun _e ->
                  clear_invalid_origination ();
                  match get_origination_info ~more:(not redirect) () with
                  | Error () -> true
                  | Ok (amount, sc_code_hash, sc_code, sc_storage, fee,
                        gas_limit, storage_limit) ->
                    begin match mk_loading_ui with
                      | Some f ->
                        let dom = f () in
                        update [ dom ]
                      | None -> ()
                    end ;
                    let tsp = Jsdate.now_tsp () in
                    let notif =
                      Metal_helpers.make_notif_ori
                        ~amount
                        ?fee
                        ?gas_limit
                        ?storage_limit
                        ?sc_code
                        ?sc_code_hash
                        ?sc_storage
                        ~id:"metal"
                        ~wid:~-1
                        ~tsp
                        ~origin:"metal"
                        ()
                    in
                    Async.async (fun () ->
                        mk_ok_ui state.fo_acc state.fo_net notif >>= function
                        | Ok res ->
                          update_anim res ;
                          return_unit
                        | Error err ->
                          let dom = mk_error_ui err in
                          update_anim dom ;
                          return_unit
                      );
                    true) ] [
              txt "Originate" ];
            button ~a:[ a_onclick (fun _e -> refresh state; true);
                        a_class btn_back_classes ] [
              txt "Cancel" ] ]
        ])
  ]

let handler_delegation_form ?(title="") ~refresh ~update ?(update_anim=update)
    ?(redirect=true) ?(collapsed=true) ?dlg_value state services ?old_delegate
    ?mk_loading_ui ~mk_ok_ui ~mk_error_ui =
  let kind = "delegation" in
  let options_f = if redirect then options_form_redirect else options_form ~collapsed in
  let services =
    Metal_helpers.shuffle @@
    List.filter (fun service -> service.srv_kind = "delegate") services in
  let delegate_container =
    match services with
    | [] ->
      begin match old_delegate with
        | None -> span []
        | Some { dn ; alias } ->
          let curr = match alias with None -> dn | Some s -> s ^ " - " ^ dn in
          let s = Printf.sprintf "Unset current - %s" curr in
          div [
            h5 [ txt "Select custom baker or unset the current one" ] ;
            select
              ~a:[ a_id Metal_ids.IdPopup.delegates_id ;
                   a_onchange (fun _e ->
                       let input_container = Js_utils.find_component delegate_dlg_id in
                       let select_container = Js_utils.find_component Metal_ids.IdPopup.delegates_id in
                       let choosen_delegate = Js_utils.Manip.value select_container in
                       Js_utils.Manip.set_value input_container choosen_delegate ;
                       true) ;
                   a_class [ BForm.custom_select ] ]
              [ option ~a:[ a_value ""] (txt "Please choose a new delegate") ;
                option ~a:[ a_value "unset"] ( txt s) ]
          ]
      end
    | _ ->
      div [
        h5 [ txt "Select an official baker or chose a custom one" ] ;
        select
          ~a:[ a_id Metal_ids.IdPopup.delegates_id ;
               a_oninput (fun _e ->
                   let input_container = Js_utils.find_component delegate_dlg_id in
                   let select_container = Js_utils.find_component Metal_ids.IdPopup.delegates_id in
                   let choosen_delegate = Js_utils.Manip.value select_container in
                   Js_utils.Manip.set_value input_container choosen_delegate ;
                   true) ;
               a_class [ BForm.custom_select ] ] @@
        (begin match old_delegate with
           | None -> [option ~a:[ a_value ""] (txt "Please choose a new delegate")]
           | Some { dn ; alias } ->
             let curr = match alias with None -> dn | Some s -> s ^ " - " ^ dn in
             let s = Printf.sprintf "Unset current - %s" curr in
             [ option ~a:[ a_value ""] (txt "Please choose a new delegate") ;
               option ~a:[ a_value "unset"] ( txt s) ]
         end
         @ (List.map (fun service ->
             let dn1 =
               match service.srv_dn1 with
               | None -> service.srv_name
               | Some dn1 -> dn1 in
             option ~a:[ a_value dn1] ( txt @@ service.srv_name ^ " - " ^ dn1 ))
             services))
      ]
  in
  update [
    div ~a:[ a_class ["operation-form"; Spacing.ma; Spacing.py3] ] ([
        div ~a:[ a_class [Text.lead] ] [ txt title ];
        delegate_container ;
        short_input ?value:dlg_value delegate_dlg_id "Delegate" ]
        @ (options_f ~kind ()) @ [
          div ~a:[ a_class [Text.center] ] [
            button ~a:[
              a_class btn_classes;
              a_onclick (fun _e ->
                  clear_invalid_delegation ();
                  match get_delegation_info ~more:(not redirect) () with
                  | Error () -> true
                  | Ok (dlg, fee, gas_limit, storage_limit) ->
                    let dlg = if dlg = "unset" || dlg = "" then None else Some dlg in
                    begin match mk_loading_ui with
                      | Some f ->
                        let dom = f () in
                        update [ dom ]
                      | None -> ()
                    end ;
                    let tsp = Jsdate.now_tsp () in
                    let notif =
                      Metal_helpers.make_notif_del
                        ?fee
                        ?gas_limit
                        ?storage_limit
                        ~id:"metal"
                        ~wid:~-1
                        ~tsp
                        ~origin:"metal"
                        dlg in
                    Async.async (fun () ->
                        mk_ok_ui state.fo_acc state.fo_net notif >>= function
                        | Ok res ->
                          update_anim res ;
                          return_unit
                        | Error err ->
                          let dom = mk_error_ui err in
                          update_anim dom ;
                          return_unit
                      );
                    true) ] [
              txt "Delegate" ];
            button ~a:[ a_onclick (fun _e -> refresh state; true); a_class btn_back_classes ] [
              txt "Cancel" ] ]
        ])
  ]

let handler_manage_account_form ?(title="") ~refresh ~update ?(update_anim=update)
    ?(redirect=true) ?(collapsed=true)
    ?maxrolls_value ?admin_value ?white_list_value state
    ?mk_loading_ui ~mk_ok_ui ~mk_error_ui =
  let kind = "manage_account" in
  let options_f = if redirect then options_form_redirect else options_form ~collapsed in
  update [
    div ~a:[ a_class ["operation-form"; Spacing.ma; Spacing.py3] ] ([
        div ~a:[ a_class [Text.lead] ] [ txt title ];
        short_input ?value:maxrolls_value maxrolls_id "Max Rolls" ;
        short_input ?value:admin_value admin_id "Admin" ;
        short_input ?value:white_list_value white_list_id "White List" ;
        short_select delegation_id "Delegation"
          ["none", ""; "true", "true"; "false", "false"] ]
        @ (options_f ~kind ()) @ [
          div ~a:[ a_class [Text.center] ] [
            button ~a:[
              a_class btn_classes;
              a_onclick (fun _e ->
                  clear_invalid_manage_account ();
                  match get_manage_account_info () with
                  | Error () -> true
                  | Ok (maxrolls, admin, white_list, delegation, fee,
                        gas_limit, storage_limit) ->
                    begin match mk_loading_ui with
                      | Some f ->
                        let dom = f () in
                        update [ dom ]
                      | None -> ()
                    end ;
                    let tsp = Jsdate.now_tsp () in
                    let notif =
                      Metal_helpers.make_notif_mac
                        ?fee
                        ?gas_limit
                        ?storage_limit
                        ?maxrolls
                        ?admin
                        ?white_list
                        ?delegation
                        ~id:"metal"
                        ~wid:~-1
                        ~tsp
                        ~origin:"metal"
                        ()
                    in
                    Async.async (fun () ->
                        mk_ok_ui state.fo_acc state.fo_net notif >>= function
                        | Ok res ->
                          update_anim res ;
                          return_unit
                        | Error err ->
                          let dom = mk_error_ui err in
                          update_anim dom ;
                          return_unit
                      );
                    true) ] [
              txt "Send" ];
            button ~a:[ a_onclick (fun _e -> refresh state; true);
                        a_class btn_back_classes ] [
              txt "Cancel" ] ]
        ])
  ]

let import_kt1 {pkh; vault; _} fo_net kt1s = match vault with
  | Local vault ->
    let rec aux hd tl =
      let next () = match tl with
        | [] -> ()
        | hd :: tl -> aux hd tl in
      Async.(async_req ~error:(fun code content -> Js_utils.log "error import kt1 %d %s" code content)
               (Vault.Lwt.decrypt ~pkh ~vault >>=? fun sk ->
                Vault.Lwt.encrypt ~manager:pkh ~pkh:hd sk)
               (fun account_kt1 ->
                  Metal_request.Node.get_account_info ~network:fo_net
                    ~error:(fun _ _ ->
                        Storage_writer.add_account account_kt1 ;
                        next ())
                    hd
                    (fun acc_info ->
                       let manager_kt = match acc_info.Dune_types_min.node_ai_script with
                         | Some (Some sc, _, _) -> Some (sc = Node.manager_kt)
                         | _ -> Some false in
                       Storage_writer.add_account { account_kt1 with manager_kt } ;
                       next ()
                    ))) in
    begin match kt1s with
      | hd :: tl -> aux hd tl
      | [] -> ()
    end
  | Ledger _ ->
    List.iter (fun kt1 ->
        let account_kt1 =
          {pkh = kt1; vault; name = kt1; revealed=[]; pending=[]; manager = pkh;
           manager_kt = None ; admin = []} in
        Metal_request.Node.get_account_info ~network:fo_net
          ~error:(fun _ _ -> Storage_writer.add_account account_kt1)
          kt1
          (fun acc_info ->
             let manager_kt = match acc_info.Dune_types_min.node_ai_script with
               | Some (Some sc, _, _) -> Some (sc = Node.manager_kt)
               | _ -> Some false in
             Storage_writer.add_account { account_kt1 with manager_kt }
          )) kt1s

let handler_import_form ~refresh ~update state =
  let import_form kt1_list =
    let len = List.length kt1_list in
    let title =
      h3 ~a:[ a_class [Text.bold; Spacing.my3] ] [
        txt "KT1 previously originated" ] in
    let kt1s =
      if len = 0 then
        [ div ~a:[ a_class [ Flex.flex_fill ] ] [ txt "No KT1 to be imported" ] ]
      else
        List.map (fun kt1 ->
            div ~a:[ a_class [Text.left; Spacing.ml3] ] [
              short_checkbox kt1 kt1 ]) kt1_list in
    let imp_btn =
      div ~a:[ a_class [ Spacing.mta ; Spacing.mb3] ] [
        button ~a:
          ((a_class (btn_classes @ (if len = 0 then [ BMisc.disabled ] else [])) ::
           if len = 0 then [ a_disabled () ] else []
          ) @ [
          a_onclick (fun _e ->
              let kt1_list = List.filter is_input_checked kt1_list in
              let len = List.length kt1_list in
              let title =
                h3 ~a:[ a_class [Text.bold; Spacing.my3] ] [
                  txt @@ Printf.sprintf "Importing %d KT1" len ] in
              let cm = div ~a:[ a_class [ "checkmark"; "draw" ] ] [] in
              let loading =
                div ~a:[ a_class [ "circle-loader"; Spacing.mxa ] ] [ cm ] in
              let back =
                button ~a:[ a_class @@
                            [ Spacing.mxa ; BMisc.disabled ] @ btn_classes;
                            a_onclick (fun _e -> refresh state; true)] [
                  txt "Continue" ] in
              let elm =
                div ~a:[ a_class [ Display.d_flex ; Flex.flex_column;
                                   Flex.justify_around ] ]
                  [ title ; loading ; back ] in
              update [ elm ] ;
              import_kt1 state.fo_acc state.fo_net kt1_list;
              Js_utils.Manip.addClass loading "load-complete" ;
              Js_utils.Manip.removeClass back BMisc.disabled ;
              true)
        ]) [
          txt "Import" ] ] in
    let back_btn =
      div ~a:[ a_class [ Spacing.mta ; Spacing.mb3 ] ] [
        button ~a:[
          a_class btn_back_classes;
          a_onclick (fun _e -> refresh state ; true)
        ] [ txt "Back" ] ] in
    [ title ;
      div ~a:[ a_class [ "kt1-list"; Flex.flex_fill; Spacing.mya ] ] kt1s ;
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_row ;
                         Spacing.mxa ] ] [
        imp_btn ; back_btn ] ]
  in
  Metal_request.Scan.get_originations ~network:state.fo_net
    state.fo_acc.manager
    (fun l_ori ->
        Storage_reader.get_accounts (fun l_accounts ->
            let l_ori = List.filter (fun (_, ori) -> not ori.Data_types_min.or_failed) l_ori in
            let kt1_list_all = List.map (fun (_, ori) -> ori.Data_types_min.or_kt1.dn) l_ori in
            let pkh_list = List.map (fun {pkh; _} -> pkh) l_accounts in
            let kt1_list = List.filter (fun kt1 -> not (List.mem kt1 pkh_list)) kt1_list_all in
            update @@ import_form kt1_list))

let handler_deposit_form ?(title="") ~refresh ~update state =
  let title = title in
  let _refresh = refresh in
  let pkh = state.fo_acc.pkh in
  let qrcode_container = div ~a:[ a_id "qr-code" ] [] in
  let qrcode_content = Js_utils.Manip.get_elt "qr-code" qrcode_container in
  Qrcode.make_code qrcode_content pkh ;
  let blockies_container =
    blockies
      ~classes:[ "rounded-blockies" ; Flex.align_self_center ]
      state.fo_acc.pkh in
  let close_button_container =
    div ~a:[ a_class [ Display.d_flex ] ] [
      button ~a:[ a_class [ Button.btn; Spacing.mla; Spacing.m2; Spacing.p0 ];
                  a_onclick (fun _e -> refresh state; true) ] [
        span ~a:[ a_class [Spacing.mx1] ] [ fas "times" ]
      ]
    ]  in
  let alias_pkh_blockies_container =
    div ~a:[ a_id Metal_ids.IdPopup.address_id ;
             a_class [ Text.center ; Spacing.ml3 ; Spacing.mb3 ;
                       Spacing.mr3 ; "custom-tooltip" ] ] [
      div ~a:[ a_class [Text.lead] ] [ txt title ] ;
      if state.fo_acc.name <> state.fo_acc.pkh then
        div ~a:[ a_class [ "account-alias" ; "account-overflow" ; Spacing.mb2 ] ] [ txt state.fo_acc.name ]
      else
        span [  ] ;
      div ~a:[ a_class [ Spacing.mb2 ] ] [ blockies_container ] ;
      a ~a:[
        a_onmouseout (fun _ ->
            let tooltip = Js_utils.find_component "x" in
            Js_utils.Manip.setInnerHtml tooltip "Copy to clipboard" ;
            true) ;
        a_onclick (fun _ ->
            let tooltip = Js_utils.find_component "x" in
            copy_str state.fo_acc.pkh ;
            Js_utils.Manip.setInnerHtml tooltip ("Copied !") ;
            true) ] [
        div ~a:[ a_class [ "custom-tooltiptext" ] ; a_id "x" ] [ txt "Copy to clipboard"] ;
        span [ txt state.fo_acc.pkh ]
      ]
    ] in
  let dunscan_link_container =
    let open Button in
    div ~a:[ a_id Metal_ids.IdPopup.dunscan_link_id ;
             a_class [ Spacing.mt3 ] ] [
      a ~a:[ a_class [ btn ; btn_light; btn_outline_primary; Spacing.m2 ] ;
             a_target "_blank" ;
             a_href @@
             Metal_helpers_ui.dunscan_path ~suffix:state.fo_acc.pkh state.fo_net ] [
        img ~a:[ a_class [ Spacing.pr2 ] ] ~src:"img/dune.png" ~alt:"DunScan" () ;
        txt "View on DunScan"
      ]
    ]
  in
  let content =
    div [
      (* Close button *)
      close_button_container ;
      div ~a:[ a_class [ Text.center ; Flex.align_self_center ; Spacing.ma ] ] [
        (* Address (with alias if setup) *)
        alias_pkh_blockies_container ;
        (* Qr code *)
        qrcode_container ;
        dunscan_link_container ;
      ] ] in
  update @@ content
