(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

(* Ocplib-jsutils *)
open Ocp_js
open Html
open Js_utils.Manip

(* Bootstrap 4 *)
open Bs4.Items
open Button
open BForm
open Bs4.Utils

(* Metal *)
open Metal_types
open Metal_request
open Metal_ids.IdPopup
open Metal_helpers_ui
open Metal_helpers

open Async

let error_callback n code err =
  let id = notif_id_of_not n in
  let data = object%js
    val code = code
    val error = Js.string err ;
  end in
  let msg =
    Metal_message.mk_message ~data ~name:"callback_error_notif" ~src:"popup" id in
  let info = Runtime_utils.mk_connection_info "popup" in
  let port = Chrome.Runtime.connect ~info () in
  port##postMessage msg ;
  port##disconnect

let deny_callback n =
  let id = notif_id_of_not n in
  let data = object%js
    val ok = Js._false
  end in
  let msg =
    Metal_message.mk_message ~data ~name:"callback_notif" ~src:"popup" id in
  let info = Runtime_utils.mk_connection_info "popup" in
  let port = Chrome.Runtime.connect ~info () in
  port##postMessage msg ;
  port##disconnect

let deny_handler refresh state n =
  deny_callback n ;
  Storage_writer.remove_notif ~callback:(fun () -> refresh state) n

let approve_callback ?msg n =
  let id = notif_id_of_not n in
  let data = object%js
    val ok = Js._true
    val msg = Js.Optdef.option msg
  end in
  let msg =
    Metal_message.mk_message ~data ~name:"callback_notif" ~src:"popup" id in
  let info = Runtime_utils.mk_connection_info "popup" in
  let port = Chrome.Runtime.connect ~info () in
  port##postMessage msg ;
  port##disconnect

let approve_handler refresh state ?msg n =
  approve_callback ~msg n ;
  Storage_writer.remove_notif ~callback:(fun () -> refresh state) n

let mk_confirm_button ~attr ~cb =
  let btn_id = "ok-btn" in
  let click = (fun _ev ->
      try
        replace1 btn_id (Metal_helpers_ui.fas "hourglass-half") ;
        cb () ;
        true
      with _ -> cb () ; true) in
  button ~a:([ a_id btn_id ;
               a_onclick click ;
               a_class [ btn; btn_outline_success; "btn-circle" ; "btn-md" ]
             ] @ attr) [
    Metal_helpers_ui.fas "check" ]

let mk_cancel_button ~attr ~cb =
  let click = (fun _ev -> cb () ; true) in
  button ~a:([ a_onclick click ;
               a_class [ btn; btn_outline_danger ; "btn-circle"; "btn-md" ] ] @
             attr) [
    Metal_helpers_ui.fas "times" ]

let mk_dismiss_button ~cb =
  let click = (fun _ev -> cb () ; true) in
  button ~a:[ a_onclick click ;
              a_class [ btn; btn_outline_danger ; "btn-circle"; "btn-md" ] ] [
    Metal_helpers_ui.fas "times" ]

let make_tab_li ?(active=false) name text =
  li ~a:[ a_class [ Nav.nav_item ] ]  [
    a ~a:[ a_id (name ^ "-tab") ;
           a_class (Nav.nav_link :: Spacing.p1 ::
                    (if active then ["active"] else [])) ;
           Attribute.a_data_toggle "tab" ;
           a_href ("#" ^ name) ;
           a_role [ "tab" ] ;
           a_aria "controls" [ name ] ;
           a_aria "selected" [ "true" ] ] [
      txt text ] ]

let make_tab_pane ?(active=false) name l =
  div ~a:[ a_id name ;
           a_class (Nav.tab_pane :: "fade" :: (if active then ["show"; "active"] else [])) ;
           a_role [ "tabpanel" ] ; a_aria "labelledby" [ name ^ "-tab" ] ] [
    div ~a:[ a_class [ Display.d_flex; Flex.flex_column ; Spacing.m3 ] ] @@
    l ]

let make_forging_dom () =
  div ~a:[ a_class [ Display.d_flex ; Spacing.mya ] ] [
    div ~a:[ a_class [ Spacing.ma ; Text.center ] ] [
      div ~a:[ a_class [ Spacing.mb5 ] ] [
        h2 [ txt "Forging the operation" ]
      ] ;
      div [
        img
          ~src:"/img/pickaxe.png"
          ~alt:"Forging"
          ~a:[ a_class [ "rotate-90-cw" ] ]
          ()
      ] ;
      div ~a:[ a_class [ Spacing.mt5 ] ] [
        h3 [ txt "Please wait..." ]
      ]
    ]
  ]

let make_error_dom dismiss err =
  let code, msg = error_content err in
  [
    div ~a:[ a_class [ Alert.alert ; Alert.adanger ; Spacing.m2 ;
                       Text.left ; Spacing.mya ] ;
             a_role [ "alert" ] ] [
      h3 ~a:[ a_class [ "alert-heading"; Display.d_flex ] ] [
        fas "exclamation" ;
        span ~a:[ a_class [ Spacing.ml2 ] ][
          txt @@
          "Oops ! An error occured..." ] ;
      ] ;
      div [
        div ~a:[ a_class [ Spacing.pb1 ] ] [
          txt "Here is some imformations about what happend :" ] ;
        div ~a:[ a_class [ Spacing.pb1 ] ] [
          txt ("Code :" ^ (string_of_int code)) ] ;
        div ~a:[ a_class [ Spacing.pb1 ] ] [ txt "Message :" ] ;
          pre ~a:[ a_class [ Spacing.p2; Color.danger_bg ] ] [ txt msg ]
      ] ;
    ] ;
    div ~a:[ a_class [ Spacing.my2 ; Spacing.mxa ; Spacing.mb5 ;
                       Display.d_flex ] ] [
      mk_dismiss_button ~cb:dismiss ;
    ]
  ]

let mk_op_page ~(confirm: ?msg:'a Js.t -> Metal_types.notif_kind -> unit)
    ?(attr=[]) ~deny acc net notif =
  let make_tr_div tr_dst_div =
    let tr_src_div =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_row;
                         Flex.align_items_center; Spacing.mb1 ] ] [
        Metal_helpers_ui.blockies
          ~classes:["account-blockies"; Spacing.mr3]
          acc.pkh ;
        span [ txt acc.pkh ]
      ] in
    let tr_arrow =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_row;
                         Flex.align_items_center ; "tr-infos" ;
                         Spacing.mr2 ] ] [
        div [ Metal_helpers_ui.fas "level-down-alt" ]
      ] in
    let accs_div =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_column ;
                         Flex.flex_fill ] ] [
        tr_src_div ;
        tr_dst_div ;
      ] in
    div ~a:[ a_class [ Display.d_flex ; Spacing.mt1 ; Spacing.mxa ] ] [
      tr_arrow ;
      accs_div ;
    ] in
  let make_amount_div ?manager_kt amount fee burn = [
    let fee_div = match manager_kt with
      | Some true ->
        div [ span [ txt "Fee" ] ;
              span ~a:[ a_class [ Color.danger_tx ; Spacing.ml2 ] ;
                        a_title "Fee will be paid by the manager" ] [
                fas "exclamation-circle" ] ]
      | _ ->
        span [ txt "Fee" ] in
    div ~a:[ a_class [ Display.d_flex; Spacing.m1 ] ] [
      div ~a:[ a_class [ "currency" ; Flex.flex_fill ;
                         Flex.align_self_center ] ] [
        img
          ~src:"img/dune-logo-square.png"
          ~alt:"Dune Network"
          ()
      ] ;
      div ~a:[ a_class [ Flex.flex_fill ] ] [
        div ~a:[ a_class [ Display.d_flex; Flex.flex_column ] ] [
          span [ txt "Amount" ] ;
          Dun.pp_amount ~precision:2 amount ;
        ]
      ] ;
      div ~a:[ a_class [ Flex.flex_fill ; "burn" ] ] [
        div ~a:[ a_class [ if burn = 0L then
                             Display.d_none
                           else
                             Display.d_flex;
                           Flex.flex_column ] ] [
          span [ txt "Burn" ] ;
          Dun.pp_amount ~precision:2 burn ;
        ]
      ] ;
      div ~a:[ a_class [ Flex.flex_fill ] ] [
        div ~a:[ a_class [ Display.d_flex; Flex.flex_column ] ] [
          fee_div ;
          Dun.pp_amount ~precision:2 fee ;
        ]
      ] ;
      div ~a:[ a_class [ Flex.flex_fill; Color.primary_tx ] ] [
        div ~a:[ a_class [ Display.d_flex; Flex.flex_column ] ] [
          span [ txt "Total" ] ;
          Dun.pp_amount ~precision:2 (Int64.add amount @@ Int64.add fee burn) ;
        ]
      ]
    ] ] in
  let cb_nok = (fun () -> deny notif) in
  let alert cb_ok =
    div ~a:[ a_class [ Alert.alert ; Alert.aprimary; Spacing.m2; Text.left ] ;
             a_role [ "alert" ] ] [
      h3 ~a:[ a_class [ "alert-heading"; Display.d_flex ;
                        Flex.justify_between ] ] [
        span [
          txt @@
          "The selected network is " ^
          Metal_helpers.network_to_string net ] ;
      ] ;
      div [
        span [ txt "Sending tokens is irreversible" ] ;
      ] ;
      div ~a:[ a_class [ Spacing.my2 ; Spacing.mx5 ;
                         Display.d_flex ; Flex.justify_between ] ] [
        mk_confirm_button ~attr ~cb:cb_ok ;
        mk_cancel_button ~attr ~cb:cb_nok
      ]
    ] in

  match notif with
  | TraNot tr ->
    let dst = match tr.not_dst with None -> "" | Some d -> d in
    let amount =
      match tr.not_amount with None -> 0L | Some am -> am in
    let fee = tr.not_fee in
    let entrypoint = tr.not_entry in
    let parameters = tr.not_params in
    let gas_limit = tr.not_gas_limit in
    let storage_limit = tr.not_storage_limit in

    begin match acc.manager_kt with
    | Some true ->
      Metal_request.Node.get_node_base_url_lwt net >>= fun base ->
      Metal_node.manager_kt_transaction_param ~base dst amount entrypoint parameters
      >>= fun parameters ->
      let amount = 0L in
      let dst = acc.pkh in
      let acc = { acc with pkh = acc.manager } in
      let entrypoint = "do" in
      Metal_request.Node.forge_transaction ~network:net
        ~entrypoint ?parameters ?gas_limit ?storage_limit
        ?fee acc dst amount
    | _ ->
      Metal_request.Node.forge_transaction ~network:net
        ?entrypoint ?parameters ?gas_limit ?storage_limit
        ?fee acc dst amount
    end >>|? fun (op, ops) ->
    let fee, gas_limit, storage_limit = Dune_utils.limits_of_operations ops in
    let burn = Int64.mul 1000L @@ Z.to_int64 storage_limit in

    let tr_dst_div =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_row ;
                         Flex.align_items_center ; Spacing.mb2 ] ] [
        Metal_helpers_ui.blockies
          ~classes:["account-blockies" ; Spacing.mr3]
          dst ;
        span [ txt dst ]
      ] in
    let tr_div = make_tr_div tr_dst_div in

    let detail_divs = Metal_helpers_ui.add_gas_limit gas_limit [] in
    let detail_divs =
      Metal_helpers_ui.add_storage_limit storage_limit detail_divs in
    let param_div = Metal_helpers_ui.add_param tr [] in
    let amount_div = make_amount_div ?manager_kt:acc.manager_kt amount fee burn in

    let tr_info = div ~a:[ a_class [ Text.left ] ] [
        ul ~a:[ a_class [ Nav.nav ; Nav.nav_tabs ;
                          Nav.navbar_dark ; Spacing.px1 ];
                a_role [ "tablist" ] ] [
          make_tab_li ~active:true "amount" "Amount";
          make_tab_li "details" "Details";
          make_tab_li "param" "Parameter";
        ] ;
        div ~a:[ a_class [ Nav.tab_content ] ] [
          make_tab_pane ~active:true "amount" amount_div;
          make_tab_pane "details" (List.rev detail_divs);
          make_tab_pane "param" (List.rev param_div) ] ] in
    let cb_ok = (fun () ->
        Metal_request.Node.send_operation ~network:net acc (op, ops)
          ~error:(fun _c _msg -> ignore @@ deny notif)
          (fun (hash, ops) ->
             let hash = Crypto.Operation_hash.b58enc hash in
             let pending =
               List.rev @@ List.fold_left (fun acc op -> match op with
                   | Dune_types_min.NTransaction tr ->
                     (Metal_helpers_ui.node_tr_to_trs hash tr) :: acc
                   | _ -> acc) [] ops in
             let fo_acc = { acc with pending } in
             Storage_writer.update_account
               ~callback:(fun _ -> confirm ~msg:(Js.string hash) notif)
               fo_acc)
      ) in

    [ tr_div ; tr_info ; alert cb_ok ]

  | OriNot ori ->
    Js_utils.log "make_op_page ori" ;
    let amount =
      match ori.not_amount with None -> 0L | Some am -> am in
    let fee = ori.not_fee in
    let gas_limit = ori.not_gas_limit in
    let storage_limit = ori.not_storage_limit in
    let script = match ori.not_sc_code, ori.not_sc_storage, ori.not_sc_code_hash with
      | None, None, None -> None
      | _, None, _ -> None
      | _, Some sc_storage, _ ->
        Some { Dune_types_min.sc_code = ori.not_sc_code ;
               sc_storage ;
               sc_code_hash = ori.not_sc_code_hash } in

    Metal_request.Node.forge_origination ~network:net ?script ?gas_limit
      ?storage_limit ?fee acc amount >>|? fun (op, ops) ->
    let dst = match script with
      | Some _ -> "New contract"
      | None -> "New account" in

    let fee, gas_limit, storage_limit = Dune_utils.limits_of_operations ops in

    let burn = Int64.mul 1000L @@ Z.to_int64 storage_limit in

    let tr_dst_div =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_row ;
                         Flex.align_items_center ; Spacing.mb2 ] ] [
        div ~a:[ a_class [ Spacing.mr3 ] ] [ kt1_filler () ] ;
        span ~a:[ a_class [ Text.left ] ] [ txt dst ]
      ] in
    let tr_div = make_tr_div tr_dst_div in

    let detail_divs = Metal_helpers_ui.add_delegate ori [] in
    let detail_divs = Metal_helpers_ui.add_gas_limit gas_limit detail_divs in
    let detail_divs = Metal_helpers_ui.add_storage_limit storage_limit detail_divs in
    let script_div = Metal_helpers_ui.add_script ori [] in
    let storage_div = Metal_helpers_ui.add_storage ori [] in
    let code_hash_div = Metal_helpers_ui.add_code_hash ori [] in
    let amount_div = make_amount_div amount fee burn in

    let tr_info = div ~a:[ a_class [ Text.left ] ] [
        ul ~a:[ a_class [ Nav.nav ; Nav.nav_tabs ;
                          Nav.navbar_dark ; Spacing.px1 ];
                a_role [ "tablist" ] ] [
          make_tab_li ~active:true "amount" "Amount";
          make_tab_li "details" "Details";
          make_tab_li "script" "Script";
          make_tab_li "storage" "Storage";
          make_tab_li "codehash" "Code Hash";
        ] ;
        div ~a:[ a_class [ Nav.tab_content ] ] [
          make_tab_pane ~active:true "amount" amount_div;
          make_tab_pane "details" (List.rev detail_divs);
          make_tab_pane "script" (List.rev script_div);
          make_tab_pane "storage" (List.rev storage_div);
          make_tab_pane "codehash" (List.rev code_hash_div) ] ] in
    let cb_ok = (fun () ->
        Metal_request.Node.send_operation ~network:net acc (op, ops)
          ~error:(fun _c _msg -> ignore @@ deny notif)
          (fun (hash, _ops) ->
             let pkh = Crypto.op_to_KT1 hash in
             let msg = Js.Unsafe.obj [||] in
             msg##.op_hash := Js.string (Crypto.Operation_hash.b58enc hash);
             msg##.contract := Js.string pkh;
             match acc.vault with
             | Local vault ->
               Async.(
                 async_req
                   (* ~error:(failed_operation ~kind ~refresh:(fun () -> refresh state) ~update) *)
                   (Vault.Lwt.decrypt ~pkh:acc.pkh ~vault >>=? fun sk ->
                    Vault.Lwt.encrypt ~manager:acc.pkh ~pkh sk)
                   (fun account_kt1 ->
                      Metal_request.Node.get_account_info ~network:net
                        ~error:(fun _ _ ->
                            Storage_writer.add_account
                              ~callback:(fun _ -> confirm ~msg notif)
                              account_kt1)
                        pkh
                        (fun acc_info ->
                           let manager_kt = match acc_info.Dune_types_min.node_ai_script with
                             | Some (Some sc, _, _) -> Some (sc = Metal_node.manager_kt)
                             | _ -> Some false in
                           Storage_writer.add_account
                             ~callback:(fun _ -> confirm ~msg notif)
                             { account_kt1 with manager_kt })))
             | Ledger _ ->
               let account_kt1 =
                 {pkh; vault = acc.vault; name = pkh;
                  revealed = []; pending = []; manager = acc.pkh;
                  manager_kt = None; admin = []} in
               Metal_request.Node.get_account_info ~network:net
                 ~error:(fun _ _ ->
                     Storage_writer.add_account
                       ~callback:(fun _ -> confirm ~msg notif)
                       account_kt1)
                 pkh
                 (fun acc_info ->
                    match acc_info.Dune_types_min.node_ai_script with
                    | Some (Some sc, _, _) ->
                      if sc = Metal_node.manager_kt then
                        Storage_writer.add_account
                          ~callback:(fun _ -> confirm ~msg notif)
                          { account_kt1 with manager_kt = Some true}
                      else
                        Storage_writer.add_account
                          ~callback:(fun _ -> confirm ~msg notif)
                          { account_kt1 with manager_kt = Some false }
                    | _ ->
                      Storage_writer.add_account
                        ~callback:(fun _ -> confirm ~msg notif)
                        { account_kt1 with manager_kt = Some false })))
    in
    [ tr_div ; tr_info ; alert cb_ok ]

  | DlgNot del ->
    Js_utils.log "make_op_page del" ;
    (* TODO : unset *)
    let delegate = match del.not_delegate with Some d -> d | None -> "" in
    let old_delegate = ref None in
    Scan.get_node_account ~network:net acc.pkh
      ~error:(fun code msg ->
          Printf.eprintf "Error [%d] %S" code msg ;
          old_delegate := None)
      (fun node_account -> old_delegate := node_account.acc_delegate) ;
    let fee = del.not_fee in
    let gas_limit = del.not_gas_limit in
    let storage_limit = del.not_storage_limit in

    begin match acc.manager_kt with
    | Some true ->
      let amount = 0L in
      let dst = acc.pkh in
      let acc = { acc with pkh = acc.manager } in
      let parameters = Metal_node.manager_kt_delegation_param delegate in
      let entrypoint = "do" in
      Metal_request.Node.forge_transaction ~network:net
        ~entrypoint ?parameters ?gas_limit ?storage_limit
        ?fee acc dst amount
    | _ ->
      Metal_request.Node.forge_delegation ~network:net
        ?gas_limit ?storage_limit ?fee acc delegate
    end >>|? fun (op, ops) ->

    let fee, gas_limit, storage_limit = Dune_utils.limits_of_operations ops in

    let detail_divs = Metal_helpers_ui.add_gas_limit gas_limit  [] in
    let detail_divs = Metal_helpers_ui.add_storage_limit storage_limit detail_divs in
    let amount_div = make_amount_div ?manager_kt:acc.manager_kt 0L fee 0L in

    let tr_info = div ~a:[ a_class [ Text.left ] ] [
        ul ~a:[ a_class [ Nav.nav ; Nav.nav_tabs ;
                          Nav.navbar_dark ; Spacing.px1 ];
                a_role [ "tablist" ] ] [
          make_tab_li ~active:true "amount" "Amount";
          make_tab_li "details" "Details";
        ] ;
        div ~a:[ a_class [ Nav.tab_content ] ] [
          make_tab_pane ~active:true "amount" amount_div;
          make_tab_pane "details" (List.rev detail_divs)
        ] ] in

    let cb_ok = (fun () ->
        Metal_request.Node.send_operation ~network:net acc (op, ops)
          ~error:(fun _c _msg -> ignore @@ deny notif)
          (fun (hash, _ops) ->
             let hash = Crypto.Operation_hash.b58enc hash in
             confirm ~msg:(Js.string hash) notif))
    in

    let dst_div =
      div ~a:[ a_class [ "delegation"; Text.center ] ] [
        div ~a:[ a_class [ Display.d_flex ; Flex.flex_row ;
                           Flex.align_items_center ; Spacing.mb2 ;
                           Flex.justify_start ; "old-delegate" ] ]
          begin match !old_delegate with
            | Some d ->
              [
                fas "hand-point-right" ;
                Metal_helpers_ui.blockies
                  ~classes:["account-blockies" ; Spacing.mx3]
                  d.dn ;
                span [ txt d.dn ]
              ]
            | None ->
              [
                fas "hand-point-right" ;
                Metal_helpers_ui.blockies
                  ~classes:["account-blockies" ; Spacing.mx3 ; BMisc.invisible]
                  "" ;
                span [ txt "No former delegate" ]
              ]
          end ;
        begin match del.not_delegate with
          | Some d when d <> "" ->
            div ~a:[ a_class [ Display.d_flex ; Flex.flex_row ;
                               Flex.align_items_center ; Spacing.mb2 ;
                               Flex.justify_start ; "new-delegate"] ] [
              fas "hand-point-right" ;
              Metal_helpers_ui.blockies
                ~classes:["account-blockies" ; Spacing.mx3]
                d ;
              span [ txt d ]
            ]
          | _ ->
            div ~a:[ a_class [ Display.d_flex ; Flex.flex_row ;
                               Flex.align_items_center ; Spacing.mb2 ;
                               Flex.justify_start ; "new-delegate"] ] [
              fas "hand-point-right" ;
              Metal_helpers_ui.blockies
                ~classes:["account-blockies" ; Spacing.mx3 ; BMisc.invisible]
                "" ;
              span [ txt "No delegate" ]
            ]
        end
      ] in

    let src_div =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_row;
                         Flex.align_items_center ] ] [
        Metal_helpers_ui.blockies
          ~classes:["account-blockies"; Spacing.mr3]
          acc.pkh ;
        span [ txt acc.pkh ]
      ] in
    let accs_div =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_column ;
                         Flex.flex_fill ] ] [
        src_div ;
        span ~a:[ a_class [ Text.bold ; Text.center ] ] [ txt "Delegate change" ] ;
        dst_div ;
      ] in
    let main_div =
      div ~a:[ a_class [ Display.d_flex ; Spacing.mt1 ; Spacing.mxa ] ] [
        accs_div ;
      ] in

    [ main_div ; tr_info ; alert cb_ok ]

  | ManNot man ->
    Js_utils.log "make_op_page man" ;
    let options = { Dune_types_min.node_mao_maxrolls = man.not_maxrolls;
                    node_mao_admin = man.not_admin;
                    node_mao_white_list = man.not_white_list;
                    node_mao_delegation = man.not_delegation} in
    let fee = man.not_fee in
    let gas_limit = man.not_gas_limit in
    let storage_limit = man.not_storage_limit in

    Metal_request.Node.forge_manage_account ~network:net
      ?gas_limit ?storage_limit ?fee acc options
    >>|? fun (op, ops) ->

    let fee, gas_limit, storage_limit = Dune_utils.limits_of_operations ops in

    let detail_divs = Metal_helpers_ui.add_gas_limit gas_limit  [] in
    let detail_divs = Metal_helpers_ui.add_storage_limit storage_limit detail_divs in
    let amount_div = make_amount_div 0L fee 0L in

    let man_info = div ~a:[ a_class [ Text.left ] ] [
        ul ~a:[ a_class [ Nav.nav ; Nav.nav_tabs ;
                          Nav.navbar_dark ; Spacing.px1 ];
                a_role [ "tablist" ] ] [
          make_tab_li ~active:true "amount" "Amount";
          make_tab_li "details" "Details";
        ] ;
        div ~a:[ a_class [ Nav.tab_content ] ] [
          make_tab_pane ~active:true "amount" amount_div;
          make_tab_pane "details" (List.rev detail_divs)
        ] ] in

    let cb_ok = (fun () ->
        Metal_request.Node.send_manage_account ~network:net acc (op, ops)
          ~error:(fun _c _msg -> ignore @@ deny notif)
          (fun (hash, _ops) ->
             let hash = Crypto.Operation_hash.b58enc hash in
             confirm ~msg:(Js.string hash) notif))
    in
    let dst_div =
      div ~a:[ a_class [ Text.center ] ] (
        (match man.not_maxrolls with
           None -> []
         | Some None -> [ span [ txt "Unset maxrolls" ]; br () ]
         | Some (Some m) -> [ span [ txt "New maxrolls" ]; br () ;
                              span [ txt @@ string_of_int m ]; br () ]) @
        (match man.not_admin with
           None -> []
         | Some None -> [ span [ txt "Unset admin" ]; br () ]
         | Some (Some a) -> [ span [ txt "New admin" ]; br ();
                              span [ txt @@ a ]; br () ]) @
        (match man.not_white_list with
           None -> []
         | Some [] -> [ span [ txt "Unset white list" ] ]
         | Some l ->
           span [ txt "New white list" ] :: br () ::
           (List.flatten @@ List.map (fun x -> [span [ txt ("  - " ^ x) ]; br ()]) l)) @
        (match man.not_delegation with
           None -> []
         | Some b -> [ span [ txt "Delegation status"]; br ();
                       span [ txt (if b then "open" else "closed") ]; br ()])) in

    let src_div =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_row;
                         Flex.align_items_center ] ] [
        Metal_helpers_ui.blockies
          ~classes:["account-blockies"; Spacing.mr3]
          acc.pkh ;
        span [ txt acc.pkh ]
      ] in
    let accs_div =
      div ~a:[ a_class [ Display.d_flex ; Flex.flex_column ;
                         Flex.flex_fill ] ] [
        src_div ;
        span ~a:[ a_class [ Text.bold ; Text.center ] ] [ txt "Baker info change" ] ;
        dst_div ;
      ] in
    let main_div =
      div ~a:[ a_class [ Display.d_flex ; Spacing.mt1 ; Spacing.mxa ] ] [
        accs_div ;
      ] in

    [ main_div ; man_info ; alert cb_ok ]


  | _ ->
    Js.raise_js_error @@
    (new%js Js.error_constr
      (Js.string @@ "[Core] TODO op page"))

let mk_approve_page ~confirm ?(attr=[]) ~deny notif = match notif with
  | ApprovNot n ->
    let name = n.not_approv_name in
    let alert_info =
      div ~a:[ a_class [ Alert.alert ; Alert.awarning ;
                         Spacing.m2 ; Text.left ] ;
               a_role [ "alert" ] ] [
        h4 ~a:[ a_class [ "alert-heading" ] ] [
          txt @@ name ^ " is requesting approval to interact with Metal" ] ;
        p [ txt "Make sure you trust this site before accepting. ";
            txt "Here is a list of the things that you \
                 are allowing to do if you accepts the request :" ] ;
        ul [
          li [ txt "get your selected network" ] ;
          li [ txt "get your selected account" ] ;
          li [ txt "send you signing requests via Metal" ] ;
        ]
      ] in
    let icon =
      match n.not_approv_icon with None -> "FILLER.PNG" | Some icon -> icon in
    let site_url = n.not_approv_url in
    let cb_ok = (fun () -> confirm ?msg:None notif) in
    let cb_nok = (fun () -> deny notif) in
    let alert_site =
      div ~a:[ a_class [ Alert.alert ; Alert.aprimary ;
                         Spacing.m2 ; Text.left ] ] [
        h3 ~a:[ a_class [ "alert-heading"; Display.d_flex ;
                          Flex.justify_between ] ] [
          span [ txt name ] ;
          img ~src:icon ~alt:"connection_requester" () ;
        ] ;
        div [
          span [ txt "url" ] ;
          span ~a:[ a_class [ Spacing.px3; "overflow-hidden" ] ] [
            txt site_url ]
        ] ;
        div ~a:[ a_class [ Spacing.my2 ; Spacing.mx5;
                           Display.d_flex; Flex.justify_between ] ] [
          mk_confirm_button ~attr ~cb:cb_ok ;
          mk_cancel_button ~attr ~cb:cb_nok
        ]
      ] in
    [ alert_info ; alert_site ]
  | _ ->
    Js.raise_js_error @@
    (new%js Js.error_constr
      (Js.string @@ "[Core] wrong notif kind for approve page"))

(** Handlers *)
let handler_options _event =
  let tab = Chrome.Tabs.make_create ~url:"settings.html" () in
  Chrome.Tabs.create tab;
  true

let handler_expand _event =
  let tab = Chrome.Tabs.make_create ~url:"home.html" () in
  Chrome.Tabs.create tab;
  true

let handler_start ?(cb = fun () -> ()) _event =
  let tab = Chrome.Tabs.make_create ~url:"presentation.html" () in
  Chrome.Tabs.create tab;
  cb () ;
  true

let handler_reset ?(cb = fun () -> ()) _event =
  let tab = Chrome.Tabs.make_create ~url:"reset.html" () in
  Chrome.Tabs.create tab;
  cb () ;
  true

let handler_accounts _event =
  Js_utils.(hide (find_component body_id));
  Js_utils.(show (find_component accounts_id));
  true

let handler_send refresh state _event =
  let confirm = fun ?msg _n ->
    ignore msg ;
    Storage_reader.get_account (fun (acc, _) -> match acc with
        | None -> ()
        | Some acc -> refresh { state with fo_acc = acc }) in
  let deny = fun _n -> refresh state in
  Metal_forms.handler_transaction_form
    ~title:"Send DUN"
    ~refresh
    ~update:(replace center_id)
    ~update_anim:(replace_fade_id center_id)
    state
    ~mk_loading_ui:make_forging_dom
    ~mk_ok_ui:(mk_op_page ~confirm ~deny)
    ~mk_error_ui:(make_error_dom (fun () -> refresh state));
  true

let handler_deposit refresh state _event =
  Metal_forms.handler_deposit_form
    ~refresh ~update:(replace1 center_id) state;
  true

let handler_originate refresh state _event =
  let confirm = fun ?msg _n ->
    ignore msg ;
    Storage_reader.get_account (fun (acc, _) -> match acc with
        | None -> ()
        | Some acc -> refresh { state with fo_acc = acc }) in
  let deny = fun _n -> refresh state in
  Metal_forms.handler_origination_form
    ~title:"Create/Deploy a contract"
    ~refresh
    ~update:(replace center_id)
    ~update_anim:(replace_fade_id center_id)
    state
    ~mk_ok_ui:(mk_op_page ~confirm ~deny)
    ~mk_error_ui:(make_error_dom (fun () -> refresh state))
    ~mk_loading_ui:make_forging_dom;
  true

let handler_delegate refresh state old_delegate services _event =
  let confirm = fun ?msg _n ->
    ignore msg ;
    Storage_reader.get_account (fun (acc, _) -> match acc with
        | None -> ()
        | Some acc -> refresh { state with fo_acc = acc }) in
  let deny = fun _n -> refresh state in
  Metal_forms.handler_delegation_form
    ~title:"Delegate this account"
    ~refresh
    ~update:(replace center_id)
    ~update_anim:(replace_fade_id center_id)
    state
    services
    ?old_delegate
    ~mk_ok_ui:(mk_op_page ~confirm ~deny)
    ~mk_error_ui:(make_error_dom (fun () -> refresh state))
    ~mk_loading_ui:make_forging_dom;
  true

let handler_network refresh state event =
  match src_value event with
  | None -> true
  | Some v ->
    let fo_net = network_of_string v in
    let callback () = refresh {state with fo_net} in
    Storage_writer.update_network ~callback fo_net;
    true

let rec handler_lock refresh state _event =
  Vault.Cb.lock ~callback:(fun _ ->
      Notif_background.send ();
      Js_utils.(hide (find_component main_container_id));
      (* Js_utils.(show (find_component unlock_container_id)); *)
      Js_utils.(Manip.removeClass (find_component unlock_container_id)) Display.d_none ;
      unlock_session unlock_container_id refresh state
    ) ();
  true

(** UI Updater *)

and unlock_session id f state =
  let validate_form () =
    let password = id_value pwd_input_id in
    Vault.Cb.unlock ~error:(fun _ _ ->
        addClass (Js_utils.find_component pwd_input_id) BForm.is_invalid)
      ~callback:(fun _ ->
          set_id_value pwd_input_id ""; Notif_background.send (); f state)
      state.fo_acc password in
  replace1 id @@
  div ~a:[ a_class [ Spacing.mxa; Spacing.mya ] ] [
    div ~a:[ a_class [ Text.center ] ] [
      img ~a:[ a_class [ "dune-gif" ] ] ~src:"img/dune.gif" ~alt:"Dune Metal" () ;
    ] ;
    div ~a:[ a_class [ Text.center ] ][
      label [ txt "Password" ];
      input ~a:[ a_id pwd_input_id; a_input_type `Password;
                 a_class [BForm.form_control] ;
                 a_onkeydown (fun e ->
                     if e##.keyCode = 13 then validate_form () ;
                     removeClass (Js_utils.find_component pwd_input_id) BForm.is_invalid ;
                     true)
               ] ();
      button ~a:[
        a_class [Button.btn; Button.btn_dark; Spacing.m4];
        a_onclick (fun _e ->
            validate_form () ;
            true) ] [ txt "Unlock" ] ] ;
    div ~a:[ a_class [BForm.invalid_feedback; Text.left] ] [ txt "Wrong password" ] ;
    div ~a:[ a_class [ "forgot-btn"; Text.right ] ; a_onclick handler_reset ]  [
      txt "Forgot your password?" ]
  ]

let update_balance state =
  let cb () =
    Node.get_account_info ~network:state.fo_net
      ~error:(fun _code _content ->
          replace1 main_balance_value_id @@
          span ~a:[ a_title "cannot get account info"; a_class [Color.danger_tx] ] [
            txt "--" ])
      state.fo_acc.pkh
      (fun {Dune_types_min.node_ai_balance; _} -> replace1 main_balance_value_id
          (Dun.pp_amount node_ai_balance)) in
  Timer.clear_create ~name:"balance" 30 cb

let tx_to_row state tx =
  let details_id = op_details_id tx.trs_hash in
  let result_color =
    if tx.trs_status = "failed" then Color.danger_tx
    else if tx.trs_status = "confirmed" then Color.success_tx
    else Color.warning_tx in
  let status =
    let cl, icon, str =
      if tx.trs_status = "failed" then
        Color.danger_tx, span ~a:[ a_class [ Spacing.pr1 ; Text.small ] ] [ fas "times"], "failed"
      else if tx.trs_status = "pending" then
        Color.warning_tx, span ~a:[ a_class [ Spacing.pr1 ; Text.small ] ] [ fas "hourglass-half"], "pending"
      else
        Color.success_tx, span ~a:[ a_class [ Spacing.pr1 ; Text.small ] ] [ fas "check"], "confirmed" in
    span ~a:[ a_class [ Float.float_left ; cl ; Text.small ] ] [
      span ~a:[ a_class [ "text-small" ] ] [ icon ] ;
      span ~a:[ a_class [ "status-text" ] ] [ txt str ]
    ] in
  let to_from_cl, to_from_txt =
    if tx.trs_src = state.fo_acc.pkh then "to-transaction", div [ txt "Sent" ]
    else "from-transaction", div [ txt "Received" ] in
  let amount =
    if tx.trs_src = state.fo_acc.pkh then Int64.neg tx.trs_amount
    else tx.trs_amount in
  let contract_call =
    if tx.trs_parameters <> None || tx.trs_entrypoint <> None then
      span ~a:[ a_class [ Color.primary_tx ; Spacing.ml2 ; "internal-call" ] ] [
        span ~a:[ a_title "Smart Contract" ] [ fas "file-contract" ]
      ]
    else span [ ] in
  let status_div =
    div ~a:[ a_class [ Grid.col8; "history-status"; Display.d_flex ;
                       Flex.align_items_center ] ] [
      div [
        blockies ~classes:["account-blockies"] @@
        if tx.trs_src = state.fo_acc.pkh then tx.trs_dst else tx.trs_src ] ;
      div ~a:[ a_class [ "ml-3" ] ] [
        to_from_txt ;
        contract_call ;
        status ;
      ]
    ] in
  let timestamp =
    if tx.trs_status <> "pending" then
      ago_str @@ float_of_iso tx.trs_tsp
    else "waiting" in
  let amount_div =
    div ~a:[ a_class [Grid.col4; "history-amount"; Text.right ] ] [
      div ~a:[ a_class [ to_from_cl ] ] [
        Dun.pp_amount ~precision:2 ~width:6 amount ] ;
      div ~a:[ a_class [ Text.italic ; Text.small ; Color.muted_tx ] ] [
        txt timestamp ] ;
    ] in
  div ~a:[ a_class ["transaction-border" ; Grid.container_fluid] ] [
    div ~a:[ a_class [ "py-2" ];
             Attribute.a_data_toggle Collapse.collapse;
             Attribute.a_data_custom "target" ("#" ^ details_id); ] [
      div ~a:[ a_class [Grid.row; Flex.align_items_center] ] [
        status_div ;
        amount_div ]
    ] ;
    div ~a:[ a_id details_id ; a_class [ "collapse" ; "details"; "pb-2" ] ] ([
      div ~a:[ a_class [ "arrow-div"; "mb-2" ] ] ([
        div ~a:[ a_class [ "details-hash"; Color.muted_tx ; Text.center ] ] [
          a ~a:[ a_href @@ dunscan_path ~suffix:tx.trs_src state.fo_net ; a_target "_blank" ] [
            txt tx.trs_src ] ] ;
        div ~a:[ a_class [ result_color ; Text.center ] ] [ fas "arrow-down" ] ;
        div ~a:[ a_class [ "details-hash"; Color.muted_tx ; Text.center ] ] [
          a ~a:[ a_href @@ dunscan_path ~suffix:tx.trs_dst state.fo_net ; a_target "_blank" ] [
            txt tx.trs_dst ] ]
      ] @ match tx.trs_parameters with
          | None -> []
          | Some p ->
            let entrypoint = match tx.trs_entrypoint with
              | None -> "No entrypoint"
              | Some s -> "Entrypoint: " ^ s in
            [ div ~a:[ a_class [ "param"; "mt-2" ] ] [
                  label ~a:[ a_class [ Text.left ] ] [ txt entrypoint ] ;
                  pre [ txt @@ p ] ];
            ]) ;
      label [ txt "operation hash"] ;
      div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
        a ~a:[ a_href @@ dunscan_path ~suffix:tx.trs_hash state.fo_net ; a_target "_blank" ] [
          txt tx.trs_hash ] ] ;
      label [ txt "included in block"] ;
      begin match tx.trs_op_level with
        | None ->
          div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
            span [ txt "pending" ] ]
        | Some lvl ->
          let lvl = string_of_int lvl in
          div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
            a ~a:[ a_href @@ dunscan_path ~suffix:lvl state.fo_net ; a_target "_blank" ] [ txt lvl ] ]
      end ;
      label [ txt "timestamp" ] ;
      div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
        span [ txt @@ if tx.trs_status = "pending" then "pending" else tx.trs_tsp ] ] ;
      label [ txt "counter" ] ;
      div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
        span [ txt @@ Int64.to_string tx.trs_counter ] ] ;
      label [ txt "fee" ] ;
      div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
        Dun.pp_amount ~precision:2 tx.trs_fee ] ;
      label [ txt "gas limit" ] ;
      div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
        span [ txt @@ Z.to_string tx.trs_gas_limit ] ] ;
      label [ txt "storage limit" ] ;
      div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
        span [ txt @@ Z.to_string tx.trs_storage_limit ] ] ] @
    begin match tx.trs_internal with
      | None -> []
      | Some intern ->
        [ label [ txt "internal" ] ;
        div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
          span [ txt @@ string_of_bool intern ] ]
      ]
    end
    @
    begin match tx.trs_burn with
      | None -> []
      | Some burn ->
        [ label [ txt "burn" ] ;
          div ~a:[ a_class [ "details-hash"; Color.muted_tx ] ] [
            Dun.pp_amount ~precision:2 burn ] ]
    end)
  ]

let update_history state =
  let cb () =
    Scan.get_transactions ~network:state.fo_net state.fo_acc.pkh
      (fun txs ->
         let pending = List.filter (fun trp ->
             not (List.exists (fun tra -> tra.trs_hash = trp.trs_hash) txs)) state.fo_acc.pending in
         Storage_writer.update_account {state.fo_acc with pending};
         match pending @ txs with
         | [] ->
           replace1 history_id @@
           div ~a:[ a_class [ Flex.align_self_center ; Text.center ; Spacing.mt4 ] ] [ txt "No transaction yet" ]
         | ops ->
           replace1 history_id @@ div (List.map (tx_to_row state) (ops))) in
  Timer.clear_create ~name:"history" 30 cb

let update_reveal refresh state =
  if not (is_revealed state.fo_net state.fo_acc) then
    Metal_request.Scan.get_revealed ~network:state.fo_net state.fo_acc.pkh
      (function
        | None -> ()
        | Some hash ->
          let fo_acc =
            {state.fo_acc with
             revealed = (state.fo_net, hash) :: state.fo_acc.revealed} in
          let callback fo_acc = refresh {state with fo_acc} in
        Storage_writer.update_account ~callback fo_acc)

(* headers *)

let update_accounts_header refresh state =
  let option_container =
    div ~a:[ a_class ["header-account-options"; Flex.align_self_center] ] [
      button ~a:[
        a_title "Accounts" ;
        a_class [ btn; btn_light ; "header-buttons" ] ;
        a_onclick handler_accounts ] [ fas "users" ]
    ]  in
  let refresh_container =
    div ~a:[ a_class ["header-account-options"; Flex.align_self_center] ] [
      button ~a:[
        a_title "Refresh" ;
        a_class [ btn; btn_light ; "header-buttons" ] ;
        a_onclick (fun _e -> refresh state; true) ] [ fas "sync" ]
    ]  in
  let expand_container =
    div ~a:[ a_class ["header-account-options"; Flex.align_self_center] ] [
      button ~a:[
        a_title "Expand" ;
        a_class [ btn; btn_light ; "header-buttons" ] ;
        a_onclick handler_expand ] [ fas "expand-arrows-alt" ]
    ] in
  let lock_container =
    div ~a:[ a_class ["header-account-options"; Flex.align_self_center] ] [
      button ~a:[
        a_title "Lock" ;
        a_class [ btn; btn_light ; "header-buttons" ] ;
        a_onclick @@ handler_lock refresh state ] [ fas "lock" ]
    ]  in
  let settings_container =
    div ~a:[ a_class ["header-account-options"; Flex.align_self_center] ] [
      button ~a:[
        a_title "Settings" ;
        a_class [ btn; btn_light ; "header-buttons" ] ;
        a_onclick @@ handler_options ] [ fas "cogs" ]
    ]  in
  replace header_account_id [
    option_container ;
    refresh_container ;
    expand_container ;
    settings_container ;
    lock_container ;
  ]

let update_approve_page refresh state notif =
  let app_dom =
    mk_approve_page
      ~confirm:(approve_handler refresh state)
      ~deny:(deny_handler refresh state)
      notif in
  replace center_id app_dom

let update_op_page refresh state notif =
  async (fun () ->
      mk_op_page
        ~confirm:(approve_handler refresh state)
        ~deny:(deny_handler refresh state)
        state.fo_acc
        state.fo_net
        notif >>= function
      | Ok res ->
        replace(* _fade_id *) center_id res ;
        return_unit
      | Error err ->
        let c, msg = error_content err in
        error_callback notif c msg ;
        Js_utils.log "ERROR %d : %s" c msg ;
        let dom =
          make_error_dom (fun () -> refresh state) err in
        replace_fade_id center_id dom ;
        return_unit)

let update_full_notif refresh state n =
  replace network_form_id
    [ p ~a:[ a_class [ "header-info" ; Text.center ] ] [ txt "Approval Request" ] ] ;
  match n with
  | ApprovNot _ -> update_approve_page refresh state n
  | TraNot _ | DlgNot _ | OriNot _ | ManNot _ ->
    let forging_dom = make_forging_dom () in
    replace_fade_id center_id [ forging_dom ] ;
    update_op_page refresh state n

let make_notif refresh state n =
  let time = notif_tsp_of_not n in
  div ~a:[ a_class [ Display.d_flex; Flex.flex_column; Spacing.p3 ] ] [
    span [ txt time ] ;
    notif_kind_to_row state.fo_net n ;
    div ~a:[ a_class [ Display.d_flex; Flex.justify_around ;
                       Spacing.mx2 ; Spacing.mt2 ] ] [
      button ~a:[ a_class [Button.btn; Button.btn_primary;
                           Display.d_flex; Flex.align_items_center ];
                  a_onclick (fun _ -> update_full_notif refresh state n; true) ] [
        fas "eye";
        span ~a:[ a_class [ Spacing.mx2 ] ] [ txt "See" ]
      ] ;
      button ~a:[ a_class [Button.btn; Button.btn_danger;
                           Display.d_flex; Flex.align_items_center ];
                  a_onclick (fun _ -> deny_handler refresh state n; true) ] [
        fas "ban";
        span ~a:[ a_class [ Spacing.mx2 ] ]  [ txt "Deny" ]
      ]
    ]
  ]

let rec update_notifications_view refresh state =
  Storage_reader.get_notifs (fun ns ->
      let ns =
        try
          (List.find (fun { notif_acc; _ } ->
               notif_acc =  state.fo_acc.pkh) ns).notifs
        with Not_found -> [] in
      let update state =
        Storage_reader.get_notifs (fun ns ->
            let ns =
              try
                (List.find (fun { notif_acc; _ } ->
                     notif_acc =  state.fo_acc.pkh) ns).notifs
              with Not_found -> [] in
            if List.length ns = 0 then
              refresh state
            else
              begin
                update_top_header refresh state ;
                update_notifications_view refresh state
              end)
      in
      let back_button =
        button ~a:[ a_class [ Button.btn; Spacing.mla; Spacing.m2; Spacing.p0 ];
                    a_onclick (fun _e -> refresh state; true) ] [
          span ~a:[ a_id "arrow-notifications"; a_class [Spacing.mx1] ] [ fas "times" ] ] in
      let notifications_list =
        div ~a:[ a_class [ "notif-list" ] ]
          (match ns with
           | [] -> [ span ~a:[ a_class [ Text.center ] ] [ txt "No notifications" ] ]
           | notif -> List.map (make_notif update state) notif) in
      replace center_id [ back_button ; notifications_list ])

and update_top_header refresh state =
  Storage_reader.get_custom_networks (fun custom_networks ->
      Storage_reader.get_notifs (fun ns ->
          let n_notif =
            List.length @@
            try
              (List.find (fun {notif_acc ; _} ->
                   notif_acc = state.fo_acc.pkh) ns).notifs
            with Not_found -> [] in
          let custom_networks =
            List.map (fun (name, _, _) -> Custom name) custom_networks in
          let badge_color =
            if n_notif = 0 then Badge.badge_dark
            else Badge.badge_danger in
          let logo_container =
            div ~a:[ a_id coin_logo_id ; a_class [ "header-logo" ] ] [
              img ~src:"img/dune-gray.png" ~alt:"Dune Metal" () ;
              button ~a:[ a_class [ Button.btn; Spacing.mla;
                                    Spacing.m2; Spacing.p0; "notif-icon"];
                          a_onclick (fun _e ->
                              update_top_header refresh state ;
                              update_notifications_view refresh state;
                              true) ] [
                span ~a:[
                  a_class Badge.[badge; badge_pill; badge_color; Spacing.mx1 ] ] [
                  txt (string_of_int n_notif)
                ]
              ]
            ] in
          let network_container =
            div ~a:[ a_class [ Flex.flex_grow; "header-network" ] ] [
              form ~a:[ a_id network_form_id ] [
                select
                  ~a:[ a_onchange (handler_network refresh state); a_id network_id ;
                       a_class [ custom_select ] ] @@
                List.map (fun n ->
                    let selected = if n = state.fo_net then [ a_selected () ] else [] in
                    option ~a:(a_value (network_to_string n) :: selected) ( txt (network_to_string n)))
                  (network_options @ custom_networks)
              ]
            ] in
          let blockies_container =
            dunscan_blockies_link
              ~classes:[ "rounded-blockies"; "header-account-blockies";
                         Flex.align_self_center ]
              state.fo_net state.fo_acc.pkh in
          replace header_top_id [
            logo_container;
            network_container;
            (* option_container; *)
            blockies_container
          ]
        )
    )

let find_baker services pkh =
  List.find_opt (fun service ->
      match service.srv_dn1 with None -> false | Some dn1 -> dn1 = pkh)
    services

let update_baker refresh state =
  let edit_container old_delegate services =
    span ~a:[ a_id edit_baker_id ] [
      button
        ~a:[ a_class [ btn; btn_link ];
             a_onclick (handler_delegate refresh state old_delegate services) ] [ fas "edit" ] ]
  in
  let delegation_content services =
    Scan.get_node_account ~network:state.fo_net state.fo_acc.pkh
      ~error:(fun code msg ->
          Printf.eprintf "Error [%d] %S" code msg ;
          replace baker_id [
            div ~a:[ a_id baker_badge_id ;
                     a_class [] ]
              [ txt "Your Delegate" ] ;
            div ~a:[ a_id baker_info_id ] [
              span ~a:[ a_id baker_logo_id ] [ fas "bread-slice" ] ;
              span ~a:[ a_id baker_alias_id ] [ txt "Choose a new delegate " ] ;
              edit_container None services
            ]
          ])
      (fun node_account ->
         match Crypto.prefix_dn state.fo_acc.pkh, state.fo_acc.manager_kt with
         | Some pref, Some false
         | Some pref, None when pref = Crypto.Prefix.contract_public_key_hash ->
           replace baker_id [
             div ~a:[ a_id no_baker_info_id ] [
               txt "You cannot delegate a contract"
             ]
           ]
         | _ ->
           begin
             let baker_dn1 =
               match node_account.acc_delegate with
               | None ->
                 "Choose a new delegate "
               | Some  baker ->
                 match baker.alias with
                 | None -> baker.dn
                 | Some alias -> alias in
             let service = find_baker services baker_dn1 in
             let logo_container, alias_container =
               match service with
               | None ->
                 fas "bread-slice",
                 txt baker_dn1
               | Some service ->
                 img
                   ~src:(dunscan_path
                           ~suffix:("images/"^ service.srv_logo) state.fo_net)
                   ~alt:service.srv_name (),
                 txt service.srv_name in
             replace baker_id @@
             [
               div ~a:[ a_id baker_badge_id ;
                        a_class [] ]
                 [ txt "Your Delegate" ] ;
               div ~a:[ a_id baker_info_id ] [
                 span ~a:[ a_id baker_logo_id ] [ logo_container ] ;
                 span ~a:[ a_id baker_alias_id ] [ alias_container ] ;
                 edit_container node_account.acc_delegate services
               ]
             ]
           end) in
  Scan.get_services ~network:state.fo_net
    ~error:(fun code error ->
        Js_utils.log "Error %d %S" code error;
        delegation_content [])
    (fun services -> delegation_content services)

let update_central_view refresh state =
  let account_info_container =
    div ~a:[ a_id address_id ;
             a_class [ Text.center ; Spacing.pt2 ; "custom-tooltip" ] ] [
      a ~a:[
        a_onmouseout (fun _ ->
            let tooltip = Js_utils.find_component copied_id in
            Js_utils.Manip.setInnerHtml tooltip "Copy to clipboard" ;
            true) ;
        a_onclick (fun _ ->
            let tooltip = Js_utils.find_component copied_id in
            copy_str state.fo_acc.pkh ;
            Js_utils.Manip.setInnerHtml tooltip ("Copied !") ;
            true) ] [
        div ~a:[ a_class [ "custom-tooltiptext" ] ; a_id copied_id ] [ txt "Copy to clipboard"] ;
        if state.fo_acc.name <> state.fo_acc.pkh then
          span [ txt state.fo_acc.name ]
        else
          span [ txt state.fo_acc.pkh ]
      ]
    ]
  in
  let logo_container =
    div ~a:[ a_class [ "main-dune-logo" ; Spacing.mt2 ] ]
      [ img ~src:"img/dune-logo-square.png" ~alt:"Dune Metal" () ] in
  let balance_container =
    span ~a:[ a_id main_balance_value_id ] [
      txt "--"
    ] in
  let send_buttons_container =
    div ~a:[ a_class [ "main-buttons" ; Spacing.mb3 ] ] [
      button ~a:[ a_class [ btn ; btn_light; btn_outline_success ; Spacing.m2 ];
                  a_onclick (handler_deposit refresh state) ] [ txt "Deposit" ]  ;
      button ~a:[ a_class [ btn ; btn_light; btn_outline_primary; Spacing.m2 ];
                  a_onclick (handler_send refresh state) ] [ txt "Send" ]
    ] in
  let delegate_label =
    div ~a:[ a_id baker_id ;
             a_class [ Flex.align_self_center ;
                       Text.center ; Text.small ] ] [ ] in
  Js_utils.log "%s %s" (match Crypto.prefix_dn state.fo_acc.pkh with None -> "None" | Some s -> s) (match state.fo_acc.manager_kt with None -> "None" | Some s -> string_of_bool s) ;
  replace center_id ([
      account_info_container ;
      logo_container ;
      balance_container ] @
      begin match Crypto.prefix_dn state.fo_acc.pkh, state.fo_acc.manager_kt with
        | pref, Some false when pref = Some Crypto.Prefix.contract_public_key_hash ->
          []
        | _ ->
          [ send_buttons_container ;
            delegate_label ]
      end @
      [ div ~a:[ a_id history_id ; a_class [ "custom-scrollbar"; Text.left ] ] [] ]
    );
  update_balance state;
  update_reveal refresh state;
  update_baker refresh state

let update_accounts_view refresh state =
  let back () =
    Js_utils.(hide (find_component accounts_id));
    Js_utils.(show (find_component body_id)) in
  Storage_reader.get_accounts (fun accounts ->
      replace1 accounts_id @@
      div ~a:[ a_class [List_group.list_group ] ] @@
      (a ~a:[ a_class [List_group.list_group_item; Color.light_bg];
              a_onclick (fun _e -> back (); true) ] [
         div ~a:[ a_class [Spacing.mx2] ] [fas "arrow-left"]
       ]) ::
      (List.map (fun acc ->
           let active =
             if acc.pkh = state.fo_acc.pkh then [ BMisc.active ]
             else [ Color.light_bg ] in
           let names =
             if acc.name = acc.pkh then [
               span ~a:[ a_class [
                   Flex.align_self_center; Spacing.mx3; "account-overflow"] ] [
                 txt acc.name ] ]
             else [
               span ~a:[ a_class [Flex.align_self_center;
                                  Spacing.mx3 ; Text.nowrap ] ] [
                 txt acc.name ];
               span ~a:[ a_class [Flex.align_self_center; "account-overflow"] ] [
                 txt acc.pkh ]
             ] in
           a ~a:[ a_class (List_group.list_group_item :: active);
                  a_onclick (fun _e ->
                      Storage_writer.update_selected
                        ~callback:(fun () -> back (); refresh {state with fo_acc = acc}) (Some acc.pkh);
                      true) ] [
             div ~a:[ a_class [Display.d_flex; Flex.justify_start] ] (
               blockies ~classes:["account-blockies"] acc.pkh :: names)
           ])
          accounts) @
      begin match Crypto.prefix_dn state.fo_acc.pkh with
        | Some pref when pref = Crypto.Prefix.contract_public_key_hash -> []
        | _ -> [
            a ~a:[ a_class [List_group.list_group_item; Color.light_bg];
                   a_onclick (fun _e ->
                       let url = "home.html?type=origination" in
                       let tab = Chrome.Tabs.make_create ~url () in
                       Chrome.Tabs.create tab;
                       true) ] [
              div ~a:[ a_class [Spacing.mx2] ] [txt "New KT1"] ] ;
            a ~a:[ a_class [List_group.list_group_item; Color.light_bg];
                   a_onclick (fun _e ->
                       back ();
                       Metal_forms.handler_import_form
                         ~refresh ~update:(replace center_id) state;
                       true) ] [
              div ~a:[ a_class [Spacing.mx2] ] [txt "Import KT1"] ] ]
      end @
      [ a ~a:[ a_class [List_group.list_group_item; Color.light_bg];
               a_onclick handler_start ] [
          div ~a:[ a_class [Spacing.mx2] ] [txt "New Account"] ] ])

let update_badge state =
  Storage_reader.get_notifs (fun ns ->
      let n_notif =
        List.length @@
        try
          (List.find (fun {notif_acc ; _} ->
               notif_acc = state.fo_acc.pkh) ns).notifs
        with Not_found -> [] in
      Chrome.Browser_action.set_badge
        ~text:(if n_notif = 0 then "" else string_of_int n_notif) ())

let rec update_account state =
  (* Js_utils.(hide (find_component unlock_container_id)); *)
  Js_utils.(Manip.addClass (find_component unlock_container_id)) Display.d_none ;
  Js_utils.(show (find_component main_container_id));
  update_top_header update_account state;
  update_accounts_header update_account state;
  update_central_view update_account state;
  update_history state;
  update_accounts_view update_account state;
  update_badge state ;
  update_baker update_account state

let update_start ?cb id =
  replace1 id @@
  div ~a:[ a_class [ Display.d_flex ; Flex.flex_column ;
                     Flex.align_items_start; Sizing.w100 ;
                     Spacing.py5 ] ] [
    div ~a:[ a_class [Spacing.mxa; Text.center ; Spacing.py5  ] ] [
      h1 ~a:[ a_class [ "display-5"; Text.bold ] ] [ txt "Welcome to Metal" ] ;
    ] ;
    div ~a:[ a_class [Spacing.mxa; Text.center; Spacing.py5 ] ] [
      img
        ~a:[ a_class [ "welcome-logo" ] ]
        ~src:"img/dune-gray.png"
        ~alt:"Dune Metal"
        () ;
    ] ;
    div ~a:[ a_class [Spacing.mxa; Text.center ; Spacing.mta ; Spacing.mb5 ] ] [
      button ~a:[ a_class [btn; btn_dark];
                  a_onclick (handler_start ?cb) ] [ txt "Start here!" ]
    ]
  ]
