(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Html
open Metal_types

open Bs4.Utils

let fas txt =
  i ~a:[ a_class [ "fas" ; "fa-" ^ txt ] ] []

let fab txt =
  i ~a:[ a_class [ "fab" ; "fa-" ^ txt ] ] []

let dunscan_path ?(suffix="") = function
  | Testnet -> "https://testnet.dunscan.io/" ^ suffix
  | Devnet  -> "https://devnet.dunscan.io/" ^ suffix
  | _ -> "https://dunscan.io/" ^ suffix

let network_options = [Mainnet; Testnet; Devnet]

let link ?(args=[]) path =
  match args with
  | [] -> path
  | _ ->
    let args =
      String.concat "&"
        (List.map (fun (key, value) -> key ^ "=" ^ value) args)
    in
    if String.contains path '?' then
      Printf.sprintf "%s&%s" path args
    else
      Printf.sprintf "%s?%s" path args

let crop_hash ?crop_len ?(crop_limit=max_int) hash =
  match crop_len with
  | Some crop_len when Dom_html.window##.screen##.width < crop_limit ->
    let len = String.length hash in
    if len < crop_len then hash
    else
      String.sub hash 0 crop_len ^ "..."
  | _ -> hash

let crop_wtitle ?crop_len s = match crop_len with
  | Some i when String.length s > i ->
    span ~a:[ a_title s ] [ txt @@ String.sub s 0 i ^ "..." ]
  | _ -> span [ txt s ]

let make_link ?crop_len ?crop_limit ?(attr=[]) ?(classes=[]) ?path content =
  let path = match path with
    | None -> content
    | Some path -> path
  in
  a ~a:(a_class classes :: a_href path :: attr) [
    txt @@ crop_hash ?crop_len ?crop_limit content ]

let dunscan_link ?crop_len ?crop_limit ?(attr=[]) ?classes network suffix =
  let attr = (a_target "_blank" :: attr) in
  make_link ?crop_len ?crop_limit ?classes ~attr
    ~path:(dunscan_path ~suffix network) suffix

let dunscan_account_link ?crop_len ?crop_limit ?(attr=[]) ?classes network account =
  let suffix = account.dn in
  let content =  match account.alias with Some alias -> alias | None -> account.dn in
  let attr = (a_target "_blank" :: attr) in
  make_link ?crop_len ?crop_limit ?classes ~attr
    ~path:(dunscan_path ~suffix network) content

let account_w_blockies ?(classes=[]) ?crop_len ?crop_limit ?attr network account =
  span ~a:[a_class ("account-w-blockies" :: classes)] [
    Base58Blockies.create account.dn;
    dunscan_account_link ?crop_len ?crop_limit ?attr network account ]

let blockies ?(classes=[]) hash =
  span ~a:[ a_class classes ] [ Base58Blockies.create hash ]

let kt1_filler () =
  fas "question-circle"

let dunscan_blockies_link ?(classes=[]) network hash =
  a ~a:[ a_class classes; a_href (dunscan_path ~suffix:hash network); a_target "_blank" ]
    [ Base58Blockies.create hash ]

let replace id elts =
  try
    Js_utils.(Manip.replaceChildren (find_component id) elts)
  with _ ->
    Js_utils.log "cannot find an element with id: %s" id

let replace1 id elt = replace id [ elt ]

let clear id =
  try Js_utils.(Manip.removeChildren (find_component id))
  with _ -> Js_utils.log "cannot find an element with id: %s" id

let id_value id = String.trim @@ Js_utils.(Manip.value (find_component id))
let set_id_value id s = Js_utils.(Manip.set_value (find_component id) s)

let node_tr_to_trs ?(status="pending") trs_hash tr =
  let open Dune_types_min in {
    trs_hash;
    trs_src = tr.node_tr_src;
    trs_dst = tr.node_tr_dst;
    trs_amount = tr.node_tr_amount;
    trs_counter = Z.to_int64 tr.node_tr_counter ;
    trs_fee = tr.node_tr_fee;
    trs_status = status;
    trs_tsp = "";
    trs_gas_limit = tr.node_tr_gas_limit ;
    trs_storage_limit = tr.node_tr_storage_limit ;
    trs_entrypoint = tr.node_tr_entrypoint ;
    trs_parameters = tr.node_tr_parameters ;
    trs_internal = None ;
    trs_burn = None ;
    trs_op_level = None ;
    trs_errors = None ;
}

let pp_tsp tsp =
  if tsp = "" then "", "" else
    let d = Jsdate.(date_fromTimeValue (parse tsp)) in
    let h, min, day, month = Jsdate.(getHours d, getMinutes d, getDate d, getMonth d) in
    Printf.(sprintf "%.2d:%.2d" h min, sprintf "%.2d/%.2d" day (month + 1))

let copy id =
  match Js.Opt.to_option @@ Dom_html.document##querySelector (Js.string ("#" ^ id)) with
  | None -> ()
  | Some elt -> (Js.Unsafe.coerce elt)##select() |> ignore;
    Dom_html.document##execCommand (Js.string "copy") Js._false Js.null

let copy_str s =
  let clipboard = Js.Unsafe.variable "navigator.clipboard" in
  Js.Unsafe.(coerce clipboard)##writeText  (Js.string s) |> ignore

let dun_str = "đ"
let mdun_str = "mđ"
let udun_str = "µđ"

let src_value (event:Dom_html.event Js.t) =
  match Js.Opt.to_option event##.srcElement with
  | None -> None
  | Some elt -> Some (Js_utils.Manip.value (Of_dom.of_element elt))

let is_input_checked input_id =
  match Js_utils.Manip.by_id input_id with
  | None -> false
  | Some input_elt ->
    let input_obj = To_dom.of_input input_elt in
    Js.to_bool input_obj##.checked

let raise_invalid id = Js_utils.(
    Manip.addClass (find_component id) Bs4.Items.BForm.is_invalid)

let clear_invalid id = Js_utils.(
    Manip.removeClass (find_component id) Bs4.Items.BForm.is_invalid)

let update_tab url =
  let props : Tabs_utils.updateProperties Js.t = Js.Unsafe.obj [||] in
  props##.url := Js.def (Js.string url);
  Chrome.Tabs.update props

let s_day = "day"
let s_days = "days"
let days d = if d > 1 then s_days else s_day

let get_now () = Jsdate.now_value ()

let get_now() = get_now()

let parse_seconds t =
  if t < 60 then
    Printf.sprintf "%02d s" t
  else
    if t < 3600 then
      let secs = t mod 60 in
      let mins = t / 60 in
      if t < 600 then
        Printf.sprintf "%dm %02ds" mins secs
      else
        Printf.sprintf "%d min" mins
    else
      let t = t / 60 in
      if t < 60 * 24 then
        let mins = t mod 60 in
        let hours = t / 60 in
        Printf.sprintf "%dh %dm" hours mins
      else
        let t = t / 60 in
        if t < 72 then
          let hours = t mod 24 in
          let ds = t / 24 in
          Printf.sprintf "%d %s %dh" ds (days ds) hours
        else
          let ds = t / 24 in
          Printf.sprintf "%d %s" ds (days ds)

let ago timestamp_f =
  (get_now () -. timestamp_f) /. 1000.

let ago_str ?(future=false) timestamp_f =
  let diff = int_of_float @@ ago timestamp_f in
  parse_seconds @@ if future then -1 * diff else diff

let float_of_iso timestamp =
  Js.date##parse (Js.string timestamp)

let update_badge account =
  Storage_reader.get_notifs (fun ns ->
      let n_notif =
        List.length @@
        try
          (List.find (fun {notif_acc ; _} ->
               notif_acc = account.pkh) ns).notifs
        with Not_found -> [] in
      Chrome.Browser_action.set_badge
        ~text:(if n_notif = 0 then "" else string_of_int n_notif) ())

let notif_kind_to_row network = function
  | TraNot n ->
    begin match n.not_dst with
    | None -> assert false
    | Some dst ->
      div ~a:[ a_class [ Display.d_flex; Flex.justify_center ;
                         Flex.align_items_center ; "rounded-blockies";
                         Text.nowrap ] ] [
        span ~a:[ a_class [ Spacing.mx2 ] ] [ txt "Send" ] ;
        Dun.pp_amount (match n.not_amount with None -> 0L | Some i -> i) ;
        span ~a:[ a_class [ Spacing.mx2 ] ][ txt "to" ] ;
        blockies dst ;
        dunscan_account_link
          ~classes:[ Spacing.mx2 ; "overflow-hidden" ]
          network
          {dn=dst; alias=None}
    ]
    end
  | DlgNot n ->
    begin match n.not_delegate with
      | None -> assert false
      | Some del ->
        div ~a:[ a_class [ Display.d_flex; Flex.justify_center ;
                           Flex.align_items_center ; "rounded-blockies";
                           Text.nowrap  ] ] [
          span ~a:[ a_class [ Spacing.mx2 ] ] [ txt "delegate to" ] ;
          blockies del ;
          dunscan_account_link network
            ~classes:[ Spacing.mx2 ; "overflow-hidden" ]
            {dn=del; alias=None}
        ]
    end
  | OriNot n ->
    div ~a:[ a_class [ Display.d_flex; Flex.justify_center ;
                       Flex.align_items_center ; "rounded-blockies";
                       Text.nowrap  ] ] [
      span ~a:[ a_class [ Spacing.mx2 ] ] [ txt "originate new kt1 with" ] ;
      Dun.pp_amount (match n.not_amount with None -> 0L | Some i -> i) ;
    ]
  | ManNot n ->
    div ~a:[ a_class [ Display.d_flex; Flex.justify_center ;
                       Flex.align_items_center ; "rounded-blockies";
                       Text.nowrap  ] ] ((
      span ~a:[ a_class [ Spacing.mx2 ] ] [ txt "manage account" ]) ::
        (match n.not_maxrolls with
         | None -> []
         | Some None -> [ span [ txt "unset maxrolls" ] ]
         | Some (Some i) -> [ span [ txt @@ Printf.sprintf "maxrolls set at %d" i ] ]) @
        (match n.not_admin with
         | None -> []
         | Some None -> [ span [ txt "unset admin" ] ]
         | Some (Some admin) -> [ span [ txt @@ Printf.sprintf "admin set at %s" admin ] ]) @
        (match n.not_white_list with
         | None -> []
         | Some [] -> [ span [ txt "unset white list" ] ]
         | Some l -> [ span [ txt @@ Printf.sprintf "white list set at %s" (String.concat " " l) ] ]) @
        (match n.not_delegation with
         | None -> []
         | Some d -> [ span [ txt @@ Printf.sprintf "delegation set at %B" d ] ]))
  | ApprovNot n ->
    let icon =
      match n.not_approv_icon with None -> "FILLER.PNG" | Some icon -> icon in
    div ~a:[ a_class [ Display.d_flex; Flex.justify_center ;
                       Flex.align_items_center ; "rounded-blockies";
                       Text.nowrap ] ] [
      span ~a:[ a_class [ Spacing.mx2 ] ] [ txt "approval for domain " ] ;
      img ~src:icon ~alt:"connection_requester" () ;
      span ~a:[ a_class [ Spacing.mx2 ; "overflow-hidden" ] ] [
        txt n.not_approv_name ] ;
    ]

let add_gas_limit n acc =
  match n.not_gas_limit with
  | None -> acc
  | Some gl ->
    div ~a:[ a_class [ Spacing.mr4 ] ] [
      span ~a:[ a_class [ Text.nowrap ] ] [ txt "Gas Limit" ] ;
      span ~a:[ a_class [ Spacing.ml3 ] ] [
        txt @@ Z.to_string gl ]
    ] :: acc

let add_storage_limit n acc = match n.not_storage_limit with
  | None -> acc
  | Some sl ->
    div ~a:[ a_class [ Spacing.mr4 ] ] [
      span ~a:[ a_class [ Text.nowrap ] ] [ txt "Storage Limit" ] ;
      span ~a:[ a_class [ Spacing.ml3 ] ] [
        txt @@ Z.to_string sl ]
    ] :: acc

let add_limit n acc = match n.not_storage_limit, n.not_gas_limit with
  | None, None -> acc
  | _, _ ->
    let limits = add_storage_limit n [] in
    let limits = add_gas_limit n limits in
    div ~a:[ a_class [ Display.d_flex ] ] limits :: acc

let add_param n acc =
  let e = match n.not_entry with
    | None -> "default"
    | Some e -> e in
  match n.not_params with
  | None -> acc
  | Some p ->
    let sc = Dune_encoding_min.Script.decode p in
    let p = Dune_script.print_script sc in
    div ~a:[ a_class [ Display.d_flex ] ] [
      div ~a:[ a_class [ Flex.flex_fill ] ]
        [ div ~a:[ a_class [ "param" ] ] ([
              span [ txt "Entrypoint" ] ;
              pre [ txt @@ e ] ] @ [
                span [ txt "Parameter" ] ;
                pre [ txt @@ p ] ]);
        ]
    ] :: acc

let add_script n acc = match n.not_sc_code with
  | None -> acc
  | Some sc ->
    let sc = Dune_encoding_min.Script.decode sc in
    let sc = Dune_script.print_script ~contract:true sc in
    div ~a:[ a_class [ Display.d_flex ] ] [
      div ~a:[ a_class [ Flex.flex_fill ] ]
        [ div ~a:[ a_class [ "param" ] ] [
              span [ txt "Script" ] ;
              pre [ txt @@ sc ] ];
        ]
    ] :: acc

let add_storage n acc = match n.not_sc_storage with
  | None -> acc
  | Some sc ->
    let sc = Dune_encoding_min.Script.decode sc in
    let sc = Dune_script.print_script sc in
    div ~a:[ a_class [ Display.d_flex ] ] [
      div ~a:[ a_class [ Flex.flex_fill ] ]
        [ div ~a:[ a_class [ "param" ] ] [
              span [ txt "Storage" ] ;
              pre [ txt @@ sc ] ];
        ]
    ] :: acc

let add_code_hash n acc = match n.not_sc_code_hash with
  | None -> acc
  | Some sc ->
    div ~a:[ a_class [ Display.d_flex ] ] [
      div ~a:[ a_class [ Flex.flex_fill ] ]
        [ div ~a:[ a_class [ "param" ] ] [
              span [ txt "Code Hash" ] ;
              pre [ txt @@ sc ] ];
        ]
    ] :: acc

let add_gas_limit gl acc =
  div ~a:[ a_class [ Spacing.mr4 ] ] [
    span ~a:[ a_class [ Text.nowrap ] ] [ txt "Gas Limit" ] ;
    span ~a:[ a_class [ Spacing.ml3 ] ] [
      txt @@ Z.to_string gl ]
  ] :: acc

let add_storage_limit sl acc =
  div ~a:[ a_class [ Spacing.mr4 ] ] [
    span ~a:[ a_class [ Text.nowrap ] ] [ txt "Storage Limit" ] ;
    span ~a:[ a_class [ Spacing.ml3 ] ] [
      txt @@ Z.to_string sl ]
  ] :: acc

let add_delegate n acc = match n.not_delegate with
  | None -> acc
  | Some m ->
    div [
      span ~a:[ a_class [ Text.nowrap; Spacing.mr4 ] ] [ txt "Delegate" ] ;
      span ~a:[a_class [ "account-w-blockies"; Text.left ]] [ txt m ]
    ] :: acc

let is_revealed network account =
  match List.assoc_opt network account.revealed with
  | None -> false
  | Some _hash -> true

let update_revealed ~callback net acc =
  if not (is_revealed net acc) then
    Metal_request.Scan.get_revealed ~network:net acc.pkh
      (function
        | None -> callback acc
        | Some hash ->
          let fo_acc =
            {acc with
             revealed = (net, hash) :: acc.revealed} in
          Storage_writer.update_account ~callback fo_acc)
  else callback acc

let update_account_admin ?callback ?accounts network account account_info =
  let admin = match account_info.Dune_types_min.node_ai_admin with
    | None ->
      List.filter (fun (net, _) -> net <> network) account.admin
    | Some admin_pkh ->
      if not (List.mem network (List.map fst account.admin)) then
        (network, admin_pkh) :: account.admin
      else
        List.map (fun (net, a) ->
            if net = network then (net, admin_pkh)
            else (net, a)) account.admin in
  let account = {account with admin} in
  Storage_writer.update_account ?callback ?accounts account

let set_timeout f time =
  Dom_html.window##setTimeout (Js.wrap_callback f) time

let replace_fade_id container_id new_dom =
  let open Js_utils in
  let container = find_component container_id in
  Manip.removeClass container "fade-in" ;
  Manip.removeClass container "fade-out" ;
  Manip.addClass container "fade-out" ;
  ignore @@
  set_timeout
    (fun () ->
       Manip.replaceChildren container new_dom ;
       Manip.addClass container "fade-in")
    1.5

let replace_fade container new_dom =
  let open Js_utils in
  Manip.removeClass container "fade-in" ;
  Manip.removeClass container "fade-out" ;
  Manip.addClass container "fade-out" ;
  ignore @@
  set_timeout
    (fun () ->
       Manip.replaceChildren container new_dom ;
       Manip.addClass container "fade-in" )
    1.

let replace_slide_id container_id new_dom =
  let open Js_utils in
  let container = find_component container_id in
  Manip.removeClass container "slide-in" ;
  Manip.removeClass container "slide-out" ;
  Manip.addClass container "slide-out" ;
  ignore @@
  set_timeout
    (fun () ->
       Manip.replaceChildren container new_dom ;
       Manip.addClass container "slide-in")
    0.5

let hide id =
  try Js_utils.(hide (find_component id))
  with _ -> Js_utils.log "cannot find an element with id: %s" id

let show id =
  try Js_utils.(show (find_component id))
  with _ -> Js_utils.log "cannot find an element with id: %s" id

module IntMap = Map.Make(struct type t = int let compare = compare end)
module Timer = struct

  let timer_id = ref 0
  let timers = ref (IntMap.empty : (string option * Dom_html.interval_id) IntMap.t)

  let get i = IntMap.find_opt i !timers

  let remove ?name i (na, timer) =
    match name with
    | Some name when Some name <> na -> ()
    | _ ->
      timers := IntMap.remove i !timers;
      Dom_html.window##clearInterval timer

  let clear ?name i = match get i with
    | None -> ()
    | Some x -> remove ?name i x

  let clear_timers ?name () =
    IntMap.iter (remove ?name) !timers

  let create ?name time_s f =
    incr timer_id;
    let id = !timer_id in
    f ();
    let cb () =
      Chrome.Tabs.getCurrent (fun t ->
          if Js.to_bool t##.active then f ()) in
    let timer = Dom_html.window##setInterval
        (Js.wrap_callback cb)
        (float_of_int time_s *. 1000.) in
    timers := IntMap.add id (name, timer) !timers

  let clear_create ?name time_s f =
    clear_timers ?name ();
    create ?name time_s f
end

(* FIREFOX : ok ?*)
let ext_re =
  new%js Js.regExp
    (Js.string
       "^chrome-extension://([0-9a-zA-Z.-]+|\\[[0-9a-zA-Z.-]+\\]|\\[[0-9A-Fa-f:.]+\\])?(:([0-9]+))?(/([^\\?#]*)(\\?([^#]*))?(#(.*))?)?$")

let get_extension_url () =
  let failed = fun () -> failwith "Not a valid extension url" in
  let path = Dom_html.window##.location##.href in
  Js.Opt.case
    (ext_re##exec path)
    failed
    (fun handle ->
       let res = Js.match_result handle in
       Url.decode_arguments @@
       Url.urldecode @@
       Js.to_string @@
       (Js.Optdef.get (Js.array_get res 7) failed))
