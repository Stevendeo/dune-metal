(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Html
open Bs4.Utils
open Bs4.Items
open Crypto
(* open Metal_types *)

let mnemonic = ["despair";"spawn";"fan";"pitch";"regret";"crane";"ten";"crime";"exotic";"allow";"defy";"fat";"senior";"inside";"layer"]
let email = "vascxmcb.mcwasydv@dune.example.org"
let password = "DdiZHefqNM"
let passphrase = email ^ password


let test () =
  let indices = match Bip39.of_words mnemonic with None -> assert false | Some li -> li in
  let passphrase = Bigstring.of_string passphrase in
  let sk = Bigstring.sub (Bip39.to_seed ~passphrase indices) 0 32 in
  let pk = Sk.to_public_key sk in
  let pk_b58 = Pk.b58enc pk in
  Js_utils.log "%s" pk_b58;
  let pkh = Pk.hash pk in
  let pkh = Pkh.b58enc pkh in
  Js_utils.log "%s" pkh;
  let open Dune_types_min in
  let tr = NTransaction {
      node_tr_src = pkh;
      node_tr_dst = pkh; node_tr_amount = 0L;
      node_tr_fee = 0L; node_tr_counter = Z.zero;
      node_tr_gas_limit = Z.zero;
      node_tr_storage_limit = Z.zero;
      node_tr_collect_fee_gas = None;
      node_tr_collect_pk = None;
      node_tr_entrypoint = None;
      node_tr_parameters = None;
      node_tr_metadata = None } in
  let head = "BL1GMMLQ7THNBF2C4DyxnxGMcqu5CXNXyNkitJNhQVXaUF7E4TJ" in
  let protocol = "Pt24m4xiPbLDhVgVfABUjirbmda3yohdN82Sp9FeuAXJ4eV9otd" in
  let bytes = match Forge.forge_operations head [ tr ] with
    | Error _ -> Js_utils.log "Cannot forge operation"; Bigstring.empty
    | Ok b -> b in
  let signature = Forge.sign_exn ~sk bytes in
  let signature = Base58.simple_encode Prefix.ed25519_signature signature in
  let base = Xhr_lwt.base ~port:80 ~scheme:"http" "testnet-node.dunscan.io" in
  Async.async (fun () -> Node.preapply ~base head protocol signature [ tr ]);
  (* Metal_vault.unlock_first "test"; *)
  (* Metal_vault.encrypt pkh sk
   *   (fun account ->
   *      Metal_vault.decrypt account
   *        (fun sk2 -> Js_utils.log "%s %s" (Sk.b58enc sk) (Sk.b58enc sk2))); *)
  (* Metal_vault.decrypt {pkh = "dn1bq1LkXKhA3WYgr3yMRA7d7dLXY9KZiwJN";
   *                      iv = Hex.to_bigstring (`Hex "efbfbd473eefbfbd6e16efbfbdefbfbdefbfbd053eefbfbdefbfbd02efbfbdefbfbd");
   *                      esk = Hex.to_bigstring (`Hex "efbfbd4b742a092208c99eefbfbd25efbfbdefbfbdefbfbd153eefbfbd7c3c1f43efbfbd2eefbfbdefbfbdefbfbdefbfbd56efbfbddca1efbfbdefbfbd7eefbfbdefbfbd4e6b437025625062efbfbd7e161b2d0920196c5befbfbdde99efbfbd360befbfbd5c1f7aefbfbdefbfbdefbfbdefbfbdefbfbd2c5b0d1d0337efbfbd72efbfbd5defbfbd");
   *                      name = "test";
   *                      revealed = None; pending = []} (fun sk ->
   *     Js_utils.log "%s" (Sk.b58enc sk)) *)

  (* let base = Xhr_lwt.base ~port:80 ~scheme:"http" "testnet-node.dunscan.io" in *)

  (* let parameters = Dune_types_min.(Prim ("Pair", [
   *     Bytes (`Hex "ce9cc78661ccb230e7cd5853161917a1166692cfbf8e3fde4e8e4be41611dd52");
   *     Bytes (`Hex "7475617265672d6d6f64652e7064665f5f74657374") ], [])) in
   * let parameters = Dune_types_min.(Prim ("Unit", [], [])) in
   * let parameters = Dune_encoding_min.Micheline.encode parameters in
   * Js_utils.log "%s" parameters; *)

  (* Xhr_lwt.async_wrapo
   *   ~error:(fun i s -> Js_utils.log "Fail %d %s" i s)
   *   (fun () ->
   *      Node.transaction ~base ~parameters ~fee:1744L ~gas_limit:(Z.of_int 14246) ~sk
   *        ~src:pkh ~dst:"KT1Vqcie56z2CqgNVhULck8FjUgsnxK2MYom"
   *        0L)
   *   (fun s _ -> Js_utils.log "op_hash: %s" s) *)

  (* Ledger_js.getAddress "44'/1729'/0'/0'" (fun x ->
   *     Js_utils.js_log x) |> ignore *)

  (* Ledger_js.getVersion (fun (major, minor, patch, bakingApp) ->
   *     Js_utils.log "%d.%d.%d %B" major minor patch bakingApp) *)
  (* let open Dune_types_min in
   * let tr = NTransaction {
   *   node_tr_src = "dn1GXgS1fM1sdMoWfkhZ3cvmLo81yRz7ythL";
   *   node_tr_dst = "dn1GXgS1fM1sdMoWfkhZ3cvmLo81yRz7ythL"; node_tr_amount = 0L;
   *   node_tr_fee = 0L; node_tr_counter = Z.zero;
   *   node_tr_gas_limit = Z.zero;
   *   node_tr_storage_limit = Z.zero;
   *   node_tr_collect_fee_gas = None;
   *   node_tr_parameters = None;
   *   node_tr_metadata = None } in
   * let head = "BKmkETq1utLi9u4ibFuKHZVtTcTHdFDuec5bESdpF1hJbWJppG4" in
   * let bytes = match Forge.forge_operations head [ tr ] with
   *   | None -> Js_utils.log "Cannot forge operation"; ""
   *   | Some b -> Forge.to_hex b in
   * Ledger_js.sign "44'/1729'/0'/0'" bytes (fun signature ->
   *     try
   *       Js_utils.log "%s" signature
   *     with _ ->
   *       Js_utils.log "sign fail"; Js_utils.js_log signature *)
()

let () =
  let b =
    button ~a:[ a_onclick (fun _e -> test (); true);
                a_class [Button.btn; Button.btn_primary; Spacing.m3] ] [
      txt "Operation" ] in
  Js_utils.(Manip.replaceChildren (find_component "content") [ b ])
