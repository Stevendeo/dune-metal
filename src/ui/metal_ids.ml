(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module IdPopup = struct
  (* Popup Main containers *)
  let coin_logo_id = "coin-logo"
  let baker_id = "delegated-baker"
  let baker_info_id = "baker-info"
  let no_baker_info_id = "no-baker-info"
  let baker_badge_id = "baker-badge"
  let baker_logo_id = "baker-logo"
  let baker_alias_id = "baker-alias"
  let edit_baker_id = "baker-switch"
  let address_id = "address"
  let unlock_container_id = "unlock-container"
  let enable_container_id = "enable-container"
  let approve_container_id = "approve-container"
  let loading_container_id = "loading-container"
  let main_container_id = "main-container"
  let header_top_id = "header-top"
  let header_account_id = "header-account"
  let body_id = "body"
  let center_id = "central-view"
  let history_id = "history"
  let accounts_id = "accounts-side"
  let deposit_blockies_id = "deposit-blockies"
  let dunscan_link_id = "dunscan-link"
  let copied_id = "custom-copied-tooltip"
  let delegates_id = "delegates-list"

  (* Popup Items *)
  let main_balance_value_id = "main-balance-value"
  let network_id = "network"
  let network_form_id = "network-form"
  let pwd_input_id = "pwd-input"

  (* Popup History*)
  let op_details_id hash = "op-details-" ^ hash
  let op_details_param_id hash = "op-details-p" ^ hash
end

module IdForms = struct
  let options_collapse_id kind = "options-collapse-" ^ kind
  let entrypoint_id = "entrypoint"
  let parameters_id = "parameters"
  let spendable_id = "spendable"
  let delegatable_id = "delegatable"
  let code_hash_id = "code-hash"
  let code_id = "code"
  let storage_id = "storage"
  let gas_limit_id kind = "gas-limit-" ^ kind
  let storage_limit_id kind = "storage-limit-" ^ kind
  let fee_id kind = "fee-" ^ kind
  let delegate_id = "delegate-origination"
  let dest_tr_id = "dest-transaction"
  let amount_tr_id = "amount-transaction"
  let balance_or_id = "balance-origination"
  let delegate_dlg_id = "delegate-delegation"
  let contract_loading_id = "contract-loading-div"
  let admin_id = "admin"
  let maxrolls_id = "maxrolls"
  let white_list_id = "white-list"
  let delegation_id = "delegation"
end

module IdSettings = struct
  let accounts_settings_id = "accounts-settings"
  let password_settings_id = "password-settings"
  let networks_settings_id = "networks-settings"
  let show_secret_id = "show-secret"
  let old_pwd_id = "old-pwd-input"
  let new_pwd_id = "new-pwd-input"
  let confirm_pwd_id = "confirm-pwd-input"
  let add_network_id = "add-network"
  let custom_name_id = "custom-name-input"
  let custom_node_id = "custom-node-input"
  let custom_api_id = "custom-api-input"
end

module IdHome = struct
  let network_home_id = "network-home"
  let account_home_id = "account-home"
  let accounts_home_id = "accounts-home"
  let tabs_home_id = "tabs-home"
  let balance_home_id = "balance-home"
  let transaction_tab_content = "transaction-tab-content"
  let delegation_tab_content = "delegation-tab-content"
  let origination_tab_content = "origination-tab-content"
  let notarization_tab_content = "notarization-tab-content"
  let manage_account_tab_content = "manage-account-tab-content"
  let transaction_form_home_id = "transaction-form-home"
  let transaction_history_home_id = "transaction-history-home"
  let delegation_form_home_id = "delegation-form-home"
  let delegation_history_home_id = "delegation-history-home"
  let origination_form_home_id = "origination-form-home"
  let origination_history_home_id = "origination-history-home"
  let notarization_form_home_id = "notarization-form-home"
  let notarization_history_home_id = "notarization-history-home"
  let manage_account_form_home_id = "manage-account-form-home"
  let import_accounts_id = "import-accounts-home"
  let hide_import_accounts_id = "hide-import-accounts-home"
  let notif_list_id = "notif-list"
  let modal_div_id = "notif-modal-container"
  let account_info_home_id = "account-home-info"
  let account_info_content_home_id = "account-home-info-content"
end

module IdStart = struct
  let main_content_id = "content"
  let home_link_id = "home-link"
  let settings_link_id = "settings-link"
end
