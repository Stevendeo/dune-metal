(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Html
open Bs4.Utils
open Bs4.Items
open Metal_types
open Metal_helpers_ui

let reset_id = "reset-div"

let rec make_reset_button () =
  button ~a:[ a_class Button.[btn; btn_danger];
              a_onclick (fun _e -> reset_confirmation (); true)
            ] [ txt "Reset Storage" ]

and reset_confirmation () =
  replace reset_id [
    div ~a:[ a_class [Color.danger_tx] ] [
      txt "Do you really wish to reset your storage?";
      br (); txt "This operation is irreversible!" ];
    div ~a:[ a_class [Text.center] ] [
      button ~a:[ a_class Button.[btn; btn_info; Spacing.m2];
                  a_onclick (fun _e -> replace1 reset_id @@ make_reset_button (); true) ]
        [ txt "Cancel" ];
      button ~a:[ a_class Button.[btn; btn_danger];
                  a_onclick (fun _e -> Storage_writer.reset_storage
                                ~callback:(fun () -> update_tab "start.html") (); true)
                ] [ txt "Yes!" ] ] ]

let make () =
  Storage_reader.get_accounts (fun accounts ->
      let account_list = String.concat "\n" @@
        List.map (fun acc -> Printf.sprintf "pkh: %s\talias: %s" acc.pkh acc.name) accounts in
      let new_div =
        BCard.make_card
      ~card_class:[ Grid.container; Sizing.h50; Spacing.mxa; Spacing.my5;
                    Spacing.py5; Text.center ]
      ~card_body_class:[ Text.center ]
      ~card_body_content:[
        div ~a:[ a_class [ Grid.row ] ] [
          div ~a:[ a_class [ Grid.col12 ] ] [
            txt "Your storage contains information about these accounts:" ];
          div ~a:[ a_class [ Grid.col12; Spacing.p3 ] ] [
            textarea ~a:[ a_readonly (); a_rows (min 10 (List.length accounts));
                          a_style "width:90%"] (txt account_list) ];
          div ~a:[ a_class [ Grid.col12 ] ] [
            txt "All sensitive information is encrypted with your password and \
                 cannot be retrieved without." ];
          div ~a:[ a_class [ Grid.col12 ] ] [
            txt "The only way to continue using Metal is to reset the storage and \
                 import again the accounts given above using their secret key, \
                 mnemonic, ICO/Faucet information, or Ledger." ];
          div ~a:[ a_id reset_id;
                   a_class [ Grid.col12; Spacing.p3; Text.center ] ] [
            make_reset_button () ]

        ] ] ()
  in
  replace1 Metal_ids.IdStart.main_content_id new_div)

let () = make ()
