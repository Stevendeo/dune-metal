(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Html
open Metal_ids.IdPopup
open Bs4.Items
open Bs4.Utils
open Metal_helpers_ui

let get_url tab =
  match Js.Optdef.to_option tab##.url with
  | None -> None
  | Some url -> Some (Js.to_string url)

let get_args url =
  match String.index_opt url '?' with
  | None -> []
  | Some i ->
    let args = String.(split_on_char '&' (sub url (i+1) ((length url) - i - 1))) in
    List.rev @@ List.fold_left (fun acc x -> match String.split_on_char '=' x with
        | [key; value] -> (key, value) :: acc
        | _ -> acc) [] args

let () =
  Chrome.Tabs.getCurrent (fun tab ->
      match get_url tab with
      | None -> Js_log.log_str "No url for this tab"
      | Some url ->
        Js_log.log_str url;
        let args = get_args url in
        match List.assoc_opt "page" args with
        | None -> Js_log.log_str "No page argument in the url"
        | Some page ->
          Storage_reader.get_account (fun (account, _)  -> match account with
              | None -> update_tab "/presentation.html"
              | Some account ->
                replace1 unlock_container_id @@
                div ~a:[ a_class [ Display.d_flex; Sizing.h50] ] [
                  div ~a:[ a_class [Spacing.mxa; Flex.align_self_center;
                                    BForm.form_group; Text.center] ] [
                    label [ txt "Password" ];
                    input ~a:[ a_id pwd_input_id; a_input_type `Password;
                               a_class [BForm.form_control] ] ();
                    div ~a:[ a_class [BForm.invalid_feedback; Text.left] ]
                      [ txt "Wrong password" ];
                    button ~a:[
                      a_class [Button.btn; Button.btn_dark; Spacing.m4];
                      a_onclick (fun _e ->
                          let password = id_value pwd_input_id in
                          Vault.Cb.unlock ~error:(fun _ _ -> raise_invalid pwd_input_id)
                            ~callback:(fun _ ->
                                set_id_value pwd_input_id "";
                                Notif_background.send ();
                                update_tab (Printf.sprintf "/%s.html" page))
                            account password;
                          true)
                    ] [ txt "Unlock" ] ] ]))
