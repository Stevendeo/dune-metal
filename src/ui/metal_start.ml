(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Html
open Bs4.Items
open Bs4.Utils
open Crypto
open Metal_types
open Metal_helpers_ui

let make_loading () =
  match Js_utils.Manip.by_id "loading-modal" with
  | None ->
    let modal =
      Modal.make_modal ~dialog_class:[Modal.modal_dialog_centered]
        ~body_class:[Display.d_flex; Flex.justify_center]
        "loading-modal" [
        div ~a:[ a_class [Spinner.spinner_border; Color.dark_tx] ] [];
        h4 ~a:[ a_class [Spacing.ml4] ] [ txt "Loading..." ] ]
    in
    Js_utils.Manip.appendToBody modal
  | Some _ -> ()

let display_loading () =
  Jquery.(jQ "#loading-modal" |> modal "show") |> ignore

let hide_loading () =
  Jquery.(jQ "#loading-modal" |> modal "hide") |> ignore

let append_modal ?(show=false) id msg f =
  let modal = Modal.make_modal
      ~dialog_class:[ Text.center; Modal.modal_dialog_centered ]
      id [
      div ~a:[ a_class [Grid.row] ] [
        div ~a:[ a_class [Grid.col12; Text.center] ] [ txt msg ];
        button ~a:[ a_class [Button.btn; Button.btn_info; Grid.col4; Grid.off4];
                    a_onclick (fun _e ->
                        Jquery.(jQ ("#" ^ id) |> modal "hide") |> ignore;
                        f (); true) ] [
          txt "Continue" ]
      ]
    ] in
  Js_utils.(Manip.appendChild (find_component "content") modal);
  if show then Jquery.(jQ ("#" ^ id) |> modal "show") |> ignore

let vault_from_mnemonic mnemonic passphrase =
  let indices = Bip39.of_words mnemonic in
  match indices with
  | None -> assert false
  | Some indices ->
    let passphrase = Bigstring.of_string passphrase in
    let sk = Bigstring.sub (Bip39.to_seed ~passphrase indices) 0 32 in
    sk, Vault.pkh_from_sk sk

let store_vault callback account =
  Storage_writer.add_account ~select:true ~callback account

let make_end () =
  let end_div  =
    div ~a:[ a_class [Grid.container; Spacing.py5; Sizing.h50; Display.d_flex] ] [
      BCard.make_card
        ~card_class:[Spacing.mxa; Flex.align_self_center]
        ~card_body_class:[Text.center; Grid.row]
        ~card_body_content:[
          h3 ~a:[ a_class [Grid.col12] ] [ txt "Congrats!" ];
          h5 ~a:[ a_class [Grid.col12] ] [ txt "You are now set up to use Dune Metal" ];
          a ~a:[ a_class [Button.btn; Button.btn_link; Button.btn_dark;
                          Spacing.mxa; Spacing.my3];
                 a_href "/home.html" ] [ txt "Continue" ];
        ] () ] in
  replace1 Metal_ids.IdStart.main_content_id end_div

let check_password pw = String.length pw >= 8

let make_encrypt cancel f =
  Storage_reader.get_account (function
      | None, _ ->
        let password_div =
          div ~a:[ a_class [Grid.container; Spacing.py5;
                            Sizing.h50; Sizing.w50 ; Display.d_flex] ] [
            BCard.make_card
              ~card_class:[Spacing.mxa; Flex.align_self_center; Sizing.w100]
              ~card_body_content:[
                div ~a:[ a_class [BForm.form_group] ] [
                  label [ txt "Password *" ];
                  input ~a:[ a_id "pw1-input"; a_input_type `Password;
                             a_class [BForm.form_control] ] () ;
                  div ~a:[ a_class [BForm.invalid_feedback; Text.left] ] [
                    txt "Password must contain at least 8 characters" ]
                ];
                div ~a:[ a_class [BForm.form_group] ] [
                  label [ txt "Confirm Password *" ];
                  input ~a:[ a_id "pw2-input"; a_input_type `Password;
                             a_class [BForm.form_control] ] () ;
                  div ~a:[ a_class [BForm.invalid_feedback; Text.left] ] [
                    txt "Not the same password" ]
                ];
                div ~a:[ a_class [ Text.center ] ] [
                  button ~a:[ a_onclick (fun _e ->
                      let pw1 = id_value "pw1-input" in
                      let pw2 = id_value "pw2-input" in
                      if pw1 = pw2 then
                        if check_password pw1 then
                          Vault.Cb.unlock_first ~callback:(fun _ -> f ()) pw1
                        else raise_invalid "pw1-input"
                      else raise_invalid "pw2-input";
                      true);
                      a_class [Button.btn; Button.btn_dark; Spacing.m2] ] [
                    txt "Encrypt" ];
                  button ~a:[ a_onclick (fun _e -> cancel (); true);
                              a_class [Button.btn; Button.btn_dark; Spacing.m2] ] [
                    txt "Cancel" ]
                ]
              ] ()
          ] in
        replace1 Metal_ids.IdStart.main_content_id password_div
      | Some account, _ ->
        Vault.Cb.password (function
            | None ->
              let unlock_div =
                div ~a:[ a_class [Grid.container; Spacing.py5; Sizing.h50; Display.d_flex] ] [
                  BCard.make_card
                    ~card_class:[Spacing.mxa; Flex.align_self_center]
                    ~card_body_class:[Grid.row]
                    ~card_body_content:[
                      div ~a:[ a_class [BForm.form_group; Grid.col12] ] [
                        label [ txt "Password" ];
                        input ~a:[ a_id "pwd-input"; a_input_type `Password;
                                   a_class [BForm.form_control] ] ();
                        button ~a:[
                          a_class [Button.btn; Button.btn_dark; Spacing.m2];
                          a_onclick (fun _e ->
                              let password = Js_utils.(Manip.value @@ find_component "pwd-input") in
                              Vault.Cb.unlock
                                ~error:(fun _ _ -> raise_invalid "pwd-input")
                                account password;
                              true) ] [ txt "Unlock" ] ] ] () ] in
              replace1 Metal_ids.IdStart.main_content_id unlock_div
            | Some _ -> f ()))

let activate ?activate_info pkh = match activate_info with
  | Some (code, network) when code <> "" ->
    let network = Metal_helpers.network_of_string network in
    Metal_request.Scan.get_activated ~network
      ~error:(fun code msg -> Js_utils.log "cannot get activation from dunscan: %d %s" code msg)
      pkh
      (function
        | false ->
          Metal_request.Node.send_activation ~network
            ~error:(fun code msg -> Js_utils.log "activation failed %d %s" code msg)
             pkh code
             (fun (h, _ops) ->
                Js_utils.log "activation %s injected" (Operation_hash.b58enc h))
        | _ -> Js_utils.log "account %s already activated" pkh
      )
  | _ -> ()

let make_local_vault ?activate_info ?secret_key ?mnemonic ?(passphrase="") cancel =
  try
    let sk, pkh_b58 = match secret_key, mnemonic with
      | Some sk, _ ->
        let sk = Sk.b58dec sk in sk, Vault.pkh_from_sk sk
      | _, Some mnemonic ->
        vault_from_mnemonic mnemonic passphrase
      | _ -> assert false in
    activate ?activate_info pkh_b58;
    let f () =
      Vault.Cb.encrypt
        ~error:(fun code content -> Js_utils.log "error in make local vault: %d %s" code content)
        ~pkh:pkh_b58 sk (store_vault (fun _ -> make_end ())) in
    make_encrypt cancel f
  with _ ->
    hide_loading ();
    append_modal ~show:true "failed-generation" "Generation of account failed!" cancel

let make_ledger_vault ~path ~pkh cancel =
  let f () = Vault.Cb.hash_password
      ~error:(fun code content -> Js_utils.log "error in make ledger vault: %d %s" code content)
      ~pkh path (store_vault (fun _ -> make_end ())) in
  make_encrypt cancel f

let make_new cancel =
  let new_words () =
    let x = Hacl.Rand.gen 20 in
    Bip39.to_words @@ Bip39.of_entropy x in
  let new_div =
    div ~a:[ a_class [Grid.container; Spacing.py5; Sizing.h50; Display.d_flex] ] [
      BCard.make_card
        ~card_class:[Spacing.mxa; Flex.align_self_center]
        ~card_body_class:[Text.center; Grid.row]
        ~card_body_content:[
          h4 ~a:[ a_class [Text.left; Spacing.mx2; Grid.col12] ] [
            txt "Mnemonic Generated" ];
          div ~a:[ a_id "mnemonic-div";
                   a_class [Grid.col10; Grid.off1; Grid.clg6; Grid.offlg3;
                            Text.lead; Spacing.p4 ] ] [
            txt (String.concat " " (new_words ())) ];
          input ~a:[ a_id "passphrase-input";
                     a_placeholder "Passphrase (optional)"; a_input_type `Text;
                     a_class [BForm.form_group; Grid.col10; Grid.off1] ] ();
          div ~a:[ a_class [ Alert.alert ; Alert.aprimary; Spacing.m2;
                             Text.left; Spacing.mxa ] ;
               a_role [ "alert" ] ] [
            h3 ~a:[ a_class [ "alert-heading"; Display.d_flex ;
                              Flex.justify_between ] ] [
              fas "exclamation-triangle" ;
              span ~a:[ a_class [ Spacing.pl2 ] ] [
                txt "Store your mnemonic and passphrase in a safe place" ] ;
            ] ;
            div [
              p [ txt "Don't share it with anyone." ] ;
              p [ txt "This is the only way to recover your account." ] ;
            ] ;
          ] ;
          div ~a:[ a_class [Grid.col12] ] [
            button ~a:[ a_onclick (fun _e ->
                  let elt = Js_utils.find_component "mnemonic-div" in
                  let mnemonic = String.split_on_char ' '
                      (Js.to_string (Js_utils.Manip.get_elt "innerHTML" elt)##.innerHTML) in
                let passphrase = id_value "passphrase-input" in
                make_local_vault ~mnemonic ~passphrase cancel; true);
                a_class [Button.btn; Button.btn_dark; Spacing.m3] ] [
              txt "I Wrote down my mnemonic" ];
            button ~a:[ a_onclick (fun _e ->
                Js_utils.(Manip.replaceChildren (find_component "mnemonic-div") [
                    txt (String.concat " " (new_words ())) ]);
                true);
                a_class [Button.btn; Button.btn_dark; Spacing.m3] ] [
              txt "Refresh" ];
            button ~a:[ a_onclick (fun _e -> cancel (); true);
                        a_class [Button.btn; Button.btn_secondary; Spacing.m3] ] [
              txt "Cancel" ]
          ]
        ] ()
    ] in
  replace1 Metal_ids.IdStart.main_content_id new_div

module Tabs = Bs4_navs.Make(struct type t = unit end)
let tabs = List.mapi (fun i (id, title) ->
    let state = if i = 0 then Bs4_navs.Active else Bs4_navs.Inactive in
    Bs4_navs.make ~state id (fun _ -> span [ txt title ])) [
    "ico", "ICO/Faucet";
    "mnemonic", "Seed Phrase";
    "secret_key", "Secret Key";
    "ledger", "Ledger" ]

let seed_div cancel =
  BCard.make_card
    ~card_class:[Flex.align_self_center; Spacing.mxa]
    ~card_body_class:[Grid.row]
    ~card_body_content:[
      div ~a:[ a_class [BForm.form_group; Grid.col12] ] [
        label [ txt "Mnemonic *" ];
        textarea ~a:[ a_maxlength 150; a_class [ BForm.form_control ];
                      a_id "mnemonic-seed-div" ]
          (txt "") ];
      div ~a:[ a_class [BForm.form_group; Grid.col12] ] [
        label [ txt "Passphrase" ];
        input ~a:[ a_id "passphrase-seed-input"; a_input_type `Text;
                   a_class [BForm.form_control] ] () ];
      div ~a:[ a_class [Grid.col12; Text.center ] ] [
        button ~a:[ a_onclick (fun _e ->
            let mnemonic = String.split_on_char ' ' (id_value "mnemonic-seed-div") in
            let passphrase = id_value "passphrase-seed-input" in
            make_local_vault ~mnemonic ~passphrase cancel;
            true);
            a_class [Button.btn; Button.btn_dark; Spacing.m3] ] [
          txt "Restore" ];
        button ~a:[ a_onclick (fun _e -> cancel (); true);
                    a_class [Button.btn; Button.btn_secondary; Spacing.m3] ] [
          txt "Cancel" ]
      ]
    ] ()

let load_json () =
  let elt = Js_utils.find_component "json-input" in
  Js_utils.Manip.upload_input ~btoa:false elt (fun s ->
      let faucet = EzEncoding.destruct Metal_encoding.faucet s in
      set_id_value "mnemonic-ico-div" (String.concat " " faucet.fc_mnemonic);
      set_id_value "email-input" faucet.fc_email;
      set_id_value "password-input" faucet.fc_password;
      set_id_value "pkh-input" faucet.fc_pkh;
      set_id_value "activation-input" faucet.fc_code)

let ico_div custom_networks cancel =
  BCard.make_card
    ~card_class:[Flex.align_self_center; Spacing.mxa]
    ~card_body_class:[Grid.row]
    ~card_body_content:[
      div ~a:[ a_class [BForm.form_group; Grid.col6] ] [
        label [ txt "Network" ];
        select ~a:[ a_id "network-select";
                    a_class [BForm.custom_select] ] @@
        List.mapi (fun i n ->
            let selected = if i = 0 then [ a_selected () ] else [] in
            let s = Metal_helpers.network_to_string n in
            option ~a:(a_value s :: selected) ( txt s ))
          (network_options @ custom_networks)
      ];
      div ~a:[ a_class [BForm.form_group; Grid.col6] ] [
        label [ txt "Json File" ];
        div ~a:[ a_class [BForm.input_group] ] [
          div ~a:[ a_class [BForm.custom_file] ] [
            input ~a:[ a_id "json-input"; a_class [BForm.custom_file_input];
                       a_input_type `File;
                       a_onchange (fun _e ->
                           let filename = id_value "json-input" in
                           try
                             let index = String.rindex filename '\\' in
                             let filename = String.sub filename (index+1)
                                 ((String.length filename) - index - 1) in
                             let label = Js_utils.find_component "json-input-label" in
                             Js_utils.Manip.addClass label "selected";
                             Js_utils.Manip.setInnerHtml label filename; true
                           with _ -> true) ] ();
            label ~a:[ a_id "json-input-label"; a_class [BForm.custom_file_label];
                       a_label_for "json-input"] [] ];
          div ~a:[ a_class [BForm.input_group_append] ] [
            button ~a:[ a_class [Button.btn; Button.btn_dark];
                        a_onclick (fun _e -> load_json ())] [ txt "Load" ]
          ]
        ]
      ];
      div ~a:[ a_class [BForm.form_group; Grid.col12] ] [
        label [ txt "Mnemonic *" ];
        textarea ~a:[ a_maxlength 150; a_class [BForm.form_control];
                      a_id "mnemonic-ico-div" ]
          (txt "") ];
      div ~a:[ a_class [BForm.form_group; Grid.col6] ] [
        label [ txt "Email *" ];
        input ~a:[ a_id "email-input"; a_input_type `Text;
                   a_class [BForm.form_control] ] () ];
      div ~a:[ a_class [BForm.form_group; Grid.col6] ] [
        label [ txt "Password *" ];
        input ~a:[ a_id "password-input"; a_input_type `Text;
                   a_class [BForm.form_control] ] () ];
      div ~a:[ a_class [BForm.form_group; Grid.col6] ] [
        label [ txt "Public Key Hash" ];
        input ~a:[ a_id "pkh-input"; a_placeholder "dn..."; a_input_type `Text;
                   a_class [BForm.form_control] ] () ];
      div ~a:[ a_class [BForm.form_group; Grid.col6] ] [
        label [ txt "Activation Code" ];
        input ~a:[ a_id "activation-input"; a_input_type `Text;
                   a_class [BForm.form_control] ] () ];
      div ~a:[ a_class [Grid.col12; Text.center] ] [
        button ~a:[ a_onclick (fun _e ->
            let mnemonic = String.split_on_char ' ' (id_value "mnemonic-ico-div") in
            let passphrase = (id_value "email-input") ^ (id_value "password-input") in
            let activate_info = id_value "activation-input", id_value "network-select" in
            make_local_vault ~activate_info ~mnemonic ~passphrase cancel;
            true);
            a_class [Button.btn; Button.btn_dark; Spacing.m3] ] [
          txt "Restore" ];
        button ~a:[ a_onclick (fun _e -> cancel (); true);
                    a_class [Button.btn; Button.btn_secondary; Spacing.m3] ] [
          txt "Cancel" ];
      ]
    ] ()

let secret_div cancel =
  BCard.make_card
    ~card_class:[Flex.align_self_center; Spacing.mxa]
    ~card_body_class:[Grid.row]
    ~card_body_content:[
      div ~a:[ a_class [BForm.form_group; Grid.col12] ] [
        label [ txt "Secret Key" ];
        input ~a:[ a_id "secret-key-input"; a_input_type `Text;
                   a_class [BForm.form_control] ] () ];
      div ~a:[ a_class [Grid.col12; Text.center ] ] [
        button ~a:[ a_onclick (fun _e ->
            let secret_key = id_value "secret-key-input" in
            make_local_vault ~secret_key cancel;
            true);
            a_class [Button.btn; Button.btn_dark; Spacing.m3] ] [
          txt "Restore" ];
        button ~a:[ a_onclick (fun _e -> cancel (); true);
                    a_class [Button.btn; Button.btn_secondary; Spacing.m3] ] [
          txt "Cancel" ]
      ]
    ] ()

let ledger_div cancel =
  BCard.make_card
    ~card_class:[Flex.align_self_center; Spacing.mxa]
    ~card_body_class:[Grid.row]
    ~card_body_content:[
      div ~a:[ a_class [BForm.form_group; Grid.col12] ] [
        label [ txt "Hardware" ];
        select ~a:[ a_id "hardware-select";
                    a_class [BForm.custom_select] ] [
          option ~a:[ a_value "ledger"; a_selected () ] (txt "Ledger") ;
          (* option ~a:[ a_value "trezor" ] (txt "Trezor") *) ] ];
      div ~a:[ a_class [BForm.form_group; Grid.col12] ] [
        label [ txt "Derivation Path" ];
        input ~a:[ a_id "derivation-path-input"; a_input_type `Text;
                   a_class [BForm.form_control]; a_value "44'/1729'/0'/0'" ] () ];
      div ~a:[ a_class [Grid.col12; Text.center ] ] [
        button ~a:[ a_onclick (fun _e ->
            let path = id_value "derivation-path-input" in
            Async.async_req
              ~error:(fun code content ->
                  Js_utils.log "error in getadress %d %s" code content;
                  raise_invalid "derivation-path-input")
              (Ledger_js.getAddress_bg path)
              (fun (_pk, pkh) -> make_ledger_vault ~pkh ~path cancel); true);
            a_class [Button.btn; Button.btn_dark; Spacing.m3] ] [
          txt "Link" ];
        button ~a:[ a_onclick (fun _e -> cancel (); true);
                    a_class [Button.btn; Button.btn_secondary; Spacing.m3] ] [
          txt "Cancel" ]
      ]
    ] ()

let make_import cancel =
  let new_div custom_networks =
    div ~a:[ a_class [Grid.container; Spacing.py5; Sizing.h50] ] [
      Tabs.make_navs ~set_args:(fun _ -> ()) tabs;
      div ~a:[ a_class [Nav.tab_content] ] @@
      List.map2 Bs4_navs.make_tab_content tabs
        [ico_div custom_networks cancel; seed_div cancel; secret_div cancel; ledger_div cancel]
    ] in
  Storage_reader.get_custom_networks (fun custom_networks ->
      let custom_networks = List.map (fun (name, _, _) -> Custom name) custom_networks in
      replace1 Metal_ids.IdStart.main_content_id (new_div custom_networks))


let rec make_start () =
  let import_div =
    BCard.make_card
      ~card_body_class:[ Text.center ]
      ~card_body_content:[
        h5 ~a:[ a_class [Spacing.m2] ] [txt "Import an existing account!"];
        br ();
        button ~a:[ a_onclick (fun _e -> make_import make_start; true);
                    a_class [Button.btn; Button.btn_dark] ] [
          txt "Import Wallet" ]
      ] () in
  let new_div =
    BCard.make_card
      ~card_body_class:[ Text.center ]
      ~card_body_content:[
        h5 ~a:[ a_class [ Spacing.m2 ] ] [txt "Create a new account!"];
        br ();
        button ~a:[ a_onclick (fun _e -> make_new make_start; true);
                    a_class [Button.btn; Button.btn_dark] ] [
          txt "Create Wallet" ]
      ] () in
  let all = div ~a:[ a_class [Grid.container; Spacing.py5; Sizing.h50; Display.d_flex] ] [
      div ~a:[ a_class [Grid.row; Flex.align_self_center] ] [
        div ~a:[ a_class [Sizing.bh1; Grid.col12; Grid.cmd12; Text.center; Spacing.my5] ] [
          txt "Welcome to Dune Metal!" ];
        div ~a:[ a_class [Grid.col12; Grid.cmd6; Spacing.py2] ] [ import_div ];
        div ~a:[ a_class [Grid.col12; Grid.cmd6; Spacing.py2] ] [ new_div ]
      ] ] in
  Storage_reader.get_network (fun _ho_net ->
      Storage_reader.get_account (fun (selected, _ho_accs) ->
          match selected with
          | None ->
            Js_utils.Manip.addClass
              (Js_utils.find_component Metal_ids.IdStart.home_link_id)
              "disabled";
            Js_utils.Manip.addClass
              (Js_utils.find_component Metal_ids.IdStart.settings_link_id)
              "disabled";
          | Some _ -> ())) ;
  replace1 Metal_ids.IdStart.main_content_id all

let () =
  make_loading ();
  make_start ()
