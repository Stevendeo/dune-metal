(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js.Html
open Js_utils.Manip
open Js_of_ocaml
open Chrome

open Bs4.Items
open Bs4.Utils
open Grid
open Flex
open Text
open Button

open Metal_message_types
open Metal_types

let remove_window notif =
  let wid = match notif with
    | OriNot n | TraNot n | DlgNot n | ManNot n -> n.not_wid
    | ApprovNot n -> n.not_approv_wid in
  Windows.getAll (fun wins ->
      List.iter (fun win ->
          Js_types.def_case
            win##.id
            (fun () -> ())
            (fun id ->
               if id = wid then
                 Windows.remove wid))
        wins
    )

let mk_header title =
  div ~a:[ a_class [ "popup-header"; row ] ] [
      div ~a:[ a_class [ col2 ] ] [
        img ~src:"img/dune.png" ~alt:"metal logo" ()
      ] ;
      div ~a:[ a_class [ col8; align_self_center; center ] ] [
        span [ txt title ]
      ]
  ]

let close_notif _n =
  Windows.getCurrent (fun win ->
      Js.Optdef.case
        (win##.id)
        (fun () -> Js_utils.log "no popup to close ?")
        (fun id -> Windows.remove id))

let mk_confirm_button ~cb =
  let click = (fun _ev -> cb () ; true) in
  button ~a:[ a_onclick click ;
              a_class [ btn; btn_outline_success; "btn-circle" ; "btn-md" ] ] [
    Metal_helpers_ui.fas "check" ]

let mk_cancel_button ~cb =
  let click = (fun _ev -> cb () ; true) in
  button ~a:[ a_onclick click ;
              a_class [ btn; btn_outline_danger ; "btn-circle"; "btn-md" ] ] [
    Metal_helpers_ui.fas "times" ]

let hide_all () =
  addClass
    (Js_utils.find_component Metal_ids.IdPopup.main_container_id) "d-none";
  removeClass
    (Js_utils.find_component Metal_ids.IdPopup.main_container_id) "d-flex";
  addClass
    (Js_utils.find_component Metal_ids.IdPopup.loading_container_id) "d-none" ;
  removeClass
    (Js_utils.find_component Metal_ids.IdPopup.loading_container_id) "d-flex" ;
  addClass
    (Js_utils.find_component Metal_ids.IdPopup.enable_container_id) "d-none" ;
  removeClass
    (Js_utils.find_component Metal_ids.IdPopup.enable_container_id) "d-flex" ;
  addClass
    (Js_utils.find_component Metal_ids.IdPopup.unlock_container_id) "d-none" ;
  removeClass
    (Js_utils.find_component Metal_ids.IdPopup.unlock_container_id) "d-flex" ;
  addClass
    (Js_utils.find_component Metal_ids.IdPopup.approve_container_id) "d-none" ;
  removeClass
    (Js_utils.find_component Metal_ids.IdPopup.approve_container_id) "d-flex"

let show elt =
  removeClass elt "d-none" ;
  addClass elt "d-flex"

let make_enable_page () =
  hide_all () ;
  show (Js_utils.find_component Metal_ids.IdPopup.enable_container_id) ;
  Metal_core.update_start ~cb:close_notif Metal_ids.IdPopup.enable_container_id

let make_unlock_page () =
  hide_all () ;
  Storage_reader.get_network (fun fo_net ->
      Storage_reader.get_account (fun (selected, _accounts)  ->
          match selected with
          | None -> ()
          | Some fo_acc ->
            hide_all () ;
            show (Js_utils.find_component Metal_ids.IdPopup.unlock_container_id) ;
            Metal_core.unlock_session
              Metal_ids.IdPopup.unlock_container_id
              (fun _st -> close_notif ())
              {fo_acc; fo_net}))

let update_header msg =
  Metal_helpers_ui.replace Metal_ids.IdPopup.header_top_id [
    div ~a:[ a_class [ "header-logo" ] ] [ img ~src:"img/dune-gray.png" ~alt:"Dune Metal" () ] ;
    div ~a:[ a_class [ "header-info"; "align-self-center"; "text-center" ] ] [ span [ txt msg ] ] ;
    div ~a:[ a_class [ "header-logo" ] ] [ ] ;
  ]

let update_approve_page notif =
  hide_all () ;
  show @@ Js_utils.find_component Metal_ids.IdPopup.main_container_id ;
  show @@ Js_utils.find_component Metal_ids.IdPopup.approve_container_id ;
  update_header "Approval Request" ;
  let app_dom =
    Metal_core.mk_approve_page
      ~confirm:(Metal_core.approve_callback)
      ~deny:(Metal_core.deny_callback)
      notif in
  let container = Js_utils.find_component Metal_ids.IdPopup.approve_container_id in
  replaceChildren container app_dom

let make_approve_page_metadata wid args =
  let _, rid = List.find (fun (n, _v) -> n = "req_id") args in
  Js_utils.log "make_approve_page_metadata %s" rid ;
  let msg : message Js.t = Metal_message.mk_metadata_req "popup" rid in
  let info = Runtime_utils.mk_connection_info "popup" in
  let port = Runtime.connect ~info () in
  port##postMessage msg ;
  Browser_utils.addListener1
    (port##.onMessage)
    (fun ans ->
       Js_utils.js_log ans ;
       if msg##.req##.id = ans##.res##.id &&
          ans##.src = Js.string "background" then
         begin
           Storage_reader.get_account (fun (acc, _) -> match acc, Js.Optdef.to_option wid with
               | Some acc, Some wid ->
                 let (metadata : siteMetadata Js.t) = ans##.res##.result in
                 let tsp = Jsdate.now_tsp () in
                 let icon =
                   Metal_helpers.convopt
                     Js.to_string
                     (Js.Optdef.to_option metadata##.icon) in
                 let name = Js.to_string metadata##.name in
                 let url = Js.to_string metadata##.url in
                 let notif =
                   Metal_helpers.make_notif_approv ~id:rid ~wid ~tsp ~icon ~name ~url in

                 Storage_writer.add_notif
                   ~callback:(fun _ ->
                       Storage_reader.get_notifs (fun ns ->
                           let n_notif =
                             List.length @@
                             try
                               (List.find (fun {notif_acc ; _} ->
                                    notif_acc = acc.pkh) ns).notifs
                             with Not_found -> [] in
                           Browser_action.set_badge ~text:(string_of_int n_notif) () ;
                           update_approve_page notif))
                   acc.pkh
                   notif
               | _, _ -> ()
             ) ;
         end
       else
         (* make_error_page *) ()
    )

let update_op_page notif =
  let open Async in
  hide_all () ;
  show @@ Js_utils.find_component Metal_ids.IdPopup.main_container_id ;
  show @@ Js_utils.find_component Metal_ids.IdPopup.approve_container_id ;
  let container = Js_utils.find_component Metal_ids.IdPopup.approve_container_id in
  let forging_dom = Metal_core.make_forging_dom () in
  begin match notif with
    | TraNot _ ->
      update_header "Transaction Request"
    | OriNot _ ->
      update_header "Origination Request"
    | DlgNot _ ->
      update_header "Delegation Request"
    | ManNot _ ->
      update_header "Manage Account Request"
    | _ ->
      update_header "TODO"
  end ;
  replaceChildren container [ forging_dom ] ;
  Storage_reader.get_network (fun net ->
      Storage_reader.get_account (fun (acc, _) -> match acc with
          | None ->
            let err = Str_err "No account selected" in
            let c, msg = error_content err in
            Metal_core.error_callback notif c msg ;
            Js_utils.log "ERROR %d : %s" c msg ;
            let dom =
              Metal_core.make_error_dom
                (fun () -> remove_window notif)
                err
            in
            Metal_helpers_ui.replace_fade container dom
          | Some acc ->
            let callback =
              (fun acc ->
                 async (fun () ->
                     Metal_core.mk_op_page
                       ~confirm:(Metal_core.approve_callback)
                       ~deny:(Metal_core.deny_callback)
                       acc
                       net
                       notif >>= function
                     | Ok res ->
                       Metal_helpers_ui.replace_fade container res ;
                       return_unit
                     | Error err ->
                       let c, msg = error_content err in
                       Metal_core.error_callback notif c msg ;
                       Js_utils.log "ERROR %d : %s" c msg ;
                       let dom =
                         Metal_core.make_error_dom
                           (fun () -> remove_window notif)
                           err in
                       Metal_helpers_ui.replace_fade container dom ;
                       return_unit)) in
            if Metal_helpers_ui.is_revealed net acc then callback acc
            else Metal_helpers_ui.update_revealed net acc ~callback))

let make_send_page wid args =
  let _, rid = List.find (fun (n, _v) -> n = "req_id") args in
  Js_utils.log "make_send_page %s" rid ;
  let msg : message Js.t = Metal_message.mk_tr_req "popup" rid in
  let info = Runtime_utils.mk_connection_info "popup" in
  let port = Runtime.connect ~info () in
  port##postMessage msg ;
  Browser_utils.addListener1
    (port##.onMessage)
    (fun ans ->
       Js_utils.js_log ans ;
       if msg##.req##.id = ans##.res##.id &&
          ans##.src = Js.string "background" then
         begin
           Storage_reader.get_account (fun (acc, _) -> match acc, Js.Optdef.to_option wid with
               | Some acc, Some wid ->
                 let open Js_types in
                 let (data : transactionData Js.t) = ans##.res##.result in
                 let tsp = Jsdate.now_tsp () in
                 let dst = to_string data##.dst in
                 let amount = Some (Int64.of_string (to_string data##.amount)) in
                 let fee =
                   to_optdef (fun s -> Int64.of_string @@ to_string s) data##.fee in
                 let params = to_optdef to_string data##.parameter in
                 let entry = to_optdef to_string data##.entrypoint in
                 let gas_limit =
                   to_optdef (fun s -> Z.of_string @@ to_string s) data##.gasLimit in
                 let storage_limit =
                   to_optdef (fun s -> Z.of_string @@ to_string s) data##.storageLimit in
                 let origin = match to_def_option port##.sender with
                   | None -> "unknown"
                   | Some sender -> match to_def_option sender##.url with
                     | None -> "unknown"
                     | Some origin -> to_string origin in
                 let msg = to_optdef to_string data##.msg in
                 let notif =
                   Metal_helpers.make_notif_tr
                     ?amount
                     ?fee
                     ?gas_limit
                     ?storage_limit
                     ?entry
                     ?params
                     ?msg
                     ~id:rid
                     ~wid
                     ~tsp
                     ~origin
                     dst
                 in
                 Storage_reader.get_notifs (fun ns ->
                     let notifs =
                       try
                         (List.find (fun {notif_acc ; _} ->
                              notif_acc = acc.pkh) ns).notifs
                       with Not_found -> [] in
                     let n_notif = List.length notifs in
                     if (List.exists
                           (function ApprovNot n -> n.not_approv_wid = wid
                                   | TraNot n | DlgNot n | OriNot n | ManNot n -> n.not_wid = wid)
                           notifs) then
                       update_op_page notif
                     else
                       Storage_writer.add_notif
                         ~callback:(fun _ ->
                             Browser_action.set_badge ~text:(string_of_int @@ n_notif + 1) () ;
                             update_op_page notif)
                         acc.pkh
                         notif)
               | _, _ -> ()
             ) ;
         end
       else
         (* make_error_page *) ()
    )

let make_originate_page wid args =
  let _, rid = List.find (fun (n, _v) -> n = "req_id") args in
  Js_utils.log "make_originate_page %s" rid ;
  let msg : message Js.t = Metal_message.mk_ori_req "popup" rid in
  let info = Runtime_utils.mk_connection_info "popup" in
  let port = Runtime.connect ~info () in
  port##postMessage msg ;
  Browser_utils.addListener1
    (port##.onMessage)
    (fun ans ->
       Js_utils.js_log ans ;
       if msg##.req##.id = ans##.res##.id &&
          ans##.src = Js.string "background" then
         begin
           Storage_reader.get_account (fun (acc, _) -> match acc, Js.Optdef.to_option wid with
               | Some acc, Some wid ->
                 let open Js_types in
                 let (data : originationData Js.t) = ans##.res##.result in
                 let tsp = Jsdate.now_tsp () in
                 let balance = Some (Int64.of_string (to_string data##.balance)) in
                 let fee =
                   to_optdef (fun s -> Int64.of_string @@ to_string s) data##.fee in
                 let gas_limit =
                   to_optdef (fun s -> Z.of_string @@ to_string s) data##.gasLimit in
                 let storage_limit =
                   to_optdef (fun s -> Z.of_string @@ to_string s) data##.storageLimit in
                 let sc_code = to_optdef to_string data##.scCode in
                 let sc_code_hash = to_optdef to_string data##.scCodeHash in
                 let sc_storage = to_optdef to_string data##.scStorage in
                 let origin = match to_def_option port##.sender with
                   | None -> "unknown"
                   | Some sender -> match to_def_option sender##.url with
                     | None -> "unknown"
                     | Some origin -> to_string origin in
                 let msg = to_optdef to_string data##.msg in
                 let notif =
                   Metal_helpers.make_notif_ori
                     ?amount:balance
                     ?fee
                     ?gas_limit
                     ?storage_limit
                     ?sc_code
                     ?sc_code_hash
                     ?sc_storage
                     ~id:rid
                     ~wid
                     ~tsp
                     ~origin
                     ?msg
                     ()
                 in
                 Storage_reader.get_notifs (fun ns ->
                     let notifs =
                       try
                         (List.find (fun {notif_acc ; _} ->
                              notif_acc = acc.pkh) ns).notifs
                       with Not_found -> [] in
                     let n_notif = List.length notifs in
                     if (List.exists
                           (function ApprovNot n -> n.not_approv_wid = wid
                                   | TraNot n | DlgNot n | OriNot n | ManNot n
                                     -> n.not_wid = wid)
                           notifs) then
                       update_op_page notif
                     else
                       Storage_writer.add_notif
                         ~callback:(fun _ ->
                             Browser_action.set_badge ~text:(string_of_int @@ n_notif + 1) () ;
                             update_op_page notif)
                         acc.pkh
                         notif)
               | _, _ -> ()
             ) ;
         end
       else
         (* make_error_page *) ()
    )

let make_delegate_page wid args =
  let _, rid = List.find (fun (n, _v) -> n = "req_id") args in
  Js_utils.log "make_delegate_page %s" rid ;
  let msg : message Js.t = Metal_message.mk_dlg_req "popup" rid in
  let info = Runtime_utils.mk_connection_info "popup" in
  let port = Runtime.connect ~info () in
  port##postMessage msg ;
  Browser_utils.addListener1
    (port##.onMessage)
    (fun ans ->
       Js_utils.js_log ans ;
       if msg##.req##.id = ans##.res##.id &&
          ans##.src = Js.string "background" then
         begin
           Storage_reader.get_account (fun (acc, _) -> match acc, Js.Optdef.to_option wid with
               | Some acc, Some wid ->
                 let open Js_types in
                 let (data : delegationData Js.t) = ans##.res##.result in
                 let tsp = Jsdate.now_tsp () in
                 let delegate = to_optdef to_string data##.delegate in
                 let fee =
                   to_optdef (fun s -> Int64.of_string @@ to_string s) data##.fee in
                 let gas_limit =
                   to_optdef (fun s -> Z.of_string @@ to_string s) data##.gasLimit in
                 let storage_limit =
                   to_optdef (fun s -> Z.of_string @@ to_string s) data##.storageLimit in
                 let origin = match to_def_option port##.sender with
                   | None -> "unknown"
                   | Some sender -> match to_def_option sender##.url with
                     | None -> "unknown"
                     | Some origin -> to_string origin in
                 let msg = to_optdef to_string data##.msg in
                 let notif =
                   Metal_helpers.make_notif_del
                     ?fee
                     ?gas_limit
                     ?storage_limit
                     ?msg
                     ~id:rid
                     ~wid
                     ~tsp
                     ~origin
                     delegate
                 in
                 Storage_reader.get_notifs (fun ns ->
                     let notifs =
                       try
                         (List.find (fun {notif_acc ; _} ->
                              notif_acc = acc.pkh) ns).notifs
                       with Not_found -> [] in
                     let n_notif = List.length notifs in
                     if (List.exists
                           (function ApprovNot n -> n.not_approv_wid = wid
                                   | TraNot n | DlgNot n | OriNot n | ManNot n -> n.not_wid = wid)
                           notifs) then
                       update_op_page notif
                     else
                       Storage_writer.add_notif
                         ~callback:(fun _ ->
                             Browser_action.set_badge ~text:(string_of_int @@ n_notif + 1) () ;
                             update_op_page notif)
                         acc.pkh
                         notif)
               | _, _ -> ()
             ) ;
         end
       else
         (* make_error_page *) ()
    )

let dispatch id args =
  try
    let _, typ = List.find (fun (n, _v) -> n = "type") args in
    match typ with
    | "enable" -> make_enable_page ()
    | "unlock" -> make_unlock_page ()
    | "approve" -> make_approve_page_metadata id args
    | "send" -> make_send_page id args
    | "originate" -> make_originate_page id args
    | "delegate" -> make_delegate_page id args
    | _ -> Js_utils.log "don't know what to do with %s" typ
  with Not_found ->
    Js_utils.log "No type argument"

let () =
  Onload.add (fun () ->
      let args = Metal_helpers_ui.get_extension_url () in
      Windows.getCurrent (fun win -> dispatch win##.id args) ;
    )
