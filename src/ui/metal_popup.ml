(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Metal_ids.IdPopup
open Metal_core
open Metal_types

let update_dune_metal () =
  Storage_reader.get_network (fun fo_net ->
      Storage_reader.get_account (fun (selected, _accounts)  ->
          match selected with
          | None -> update_start unlock_container_id
          | Some fo_acc ->
            Vault.Cb.password (function
                | None ->
                  unlock_session unlock_container_id update_account {fo_acc; fo_net}
                | Some _ ->
                  begin match fo_acc.manager_kt with
                    | None ->
                      Metal_request.Node.get_account_info ~network:fo_net
                        ~error:(fun _ _ -> update_account {fo_acc; fo_net})
                        fo_acc.pkh
                        (fun acc_info ->
                           match acc_info.Dune_types_min.node_ai_script with
                           | Some (Some sc, _, _) ->
                             if sc = Node.manager_kt then
                               Storage_writer.update_account
                                 ~callback:(fun fo_acc ->
                                     update_account { fo_acc ; fo_net })
                                 { fo_acc with manager_kt = Some true }
                             else
                               Storage_writer.update_account
                                 ~callback:(fun fo_acc ->
                                     update_account { fo_acc ; fo_net })
                                 { fo_acc with manager_kt = Some false }
                           | _ ->
                             Storage_writer.update_account
                               ~callback:(fun fo_acc ->
                                   update_account { fo_acc ; fo_net })
                               { fo_acc with manager_kt = Some false })
                    | Some _ ->
                      update_account {fo_acc; fo_net}
                  end)))

let () =
  Onload.add (fun () -> update_dune_metal ())
