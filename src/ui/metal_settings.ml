(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Html
open Bs4.Items
open Bs4.Utils
open Metal_types
open Metal_ids.IdSettings
open Metal_helpers_ui

let gclass = [Grid.off2; Grid.col8; Grid.offmd0; Grid.cmd4; BForm.form_group]
let btn_classes = [Button.btn; Button.btn_dark; Spacing.m2]

let (>>=) = Async.(>>=)
let (>>=?) = Async.(>>=?)

let reencrypt_accounts ?error old_pwd new_pwd accounts f =
  let rec iter old_accounts new_accounts = match old_accounts, new_accounts with
    | [], new_accounts -> Async.return (Ok (List.rev new_accounts))
    | {pkh; vault; _} :: old_accounts, new_accounts ->
      match vault with
      | Local vault ->
        Vault.Lwt.decrypt_base ~password:old_pwd ~pkh vault >>=? fun sk ->
        Vault.Lwt.encrypt_base ~password:new_pwd ~pkh sk >>=? fun new_account ->
        iter old_accounts (new_account :: new_accounts)
      | Ledger (path, _) ->
        Vault.Lwt.hash_password ~pwd:new_pwd ~pkh path >>=? fun new_account ->
        iter old_accounts (new_account :: new_accounts) in
  Async.async_req ?error (iter accounts []) f

let sort_buttons refresh state pkh =
  let up _e =
    let rec iter = function
      | x :: y :: t when y.pkh = pkh -> y :: x :: t
      | h :: t -> h :: (iter t)
      | [] -> [] in
    let se_accs = iter state.se_accs in
    Storage_writer.update_accounts
      ~callback:(fun () -> refresh {state with se_accs}) se_accs;
    true in
  let down _e =
    let rec iter = function
      | x :: y :: t when x.pkh = pkh -> y :: x :: t
      | h :: t -> h :: (iter t)
      | [] -> [] in
    let se_accs = iter state.se_accs in
    Storage_writer.update_accounts
      ~callback:(fun () -> refresh {state with se_accs}) se_accs;
    true in
  div ~a:[ a_class [Button.btn_group_vertical] ] [
    button ~a:[ a_class [Button.btn; Button.btn_light; Button.btn_sm; Spacing.py0]; a_onclick up;
                a_style "font-size:10px"] [
      entity "#9652" ];
    button ~a:[ a_class [Button.btn; Button.btn_light; Button.btn_sm; Spacing.py0]; a_onclick down;
                a_style "font-size:10px" ] [
      entity "#9662" ]
  ]

let remove_account_handler refresh state pkh _e =
  Storage_writer.remove_account
    ~callback:(fun se_accs ->
        match state.se_acc with
        | Some selected when pkh = selected.pkh ->
          let se_acc, selected_pkh = match List.nth_opt se_accs 0 with
            | None -> None, Some ""
            | Some a -> Some a, Some a.pkh in
          Storage_writer.update_selected
            ~callback:(fun () -> refresh {se_acc; se_accs})
            selected_pkh
        | _ ->
          refresh {state with se_accs}) pkh;
  true


module Accounts =
  Bs4_card.MakeCardTable(struct
    type t = account
    type state = ((se_state -> unit) * se_state)
    let name = "accounts"
    let title_span _ = span []
    let page_size = 20
    let table_class = []
    let card_classes = []
    let heads = [ txt ""; txt "Address"; txt "Alias"; txt "" ]
    let empty = Some "No accounts"
    let head_class = [Table.thead_dark]
    let to_row state {pkh; name; _} =
      let onclick, sort = match state with
        | None -> (fun _ -> true), div []
        | Some (refresh, state) ->
          remove_account_handler refresh state pkh, sort_buttons refresh state pkh
      in
      tr [
        td [ sort ];
        td [ account_w_blockies ~crop_len:10 Mainnet {dn=pkh; alias=None} ];
        td [
          input ~a:[ a_input_type `Text; a_value name; a_class [BForm.form_control];
                     a_id ("alias-input-" ^ pkh) ] () ];
        td [ button ~a:[
            a_class [Button.btn; Button.btn_sm; Button.btn_light];
            a_onclick onclick ] [ entity "times" ] ]
      ]
  end)

let update_accounts_table refresh state =
  let get_accounts accounts =
    List.map (fun a ->
        {a with
         name = let s = id_value ("alias-input-" ^ a.pkh) in
           if s = "" then a.name else s})
      accounts in
  let save_names _e = Storage_writer.update_accounts (get_accounts state.se_accs); true in
  let card_header_class = if List.length state.se_accs > 20 then [] else [Display.d_none] in
  let table = Accounts.make ~card_header_class ~card_body_class:[Spacing.p0] () in
  replace1 accounts_settings_id (
    div ~a:[ a_class [Text.center] ] [
      table;
      button ~a:[ a_class btn_classes; a_onclick save_names ]
        [ txt "Save Aliases" ]
    ]);
  Accounts.update_from_all ~state:(refresh, state) (fun f -> f state.se_accs)

let change_pwd_handler refresh state _e =
  let old_pwd = id_value old_pwd_id in
  let new_pwd = id_value new_pwd_id in
  let confirm_pwd = id_value confirm_pwd_id in
  Vault.Cb.check_password old_pwd (function
      | true when new_pwd = confirm_pwd && new_pwd <> old_pwd ->
        reencrypt_accounts old_pwd new_pwd state.se_accs (fun se_accs ->
            Vault.Cb.set_password ~callback:(fun _ ->
                Storage_writer.update_accounts
                  ~callback:(fun () -> refresh {state with se_accs}) se_accs)
              (Some new_pwd))
      | _ -> raise_invalid new_pwd_id);
  true

let update_password_settings refresh state =
  replace1 password_settings_id @@
  div ~a:[ a_class [Grid.row] ] [
    Metal_forms.short_input_wlabel ~input_type:`Password ~classes:gclass
      old_pwd_id "Old Password";
    Metal_forms.short_input_wlabel ~input_type:`Password ~classes:gclass
      new_pwd_id "New Password";
    Metal_forms.short_input_wlabel ~input_type:`Password ~classes:gclass
      confirm_pwd_id "Confirm Password";
    div ~a:[ a_class [Grid.col4; Grid.off4; Text.center] ] [
      button ~a:[ a_class btn_classes;
                  a_onclick (change_pwd_handler refresh state) ] [
        txt "Change password" ] ] ]

let show_secret_handler accounts _e =
  let a_pkh = id_value "sk-account-select" in
  let pwd = id_value "sk-pwd-input" in
  let account = List.find (fun {pkh; _} -> a_pkh = pkh) accounts in
  match account.vault with
  | Ledger _ -> raise_invalid "sk-account-select"; true
  | Local vault ->
    Vault.Cb.password (fun password ->
        if Some pwd <> password then raise_invalid "sk-pwd-input"
        else
          Vault.Cb.decrypt_base ?password ~pkh:account.pkh vault (fun sk ->
              let sk_b58 = Crypto.Sk.b58enc sk in
              set_id_value "sk-show" sk_b58));
    true

let update_show_secret accounts =
  replace1 show_secret_id @@
  div ~a:[ a_class [Grid.row] ] [
    div ~a:[ a_class gclass ] [
      label [ txt "Account" ];
      select ~a:[ a_id "sk-account-select";
                  a_class [BForm.form_control; BForm.custom_select] ] @@
      List.mapi (fun i a ->
          let selected = if i = 0 then [ a_selected () ] else [] in
          option ~a:((a_value a.pkh) :: selected) ( txt a.name))
        accounts
    ];
    div ~a:[ a_class gclass ] [
      label [ txt "Password" ];
      div ~a:[ a_class [BForm.input_group] ] [
        input ~a:[ a_id "sk-pwd-input"; a_input_type `Password;
                   a_class [BForm.form_control] ] ();
        div ~a:[ a_class [BForm.input_group_append ] ] [
          button ~a:[ a_class [Button.btn; Button.btn_dark];
                      a_onclick (show_secret_handler accounts)
                    ] [ txt "Show" ] ];
      ] ];
    div ~a:[ a_class gclass ] [
      label [ txt "Secret key" ];
      div ~a:[ a_class [BForm.input_group] ] [
        input ~a:[ a_id "sk-show"; a_readonly (); a_class [BForm.form_control] ] ();
        div ~a:[ a_class [BForm.input_group_append ] ] [
          button ~a:[ a_class [Button.btn; Button.btn_dark];
                      a_onclick (fun _e -> copy "sk-show"; true)
                    ] [ fas "copy" ] ] ] ] ]


let rec update_accounts state =
  update_accounts_table update_accounts state;
  update_password_settings update_accounts state;
  update_show_secret state.se_accs

let remove_network_handler refresh networks name _e =
  Storage_writer.remove_custom_network ~callback:refresh ~networks name; true

let add_network_handler refresh networks _e =
  let node = id_value "custom-node-input" in
  let api = id_value "custom-api-input" in
  let name = id_value "custom-name-input" in
  if node <> "" && api <> "" && name <> "" then
    Storage_writer.add_custom_network ~networks
      ~callback:refresh name node api;
  true

let update_add_network refresh networks =
  replace1 add_network_id @@ (
    div ~a:[ a_class [Grid.row; Text.left] ] [
      Metal_forms.short_input_wlabel ~classes:gclass custom_name_id "Name";
      Metal_forms.short_input_wlabel ~classes:gclass custom_node_id "Node";
      Metal_forms.short_input_wlabel ~classes:gclass custom_api_id "Scan";
      div ~a:[ a_class [ Text.center; Grid.col12 ] ] [
        button ~a:[ a_class btn_classes;
                    a_onclick (add_network_handler refresh networks) ] [ txt "Add" ] ]
  ])

module Networks =
  Bs4_card.MakeCardTable(struct
    type t = (string * Url.url * Url.url)
    type state = ((t list -> unit) * t list)
    let name = "networks"
    let title_span _ = span []
    let page_size = 20
    let table_class = []
    let card_classes = []
    let heads = [ txt "Name"; txt "Node"; txt "Scan"; txt "" ]
    let empty = Some "No custom networks added"
    let head_class = [Table.thead_dark]
    let to_row state (name, node, api) =
      let onclick = match state with
        | None -> (fun _ -> true)
        | Some (refresh, networks) -> remove_network_handler refresh networks name in
      tr [
        td [ txt name ];
        td [ txt @@ Url.string_of_url node ];
        td [ txt @@ Url.string_of_url api ];
        td [ button ~a:[
            a_onclick onclick ]
            [ entity "times" ] ]
      ]
  end)

let update_networks_table refresh networks =
  let card_header_class = if List.length networks > 20 then [] else [Display.d_none] in
  let table = Networks.make ~card_header_class ~card_body_class:[Spacing.p0] () in
  replace1 networks_settings_id (
    div ~a:[ a_class [Text.center] ] [
      table;
      div ~a:[ a_id add_network_id; a_class [Spacing.py3] ] []
    ]);
  Networks.update_from_all ~state:(refresh, networks) (fun f -> f networks);
  update_add_network refresh networks

let rec update_networks networks =
  update_networks_table update_networks networks

let update_settings () =
  Storage_reader.get_account (fun (se_acc, se_accs) ->
      update_accounts {se_acc; se_accs} );
  Storage_reader.get_custom_networks update_networks

let () =
  Storage_reader.is_enabled (fun enabled ->
      Storage_reader.is_unlocked (fun unlocked -> match enabled, unlocked with
          | false, _ -> update_tab "/presentation.html"
          | true, false -> update_tab "/unlock.html?page=settings"
          | _ -> update_settings ()))
