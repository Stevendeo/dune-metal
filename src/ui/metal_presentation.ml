open Ocp_js
open Html
open Bs4.Utils
open Bs4.Items
open Bs4.Items.Button
open Metal_helpers_ui

let total_page = 3


(* Page 1 *)
let wallet_txt =
  "... a browser wallet. As any wallet, Metal offers an interface to the Dune \
   Network blockchain. It allows anyone to execute Dune Network \
   operations: send coins, pick a delegation service and delegate coins to \
   them, originate new smart contract, etc.. This enables anyone to interact \
   with the blockchain without having to run a full node."

let dapp_txt =
  "... a bridge to dApp. It aims to create a hub that will help dApp developers \
   and remove friction for the users. DApp developers can start working on \
   their app and use the injected metal api to delegate the crypto part to \
   metal. The user will receive notification of any signing requests \
   coming from the visited dApp pages and decide to accept or not."

let open_txt =
  "open source and free. You can install it from the extension store on Firefox \
   and Chrome. You can view the source code on gitlab. You can also use gitlab \
   to contribute to the code base by making merge requests or reporting any \
   issues that you may encounters."

(* Page 2*)

let bc_txt =
  "A wallet is a piece of software that holds your public address and private \
   key. This pair of data is the only thing you need to send or receive \
   tokens. The wallet aims to allow interactions with the blockchain in a \
   simple and secure way, it is just a convinient interface. Your tokens are \
   not store in the wallet, they are on the blockchain itself. This is why it \
   important to keep your keys safe, having control over them means \
   controlling the funds on the corresponding address."

(* Page 3 *)

let intro_txt =
  "Metal is not just another browser wallet. It is the gateway to the \
   decentralized web. It aims to make dApp development easier by allowing \
   dApp developer to delegate the account managing to Metal. Hence Metal \
   provides an API that is injected in the web page. DApp developers can \
   use this API to interact with your account (via notification) and with \
   the blockchain (send operations)."

let approve_txt =
  "As soon as a dApp will try to interact with the background of metal, \
   a notification popup will be open to ask permission. Since the API is \
   injected in any page you visit, you will have to whitelist domain that \
   can get a response from the API. Whitelisting a domain will make it able \
   to query the current configuration of your wallet (selected account, \
   selected network, unlock state, etc) and most importantly to send sign \
   requests (transaction, origination, delegation, etc). So make sure you are \
   on a trusted domain before accepting. The Dune operations \
   sent by dApp will by notified to you and wait for your approval. You always \
   should carrefuly review the operation that the dApp want you to sign."

let tra_txt =
  "This kind of notification popup will open when a dApp request you to sign \
   an operation. May it be transaction, origination or delegation you will be \
   able to review it in this window. Make sure to understand what the \
   operation implies before accepting it."

let mk_img ?(aclass=[]) src alt =
  img ~a:[ a_class aclass ] ~src ~alt ()

let mk_para_fa illu cnt =
  div ~a:[ a_class [ Display.d_flex ; Flex.align_items_center ;
                     Spacing.m5 ; Spacing.p5 ;
                     Border.border_top ; Border.border_light ;
                     "para" ] ] [
    div ~a:[ a_class [ Spacing.m5 ] ] [ illu ] ;
    h4 ~a:[ a_class [ Text.justify ] ] [ txt cnt ]
  ]

let make_img_txt_block img param_txt =
  div ~a:[ a_class [ Display.d_flex ; Spacing.m5 ; Spacing.py4 ;
                     Flex.align_items_center ; Border.border_bottom ;
                     Border.border_light ; Text.justify ] ] [
    div ~a:[ a_class [ Spacing.mx5 ] ] [ img ] ;
    span [ txt param_txt ]
  ]

let mk_pred_btn ?(aclass=[]) onclick =
  let onclick = match onclick with None -> fun () -> () | Some f -> f in
  button ~a:[ a_class
                ([ btn ; btn_outline_primary ; Spacing.m2 ] @ aclass);
              a_onclick (fun _ -> onclick () ; true) ] [ txt "Pred" ]

let mk_next_btn ?(aclass=[]) onclick =
  let onclick = match onclick with None -> fun () -> () | Some f -> f in
  button ~a:[ a_class
                ([ btn ; btn_outline_primary ; Spacing.m2 ] @ aclass);
              a_onclick (fun _ -> onclick () ; true) ] [ txt "Next" ]

let mk_create_btn ?(aclass=[]) () =
  a ~a:[ a_class
           ([ btn ; btn_outline_primary ; Spacing.m2 ;
              Display.d_flex; Flex.align_items_center ] @ aclass);
         a_href "/start.html" ;
       ] [ fas "check" ; span ~a:[ a_class [ Spacing.ml2 ] ] [
      txt "Setup up Metal" ] ]

let make_circles nb_page =
  let rec aux acc nb total =
    if total = 0 then acc
    else
    if nb = 1 then
      aux
        (span ~a:[ a_class [ "step-current" ] ] [ fas "circle" ] :: acc)
        (nb - 1)
        (total - 1)
    else
      aux
        (span ~a:[ a_class [ "step" ] ] [ fas "circle" ] :: acc)
        (nb - 1)
        (total - 1)
  in
  List.rev @@ aux [] nb_page total_page

let mk_navigation ?(pred=None) ?(next=None) ?(setup=false) nb =
  div ~a:[ a_class [ Display.d_flex ; Flex.justify_between ;
                     Flex.align_items_center ; Spacing.mb3 ] ]@@
  [ mk_pred_btn
      ~aclass:(match pred with None -> [ BMisc.invisible ] | Some _ -> [])
      pred
  ] @
  [ div @@ make_circles nb ] @
  [ if setup then
      mk_create_btn ()
    else
      mk_next_btn
        ~aclass:(match next with None -> [ BMisc.invisible ] | Some _ -> [])
        next
  ]

let mk_header str =
  div ~a:[ a_class [ Display.d_flex; Flex.align_items_center ;
                     Spacing.mxa; Spacing.mt5 ; "header" ] ] [
    mk_img "img/dune-gray.png" "Metal" ;
    h1 ~a:[ a_class [ Spacing.mx5 ] ] [ txt str ]
  ]

let rec step_3 () =
  let header = mk_header "Metal with dApp" in
  let intro_img = fas "network-wired" in
  let approve_img = mk_img "img/metal_dapp1.png" "Metal approve" in
  let tra_img = mk_img "img/metal_dapp2.png" "Metal transaction" in
  let content = div [
      mk_para_fa intro_img intro_txt ;
      mk_para_fa approve_img approve_txt ;
      mk_para_fa tra_img tra_txt
    ] in
  let navigation = mk_navigation ~pred:(Some step_2) ~setup:true 3 in
  let main_div = [
    header ;
    content ;
    navigation
  ] in
  replace_slide_id "content" main_div

and step_2 () =
  let header = mk_header "Metal as a wallet" in
  let bc_img = fas "wallet" in
  let content = div [
      mk_para_fa bc_img bc_txt ;
      div ~a:[ a_class [ Alert.alert ; Alert.adanger ;
                         Spacing.mx5; Spacing.mb5; Spacing.p5 ] ] [
        h3 ~a:[ a_class [ Text.center ; Spacing.mb5 ] ] [
          fas "exclamation-triangle" ;
          span ~a:[ a_class [ Spacing.ml3 ] ] [ txt "Safety tips" ]
        ] ;
        ol [
          li [
            span ~a:[ a_class [ Text.bold ] ] [
              txt "Make sure to keep your keys and password"
            ] ;
            p [ txt "Write it down and keep them safe. We can't restore \
                     your keys or reset your password." ]
          ] ;
          li [
            span ~a:[ a_class [ Text.bold ] ] [
              txt "You are in control of your funds" ;
            ] ;
            p [ txt "Metal encrypts and stores the encrypted data it needs \
                     to operate in your browser storage. And that's it, so \
                     if you lost your mnemonic or secret key the funds are \
                     lost for good."] ]  ;
          li [
            span ~a:[ a_class [ Text.bold ] ] [
              txt "Use a hardware wallet"
            ] ;
            p [ txt "It remains the most secure way to store you keys."] ] ;
        ]
      ]
    ] in
  let navigation = mk_navigation ~pred:(Some step_1) ~next:(Some step_3) 2 in
  let main_div = [
    header ;
    content ;
    navigation
  ] in
  replace_slide_id "content" main_div

and step_1 () =
  let header = mk_header "Welcome to Metal" in
  let wallet_img = fas "wallet" in
  let wallet_div = make_img_txt_block wallet_img wallet_txt in
  let dapp_img = fas "network-wired" in
  let dapp_div = make_img_txt_block dapp_img dapp_txt in
  let open_img = fas "laptop-code" in
  let open_div = make_img_txt_block open_img open_txt in
  let content = div ~a:[ a_class [ Spacing.p3; Border.rounded ] ] [
      h3 ~a:[ a_class [ Text.center ] ] [ txt "Metal is..." ] ;
      wallet_div ;
      dapp_div ;
      open_div ;
    ] in
  let navigation = mk_navigation ~next:(Some step_2) 1 in
  let main_div = [
    header ;
    content ;
    navigation
  ] in
  replace_slide_id "content" main_div

let start_pres () =
  step_1 ()

let _ =
  Onload.add start_pres
