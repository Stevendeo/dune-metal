(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Metal_types

let to_fo_state {ho_acc; ho_net; _} = {fo_acc = ho_acc; fo_net = ho_net}
let to_ho_state {fo_acc; fo_net} ho_accs = {ho_acc = fo_acc; ho_net = fo_net; ho_accs}
let update_ho_state state ho_acc =
  let ho_accs = List.map (fun a -> if a.pkh = ho_acc.pkh then ho_acc else a)
      state.ho_accs in
  {state with ho_acc; ho_accs}

let network_to_string = function
  | Mainnet -> "Mainnet"
  | Testnet -> "Testnet"
  | Devnet  -> "Devnet"
  | Custom s -> s

let network_of_string = function
  | "Mainnet" -> Mainnet
  | "Testnet" -> Testnet
  | "Devnet"  -> Devnet
  | s -> Custom s

let state_to_string = function
  | Disabled -> "disabled"
  | Locked -> "locked"
  | Unlocked  -> "unlocked"

let state_of_string = function
  | Disabled -> "disabled"
  | Locked -> "locked"
  | Unlocked  -> "unlocked"

let unoptf def f = function
  | None -> def
  | Some x -> f x

let unopt def = function
  | None -> def
  | Some x -> x

let convopt f = function
  | None -> None
  | Some x -> Some (f x)

let filter_origination ?(failed=false) ops =
  List.rev @@
  List.fold_left (fun acc op -> match op.op_type with
      | Sourced ( Manager (_, _, mops) ) ->
        List.fold_left (fun acc2 op2 -> match op2 with
            | Origination ori when (failed || not ori.or_failed) -> (op.op_hash, ori) :: acc2
            | _ -> acc2) acc mops
      | _ -> acc) [] ops

let filter_transaction ?(failed=false) ops =
  List.rev @@
  List.fold_left (fun acc op -> match op.op_type with
      | Sourced ( Manager (_, _, mops) ) ->
        List.fold_left (fun acc2 op2 -> match op2 with
            | Transaction tr when (failed || not tr.tr_failed) -> (op.op_hash, tr) :: acc2
            | _ -> acc2) acc mops
      | _ -> acc) [] ops

let filter_delegation ?(failed=false) ops =
  List.rev @@
  List.fold_left (fun acc op -> match op.op_type with
      | Sourced ( Manager (_, _, mops) ) ->
        List.fold_left (fun acc2 op2 -> match op2 with
            | Delegation dlg when (failed || not dlg.del_failed) -> (op.op_hash, dlg) :: acc2
            | _ -> acc2) acc mops
      | _ -> acc) [] ops

let filter_revelation ?(failed=false) ops =
  List.rev @@
  List.fold_left (fun acc op -> match op.op_type with
      | Sourced ( Manager (_, _, mops) ) ->
        List.fold_left (fun acc2 op2 -> match op2 with
            | Reveal rvl when (failed || not rvl.rvl_failed) -> (op.op_hash, rvl) :: acc2
            | _ -> acc2) acc mops
      | _ -> acc) [] ops

let filter_manage_account ?(failed=false) ops =
  List.rev @@
  List.fold_left (fun acc op -> match op.op_type with
      | Sourced ( Dune_manager (_, mops) ) ->
        List.fold_left (fun acc2 op2 -> match op2 with
            | Manage_account mac when (failed || not mac.mac_failed) -> (op.op_hash, mac) :: acc2
            | _ -> acc2) acc mops
      | _ -> acc) [] ops

let notif_kind_to_string = function
  | TraNot _ -> "transaction"
  | DlgNot _ -> "delegation"
  | OriNot _ -> "origination"
  | ManNot _ -> "manage_account"
  | ApprovNot _ -> "approval"

let notif_op_of_kind k not_op = match k with
  | "transaction" -> TraNot not_op
  | "delegation" -> DlgNot not_op
  | "origination" -> OriNot not_op
  | _ -> assert false

let notif_id_of_not = function
  | TraNot n | DlgNot n | OriNot n | ManNot n -> n.not_id
  | ApprovNot n -> n.not_approv_id

let notif_tsp_of_not = function
  | TraNot n | DlgNot n | OriNot n | ManNot n -> n.not_tsp
  | ApprovNot n -> n.not_approv_tsp

let parse_script = function
  | None -> None
  | Some s -> match Dune_script.json_of_string s with
    | Error _ -> None
    | Ok p -> Some p

let make_notif_tr ?amount ?fee ?gas_limit ?storage_limit ?entry ?params ?msg ~id ~wid ~tsp ~origin dst =
  let params = parse_script params in
  TraNot {
      not_origin = origin; not_msg = msg; not_dst = Some dst; not_tsp = tsp;
      not_id = id; not_wid = wid ; not_amount = amount;
      not_fee = fee; not_gas_limit = gas_limit; not_storage_limit = storage_limit;
      not_entry = entry;
      not_params = params; not_sc_code = None; not_sc_code_hash = None ;
      not_sc_storage = None ; not_delegate = None;
      not_maxrolls = None; not_admin = None; not_white_list = None;
      not_delegation = None
    }

let make_notif_del ?fee ?gas_limit ?storage_limit ?msg ~id ~wid ~tsp ~origin delegate =
    DlgNot {
      not_origin = origin; not_msg = msg; not_dst = None; not_tsp = tsp;
      not_id = id; not_wid = wid ; not_amount = None;
      not_fee = fee; not_gas_limit = gas_limit;
      not_storage_limit = storage_limit;
      not_entry = None;
      not_params = None ; not_sc_code = None; not_sc_code_hash = None ;
      not_sc_storage = None ; not_delegate = delegate;
      not_maxrolls = None; not_admin = None; not_white_list = None;
      not_delegation = None
    }

let make_notif_ori ?amount ?fee ?gas_limit ?storage_limit ?sc_code
    ?sc_code_hash ?sc_storage ?msg
    ~id ~wid ~tsp ~origin () =
  let sc_storage = parse_script sc_storage in
  let sc_code = parse_script sc_code in
  OriNot {
      not_origin = origin; not_msg = msg; not_dst = None; not_tsp = tsp;
      not_id = id; not_wid = wid ; not_amount = amount;
      not_fee = fee; not_gas_limit = gas_limit; not_storage_limit = storage_limit;
      not_entry = None;
      not_params = None; not_sc_code = sc_code; not_sc_code_hash = sc_code_hash ;
      not_sc_storage = sc_storage ; not_delegate = None;
      not_maxrolls = None; not_admin = None; not_white_list = None;
      not_delegation = None
    }

let make_notif_mac ?fee ?gas_limit ?storage_limit ?maxrolls ?admin ?white_list
    ?delegation ?msg ~id ~wid ~tsp ~origin () =
  ManNot {
      not_origin = origin; not_msg = msg; not_dst = None; not_tsp = tsp;
      not_id = id; not_wid = wid ; not_amount = None;
      not_fee = fee; not_gas_limit = gas_limit; not_storage_limit = storage_limit;
      not_entry = None;
      not_params = None; not_sc_code = None; not_sc_code_hash = None ;
      not_sc_storage = None ; not_delegate = None;
      not_maxrolls = maxrolls; not_admin = admin; not_white_list = white_list;
      not_delegation = delegation
    }

let make_notif_approv ~id ~wid ~tsp ~icon ~name ~url =
  ApprovNot {
    not_approv_id = id ;
    not_approv_wid = wid ;
    not_approv_icon = icon ;
    not_approv_name = name ;
    not_approv_url = url ;
    not_approv_tsp = tsp ;
  }

let shuffle l =
  List.map (fun v -> Random.int 1_000_000, v) l
  |> List.sort (fun (i, _) (i', _) -> compare i i')
  |> List.map snd
