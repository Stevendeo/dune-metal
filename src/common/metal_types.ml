(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

include Data_types_min

type network =
  | Mainnet | Testnet | Devnet
  | Custom of string

type metal_state = Disabled | Locked | Unlocked

type transaction_sh = {
  trs_hash : operation_hash;
  trs_src : account_hash;
  trs_dst : account_hash;
  trs_amount : int64;
  trs_counter : int64;
  trs_fee : int64;
  trs_gas_limit : Z.t ;
  trs_storage_limit : Z.t ;
  trs_entrypoint : string option ;
  trs_parameters : string option ;
  trs_internal : bool option ;
  trs_burn : int64 option ;
  trs_op_level : int option ;
  trs_tsp : timestamp;
  trs_status : string;
  trs_errors : op_error list option ;
}

type notif_op = {
  not_origin : string;
  not_msg : string option;
  not_dst : account_hash option;
  not_tsp : string;
  not_id : string;
  not_wid : int;
  not_amount : int64 option;
  not_fee : int64 option;
  not_gas_limit : Z.t option;
  not_storage_limit : Z.t option;
  not_entry : string option;
  not_params : string option;
  not_sc_code : string option;
  not_sc_code_hash : string option;
  not_sc_storage : string option;
  not_delegate : account_hash option;
  not_maxrolls : int option option;
  not_admin : account_hash option option;
  not_white_list : account_hash list option;
  not_delegation : bool option
}

type notif_app = {
  not_approv_id : string ;
  not_approv_wid : int ;
  not_approv_tsp : string ;
  not_approv_icon : string option ;
  not_approv_name : string ;
  not_approv_url : string ;
}

type notif_kind =
  | TraNot of notif_op | DlgNot of notif_op | OriNot of notif_op
  | ManNot of notif_op
  | ApprovNot of notif_app

type vault =
  | Ledger of (string * Bigstring.t)
  | Local of (Bigstring.t * Bigstring.t)

type account = {
  pkh : account_hash;
  manager : account_hash;
  vault : vault;
  name : string;
  revealed : (network * operation_hash) list;
  pending : transaction_sh list;
  manager_kt : bool option;
  admin : (network * account_hash) list;
}

type local_notif = {
  notif_acc  : account_hash ;
  notifs : notif_kind list ;
}

type faucet = {
  fc_mnemonic : string list;
  fc_code : string;
  fc_pkh: account_hash;
  fc_password : string;
  fc_email : string;
  fc_amount : int64 option
}

type service = {
  srv_kind : string;
  srv_dn1 : string option;
  srv_name : string;
  srv_url : string;
  srv_logo : string;
  srv_logo2 : string option;
  srv_logo_payout : string option;
  srv_descr : string option;
  srv_sponsored : string option;
  srv_page : string option;
  srv_delegations_page : bool ;
  srv_account_page : bool ;
  srv_aliases: account_name list option ;
  srv_display_delegation_page : bool ;
  srv_display_account_page : bool ;
}

type state = {
  fo_acc : account;
  fo_net : network;
}
type full_state = {
  ho_acc : account;
  ho_net : network;
  ho_accs : account list
}
type se_state = {
  se_acc : account option;
  se_accs : account list
}

type entry_ty =
  | EAUnit
  | EABytes
  | EAString
  | EANat
  | EAInt
  | EABool
  | EATimestamp
  | EAMutez
  | EAAddress
  | EAOperation
  | EAKey
  | EAKey_hash
  | EASignature
  | EAChain_id
  | EAContract
  | EAList of entry_arg
  | EAPair of entry_arg * entry_arg
  | EAOption of entry_arg
  | EAOr of entry_arg * entry_arg
  | EASet of entry_arg
  | EAMap of entry_arg * entry_arg
  | EABigmap of entry_arg

and entry_arg = {
  ea_name : string option ;
  ea_type : string option ;
  ea_arg : entry_ty ;
}

type entry_point = {
  ep_name : string option;
  ep_type : string option ;
  ep_args : entry_arg ;
  ep_mic : string list ;
}
