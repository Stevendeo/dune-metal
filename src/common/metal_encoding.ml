(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Json_encoding
open Metal_types

let faucet =
  conv
    (fun {fc_mnemonic; fc_code; fc_pkh; fc_password; fc_email; fc_amount}
      -> (fc_mnemonic, fc_code, fc_pkh, fc_password, fc_email, fc_amount))
    (fun (fc_mnemonic, fc_code, fc_pkh, fc_password, fc_email, fc_amount)
      -> {fc_mnemonic; fc_code; fc_pkh; fc_password; fc_email; fc_amount})
    (obj6
       (req "mnemonic" (list string))
       (req "activation_code" string)
       (req "pkh" string)
       (req "password" string)
       (req "email" string)
       (opt "amount" EzEncoding.int64))

let dunscan_services =
  let multiline =
    union [
      case
        (list string)
        (fun s ->
           match String.split_on_char '\n' s with
             [] | [_] -> None
           | list -> Some list)
        (fun list -> String.concat "\n" list);
      case string (fun s -> Some s) (fun s -> s) ] in
  let service =
    conv
      (fun { srv_kind; srv_dn1; srv_name; srv_url; srv_logo; srv_logo2;
             srv_logo_payout; srv_descr; srv_sponsored; srv_page;
             srv_delegations_page ; srv_account_page ; srv_aliases ;
             srv_display_delegation_page ; srv_display_account_page }
        -> ( srv_kind, srv_dn1, srv_name, srv_url, srv_logo, srv_logo2,
             srv_logo_payout, srv_descr, srv_sponsored, srv_page,
             srv_delegations_page, srv_account_page, srv_aliases,
             srv_display_delegation_page, srv_display_account_page ) )
      (fun ( srv_kind, srv_dn1, srv_name, srv_url, srv_logo, srv_logo2,
             srv_logo_payout, srv_descr, srv_sponsored, srv_page,
             srv_delegations_page, srv_account_page, srv_aliases,
             srv_display_delegation_page, srv_display_account_page)
        -> { srv_kind; srv_dn1; srv_name; srv_url; srv_logo; srv_logo2;
             srv_logo_payout; srv_descr; srv_sponsored; srv_page;
             srv_delegations_page; srv_account_page; srv_aliases ;
             srv_display_delegation_page ; srv_display_account_page} )
      (EzEncoding.obj15
         (dft "kind" string "delegate")
         (opt "address" string)
         (req "name" string)
         (dft "url" string "")
         (dft "logo" string "")
         (opt "logo2" string)
         (opt "logo_payout" string)
         (opt "descr" multiline)
         (opt "sponsored" string)
         (opt "page" string)
         (dft "delegation-services-page" bool true)
         (dft "account-page" bool true)
         (opt "aliases" (list Api_encoding_min.Base.account_name_encoding))
         (dft "display-delegation-page" bool true)
         (dft "display-account-page" bool true)) in
  list service
