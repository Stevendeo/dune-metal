(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Metal_types
module Metal_node = Node
open Async

module Node = struct
  let mainnet_node_url = "mainnet-node.dunscan.io"
  let mainnet_node_base_url = Xhr_lwt.base mainnet_node_url
  let testnet_node_url = "testnet-node.dunscan.io"
  let testnet_node_base_url = Xhr_lwt.base testnet_node_url
  let devnet_node_url = "devnet-node.dunscan.io"
  let devnet_node_base_url = Xhr_lwt.base devnet_node_url

  let get_node_base_url network f = match network with
    | Mainnet -> f mainnet_node_base_url
    | Testnet -> f testnet_node_base_url
    | Devnet  -> f devnet_node_base_url
    | Custom name -> Storage_reader.get_custom_networks (fun l ->
        match List.find_opt (fun (n, _node, _api) -> name = n) l with
        | None -> f testnet_node_base_url
        | Some (_, node, _) -> f node)

  let get_node_base_url_lwt network =
    let (promise, resolver) = Lwt.task () in
    get_node_base_url network (Lwt.wakeup resolver);
    promise

  let get_account_info ?(network=Mainnet) ?error pkh f =
    get_node_base_url network (fun base ->
        async_req ?error (Metal_node.get_account_info ~base pkh) f)

  let get_entrypoints ?(network=Mainnet) ?error pkh f =
    get_node_base_url network (fun base ->
        async_req ?error (Metal_node.get_entrypoints ~base pkh) f)

  let forge_transaction ?(network=Mainnet) ?counter ?gas_limit ?storage_limit
      ?fee ?entrypoint ?parameters account dst amount =
    let get_pk () = Vault.Lwt.pk_of_vault account in
    get_node_base_url_lwt network >>= fun base ->
    Metal_node.forge_transaction ~base ?counter ?gas_limit
      ?storage_limit ?fee ?entrypoint ?parameters ~get_pk
      ~src:account.pkh ~dst amount

  let forge_delegation ?(network=Mainnet) ?counter ?gas_limit ?storage_limit
      ?fee account dlg =
    let get_pk () = Vault.Lwt.pk_of_vault account in
    get_node_base_url_lwt network >>= fun base ->
    Metal_node.forge_delegation ~base ?counter ?gas_limit
      ?storage_limit ?fee ~get_pk ~src:account.pkh dlg

  let forge_origination ?(network=Mainnet) ?counter ?gas_limit ?storage_limit
      ?fee ?script account balance =
    let get_pk () = Vault.Lwt.pk_of_vault account in
    get_node_base_url_lwt network >>= fun base ->
    Metal_node.forge_origination ~base ?counter ?gas_limit ?storage_limit
      ?fee ?script ~get_pk ~src:account.pkh balance

  let send_operation ?(network=Mainnet) ?error account (op, ops) f =
    get_node_base_url network (fun base ->
        async_req ?error
          (Metal_node.sign_operation ~sign:(Vault.Lwt.sign account) op >>=? fun op ->
           Metal_node.inject ~base op >>|? fun h ->
           h, ops
          ) f)

  let send_activation ?(network=Mainnet) ?error pkh secret f =
    get_node_base_url network (fun base ->
        async_req ?error
          (Metal_node.forge_activation ~base ~pkh secret >>=? fun (op, ops) ->
           Metal_node.sign_zero_operation op >>=? fun op ->
           Metal_node.inject ~base op >>|? fun h ->
           h, ops
          ) f)

  let get_admin network accounts account = match List.assoc_opt network account.admin with
    | None -> Ok None
    | Some admin -> match List.find_opt (fun a -> a.pkh = admin) accounts with
      | None -> Error (Str_err (
          Printf.sprintf "You are not admin (%s) for account %s"
            admin account.pkh))
      | Some a -> Ok (Some a)

  let get_accounts_lwt () =
    let (promise, resolver) = Lwt.task () in
    Storage_reader.get_accounts (Lwt.wakeup resolver);
    promise

  let forge_manage_account ?(network=Mainnet) ?counter ?gas_limit ?storage_limit
      ?fee ?accounts account options =
    (match accounts with
     | None -> get_accounts_lwt ()
     | Some accounts -> return accounts) >>= fun accounts ->
    (match get_admin network accounts account with
     | Ok (Some admin) ->
       return @@ Ok (admin.pkh, Some (account.pkh, Vault.Lwt.sign ~watermark:"" account),
                     fun () -> Vault.Lwt.pk_of_vault admin)
     | Ok _ -> return @@ Ok (account.pkh, None, fun () -> Vault.Lwt.pk_of_vault account)
     | Error e -> return (Error e)) >>=? fun (src, target, get_pk) ->
    get_node_base_url_lwt network >>= fun base ->
    Metal_node.forge_manage_account ~base ?counter ?gas_limit ?storage_limit
      ?fee ~get_pk ~src ?target options

  let admin_sign network accounts account bytes =
    match get_admin network accounts account with
    | Ok (Some admin) -> Vault.Lwt.sign admin bytes
    | Ok _ -> Vault.Lwt.sign account bytes
    | Error e -> return (Error e)

  let send_manage_account ?(network=Mainnet) ?error ?accounts account (op, ops) f =
    let aux accounts =
      get_node_base_url network (fun base ->
          async_req ?error
            (Metal_node.sign_operation ~sign:(admin_sign network accounts account) op >>=? fun op ->
             Metal_node.inject ~base op >>|? fun h ->
             h, ops
            ) f) in
    match accounts with
    | Some accounts -> aux accounts
    | None -> Storage_reader.get_accounts aux

end

module Scan = struct
  open Data_types_min

  let mainnet_dunscan_base_url = Xhr_lwt.base "api.dunscan.io"
  let testnet_dunscan_base_url = Xhr_lwt.base "api.testnet.dunscan.io"
  let devnet_dunscan_base_url = Xhr_lwt.base "api.devnet.dunscan.io"

  let get_dunscan_base_url network f = match network with
    | Mainnet -> f mainnet_dunscan_base_url
    | Testnet -> f testnet_dunscan_base_url
    | Devnet  -> f devnet_dunscan_base_url
    | Custom name -> Storage_reader.get_custom_networks (fun l ->
        match List.find_opt (fun (n, _node, _api) -> name = n) l with
        | None -> f testnet_dunscan_base_url
        | Some (_, _, api) -> f api)

  let page_args ~page ~page_size args =
    ["p", string_of_int page; "number", string_of_int page_size] @ args

  let page_wrap ?network ?error ?title ?suf_id ?page_sizer ?state nb_xhr xhr update pkh =
    nb_xhr ?network ?error pkh (fun n ->
        update ?title ?suf_id ?page_sizer ?state n (fun page page_size f ->
            xhr ?network ?error ?page:(Some page) ?page_size:(Some page_size) pkh f))

  let get ?error ?args ~network enc url f =
    get_dunscan_base_url network (fun base ->
        async_req ?error
          (Metal_node.Xhr.get ~base ?args (Xhr_lwt.Enc enc) url) f)

  let nb_transactions ?(network=Mainnet) ?error dn1 =
    get ?error ~args:["type","Transaction"] ~network EzEncoding.tup1_int
      ("v3/number_operations/" ^ dn1)

  let get_transactions ?(network=Mainnet) ?error ?(page=0) ?(page_size=20) dn1 f =
    get ?error ~args:(page_args ~page ~page_size ["type","Transaction"])
      ~network (Api_encoding_min.V1.Op.operations false)
      ("v3/operations_nomic/" ^ dn1)
      (fun l -> f @@
        List.fold_left (fun acc op -> match op.op_type with
            | Sourced ( Manager (_, _, mops) ) ->
              List.fold_left (fun acc2 op2 -> match op2 with
                  | Transaction tx -> acc2 @ [{
                      trs_hash = op.op_hash;
                      trs_tsp = tx.tr_timestamp;
                      trs_src = tx.tr_src.dn;
                      trs_dst = tx.tr_dst.dn;
                      trs_amount = tx.tr_amount;
                      trs_fee = tx.tr_fee;
                      trs_status = if tx.tr_failed then "failed" else "confirmed";
                      trs_counter = Int64.of_int32 tx.tr_counter;
                      trs_gas_limit = tx.tr_gas_limit;
                      trs_storage_limit = tx.tr_storage_limit;
                      trs_entrypoint = (match tx.tr_parameters with
                          | None -> None
                          | Some p -> p.p_entrypoint);
                      trs_parameters = (match tx.tr_parameters with
                          | None | Some { p_value = None; _ } -> None
                          | Some { p_value = Some v; _ } -> Some (Format.asprintf "%a" (Json_repr.pp_any ()) v));
                      trs_internal = Some tx.tr_internal;
                      trs_burn = Some tx.tr_burn;
                      trs_op_level = Some tx.tr_op_level;
                      trs_errors = tx.tr_errors}]
                  | _ -> acc2) acc mops
            | _ -> acc) [] l)

  let nb_originations ?(network=Mainnet) ?error dn1 =
    get ?error ~args:["type", "Origination"] ~network EzEncoding.tup1_int
      ("v3/number_operations/" ^ dn1)

  let get_originations ?(network=Mainnet) ?error ?(page=0) ?(page_size=20) dn1 f =
    get ?error ~args:(page_args ~page ~page_size ["type", "Origination"])
      ~network (Api_encoding_min.V1.Op.operations false)
      ("v3/operations_nomic/" ^ dn1)
      (fun l -> f (Metal_helpers.filter_origination l))

  let nb_delegations ?(network=Mainnet) ?error dn1 =
    get ?error ~args:["type", "Delegation"] ~network EzEncoding.tup1_int
      ("v3/number_operations/" ^ dn1)

  let get_delegations ?(network=Mainnet) ?error ?(page=0) ?(page_size=20) dn1 f =
    get ?error ~args:(page_args ~page ~page_size ["type", "Delegation"])
      ~network (Api_encoding_min.V1.Op.operations false)
      ("v3/operations_nomic/" ^ dn1)
      (fun l -> f (Metal_helpers.filter_delegation l))

  let nb_manage_account ?(network=Mainnet) ?error dn1 =
    get ?error ~args:["type", "Manage_account"] ~network EzEncoding.tup1_int
      ("v3/number_operations/" ^ dn1)

  let get_manage_account ?(network=Mainnet) ?error ?(page=0) ?(page_size=20) dn1 f =
    get ?error ~args:(page_args ~page ~page_size ["type", "Manage_account"])
      ~network (Api_encoding_min.V1.Op.operations false)
      ("v3/operations_nomic/" ^ dn1)
      (fun l -> f (Metal_helpers.filter_manage_account l))

  let get_revealed ?(network=Mainnet) ?error dn1 f =
    get ?error ~args:["type", "Reveal"]
      ~network (Api_encoding_min.V1.Op.operations false)
      ("v3/operations_nomic/" ^ dn1)
      (function
        | {op_type = Sourced (Manager (_, _, (Reveal {rvl_failed; _} :: _))); op_hash; _} :: _
          when rvl_failed = false -> f (Some op_hash)
        | _ -> f None)

  let get_activated ?(network=Mainnet) ?error dn1 f =
    get ?error ~args:["type", "Activation"]
      ~network (Api_encoding_min.V1.Op.operations false)
      ("v3/operations_nomic/" ^ dn1)
      (function
        | {op_type = Anonymous (Activation _ :: _); _} :: _ -> f true
        | _ -> f false)

  let get_node_account ?(network=Mainnet) ?error kt1 f =
    get ?error ~network Api_encoding_min.V1.Account_details.encoding
      ("v3/node_account/" ^ kt1)
      f

  let get_services ?(network=Mainnet) ?error f =
    get ?error ~network
      Metal_encoding.dunscan_services
      "v3/services/"
      f
end
