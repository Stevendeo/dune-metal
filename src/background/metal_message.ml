(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_min.Js
open Metal_message_types

let mk_request ?data ~name id =
  let req : request t = Unsafe.obj [||] in
  req##.id := id;
  req##.name := string name;
  req##.data := Optdef.option data;
  req

let mk_message ?data ~name ~src id =
  let req = mk_request ?data ~name @@ string id in
  let m : message t = Unsafe.obj [||] in
  m##.src := string src ;
  m##.req := req ;
  m

let mk_metadata_req src rid =
  mk_message ~name:"get_metadata" ~src rid

let mk_tr_req src rid =
  mk_message ~name:"get_trdata" ~src rid

let mk_ori_req src rid =
  mk_message ~name:"get_oridata" ~src rid

let mk_dlg_req src rid =
  mk_message ~name:"get_dlgdata" ~src rid

let mk_state_changed_notif () =
  mk_message ~name:"state_changed" ~src:"popup" "-1"

let mk_answer ~src id res =
  let res =
    let obj : response t = Unsafe.obj [||] in
    obj##.id := id ;
    obj##.result := res ;
    obj
  in
  let obj : answer t = Unsafe.obj [||] in
  obj##.src := string src ;
  obj##.res := res ;
  obj

let mk_err_answer ~src id errs =
  let err_js = def (array (Array.of_list (List.map string errs))) in
  let res =
    let obj : response t = Unsafe.obj [||] in
    obj##.id := id ;
    obj##.result := string "Error" ;
    obj##.data := err_js ;
    obj
  in
  let obj : answer t = Unsafe.obj [||] in
  obj##.src := string src ;
  obj##.res := res ;
  obj
