(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_types
open Js_min
open Metal_message_types

let ledger_iframe_id = "ledger-iframe"

let ledger_ports : (string, Runtime_utils.port t) Hashtbl.t =
  Hashtbl.create 10

let make_iframe () =
  let iframe = El.iframe
      ~attr:["src", "https://dune.network/ledger-bridge/index.html?version=0"; "id", ledger_iframe_id] [] in
  appendChild Dom_html.document##.head iframe;
  Js_of_ocaml.Dom_events.listen
    Dom_html.window
    (Dom_html.Event.make "message")
    (fun _target ev ->
       let action = to_string ev##.data##.action in
       let target = to_string ev##.data##.target in
       if (action = "sign-reply" || action = "getAddress-reply") &&
          target = "DUNE-METAL-BG" then
         begin match Hashtbl.find_opt ledger_ports action with
           | None -> log_str ("No ports action: " ^ action)
           | Some port -> port##postMessage ev##.data##.params end;
       false) |> ignore

let send_message port (msg : ledgerMessage t) =
  match Js.Opt.to_option @@ Dom_html.CoerceTo.iframe (by_id ledger_iframe_id) with
  | None -> ()
  | Some ledger_iframe ->
    let action = to_string msg##.action in
    msg##.target := Js.some (Js.string "DUNE-METAL-BG-LEDGER-IFRAME");
    Hashtbl.add ledger_ports (action ^ "-reply") port;
    (Js.Unsafe.coerce ledger_iframe##.contentWindow)##postMessage msg (Js.string "*")
