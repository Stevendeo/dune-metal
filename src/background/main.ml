(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_min
open Chrome

let print_conn_recv (port : Runtime_utils.port Js.t) =
  Js.Optdef.case
    (port##.sender)
    (fun () -> ())
    (fun sender ->
       Js.Optdef.case
         (sender##.url)
         (fun _ -> log_str "[Background] connection recv")
         (fun url ->
            log_str @@ "[Background] " ^ (Js.to_string url) ^ " connected"))

let () =
  Ledger_bridge.make_iframe ();
  Browser_action.set_badge_bg ~color:"#c62828" ();
  Runtime.onConnect
    (fun port ->
       let port_name = Js.to_string port##.name in
       if port_name = "ledger" then (
         Browser_utils.addListener1
           (port##.onMessage)
           (fun msg ->
              if msg##.action = Js.string "getAddress" || msg##.action = Js.string "sign" then
                Ledger_bridge.send_message port msg))
       else (
         print_conn_recv port ;
         Handler.add_port port ;
         Browser_utils.addListener1
           (port##.onMessage)
           (fun msg -> Handler.handle port msg) ;
         Browser_utils.addListener1
           (port##.onDisconnect)
           (fun p ->
              log_str @@ "Disconnect " ^ (Js.to_string p##.name) ;
              Handler.remove_port port))) ;

  Runtime.onInstalled (fun (event : Runtime.onInstalledEvent Js.t) ->
      if event##.reason = Js.string "install" then
        let tab = Tabs.make_create ~url:"presentation.html" () in
        Tabs.create tab);

  Storage_types.update_storage ();
  (* WARNING: would be useful to uncomment for the proper release *)
  (* Runtime.onUpdateAvailabale (fun _ -> Storage_types.update_storage ()) *)
