(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_of_ocaml
open Js

(* Message types *)
class type request = object
  method id : js_string t prop
  method name : js_string t prop
  method data : 'a t optdef prop
  method metadata : 'a t optdef prop
end

class type response = object
  method id : js_string t prop
  method result : 'a t prop
  method data : 'a t optdef prop
end

class type message = object
  method src : js_string t prop
  method req : request t prop
end

class type notif_response = object
  method id : js_string t prop
  method name : js_string t prop
  method res : bool t prop
  method data : 'a t optdef prop
end

class type notif_answer = object
  method src : js_string t prop
  method notifRes : notif_response t prop
end

class type answer = object
  method src : js_string t prop
  method res : response t prop
end

(* Extra data types *)
class type siteMetadata = object
  method name : js_string t prop
  method icon : js_string t optdef prop
  method url : js_string t prop
end

class type operation = object
  method _type : js_string t prop
end

class type transactionData = object
  method dst : js_string t prop
  method amount : js_string t prop
  method fee : js_string t optdef prop
  method entrypoint : js_string t optdef prop
  method parameter : js_string t optdef prop
  method gasLimit : js_string t optdef prop
  method storageLimit : js_string t optdef prop
  method msg : js_string t optdef prop
end

class type originationData = object
  method balance : js_string t prop
  method fee : js_string t optdef prop
  method scCode : js_string t optdef prop
  method scCodeHash : js_string t optdef prop
  method scStorage : js_string t optdef prop
  method gasLimit : js_string t optdef prop
  method storageLimit : js_string t optdef prop
  method msg : js_string t optdef prop
end

class type delegationData = object
  method delegate : js_string t optdef prop
  method fee : js_string t optdef prop
  method gasLimit : js_string t optdef prop
  method storageLimit : js_string t optdef prop
  method msg : js_string t optdef prop
end

(* ledger message *)

class type ledgerParams = object
  method path : js_string t prop
  method data : js_string t opt prop
  method success : bool t opt prop
end

class type ledgerMessage = object
  method action : js_string t prop
  method target : js_string t opt prop
  method params : ledgerParams t prop
end
