(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_types
open Metal_message_types
open Metal_types
open Chrome

let pending_approved : (string * siteMetadata t) list ref = ref []
let pending_send : (string * transactionData t) list ref = ref []
let pending_ori : (string * originationData t) list ref = ref []
let pending_dlg : (string * delegationData t) list ref = ref []

let port_table : (string, Runtime_utils.port t) Hashtbl.t = Hashtbl.create 100

let send (port : Runtime_utils.port t) (req : request t) res =
  let obj = Metal_message.mk_answer ~src:"background" req##.id res in
  port##postMessage (obj)

let send_error (port : Runtime_utils.port t) (req : request t) errs =
  let obj = Metal_message.mk_err_answer ~src:"background" req##.id errs in
  port##postMessage (obj)

let state_changed_notif_ports = ref []

let add_port (port : Runtime_utils.port t) =
  def_case port##.sender
    (fun () -> ())
    (fun sender -> def_case sender##.url
        (fun () -> ())
        (fun url ->
           state_changed_notif_ports :=
             (to_string url, port) :: !state_changed_notif_ports))

let remove_port (port : Runtime_utils.port t) =
  def_case port##.sender
    (fun () -> ())
    (fun sender -> def_case sender##.url
        (fun () -> ())
        (fun url ->
           state_changed_notif_ports :=
             List.remove_assoc (to_string url) !state_changed_notif_ports))

let state_changed () =
  Js_log.log_str "state_changed" ;
  Js_log.log !state_changed_notif_ports ;
  let notif = Metal_message.mk_state_changed_notif () in
  List.iter (fun (_url, (port : Runtime_utils.port t)) ->
      port##postMessage(notif))
    !state_changed_notif_ports

(* TODO : background_utils *)
let update_badge () =
  Storage_reader.get_account (fun (acc, _) ->
      Storage_writer.get_notifs (fun ns ->
          match acc with
          | None -> ()
          | Some account ->
            let n_notif =
              List.length @@
              try
                (List.find (fun {notif_acc ; _} ->
                     notif_acc = account.pkh) ns).notifs
              with Not_found -> [] in
            Chrome.Browser_action.set_badge
              ~text:(if n_notif = 0 then "" else string_of_int n_notif) ()))

let callback_handler ?callback ?refresh data = function
  | ApprovNot n as notif ->
    if Js.to_bool data##.ok then
      Storage_writer.add_approved (string n.not_approv_url) (fun () ->
          Storage_writer.remove_notif
            ~callback:(fun fo_acc ->
                update_badge () ;
                begin match refresh with
                  | Some rf -> rf fo_acc
                  | None -> ()
                end ;
                match callback with
                | None -> ()
                | Some cb -> cb notif
              )
            notif)
    else
      Storage_writer.remove_notif
        ~callback:(fun fo_acc ->
            update_badge () ;
            begin match refresh with
              | Some rf -> rf fo_acc
              | None -> ()
            end ;
            match callback with
            | None -> ()
            | Some cb -> cb notif
          )
        notif
  | _ as notif ->
    Storage_writer.remove_notif
      ~callback:(fun fo_acc ->
          update_badge () ;
          begin match refresh with
            | Some rf -> rf fo_acc
            | None -> ()
          end ;
          match callback with
          | None -> ()
          | Some cb -> cb notif
        )
      notif

let is_approved (port : Runtime_utils.port t) f = match to_def_option port##.sender with
  | Some sender ->
    begin match to_def_option sender##.url with
      | Some url ->
        begin match Js_of_ocaml.Url.url_of_string (Js.to_string url) with
          | None -> Js_log.log_str "Can't find sender's url"
          | Some url ->
            let hostname = match url with
              | Js_of_ocaml.Url.(Http { hu_host; _ })
              | Js_of_ocaml.Url.(Https { hu_host; _ }) -> hu_host
              | Js_of_ocaml.Url.File _ -> "" in
            Storage_reader.is_approved (string hostname) f
        end
      | None -> Js_log.log_str "Can't find sender's url"
    end
  | None -> Js_log.log_str "Can't find sender"

(* let wrap_approved port req f = match Optdef.to_option port##sender with
 *   | Some sender ->
 *     begin match Optdef.to_option sender##url with
 *       | Some url ->
 *         Metal_storage.is_approved
 *           url
 *           (fun b ->
 *              if b then send port req (f ())
 *              else
 *                Notification.show_popup_approve
 *                  (fun () ->
 *                     Metal_storage.is_approved
 *                       url
 *                       (fun b ->
 *                          if b then send port req (f ())
 *                          else log_str "It seems the user refused to approve this domain")))
 *       | None -> log_str "Can't find sender's url"
 *     end
 *   | None -> log_str "Can't find sender" *)

let wrap_approved port (req : request t) f =
  let id = Js.to_string req##.id in
  is_approved port
    (fun b ->
       if b then f ()
       else begin
         let should_popup =
           def_case req##.metadata
             (fun () -> false)
             (fun data ->
                if List.exists (fun (_, d) -> data = d) !pending_approved then
                  false
                else
                  begin
                    pending_approved := (id, data) :: !pending_approved ;
                    true
                  end) in
         if should_popup then
           (Js_utils.js_log req ;
            Notification.show_popup_approve
              port
              (fun () ->
                 pending_approved := List.remove_assoc id !pending_approved ;
                 is_approved
                   port
                   (fun b ->
                      if b then f ()
                      else Js_log.log_str "It seems the user refused to approve this domain"))
              id)
       end)

let wrap_unlocked port req f =
  Storage_reader.is_unlocked (fun b ->
      if b then wrap_approved port req f
      else
        Notification.show_popup_unlock
          port
          (fun () ->
             Storage_reader.is_unlocked (fun b ->
                 if b then wrap_approved port req f
                 else failwith "It seems the user refused to unlock metal")))

let wrap_enabled port req f =
  Storage_reader.is_enabled (fun b ->
      if b then wrap_unlocked port req f
      else
        Notification.show_popup_enable
          port
          (fun () ->
             Storage_reader.is_enabled (fun b ->
                 if b then wrap_unlocked port req f
                 else failwith "It seems the user refused to enable metal")))

let wrap_handler port req f = wrap_enabled port req f

let wrap_only_enabled port req f =
  Storage_reader.is_enabled (fun b ->
      if b then wrap_unlocked port req f
      else
        Notification.show_popup_enable
          port
          (fun () ->
             Storage_reader.is_enabled (fun b ->
                 if b then wrap_unlocked port req f
                 else failwith "It seems the user refused to enable metal")))

let remove_window wid =
  Windows.getAll (fun wins ->
      List.iter (fun win ->
          def_case
            win##.id
            (fun () -> ())
            (fun id ->
               if id = wid then
                 Windows.remove wid))
        wins
    )

let check_pkh str = Crypto.check_pkh str

let check_tr (data : transactionData t) =
  let b_dst =
    try check_pkh @@ to_string data##.dst with _ -> false in
  let b_amount =
    try
      ignore @@ Int64.of_string @@ to_string data##.amount ;
      true
    with _ -> false in
  let b_fee =
    try
      match to_optdef to_string data##.fee with
      | None -> true
      | Some f -> try ignore @@ Int64.of_string f ; true with _ -> false
    with _ -> false in
  (* let b_parameter =
   *   try
   *     match to_optdef to_string data##.parameter with
   *     | None -> true
   *     | Some p ->
   *       try
   *         ignore @@ Api_encoding_min.Base.Micheline.decode p ; true
   *       with _ -> false
   *   with _ -> false in *)
  let b_gas_limit =
    try
      match to_optdef to_string data##.gasLimit with
      | None -> true
      | Some gl -> try ignore @@ Z.of_string gl ; true with _ -> false
    with _ -> false in
  let b_storage_limit =
    try
      match to_optdef to_string data##.storageLimit with
      | None -> true
      | Some sl -> try ignore @@ Z.of_string sl ; true with _ -> false
    with _ -> false in
  b_dst && b_amount && b_fee (* && b_parameter *) && b_gas_limit && b_storage_limit,
  List.fold_left (fun acc (b, str) -> if not b then acc @ [ str ] else acc)
    []
    [ b_dst, "dst is not a public key hash" ;
      b_amount, "amount is not a number" ;
      b_fee, "fee is not a number" ;
      (* b_parameter, "wrong parameter format (should be michelson)" ; *)
      b_gas_limit, "gas_limit is not a number" ;
      b_storage_limit,  "storage_limit is not a number" ]

let check_ori_unit (data : originationData t) =
  let b_balance =
    try ignore @@ Int64.of_string @@ to_string data##.balance ; true with _ -> false in
  let b_fee =
    match to_optdef to_string data##.fee with
    | None -> true
    | Some f -> try ignore @@ Int64.of_string f ; true with _ -> false in
  let b_sc_code =
    try
      match to_optdef to_string data##.scCode with
      | None -> true
      | Some sc ->
        try
          ignore @@ Api_encoding_min.Base.Micheline.decode sc ; true
        with _ -> false
    with _ -> false in
  let b_sc_code_hash =
    try
      match to_optdef to_string data##.scCodeHash with
      | None -> true
      | Some sc ->
        try
          ignore @@ Api_encoding_min.Base.Micheline.decode sc ; true
        with _ -> false
    with _ -> false in
  let b_sc_storage =
    try
      match to_optdef to_string data##.scStorage with
      | None -> true
      | Some sc ->
        try
          ignore @@ Api_encoding_min.Base.Micheline.decode sc ; true
        with _ -> false
    with _ -> false in
  let b_gas_limit =
    try
      match to_optdef to_string data##.gasLimit with
      | None -> true
      | Some gl -> try ignore @@ Z.of_string gl ; true with _ -> false
    with _ -> false in
  let b_storage_limit =
    try
      match to_optdef to_string data##.storageLimit with
      | None -> true
      | Some sl -> try ignore @@ Z.of_string sl ; true with _ -> false
    with _ -> false in
  b_balance && b_fee && b_sc_code && b_sc_code_hash &&
  b_sc_storage && b_gas_limit && b_storage_limit,
  List.fold_left (fun acc (b, str) -> if not b then acc @ [ str ] else acc)
    []
    [ b_balance, "balance is not a number";
      b_fee, "fee is not a number" ;
      b_sc_code, "wrong sc_code format (should be michelson)";
      b_sc_code_hash, "wrong sc_code_hash format (should be michelson)";
      b_sc_storage, "wrong sc_storage format (should be michelson)";
      b_gas_limit, "gas_limit is not a number" ;
      b_storage_limit,  "storage_limit is not a number" ]

let check_ori_merge (data : originationData t) =
  let has_sc_code =
    match to_optdef to_string data##.scCode with
    | None -> false
    | Some _c -> true in
  let has_sc_code_hash =
    match to_optdef to_string data##.scCodeHash with
    | None -> false
    | Some _c -> true in
  let has_sc_storage =
    match to_optdef to_string data##.scStorage with
    | None -> false
    | Some _s -> true in
  not ((has_sc_code && has_sc_code_hash) ||
       (has_sc_storage && (not has_sc_code && not has_sc_code_hash)) ||
       (not has_sc_storage && (has_sc_code || has_sc_code_hash))),
  List.fold_left (fun acc (b, str) -> if b then acc @ [ str ] else acc)
    []
    [ (has_sc_code && has_sc_code_hash),
      "code and code_hash can't be specified together" ;
      (has_sc_storage && (not has_sc_code && not has_sc_code_hash)),
      "storage specified but no code or code_hash provided" ;
      (not has_sc_storage && (has_sc_code || has_sc_code_hash)),
      "code or code_hash specified but not storage provided" ;
    ]

let check_ori (data : originationData t) =
  let b, errs = check_ori_unit data in
  if b then check_ori_merge data
  else b, errs

let check_dlg_unit (data : delegationData t) =
  let b_delegate =
    try
      match to_optdef to_string data##.delegate with
      | None -> true
      | Some d when d = "" -> true
      | Some d when d <> "" -> check_pkh d
      | _ -> false
    with _ -> false in
  let b_fee =
    match to_optdef to_string data##.fee with
    | None -> true
    | Some f -> try ignore @@ Int64.of_string f ; true with _ -> false in
  let b_gas_limit =
    try
      match to_optdef to_string data##.gasLimit with
      | None -> true
      | Some gl -> try ignore @@ Z.of_string gl ; true with _ -> false
    with _ -> false in
  let b_storage_limit =
    try
      match to_optdef to_string data##.storageLimit with
      | None -> true
      | Some sl -> try ignore @@ Z.of_string sl ; true with _ -> false
    with _ -> false in
  b_delegate && b_fee && b_gas_limit && b_storage_limit,
  List.fold_left (fun acc (b, str) -> if not b then acc @ [ str ] else acc)
    []
    [ b_delegate, "delegate is not a public key hash";
      b_fee, "fee is not a number" ;
      b_gas_limit, "gas_limit is not a number" ;
      b_storage_limit,  "storage_limit is not a number" ]

let check_dlg (data : delegationData t) =
  check_dlg_unit data

let dispatch (port : Runtime_utils.port t) (msg : message t) =
  let src = to_string msg##.src in
  let req = msg##.req in
  let meth = to_string req##.name in
  let id = to_string req##.id in
  if src <> "contentscript" then
    match meth with
    | "is_enabled" ->
      Storage_reader.is_enabled (fun b -> send port req (bool b)) ;
    | "is_unlocked" ->
      Storage_reader.is_enabled (fun b ->
          if b then
            Storage_reader.is_unlocked (fun b -> send port req (bool b))
          else
            send port req (bool b))
    | "is_approved" ->
      Storage_reader.is_enabled (fun b ->
          if b then
            Storage_reader.is_unlocked (fun b ->
                if b then
                  is_approved port (fun b -> send port req (bool b))
                else send port req (bool b))
          else
            send port req (bool b))

    | "get_account" ->
      let f () =
        Storage_reader.get_account (fun (acc, _) -> match acc with
            | None -> ()
            (* Error gestion ?, this should not happen because
               the extension can't be enabled without an account *)
            | Some acc -> send port req @@ (Storage_utils.of_account acc)##.pkh) in
      wrap_handler port req f
    | "get_network" ->
      let f () =
        Storage_reader.get_network (fun net ->
            Metal_request.Node.get_node_base_url net (fun url ->
                let res = Js.Unsafe.obj [||] in
                res##.name := string @@ Metal_helpers.network_to_string net ;
                res##.url := string @@ Js_of_ocaml.Url.string_of_url url ;
                send port req res)) in
      wrap_handler port req f
    | "send" ->
      def_case
        (msg##.req##.data)
        (fun () -> send port req @@ string "EMPTY DATA")
        (fun (data : transactionData t) ->
           try
             let _ = List.assoc id !pending_send in
             send port req @@ string "ID already used"
           with Not_found ->
             let f () =
               let b, errs = check_tr data in
               if b then
                 begin
                   pending_send := (id, data) :: !pending_send ;
                   Notification.show_popup_transaction port (fun () -> ()) id
                 end
               else send_error port req errs
             in
             Hashtbl.add port_table id port;
             wrap_handler port req f
        )
    | "originate" ->
      def_case
        (msg##.req##.data)
        (fun () -> send port req @@ string "EMPTY DATA")
        (fun (data : originationData t) ->
           try
             let _ = List.assoc id !pending_ori in
             send port req @@ string "ID already used"
           with Not_found ->
             let f () =
               let b, errs = check_ori data in
               if b then
                 begin
                   pending_ori := (id, data) :: !pending_ori ;
                   Notification.show_popup_origination port (fun () -> ()) id
                 end
               else send_error port req errs
             in
             Hashtbl.add port_table id port;
             wrap_handler port req f
        )
    | "delegate" ->
      def_case
        (msg##.req##.data)
        (fun () -> send port req @@ string "EMPTY DATA")
        (fun (data : delegationData t) ->
           try
             let _ = List.assoc id !pending_dlg in
             send port req @@ string "ID already used"
           with Not_found ->
             let f () =
               let b, errs = check_dlg data in
               if b then
                 begin
                   pending_dlg := (id, data) :: !pending_dlg ;
                   Notification.show_popup_delegation port (fun () -> ()) id
                 end
               else send_error port req errs
             in
             Hashtbl.add port_table id port;
             wrap_handler port req f
        )
    | _ -> failwith ("[Background|dispatch] Don't know what to do with : " ^ meth)
  else
    Js.raise_js_error @@
    (new%js Js.error_constr
      (string @@ "[Background|dispatch] query with wrong source: " ^ src))

let dispatch_popup port (msg : message t) =
  let src = to_string msg##.src in
  let req = msg##.req in
  let id = to_string req##.id in
  let meth = to_string req##.name in
  if src = "popup" then
    match meth with
    | "get_metadata" ->
      begin try
          let meta = List.assoc id !pending_approved in
          send port req meta
        with Not_found ->
          let err = new%js Js.error_constr (string "Can't find origin request") in
          send port req err
      end
    | "get_trdata" ->
      begin try
          let opdata = List.assoc id !pending_send in
          send port req opdata
        with Not_found ->
          let err = new%js Js.error_constr (string "Can't find origin request") in
          send port req err
      end
    | "get_oridata" ->
      begin try
          let opdata = List.assoc id !pending_ori in
          send port req opdata
        with Not_found ->
          let err = new%js Js.error_constr (string "Can't find origin request") in
          send port req err
      end
    | "get_dlgdata" ->
      begin try
          let opdata = List.assoc id !pending_dlg in
          send port req opdata
        with Not_found ->
          let err = new%js Js.error_constr (string "Can't find origin request") in
          send port req err
      end
    | "state_changed" -> state_changed ()
    | "callback_error_notif" ->
      begin match to_def_option req##.data with
        | Some data ->
          Storage_writer.get_notifs (fun ns ->
              List.iter (fun { notifs ; _ } ->
                  List.iter (function
                      | ApprovNot n as notif ->
                        if n.not_approv_id = to_string req##.id then
                          data##.ok := Js._false ;
                        callback_handler ~callback:(fun _ ->
                            pending_approved := List.remove_assoc id !pending_approved)
                          data
                          notif
                      | TraNot n | DlgNot n | OriNot n | ManNot n as notif ->
                        if n.not_id = to_string req##.id then
                          callback_handler ~callback:(fun _ ->
                              let obj =
                                Metal_message.mk_answer
                                  ~src:"background"
                                  req##.id
                                  data in
                              begin
                                match Hashtbl.find_opt port_table @@ to_string req##.id with
                                | None -> Js_utils.log "None"; ()
                                | Some port ->
                                  remove_port port ;
                                  port##postMessage (obj)
                              end)
                            data
                            notif)
                    notifs)
                ns)
        | _ -> ()
      end
    | "callback_notif" ->
      begin match to_def_option req##.data with
        | Some data ->
          (* Close open notif popup *)
          Storage_writer.get_notifs (fun ns ->
              List.iter (fun { notifs ; _ } ->
                  List.iter (function
                      | ApprovNot n as notif ->
                        if n.not_approv_id = to_string req##.id then
                          callback_handler
                            ~callback:(fun _ ->
                                pending_approved := List.remove_assoc id !pending_approved ;
                                remove_window n.not_approv_wid)
                            data
                            notif
                      | TraNot n | DlgNot n | OriNot n | ManNot n as notif ->
                        if n.not_id = to_string req##.id then
                          callback_handler
                            ~callback:(fun _ ->
                                let obj = Metal_message.mk_answer ~src:"background" req##.id data in
                                begin match Hashtbl.find_opt port_table @@ to_string req##.id with
                                  | None -> Js_utils.log "None"; ()
                                  | Some port ->
                                    remove_port port ;
                                    port##postMessage (obj)
                                end ;
                                remove_window n.not_wid)
                            data
                            notif)
                    notifs)
                ns)
        | _ -> () end
    | _ -> failwith ("[Background|dispatch_popup] Don't know what to do with : " ^ meth)
  else
    Js.raise_js_error @@
    (new%js Js.error_constr
      (string @@ "[Background|dispatch_popup] query with wrong source: " ^ src))

let handle (port : Runtime_utils.port t) (msg : message t) =
  Js_log.log_str @@ "[Background] recv request \"" ^ (to_string port##.name) ^ "\"" ;
  Js_utils.js_log msg ;
  match Js.to_string port##.name with
  | "popup" -> dispatch_popup port msg
  | "contentscript" -> dispatch port msg
  | _ ->
    Js_log.log_str "No handler for connection with this name" ;
    assert false
