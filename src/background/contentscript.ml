(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_min
open Chrome
open Metal_message_types

let inject_api () =
  log_str "[Content-script] injecting api" ;
  let url = Runtime.getURL "inpage.js" in
  let container = Dom_html.window##.document##.documentElement in
  Js.Opt.case
    (Dom.CoerceTo.element container)
    (fun () -> failwith "error while injecting : coerce")
    (fun container ->
       let script = Js_min.El.script [] in
       script##.async := Js._false ;
       script##.src := Js.string url ;
       Dom.insertBefore container script Js.null ;
       Dom.removeChild container script
    )

let get_site_name () =
  let doc = Dom_html.window##.document in
  let meta_site_name =
    match Js.Opt.to_option @@
      doc##querySelector(Js.string "head > meta[property=\"og:site_name\"]") with
    | Some meta ->
      begin match Js.Opt.to_option @@ Dom_html.CoerceTo.meta meta with
        | Some meta -> Some meta##.content
        | None -> None
      end
    | None -> None in
  match meta_site_name with
  | Some sn -> sn
  | None ->
    begin
      match Js.Opt.to_option @@
        doc##querySelector (Js.string "head > meta[name=\"title\"]") with
      | Some meta ->
        begin match Js.Opt.to_option @@ Dom_html.CoerceTo.meta meta with
          | Some meta -> meta##.content
          | None -> doc##.title
        end
      | None -> doc##.title
    end

let get_site_icon () =
  let doc = Dom_html.window##.document in
  let shortcut_icon =
    match Js.Opt.to_option @@
      doc##querySelector (Js.string "head > link[rel=\"shortcut icon\"]") with
    | Some icon ->
      begin match Js.Opt.to_option @@ Dom_html.CoerceTo.link icon with
        | Some icon -> Some icon##.href
        | None -> None
      end
    | None -> None
  in
  match shortcut_icon with
  | Some icon -> Some icon
  | None ->
    begin match Js.Opt.to_option @@
        doc##querySelector(Js.string "head > link[rel=\"icon\"]") with
    | Some icon ->
      begin match Js.Opt.to_option @@ Dom_html.CoerceTo.link icon with
        | Some icon -> Some icon##.href
        | None -> None
      end
    | None -> None
    end

let get_site_metadata () =
  let name = get_site_name () in
  let icon = get_site_icon () in
  let sm : siteMetadata Js.t = Js.Unsafe.obj [||] in
  sm##.name := name ;
  sm##.icon := Js.Optdef.option @@ icon ;
  sm##.url := Dom_html.window##.location##.hostname ;
  sm

let enrich_msg (msg : message Js.t) =
  msg##.req##.metadata := Js.def @@ get_site_metadata () ;
  msg

let () =
  (* Connect to the background and listens for message *)
  let info = Runtime_utils.mk_connection_info "contentscript" in
  let port = Runtime.connect ~info () in
  Browser_utils.addListener1
    (port##.onMessage)
    (fun msg ->
       (Js.Unsafe.coerce Dom_html.window)##postMessage(msg)) ;
  (* Listens for message from inpage *)
  ignore @@
  Js_of_ocaml.Dom_events.listen
    Dom_html.window
    (Dom_html.Event.make "message")
    (fun _target ev ->
       (* log ev ; *)
       Js.Opt.case
         (ev##.srcElement)
         (fun () -> failwith "[Content-script] recveive a message with no source")
         (fun src ->
            if src = (Js.Unsafe.coerce Dom_html.window) then
              try
                let (msg : message Js.t) = ev##.data in
                let src = Js.to_string msg##.src in
                if src = "inpage" then port##postMessage (enrich_msg msg)
                else ()
              with _ -> ()
            else ());
       true) ;
  (* Runtime.onMessage (fun (req : simpleFrontEndMessage Js.t) _sender resp ->
   *     log req ;
   *     let src = Js.to_string req##src in
   *     let msg = Js.to_string req##msg in
   *     if src = "notif" && msg = "site_metadata" then
   *       let sm = get_site_metadata () in
   *       Js.Unsafe.fun_call resp [| (Js.Unsafe.inject sm) |]
   *     else ()
   *   ) ; *)
  inject_api () ;
  (* Send state at connection *)
  (* (Js.Unsafe.coerce Dom_html.window)##postMessage(msg) *)
