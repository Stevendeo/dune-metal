(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Js_types
open Chrome
open Js_min

(* Changes need to be made in static/scss/main.scss aswell *)
let notif_height = 600
let notif_width = 357

let port_cbs : (Runtime_utils.port t, 'a list) Hashtbl.t = Hashtbl.create 100

(* let filter_metal_popup (popups : Windows.window Js.t list) =
 *   List.filter (fun popup ->
 *       Js.Optdef.case
 *         (popup##._type)
 *         (fun () -> false)
 *         (fun typ ->
 *            if typ = Js.string "popup" then
 *              Js.Optdef.case
 *                (popup##.id)
 *                (fun () -> false)
 *                (fun id -> id = !current_popup_id)
 *            else false))
 *     popups *)

let show_popup port cb url =
  let should_open =
    try
      let cbs = Hashtbl.find port_cbs port in
      Hashtbl.replace port_cbs port (cb :: cbs) ;
      false
    with Not_found ->
      Hashtbl.add port_cbs port [ cb ] ;
      true in
  if should_open then
    (* Windows.getAll (fun popups -> *)
        (* Try to find all the metal popup to place the new window next to the last one *)
        (* let popups = filter_metal_popup popups in
         * if popups <> [] then
         *   begin
         *     let popup = List.hd popups in
         *     (Js.Optdef.case (popup##.id))
         *       (fun () -> ())
         *       (fun id ->
         *          let info = Windows.make_updateInfo ~focused:true () in
         *          Windows.update id info)
         *   end
         * else *)
        begin
          let data =
            Windows.make_createData
              ~url_l:[ url ]
              ~typ:"popup"
              ~width:notif_width
              ~height:notif_height
              ~top:0
              ~left:0
              () in
          Js_utils.log "data"  ;
          Windows.create
            ~info:data
            ~callback:(fun popup ->
                Js_utils.js_log popup ;
                let id =
                  Js.Optdef.get
                    (popup##.id)
                    (fun () -> failwith "[notification] no id for fresh popup") in
                Windows.onRemoved
                  (fun winid ->
                     if winid = id then
                       try
                         let cbs = Hashtbl.find port_cbs port in
                         List.iter (fun cb -> cb ()) @@ List.rev cbs ;
                         Hashtbl.remove port_cbs port
                       with Not_found -> ()
                     else ()))
            ()
        end(* ) *)

let show_popup_approve port cb id =
  Js_utils.log "[notification] show_popup_approve";
  let url = "notification.html?type=approve&req_id=" ^ id in
  show_popup port cb url

let show_popup_unlock port cb =
  log_str "[notification] show_popup_unlock" ;
  let url = "notification.html?type=unlock" in
  show_popup port cb url

let show_popup_enable port cb =
  log_str "[notification] show_popup_enable" ;
  let url = "notification.html?type=enable" in
  show_popup port cb url

let show_popup_transaction port cb id =
  log_str "[notification] show_popup_transaction" ;
  let url = "notification.html?type=send&req_id=" ^ id in
  show_popup port cb url

let show_popup_origination port cb id =
  log_str "[notification] show_popup_origination" ;
  let url = "notification.html?type=originate&req_id=" ^ id in
  show_popup port cb url

let show_popup_delegation port cb id =
  log_str "[notification] show_popup_delegation" ;
  let url = "notification.html?type=delegate&req_id=" ^ id in
  show_popup port cb url
