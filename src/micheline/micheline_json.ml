(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Error_monad

let contract_encoding =
  Micheline_encoding.canonical_encoding
    ~variant:"michelson_v1.contract"
    Data_encoding.string |> Data_encoding.list

let const_encoding =
  Micheline_encoding.canonical_encoding
    ~variant:"michelson_v1.const"
    Data_encoding.string

let json_of_contract c =
  Data_encoding.construct contract_encoding c
  |> EzEncodingJS.string_of_json

let contract_of_json s =
  EzEncodingJS.json_of_string s
  |> Data_encoding.destruct contract_encoding

let json_of_const c =
  Data_encoding.construct const_encoding c
  |> EzEncodingJS.string_of_json

let const_of_json s =
  EzEncodingJS.json_of_string s
  |> Data_encoding.destruct const_encoding

let contract_to_json s =
  let tokens, errors = Micheline_parser.tokenize s in
  match errors with
  | _ :: _ -> Error (Exn_err errors)
  | [] ->
    let nodes, errors = Micheline_parser.parse_toplevel ~check:false tokens in
    match errors with
    | _ :: _ -> Error (Exn_err errors)
    | [] ->
      let nodes, errors =
        List.split @@
        List.map Michelson_v1_macros.expand_rec nodes in
      let nodes = List.map Micheline.strip_locations nodes in
      match List.flatten errors with
      | errors :: _ -> Error errors
      | [] -> Ok (json_of_contract nodes)

let json_to_contract s =
  let c = contract_of_json s in
  let nodes = List.map (Micheline.inject_locations (fun _ ->
      { Micheline_printer.comment = None })) c in
  let nodes = List.map Michelson_v1_macros.unexpand_rec nodes in
  Format.asprintf "@[<v>%a@]"
    (Format.pp_print_list ~pp_sep:(fun ppf () -> Format.fprintf ppf " ;\n")
       Micheline_printer.print_expr_unwrapped) nodes

let const_to_json s =
  let tokens, errors = Micheline_parser.tokenize s in
  match errors with
  | _ :: _ -> Error (Exn_err errors)
  | [] ->
    let node, errors = Micheline_parser.parse_expression ~check:false tokens in
    match errors with
    | _ :: _ -> Error (Exn_err errors)
    | [] ->
      let node, errors = Michelson_v1_macros.expand_rec node in
      match errors with
      | error :: _ -> Error error
      | [] -> Ok (json_of_const (Micheline.strip_locations node))

let json_to_const s =
  let c = const_of_json s in
  let node = Micheline.inject_locations (fun _ ->
      { Micheline_printer.comment = None }) c in
  let node = Michelson_v1_macros.unexpand_rec node in
  Format.asprintf "@[<v>%a@]" Micheline_printer.print_expr_unwrapped node
