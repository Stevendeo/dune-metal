(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

open Ocp_js
open Js

let (>>=) = Lwt.(>>=)
let return = Lwt.return

class type ['a] promise = object
  method _then : ('a t -> unit) callback -> 'a promise t meth
  method catch : ('b t -> unit) callback -> 'a promise t meth
end

let to_lwt (p : promise t) =
  let waiter, notifier = Lwt.wait () in
  p##_then(wrap_callback (fun x -> Lwt.wakeup notifier (Ok x)))##catch(
    wrap_callback (fun x -> Lwt.wakeup notifier (Error x))) |> ignore;
  waiter

let to_lwt_exn (p : promise t) =
  let waiter, notifier = Lwt.wait () in
  p##_then(wrap_callback (Lwt.wakeup notifier)) |> ignore;
  waiter
