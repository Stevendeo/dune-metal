(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2019 Origin Labs - contact@origin-labs.com                  *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining     *)
(* a copy of this software and associated documentation files (the           *)
(* "Software"), to deal in the Software without restriction, including       *)
(* without limitation the rights to use, copy, modify, merge, publish,       *)
(* distribute, sublicense, and/or sell copies of the Software, and to        *)
(* permit persons to whom the Software is furnished to do so, subject to     *)
(* the following conditions:                                                 *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be            *)
(* included in all copies or substantial portions of the Software.           *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,           *)
(* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF        *)
(* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND                     *)
(* NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE    *)
(* LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION    *)
(* OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION     *)
(* WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.           *)
(*                                                                           *)
(*****************************************************************************)

module Xhr = Xhr_lwt
open Dune_types_min
open Async

let spf = Printf.sprintf

module Encoding = struct
  open Json_encoding

  let int64 = EzEncoding.int64
  let z_encoding = Dune_encoding_min.z_encoding

  let header = Xhr.Enc Dune_encoding_min.Header.encoding
  let constants = Xhr.Enc Dune_encoding_min.Constants.encoding

  let manager_key_004 = (obj2
                           (req "manager" string)
                           (opt "key" string))
  let manager_key_005 = (option string)

  let manager_key =
    Xhr.Enc (
      union [
        case manager_key_004
          (fun _ -> assert false)
          (fun (_man, k) -> k) ;
        case manager_key_005
          (fun _ -> assert false)
          (fun k -> k)
      ])

  let account_info = Xhr.Enc Dune_encoding_min.Account.account_info

  let forge_ops = Xhr.Enc (
      obj2
        (req "branch" string)
        (req "contents" Dune_encoding_min.Operation.contents_and_result_list_encoding))

  let preapply_ops = Xhr.Enc (
      list (obj4
              (opt "branch" string)
              (req "contents" Dune_encoding_min.Operation.contents_and_result_list_encoding)
              (opt "protocol" string)
              (opt "signature" string)))

  let run_operation_input = Xhr.Enc (
      (obj2
         (req "operation"
            (obj3
               (req "branch" string)
               (req "contents" Dune_encoding_min.Operation.contents_and_result_list_encoding)
               (req "signature" string)))
         (req "chain_id" string)))

  let run_operation_output = Xhr.Enc (
      (obj2
         (req "contents" Dune_encoding_min.Operation.contents_and_result_list_encoding)
         (opt "signature" string)))

  let raw_string = Xhr.Raw ((fun s -> s), (fun s -> s))
  let raw_int64 = Xhr.Raw (Int64.to_string, Int64.of_string)
  let raw_int = Xhr.Raw (string_of_int, int_of_string)
  let hex_bytes =
    Xhr.Enc
      (conv Hex.of_bytes Hex.to_bytes Dune_encoding_min.hex_encoding)
  let hex_bigstring : Bigstring.t Xhr.encoding =
    Xhr.Raw (Forge.to_hex, Forge.of_hex)
  let operation_hash =
    Xhr.Raw (Crypto.Operation_hash.b58enc, Crypto.Operation_hash.b58dec)
  let entrypoints = Xhr.Enc (
      (obj1
         (req "entrypoints" (assoc Dune_encoding_min.Script.expr_encoding))))
end

(** Get *)
let get_account_info ?base hash =
  Xhr.get ?base Encoding.account_info ("chains/main/blocks/head/context/contracts/" ^ hash ^ "/info")
let get_header ?base ?(block="head") () =
  Xhr.get ?base Encoding.header (spf "chains/main/blocks/%s/header" block)
let get_head ?base () =
  Xhr.get ?base Encoding.header "chains/main/blocks/head"
let get_head_hash ?base () =
  Xhr.get ?base Encoding.raw_string "chains/main/blocks/head/hash"
let get_block ?base hash =
  Xhr.get ?base Encoding.header ("chains/main/blocks/" ^ hash)
let get_constants ?base () =
  Xhr.get ?base Encoding.constants "chains/main/blocks/head/context/constants"
let get_manager_key ?base hash =
  Xhr.get ?base Encoding.manager_key
    (String.concat "/"
       ["chains/main/blocks/head/context/contracts"; hash; "manager_key"])
let get_manager ?base hash =
  Xhr.get ?base Encoding.raw_string
    (String.concat "/"
       ["chains/main/blocks/head/context/contracts"; hash; "manager"])
let get_entrypoints ?base hash =
  Xhr.get ?base Encoding.entrypoints
    (String.concat "/"
    ["chains/main/blocks/head/context/contracts" ; hash ; "entrypoints" ])
let get_entrypoint_type ?base hash ep =
  Xhr.get ?base (Xhr.Enc Dune_encoding_min.Script.expr_encoding)
    (String.concat "/"
    ["chains/main/blocks/head/context/contracts" ; hash ; "entrypoints" ; ep ])

(** Post *)
let prepare_ops ?base src ops =
  get_account_info ?base src >>=? fun {node_ai_counter; _} ->
  let counter = match node_ai_counter with None -> Z.zero | Some cnt -> cnt in
  let _, ops = List.fold_left (fun (counter, acc) op ->
      let counter = Z.succ counter in match op with
      | NTransaction tr -> counter, NTransaction {
          tr with node_tr_counter = Z.max counter tr.node_tr_counter} :: acc
      | NOrigination ori -> counter, NOrigination {
          ori with node_or_counter = Z.max counter ori.node_or_counter} :: acc
      | NDelegation del -> counter, NDelegation {
          del with node_del_counter = Z.max counter del.node_del_counter} :: acc
      | NReveal rvl -> counter, NReveal {
          rvl with node_rvl_counter = Z.max counter rvl.node_rvl_counter} :: acc
      | NManage_account man -> counter, NManage_account {
          man with node_mac_counter = Z.max counter man.node_mac_counter} :: acc
      | op -> counter, op :: acc) (counter, []) ops in
  return (Ok (List.rev ops))

let remote_forge ?base ~head ops =
  Xhr.post ?base Encoding.forge_ops Encoding.hex_bigstring
    (spf "chains/main/blocks/%s/helpers/forge/operations" head)
    (head, ops)

let forge_operations ?base ~head ops =
  (match Forge.forge_operations head ops with
   | Error err -> return (Error (Str_err ("Cannot forge operations: " ^ err)))
   | Ok bytes -> return (Ok bytes)) >>=? fun local_forged ->
  remote_forge ?base ~head ops >>=? fun remote_forged ->
  if not @@ Bigstring.equal local_forged remote_forged then
    return (Error (Str_err (
        Printf.sprintf "Mismatch in forge operations\nlocal  %s\nremote %s"
          Hex.(show (of_bigstring local_forged)) Hex.(show (of_bigstring remote_forged))
      )))
  else
    return (Ok local_forged)

let preapply ?base head protocol signature ops =
  Xhr.post ?base Encoding.preapply_ops Encoding.preapply_ops
    "/chains/main/blocks/head/helpers/preapply/operations"
    [Some head, ops, Some protocol, Some signature]

let dummy_sign = "edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQ\
                  rUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q"

let run_operation ?base ~head ~chain_id ops =
  Xhr.post ?base Encoding.run_operation_input Encoding.run_operation_output
    "/chains/main/blocks/head/helpers/scripts/run_operation"
    ((head, ops, dummy_sign), chain_id) >>|? fun (ops, _) -> ops

let inject ?base bytes =
  let expected_hash = Crypto.Operation_hash.hash bytes in
  Xhr.post ?base Encoding.hex_bigstring Encoding.operation_hash
    "injection/operation"
    bytes >>=? fun hash ->
  if not (Crypto.Operation_hash.equal hash expected_hash) then
    return (Error (Str_err "Unexpected operation hash"))
  else return (Ok hash)

let is_revealed ?base addr =
  get_manager_key ?base addr >>|? function
  | Some _ -> true
  | None -> false

let pre_operation ?base ops =
  get_header ?base () >>= function
  | Error e -> return (Error e)
  | Ok {header_hash = None; _} -> return (Error (Str_err "No hash in header"))
  | Ok {header_hash = Some head; _} -> forge_operations ?base ~head ops

let sign_operation ~sign bytes =
  sign bytes >>|? fun signature ->
  Binary_writer.list [bytes; signature]

let sign_zero_operation bytes =
  return (Ok (Binary_writer.list [bytes; Bigstring.make 64 '\000']))

let forge_operation ?base ~src ops =
  prepare_ops ?base src ops >>=? fun ops ->
  pre_operation ?base ops >>|? fun bytes ->
  bytes, ops

let forge_unsigned_operation ?base ops =
  pre_operation ?base ops >>|? fun bytes ->
  bytes, ops

let rvl ?(counter=Z.zero) ?(gas_limit=Z.of_int 10200)
    ?(storage_limit=Z.zero) ?(fee=1300L)
    node_rvl_src node_rvl_pubkey =
  NReveal {
    node_rvl_src; node_rvl_pubkey;
    node_rvl_fee = fee; node_rvl_counter = counter;
    node_rvl_gas_limit = gas_limit;
    node_rvl_storage_limit = storage_limit;
    node_rvl_metadata = None }

let forge_reveal ?base ?counter ?gas_limit ?storage_limit ?fee ~src pubkey =
  forge_operation ?base ~src [
    rvl ?counter ?gas_limit ?storage_limit ?fee src pubkey ]

let make_reveal ?base ~get_pk src =
  is_revealed ?base src >>=? function
  | true -> return (Ok None)
  | false -> get_pk () >>= function
    | Error e -> return (Error e)
    | Ok pk -> return (Ok (Some (rvl src pk)))

let forge_with_reveal ?base ?reveal ~src ops =
  let ops = match reveal with None -> ops | Some rvl -> rvl :: ops in
  forge_operation ?base ~src ops

let rec extract_gas_storage_from_result results =
  (match results with
   | [] -> return (Error (Str_err "No results"))
   | [x] | [NReveal _; x] -> return (Ok x)
   | _ -> return (Error (Str_err "Too many results"))
  ) >>=? fun result ->
  let metadata = match result with
    | NTransaction { node_tr_metadata = meta ; _ }
    | NOrigination { node_or_metadata = meta ; _ }
    | NReveal { node_rvl_metadata = meta ; _ }
    | NDelegation { node_del_metadata = meta ; _ }
    | NManage_account { node_mac_metadata = meta ; _ }
      -> meta
    | _ -> None in
  match metadata with
  | None | Some { manager_meta_operation_result = None ; _ } ->
    return (Error (Str_err "No metadata"))
  | Some { manager_meta_operation_result =
             Some ({ meta_op_status = Some "applied" ; _ } as op_res) ;
           manager_meta_internal_operation_results = op_internals ; _ } ->
    let consumed_gas = op_res.meta_op_consumed_gas in
    let consumed_storage = op_res.meta_op_paid_storage_size_diff in
    let allocated = if op_res.meta_op_allocated_destination_contract then 1 else 0 in
    let allocated = allocated + List.length op_res.meta_op_originated_contracts in
    let consumed_storage =
      Z.add (Z.mul (Z.of_int allocated) (Z.of_int 257)) consumed_storage in
    Lwt_list.fold_left_s (fun acc int_res ->
        return acc >>=? fun (acc_gas, acc_storage) ->
        extract_gas_storage_from_result [int_res] >>|? fun (gas, storage) ->
        Z.add gas acc_gas, Z.add storage acc_storage
      ) (Ok (consumed_gas, consumed_storage)) op_internals
  | Some { manager_meta_operation_result = Some op_res ; _ } ->
    let status = match op_res.meta_op_status with
      | None -> "none"
      | Some s -> s in
    Format.kasprintf (fun s -> return (Error (Str_err s)))
      "@[<v>\
       Status: %s@,\
       @[<v 2>Errors:@,%a@]\
       @]"
      status
      (Format.pp_print_list (fun ppf err ->
           Format.fprintf ppf "- %s (%s) : %s"
             err.node_err_kind
             err.node_err_id
             err.node_err_info
         )) op_res.meta_op_errors

let minimal_fees = Z.of_int 100
let nanodun_per_gas_unit = Z.of_int 100
let nanodun_per_byte = Z.of_int 1000
let to_nanodun m = Z.mul (Z.of_int 1000) m
let of_nanodun n = Z.div (Z.add (Z.of_int 999) n) (Z.of_int 1000)

let compute_fees ~gas_limit ~size =
  let minimal_fees_in_nanodun = to_nanodun minimal_fees in
  let fees_for_gas_in_nanodun =
    Z.mul nanodun_per_gas_unit gas_limit in
  let fees_for_size_in_nanodun = Z.mul nanodun_per_byte (Z.of_int size) in
  let fees_in_nanodun =
    Z.add minimal_fees_in_nanodun @@
    Z.add fees_for_gas_in_nanodun fees_for_size_in_nanodun in
  of_nanodun fees_in_nanodun

let forge_with_auto_fees ?base ?counter ?fee ?gas_limit ?storage_limit
    ~(forge: ?base:Js_of_ocaml.Url.url ->
      ?counter:Z.t ->
      ?gas_limit:Z.t ->
      ?storage_limit:Z.t ->
      ?fee:int64 ->
      'b ->
      (Bigstring.t * Dune_types_min.node_operation_type list,
       'c Async.metal_error)
        result Lwt.t)
    (arg : 'b) =
  get_constants ?base ()
  >>=? fun { hard_gas_limit_per_operation; hard_storage_limit_per_operation ; _ } ->
  get_header ?base () >>=? fun header ->
  let chain_id = match header.header_network with None -> "" | Some c -> c in
  let head = match header.header_hash with None -> "" | Some h -> h in
  let rec forge_aux op_size =
    forge ?base ?counter
      ~gas_limit:hard_gas_limit_per_operation
      ~storage_limit:hard_storage_limit_per_operation
      ~fee:0L arg >>=? fun (op, ops) ->
    run_operation ?base ~head ~chain_id ops >>=? fun ops_results ->
    extract_gas_storage_from_result ops_results
    >>=? fun (consumed_gas, consumed_storage) ->
    let consumed_gas = Z.add consumed_gas (Z.of_int 100) in (* Error margin *)
    let size = max op_size (Bigstring.length op + 64) in
    let computed_fee = Z.to_int64 (compute_fees ~gas_limit:consumed_gas ~size) in
    (match fee with
     | None -> return (Ok computed_fee)
     | Some fee when Int64.compare fee computed_fee < 0 ->
       Format.kasprintf (fun e -> return (Error (Str_err e)))
         "Fee too low, operation will never be included (minimum %Ld mudun)"
         computed_fee
     | Some fee -> return (Ok fee)
    ) >>=? fun fee ->
    (match gas_limit with
     | None -> return (Ok consumed_gas)
     | Some gas_limit when Z.compare gas_limit consumed_gas < 0 ->
       Format.kasprintf (fun e -> return (Error (Str_err e)))
         "Gas limit too low, operation will fail (minimum %a)"
         Z.pp_print consumed_gas
     | Some gas_limit -> return (Ok gas_limit)
    ) >>=? fun gas_limit ->
    (match storage_limit with
     | None -> return (Ok consumed_storage)
     | Some storage_limit when Z.compare storage_limit consumed_storage < 0 ->
       Format.kasprintf (fun e -> return (Error (Str_err e)))
         "Storage limit too low, operation will fail (minimum %a bytes)"
         Z.pp_print consumed_storage
     | Some storage_limit -> return (Ok storage_limit)
    ) >>=? fun storage_limit ->
    (* Format.eprintf "gas : %a; storage : %a@."
     *   Z.pp_print consumed_gas Z.pp_print consumed_storage;
     * Format.eprintf "size : %d@." size;
     * Format.eprintf "fee : %d@." (Z.to_int fee); *)
    forge ?base ?counter
      ~gas_limit
      ~storage_limit
      ~fee
      arg >>=? fun (op, ops) ->
    let actual_size = Bigstring.length op + 64 in
    if actual_size <= size then
      return (Ok (op, ops))
    else
      forge_aux actual_size in
  forge_aux 0

let forge_transaction_base ?base ?(counter=Z.zero) ?(gas_limit=Z.of_int 10200)
    ?(storage_limit=Z.of_int 300) ?(fee=1420L) ?entrypoint ?parameters ?reveal
    ~src ~dst node_tr_amount =
  let tr = {
    node_tr_src = src; node_tr_dst = dst; node_tr_amount;
    node_tr_fee = fee; node_tr_counter = counter;
    node_tr_gas_limit = gas_limit;
    node_tr_storage_limit = storage_limit;
    node_tr_collect_fee_gas = None;
    node_tr_collect_pk = None;
    node_tr_entrypoint = entrypoint;
    node_tr_parameters = parameters;
    node_tr_metadata = None } in
  forge_with_reveal ?base ?reveal ~src [ NTransaction tr ]

let forge_transaction ?base ?counter ?gas_limit
    ?storage_limit ?fee ?entrypoint ?parameters ~get_pk
    ~src ~dst node_tr_amount =
  make_reveal ?base ~get_pk src >>=? fun reveal ->
  forge_with_auto_fees ?base ?counter
    ?gas_limit ?storage_limit ?fee
    node_tr_amount
    ~forge:(forge_transaction_base ?entrypoint ?parameters ?reveal ~src ~dst)

let forge_delegation_base ?base ?(counter=Z.zero) ?(gas_limit=Z.of_int 10200)
    ?(storage_limit=Z.of_int 300) ?(fee=1420L) ?reveal
    ~src node_del_delegate  =
  let del = {
    node_del_src = src; node_del_delegate;
    node_del_fee = fee; node_del_counter = counter;
    node_del_gas_limit = gas_limit;
    node_del_storage_limit = storage_limit;
    node_del_metadata = None } in
  forge_with_reveal ?base ?reveal ~src [ NDelegation del ]

let forge_delegation ?base ?counter ?gas_limit
    ?storage_limit ?fee ~get_pk
    ~src node_del_delegate =
  make_reveal ?base ~get_pk src >>=? fun reveal ->
  forge_with_auto_fees ?base ?counter
    ?gas_limit ?storage_limit ?fee
    node_del_delegate
    ~forge:(forge_delegation_base ?reveal ~src)

let forge_origination_base ?base ?(counter=Z.zero) ?(gas_limit=Z.of_int 10200)
    ?(storage_limit=Z.of_int 300) ?(fee=1420L) ?reveal
    ?script ~src node_or_balance =
  let ori = {
    node_or_src = src; node_or_balance;
    node_or_fee = fee; node_or_counter = counter; node_or_gas_limit = gas_limit;
    node_or_storage_limit = storage_limit ;
    node_or_script = script; node_or_metadata = None} in
  forge_with_reveal ?base ?reveal ~src [ NOrigination ori ]

let forge_origination ?base ?counter ?gas_limit
    ?storage_limit ?fee
    ?script ~get_pk ~src node_or_balance =
  make_reveal ?base ~get_pk src >>=? fun reveal ->
  forge_with_auto_fees ?base ?counter
    ?gas_limit ?storage_limit ?fee
    node_or_balance
    ~forge:(forge_origination_base ?reveal ?script ~src)

let sign_bytes_target options = function
  | None -> return (Ok None)
  | Some (pkh, sign) ->
    let bytes = Forge.forge_manage_account_options options in
    sign bytes >>|? fun s ->
    Some (pkh, Some (Crypto.(Base58.simple_encode Prefix.ed25519_signature s)))

let forge_manage_account_base ?base ?(counter=Z.zero) ?(gas_limit=Z.of_int 10200)
    ?(storage_limit=Z.of_int 300) ?(fee=1420L) ?reveal ?target
    ~src node_mac_options =
  sign_bytes_target node_mac_options target >>=? fun node_mac_target ->
  let mac = {
    node_mac_src = src; node_mac_options; node_mac_target;
    node_mac_fee = fee; node_mac_counter = counter; node_mac_gas_limit = gas_limit;
    node_mac_storage_limit = storage_limit ;
    node_mac_metadata = None} in
  forge_with_reveal ?base ?reveal ~src [ NManage_account mac ]

let forge_manage_account ?base ?counter ?gas_limit ?storage_limit ?fee
    ?target ~get_pk ~src options  =
  make_reveal ?base ~get_pk src >>=? fun reveal ->
  forge_with_auto_fees ?base ?counter
    ?gas_limit ?storage_limit ?fee options
    ~forge:(forge_manage_account_base ?reveal ?target ~src)

let forge_activation ?base ~pkh node_act_secret =
  let act = { node_act_pkh = pkh; node_act_secret; node_act_metadata = None} in
  forge_unsigned_operation ?base [ NActivation act ]

let manager_kt =
  Micheline (Micheline.(strip_locations (Seq (0, [Prim (0, "parameter", [Prim (0, "or", [Prim (0, "lambda", [Prim (0, "unit", [], []); Prim (0, "list", [Prim (0, "operation", [], [])], [])], ["%do"]); Prim (0, "unit", [], ["%default"])], [])], []); Prim (0, "storage", [Prim (0, "key_hash", [], [])], []); Prim (0, "code", [Seq (0, [Seq (0, [Seq (0, [Prim (0, "DUP", [], []); Prim (0, "CAR", [], []); Prim (0, "DIP", [Seq (0, [Prim (0, "CDR", [], [])])], [])])]); Prim (0, "IF_LEFT", [Seq (0, [Prim (0, "PUSH", [Prim (0, "mutez", [], []); Int (0, Z.zero)], []); Prim (0, "AMOUNT", [], []); Seq (0, [Seq (0, [Prim (0, "COMPARE", [], []); Prim (0, "EQ", [], [])]); Prim (0, "IF", [Seq (0, []); Seq (0, [Seq (0, [Prim (0, "UNIT", [], []); Prim (0, "FAILWITH", [], [])])])], [])]); Seq (0, [Prim (0, "DIP", [Seq (0, [Prim (0, "DUP", [], [])])], []); Prim (0, "SWAP", [], [])]); Prim (0, "IMPLICIT_ACCOUNT", [], []); Prim (0, "ADDRESS", [], []); Prim (0, "SENDER", [], []); Seq (0, [Seq (0, [Prim (0, "COMPARE", [], []); Prim (0, "EQ", [], [])]); Prim (0, "IF", [Seq (0, []); Seq (0, [Seq (0, [Prim (0, "UNIT", [], []); Prim (0, "FAILWITH", [], [])])])], [])]); Prim (0, "UNIT", [], []); Prim (0, "EXEC", [], []); Prim (0, "PAIR", [], [])]); Seq (0, [Prim (0, "DROP", [], []); Prim (0, "NIL", [Prim (0, "operation", [], [])], []); Prim (0, "PAIR", [], [])])], [])])], [])]))))

let manager_kt_delegation_param delegate =
  let json = if delegate = "" then
      Micheline_json.const_to_json @@
      "{ DROP ; NIL operation ; NONE key_hash ; SET_DELEGATE ; CONS }"
    else
      Micheline_json.const_to_json @@
      Printf.sprintf
        "{ DROP ; NIL operation ; PUSH key_hash %S ; SOME ; SET_DELEGATE ; CONS }"
        delegate in
  match json with
  | Error _ -> None
  | Ok j -> Some j

let manager_kt_transaction_param ?base dst amount ep param =
  begin match Crypto.prefix_dn dst with
    | pref when pref = Some Crypto.Prefix.contract_public_key_hash ->
      begin match ep with
        | None | Some "default" | Some "" -> Lwt.return ("", "unit")
        | Some s ->
          get_entrypoint_type ?base dst s >>= fun res ->
          begin match res with
            | Error _ -> assert false
            | Ok ty ->
              let ty_json = Dune_encoding_min.Script.encode ty in
              let ty = Micheline_json.json_to_const ty_json in
              Lwt.return ("%" ^ s, ty)
          end
      end >>= fun (ep, pty) ->
      let p = match param with
        | Some p -> Micheline_json.json_to_const p
        | None -> "Unit" in
      Lwt.return @@
      Printf.sprintf
        "{ DROP ; NIL operation ;\
         PUSH address %S; CONTRACT %s (%s); ASSERT_SOME;\
         PUSH mutez %Ld ;\
         PUSH (%s) (%s);\
         TRANSFER_TOKENS ; CONS }"
        dst
        ep
        pty
        amount
        pty
        p
    | _ ->
      begin match param with
        | Some p when p <> "" -> assert false
        | _ ->
          Lwt.return @@
          Printf.sprintf
            "{ DROP ; NIL operation ;\
             PUSH key_hash %S; IMPLICIT_ACCOUNT ;\
             PUSH mutez %Ld ;\
             UNIT ;\
             TRANSFER_TOKENS ; CONS }"
            dst
            amount
      end
  end >>= fun const ->
  match Micheline_json.const_to_json const with
  | Error _ -> Lwt.return None
  | Ok j -> Lwt.return (Some j)
