console.log("[Inpage] Injected")

// Request's ID management
const MAX = 4294967295
let idCounter = Math.floor(Math.random() * MAX)

function getUniqueId () {
  idCounter = (idCounter + 1) % MAX
  return idCounter
}

// Callbacks management
var state_cb = undefined
let cbs = {}

function addCb(id, cb) {
    if (cb != undefined) {
        cbs[id] = cb ;
    }
}

function triggerCb(res) {
    if (cbs[res.id] != undefined) {
        cbs[res.id](res.result)
    }
    else console.log("[inpage] no callback for %i", res.id)
}

window.addEventListener("message", function(event) {
    if (event.source != window)
        return;
    console.log("%o", event.data) ;
    try {
        var obj = event.data ;
        if (obj.src && (obj.src == "background" || obj.src == "popup")) {
            if (obj.res != undefined) {
                triggerCb(obj.res)
            }
            else if (obj.req.id == -1) {
                state_cb()
            }
            else {
                console.log("[inpage] recv message with no result") ;
            }
        }
    }
    catch(error) { console.log(error) ;return; }
}, false)

function post (method, cb, data) {
    var uid = getUniqueId().toString() ;
    var request = { id: uid, name: method, data: data } ;
    addCb(uid, cb) ;
    window.postMessage({ src: "inpage", req: request }, "*")
}

// metal state = { enabled ; unlocked ; approved ; selected_account }
// onstatechanged_listener : trigger when state is changed

metal = {

    /**
     * Callback that return a boolean.
     *
     * @callback boolCallback
     * @param {bool} res
     */

    /**
     * Callback that return the hash of the operation when injected.
     *
     * @callback hashCallback
     * @param {string} hash - hash of the injected operation
     */

    /**
     * Callback that return informations on selected network.
     *
     * @callback networkCallback
     * @param {Object} res
     * @param {string} res.name - name of the network ("Mainnet", "Testnet" or "Custom")
     * @param {string} res.url - url of the node that metal uses to inject operations and query balance
     */

    /**
     * This callback type is called `unitCallback` and is displayed as a global symbol.
     *
     * @callback unitCallback
     */

    /**
     * Get state of metal extension, enabled means the extension have been
     * setup (first account has been created)
     * @function isEnabled
     * @param {boolCallback} cb - callback to handle the result
     */
    isEnabled: function (cb) {
        post("is_enabled", cb)
    },

    /**
     * Get state of metal extension, check if the extension have been unlocked
     * (user has enter is password)
     * @function isUnlocked
     * @param {boolCallback} cb - callback to handle the result
    */
    isUnlocked: function (cb) {
        post("is_unlocked", cb)
    },

    /**
     * Get state of metal extension, approved means the domain have been approved
     * by the user (before sending sign requests a domain must be approve by the
     * user)
     * @function isApproved
     * @param {boolCallback} cb - callback to handle the result
    */
    isApproved: function (cb) {
        post("is_approved", cb)
    },

    /**
     * Get the selected account hash
     * @function getAccount
     * @param {hashCallback} cb - callback to handle the result
    */
    getAccount: function (cb) {
        post("get_account", cb)
    },


    /**
     * Get the selected network and its node address
     * @function getNetwork
     * @param {networkCallback} cb - callback to handle the result
    */
    getNetwork: function (cb) {
        post("get_network", cb)
    },

    /**
     * Send dune tokens
     * @function send
     * @param {Object} obj
     * @param {string} obj.dst - destination account hash
     * @param {string} obj.amount - amount in mudun
     * @param {string} [ obj.fee = auto ] - fee in mudun
     * @param {string} [ obj.entrypoint = default ] - entry point
     * @param {string} [ obj.parameter ] - json or michelson format parameter
     * @param {string} [ obj.gasLimit = auto ] - specify gas limit
     * @param {string} [ obj.storageLimit = auto ] - specify storage limit
     * @param {hashCallback} obj.cb - callback with the hash of the injected operation as param
    */
    send: function({dst, amount, fee, entrypoint, parameter, gas_limit, storage_limit, cb}) {
        var data = {
            type: "transaction",
            dst: dst,
            amount: amount,
            fee: fee,
            entrypoint: entrypoint,
            parameter: parameter,
            gasLimit: gas_limit,
            storageLimit: storage_limit
        }
        post("send", cb, data)
    },

  /**
     * Originate a new contract
     * @function originate
     * @param {Object} obj
     * @param {string} obj.balance - balance in mudun
     * @param {string} obj.fee - fee in mudun
     * @param {string} [ obj.gasLimit = auto ] - specify gas limit
     * @param {string} [ obj.storageLimit = auto ] - specify storage limit
     * @param {string} obj.sc_code_hash - json or michelson format contract code hash
     * @param {string} obj.sc_storage - json or michelson format contract storage
     * @param {string} obj.sc_code - json or michelson format contract code
     * @param {hashCallback} obj.cb - callback with the hash of the injected operation as param
    */
    originate: function({balance, fee, gas_limit, storage_limit,
                         sc_code_hash, sc_storage, sc_code, cb}) {
        var data = {
            type: "origination",
            balance: balance,
            fee: fee,
            gasLimit: gas_limit,
            storageLimit: storage_limit,
            scCodeHash: sc_code_hash,
            scStorage: sc_storage,
            scCode: sc_code
        }
        post("originate", cb, data)
    },

    /**
     * Delegate
     * @function delegate
     * @param {Object} obj
     * @param {string} [ obj.delegate = unset ] - new delegate
     * @param {string} [ obj.fee = auto ] - fee in mudun
     * @param {string} [ obj.gasLimit = auto ] - specify gas limit
     * @param {string} [ obj.storageLimit = auto ] - specify storage limit
     * @param {hashCallback} obj.cb - callback with the hash of the injected operation as param
    */
    delegate: function({delegate, fee, gas_limit, storage_limit, cb}) {
        var data = {
            type: "delegation",
            delegate: delegate,
            fee: fee,
            gasLimit: gas_limit,
            storageLimit: storage_limit
        }
        post("delegate", cb, data)
    },


    /**
     * Register a callback that will be trigger on metal state changes
     * (as of now only trigger on lock changes)
     * @function onStateChanged
     * @param {unitCallback} cb
     */
    onStateChanged: function (cb) {
        state_cb = cb
    }

}
