
all: _obuild libs/ez-api/autoconf/config.ocp2gen libs/ocplib-jsutils/autoconf/config.ocp2gen
	$(MAKE) BASE64_3=true -C libs/ocplib-jsutils base64-conf
	ocp-build
	@cp _obuild/metal-popup/metal-popup.js static
	@cp _obuild/dune-metal-notif/dune-metal-notif.js static/dune-metal-notif.js
	@cp _obuild/metal-background/metal-background.js static/background.js
	@cp _obuild/metal-contentscript/metal-contentscript.js static/contentscript.js
	@cp _obuild/metal-home/metal-home.js static
	@cp _obuild/metal-start/metal-start.js static
	@cp _obuild/metal-settings/metal-settings.js static
	@cp _obuild/metal-unlock/metal-unlock.js static
	@cp _obuild/metal-reset/metal-reset.js static
	@cp _obuild/metal-test/metal-test.js static
	@cp _obuild/metal-pres/metal-pres.js static
	@$(MAKE) static/css/main.css

static/css/main.css: static/scss/main.scss
	@sass static/scss/main.scss static/css/main.css

build-deps: _opam
	@opam install . --deps-only --working-dir -y

submodules:
	git submodule update --init

_opam:
	opam switch create . 4.07.1 --no-install

_obuild:
	ocp-build init

libs/ez-api/autoconf/config.ocp2gen:
	(cd libs/ez-api && ./configure && cd ../..)

libs/ocplib-jsutils/autoconf/config.ocp2gen:
	(cd libs/ocplib-jsutils && ./configure && cd ../..)

release:
	@-mkdir -p dune-metal-extension
	cp -r static/* dune-metal-extension

api-doc:
	jsdoc static/inpage.js -d docs

clean:
	ocp-build clean
