//Provides: caml_fill_bigstring
//Requires: caml_ba_set_1
function caml_fill_bigstring(buf, buf_off, buf_len, v) {
  var i;
  for (i = 0; i < buf_len; i++) {
    caml_ba_set_1(buf, buf_off + i, v);
  }
  return 0;
}
