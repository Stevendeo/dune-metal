
//Provides: ml_z_abs const
function ml_z_abs(z) {
  if (z < 0) return (-z);
  else return z;
}

//Provides: ml_z_neg const
function ml_z_neg(z) {
  return -z ;
}

//Provides: ml_z_pred const
function ml_z_pred(z) {
  return z - 1 ;
}

//Provides: ml_z_add const
function ml_z_add(z1, z2) {
  return z1 + z2;
}

//Provides: ml_z_div const
function ml_z_div(z1, z2) {
  return z1 / z2;
}

//Provides: ml_z_rem const
function ml_z_rem(z1, z2) {
  return z1 % z2;
}

//Provides: ml_z_div_rem const
//Requires: caml_obj_block, caml_array_set
function ml_z_div_rem(z1, z2) {
  var res = caml_obj_block(0, 2);
  caml_array_set(res, 0, z1 / z2);
  caml_array_set(res, 1, z1 % z2);
  return res;
}

//Provides: ml_z_equal const
function ml_z_equal(z1, z2) {
  return z1 == z2;
}

//Provides: ml_z_format const
//Requires: caml_to_js_string,  caml_js_to_string, caml_failwith
function ml_z_format(fmt, z) {
  fmt = caml_to_js_string(fmt);
  if (fmt == "%d") {
    return caml_js_to_string(z.toString());
  } else {
    caml_failwith("Unsupported format '" + fmt + "'");
  }
}

//Provides: ml_z_init const
function ml_z_init() {
}

//Provides: ml_z_install_frametable const
function ml_z_install_frametable() {
}

//Provides: ml_z_mul const
function ml_z_mul(z1, z2) {
  return z1 * z2;
}

//Provides: ml_z_of_int const
function ml_z_of_int(i) {
  return joo_global_object.BigInt(i);
}

//Provides: ml_z_of_int64 const
//Requires: caml_int64_is_negative, caml_int64_neg, ml_z_neg
function ml_z_of_int64(i64) {
  if (caml_int64_is_negative(i64)) {
    i64 = caml_int64_neg(i64);
    return ml_z_neg((joo_global_object.BigInt(i64[3]) << joo_global_object.BigInt(48)) + (joo_global_object.BigInt(i64[2]) << joo_global_object.BigInt(24)) + joo_global_object.BigInt(i64[1]));
  }
  else return (joo_global_object.BigInt(i64[3]) << joo_global_object.BigInt(48)) + (joo_global_object.BigInt(i64[2]) << joo_global_object.BigInt(24)) + joo_global_object.BigInt(i64[1])
}

//Provides: ml_z_of_substring_base const
//Requires: caml_to_js_string
function ml_z_of_substring_base(_base, s, pos, len) {
  return joo_global_object.BigInt(caml_to_js_string(s).substring(pos, pos + len));
}

//Provides: ml_z_shift_left const
function ml_z_shift_left(z, amt) {
  return z << joo_global_object.BigInt(amt);
}

//Provides: ml_z_shift_right const
function ml_z_shift_right(z, amt) {
  return z >> joo_global_object.BigInt(amt);
}

//Provides: ml_z_sub const
function ml_z_sub(z1, z2) {
  return z1 - z2;
}

//Provides: ml_z_succ const
function ml_z_succ(z) {
  return z + joo_global_object.BigInt(1) ;
}

//Provides: ml_z_to_bits const
//Requires: caml_new_string
function ml_z_to_bits(z) {
  var zbase = joo_global_object.BigInt(255);
  var res = "";
  while (!(z == joo_global_object.BigInt(0))) {
    res += String.fromCharCode(Number(z & zbase));
    z = z >> joo_global_object.BigInt(8);
  }
  return caml_new_string(res);
}

//Provides: ml_z_to_int const
//Requires: caml_failwith
function ml_z_to_int(z) {
  var max_safe = joo_global_object.BigInt(Number.MAX_SAFE_INTEGER);
  var min_safe = joo_global_object.BigInt(Number.MIN_SAFE_INTEGER);
  if (min_safe <= z <= max_safe) return Number(z);
  else caml_failwith("BigInt to big to be converted to Integer");
}

//Provides: ml_z_to_int64 const
//Requires: ml_z_shift_right
function ml_z_to_int64(z) {
  return [255, Number(z & joo_global_object.BigInt("0xffffff")), Number(ml_z_shift_right(z, 24) & joo_global_object.BigInt("0xffffff")), Number(ml_z_shift_right(z, 48) & joo_global_object.BigInt("0xffff"))];
}

//Provides: ml_z_numbits const
function ml_z_numbits(z) {
  if (z < 0) z = - joo_global_object.BigInt(1) * z;
  var x = 0;
  var upperBound = joo_global_object.BigInt(1);
  while (upperBound <= z) {
    x = x + 1;
    upperBound = upperBound * joo_global_object.BigInt(2);
  }
  return x;
}

//Provides: ml_z_of_bits const
function ml_z_of_bits(arg) {
  var zbase = joo_global_object.BigInt(256);
  var acc = joo_global_object.BigInt(0);
  var pow = joo_global_object.BigInt(1);
  for (var i=0; i<arg.l; i++) {
    var base_bump = joo_global_object.BigInt(arg.c.charCodeAt(i));
    var bump = base_bump * pow;
    acc = acc + bump;
    pow = pow * zbase;
  }
  return acc;
}

//Provides: ml_z_pow const
//Requires: caml_failwith
function ml_z_pow(z, n) {
  if (n === 0) return joo_global_object.BigInt(1);
  if (z === joo_global_object.BigInt(0)) return joo_global_object.BigInt(0);
  if (z === joo_global_object.BigInt(1)) return joo_global_object.BigInt(1);
  if (z === joo_global_object.BigInt(-1)) return (n & 1 === 0) ? joo_global_object.BigInt(1) : joo_global_object.BigInt(-1);
  if (n < 0) caml_failwith("Cannot exponentiate with negative values");
  var y = joo_global_object.BigInt(1);
  while (true) {
    if (n & 1 === 1) {
      y = y * z;
      --n;
    }
    if (n === 0) break;
    n /= 2;
    z = z * z;
  }
  return y;
}

//Provides: ml_z_compare const
function ml_z_compare(z1, z2) {
  if (z1 < z2) return -1;
  else if (z1 = z2) return 0;
  else return 1;
}

//Provides: ml_z_logand const
function ml_z_logand(z1, z2) {
  return z1 & z2;
}

//Provides: ml_z_logor const
function ml_z_logor(z1, z2) {
  return z1 | z2;
}

//Provides: ml_z_sign const
function ml_z_sign(z) {
  var zero = joo_global_object.BigInt(0);
  if (z < zero) return -1;
  else if (z = zero) return 0;
  else return 1;
}

//Provides: ml_z_extract const
function ml_z_extract(z, off, len) {
  return (z >> joo_global_object.BigInt(off)) & (joo_global_object.BigInt(1) << joo_global_object.BigInt(len)) - joo_global_object.BigInt(1);
}
